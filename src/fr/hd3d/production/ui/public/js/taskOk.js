
{
	/**
	 * @param {Object} model
	 * @param {String} property
	 */
    render:function(model, property, config)
    {
        var value = model.get(property);
        var part = value.split("/");
        if( part.length == 2)
        {
            var done = parseInt( part[0] );
            var total = parseInt( part[1] );
            if( done < total)
            {
                config.style = config.style + "background-color:red;";
            }
            else
            {
                config.style = config.style + "background-color:green;";
            } 
        }
        return value;
    }
}
