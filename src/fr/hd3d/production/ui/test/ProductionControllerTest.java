package fr.hd3d.production.ui.test;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.production.ui.client.controller.ProductionController;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.test.mock.ProductionMock;


public class ProductionControllerTest extends UITestCase
{
    ProductionModel model;
    ProductionMock view;
    ProductionController controller;

    ProjectModelData project = new ProjectModelData();

    @Override
    public void setUp()
    {
        super.setUp();

        model = new ProductionModel();
        view = new ProductionMock();
        controller = new ProductionController(view, model);

        EventDispatcher.get().getControllers().clear();
        EventDispatcher.get().addController(controller);

    }

    /**
     * Test that when new sheet button is clicked, the sheet dialog is shown with the associated project properly set.
     */
    @Test
    public void testShowNewSheetWindow()
    {
        assertFalse(this.view.isSheetEditorShown());
        EventDispatcher.forwardEvent(SheetEditorEvents.NEW_SHEET_CLICKED);
        assertTrue(this.view.isSheetEditorShown());
        assertTrue(this.view.isNewSheet());
        assertEquals(this.view.getSheetProjectId(), project.getId());
    }

    /**
     * Test that when edit sheet button is clicked, the sheet dialog is shown with the associated project properly set.
     */
    @Test
    public void testShowEditSheetWindow()
    {
        assertFalse(this.view.isSheetEditorShown());
        EventDispatcher.forwardEvent(SheetEditorEvents.EDIT_SHEET_CLICKED);
        assertTrue(this.view.isSheetEditorShown());
        assertFalse(this.view.isNewSheet());
        assertEquals(this.view.getSheetProjectId(), project.getId());
    }
}
