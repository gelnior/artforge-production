package fr.hd3d.production.ui.test.mock;

import java.util.List;

import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.TreeLibraryView;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.TreeTemporalView;
import fr.hd3d.production.ui.client.view.IProductionView;


public class ProductionMock implements IProductionView
{

    private boolean isSheetEditorShown = false;
    private Long sheetProjectId = null;
    private boolean isNewSheet = true;

    public Long getSheetProjectId()
    {
        return this.sheetProjectId;
    }

    public boolean isSheetEditorShown()
    {
        return isSheetEditorShown;
    }

    public boolean isNewSheet()
    {
        return isNewSheet;
    }

    public void enableViewComboBox(boolean b)
    {
    // TODO Auto-generated method stub

    }

    public SheetModelData getCurrentSheet()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public TreeLibraryView getLibraryTree()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<Hd3dModelData> getSelection()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public TreeTemporalView getTemporalTree()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void hideSaving()
    {
    // TODO Auto-generated method stub

    }

    public void initWidgets()
    {
    // TODO Auto-generated method stub

    }

    public void refreshExplorateurView()
    {
    // TODO Auto-generated method stub

    }

    public void refreshTagEditor(List<Hd3dModelData> arrayList)
    {
    // TODO Auto-generated method stub

    }

    public void removeSheetFromCombo(SheetModelData currentSheet)
    {
    // TODO Auto-generated method stub

    }

    public void saveViewIdToCookie(Long projectId, Long id, String simpleClassName)
    {
    // TODO Auto-generated method stub

    }

    public void selectFirstSheet()
    {
    // TODO Auto-generated method stub

    }

    public void selectLastProject()
    {
    // TODO Auto-generated method stub

    }

    public void selectLastSheet(Long projectId, String simpleClassName)
    {
    // TODO Auto-generated method stub

    }

    public void setExplorerView(SheetModelData view)
    {
    // TODO Auto-generated method stub

    }

    public void showSaving(String progressText)
    {
    // TODO Auto-generated method stub

    }

    public void showSheetEditorWindow(Long projectId, boolean newSheet)
    {
        this.sheetProjectId = projectId;
        this.isNewSheet = newSheet;
        this.isSheetEditorShown = true;
    }

    public void updateProgressBar(Long progress)
    {
    // TODO Auto-generated method stub

    }

    public void displayError(Integer error)
    {
    // TODO Auto-generated method stub

    }

    public void hideStartPanel()
    {
    // TODO Auto-generated method stub

    }

    public void showStartPanel()
    {
    // TODO Auto-generated method stub

    }

    public void displayError(Integer error, String stack)
    {
    // TODO Auto-generated method stub

    }

    public void showThumbnailEditor()
    {
    // TODO Auto-generated method stub

    }

    public void refreshThumbnails()
    {
    // TODO Auto-generated method stub

    }

    public void displayError(Integer intValue, String userMsg, String stack)
    {
    // TODO Auto-generated method stub

    }

    public SheetModelData getExplorerView()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void setListeners()
    {
    // TODO Auto-generated method stub

    }

    public Hd3dModelData getFirstSelected()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public ColumnConfig getColumn(int colIndex)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void showSaving()
    {
    // TODO Auto-generated method stub

    }

    public void reloadTrees(ProjectModelData project)
    {
    // TODO Auto-generated method stub

    }

    public void idleConstituentView()
    {
    // TODO Auto-generated method stub

    }

    public void idleShotView()
    {
    // TODO Auto-generated method stub

    }

    public void unidleConstituentView()
    {
    // TODO Auto-generated method stub

    }

    public void unidleShotView()
    {
    // TODO Auto-generated method stub

    }

    public void rebuildLastEnvironment()
    {
    // TODO Auto-generated method stub

    }

    public void setControllers()
    {
    // TODO Auto-generated method stub

    }

    public String getFavValue(String key)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void selectTab(String lastSelectedTab)
    {
    // TODO Auto-generated method stub

    }

    public void idlePlaylistView()
    {
    // TODO Auto-generated method stub

    }

    public void unidlePlaylistView()
    {
    // TODO Auto-generated method stub

    }

    public void enablePlaylistUi(boolean enable)
    {
    // TODO Auto-generated method stub

    }

    public void selectProject(Long valueOf)
    {
    // TODO Auto-generated method stub

    }

    public void toggleAutoSave()
    {
    // TODO Auto-generated method stub

    }

    public void showScriptRunningPanel()
    {
    // TODO Auto-generated method stub

    }

    public void hideScriptRunningPanel()
    {
    // TODO Auto-generated method stub

    }

    public void idleStatDetailsView()
    {
    // TODO Auto-generated method stub

    }

    public void idleStatView()
    {
    // TODO Auto-generated method stub

    }

    public void unidleStatDetailsView()
    {
    // TODO Auto-generated method stub

    }

    public void unidleStatView()
    {
    // TODO Auto-generated method stub

    }

    public void enableStatUi(boolean stat)
    {
    // TODO Auto-generated method stub

    }

}
