package fr.hd3d.production.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ApprovalNoteTypeReader;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.production.ui.client.config.ProductionConfigCallBack;
import fr.hd3d.production.ui.client.event.ProductionEvents;


/**
 * Model handling production view data (main model inheritance). It handles two variables : is project change to store
 * the fact that project has changed and current entity to always know if the user is working on shot or constituent.
 * 
 * @author HD3D
 */
public class ProductionModel extends MainModel
{
    public static final ServiceStore<ApprovalNoteTypeModelData> approvalStore = new ServiceStore<ApprovalNoteTypeModelData>(
            new ApprovalNoteTypeReader());

    private static FastMap<List<ItemModelData>> approvalColumns = new FastMap<List<ItemModelData>>();

    /** Name of configuration file needed by sheet editor. */
    private static final String DATA_TYPE_FILE = "config/items_info_production.json";

    /** path of production UI XML configuration file */
    private static final String PRODUCTION_CONFIG_FILE_PATH = "config/production_config.xml";

    /** True if project has changed recently (useful to know if some widgets should be refreshed). */
    private Boolean isProjectChanged = false;
    /** Current entity is used to determine current active tabulation. */
    private static String currentEntity = ConstituentModelData.SIMPLE_CLASS_NAME;

    private String lastSelectedTab;

    private static boolean enablePlaylistUi = false;
    private boolean enableStatUi = true;

    private static final EqConstraint projectConstraint = new EqConstraint("project.id");

    private static LogicConstraint myFilterGroup;

    /**
     * Default constructor : configure sheet editor.
     */
    public ProductionModel()
    {
        SheetEditorModel.addDataFile(DATA_TYPE_FILE);
        SheetEditorModel.setType(ESheetType.PRODUCTION);

        approvalStore.addEventLoadListener(ProductionEvents.APPROVAL_TYPES_LOADED);
    }

    @Override
    public void initServicesConfig()
    {
        try
        {
            requestHandler.getRequest(CommonConfig.CONFIG_FILE_PATH, new ProductionConfigCallBack(this));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
        }
    }

    /**
     * loads production_config.xml file
     */
    public void initProductionConfig()
    {
        try
        {
            requestHandler.getRequest(PRODUCTION_CONFIG_FILE_PATH, new ProductionConfigCallBack(this));
        }
        catch (Exception e)
        {
            // TODO log
            Info.display("Production properties", "Cannot load production ui properties -> setting values to default");
        }
    }

    public static boolean isEnablePlaylistUi()
    {
        return ProductionModel.enablePlaylistUi;
    }

    public static void setEnablePlaylistUi(boolean enablePlaylistUi)
    {
        ProductionModel.enablePlaylistUi = enablePlaylistUi;
    }

    public boolean isEnableStatUi()
    {
        return enableStatUi;
    }

    public void setEnableStatUi(boolean enableStatUi)
    {
        this.enableStatUi = enableStatUi;
    }

    /**
     * @return if project has changed recently (useful to know if some widgets should be refreshed).
     */
    public Boolean isProjectChanged()
    {
        return isProjectChanged;
    }

    /**
     * Set if project has changed recently.
     * 
     * @param isProjectChanged
     *            The new value for isProjectChanged field.
     */
    public void setIsProjectChanged(Boolean isProjectChanged)
    {
        this.isProjectChanged = isProjectChanged;
    }

    /**
     * @return Currently active tab.
     */
    public static String getCurrentEntity()
    {
        return currentEntity;
    }

    /**
     * @param currentEntity
     *            Set currently active tab.
     */
    public static void setCurrentEntity(String entity)
    {
        currentEntity = entity;
    }

    public String getLastSelectedTab()
    {
        return this.lastSelectedTab;
    }

    public void setLastSelectedTab(String tabName)
    {
        this.lastSelectedTab = tabName;
    }

    public static LogicConstraint getFilter()
    {
        if (myFilterGroup == null)
        {
            myFilterGroup = new LogicConstraint();
            myFilterGroup.setOperator(EConstraintLogicalOperator.AND);
        }
        return myFilterGroup;
    }

    public static void setCurrentProjectSelected(ProjectModelData project)
    {
        if (project != null)
            projectConstraint.setLeftMember(project.getId());
        getFilter().setLeftMember(projectConstraint);
        getFilter().setRightMember(null);
    }

    public static List<ItemModelData> getApprovalColumns()
    {
        List<ItemModelData> list = approvalColumns.get(currentEntity);
        if (list == null)
        {
            list = new ArrayList<ItemModelData>();
            approvalColumns.put(currentEntity, list);
        }
        return list;
    }
}
