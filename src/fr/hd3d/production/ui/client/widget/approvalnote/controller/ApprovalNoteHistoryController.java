package fr.hd3d.production.ui.client.widget.approvalnote.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConfirmMessageBoxCallback;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNoteHistoryModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNoteHistoryPanel;


/**
 * Controller handling approval note history events.
 * 
 * @author HD3D
 */
public class ApprovalNoteHistoryController extends Controller
{
    /** Common constants for displaying. */
    private static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** View containing widgets. */
    private IApprovalNoteHistoryPanel view;
    /** Model handling data. */
    private ApprovalNoteHistoryModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            View containing widgets.
     * @param model
     *            Model handling data.
     */
    public ApprovalNoteHistoryController(IApprovalNoteHistoryPanel view, ApprovalNoteHistoryModel model)
    {
        this.view = view;
        this.model = model;

        EventDispatcher.get().addController(this);

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == ProductionEvents.EDIT_APPROVAL_NOTE_CLICKED)
        {
            this.onEditApprovalNote(event);
        }
        else if (type == ProductionEvents.DELETE_APPROVAL_NOTE_CLICKED)
        {
            this.onDeleteApprovalNote(event);
        }
        else if (type == ProductionEvents.DELETE_APPROVAL_NOTE_CONFIRMED)
        {
            this.onDeleteConfirmed(event);
        }
        else if (type == ProductionEvents.APPROVAL_NOTE_DELETED)
        {}
    }

    /**
     * When user asks for approval note edition, the approval note dialog appears.
     * 
     * @param event
     *            The approval note event.
     */
    private void onEditApprovalNote(AppEvent event)
    {
        this.view.editApprovalNote((ApprovalNoteModelData) event.getData());
    }

    /**
     * When approval note is asked for deletion a confirmation dialog box is displayed.
     * 
     * @param event
     *            The delete approval note event.
     */
    private void onDeleteApprovalNote(AppEvent event)
    {
        MessageBox.confirm(CONSTANTS.Confirm(), CONSTANTS.confirmDeleteApprovalNote(), new ConfirmMessageBoxCallback(
                ProductionEvents.DELETE_APPROVAL_NOTE_CONFIRMED, null, event.getData()));
    }

    /**
     * When deletion is confirmed, the note is deleted from server and locally.
     * 
     * @param event
     *            The delete confirmed event.
     */
    private void onDeleteConfirmed(AppEvent event)
    {
        ApprovalNoteModelData note = event.getData();
        note.setDefaultPath(ServicesPath.getApprovalNotePath(note.getId()));
        note.delete(ProductionEvents.APPROVAL_NOTE_DELETED);
        this.model.getStore().remove(note);
    }

    /**
     * @return True if a record has been modified and not saved.
     */
    public Boolean isDirty()
    {
        List<ApprovalNoteModelData> models = this.model.getStore().getModels();
        for (ApprovalNoteModelData approval : models)
        {
            if (this.model.getStore().getRecord(approval).isDirty())
            {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    /**
     * Register events.
     */
    private void registerEvents()
    {
        this.registerEventTypes(ProductionEvents.EDIT_APPROVAL_NOTE_CLICKED);
        this.registerEventTypes(ProductionEvents.DELETE_APPROVAL_NOTE_CLICKED);
        this.registerEventTypes(ProductionEvents.DELETE_APPROVAL_NOTE_CONFIRMED);
        this.registerEventTypes(ProductionEvents.APPROVAL_NOTE_DELETED);
    }
}
