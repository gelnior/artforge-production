package fr.hd3d.production.ui.client.widget.approvalnote.view;

import java.util.Date;

import fr.hd3d.common.ui.client.error.Hd3dException;


public interface IApprovalNotePanel
{
    public Date getDate();

    public String getStatus() throws Hd3dException;

    public String getComment();

    public void setDate(Date date);

    public void setComment(String comment);

    public void setStatus(String status);
}
