package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.HourField;
import fr.hd3d.common.ui.client.widget.MinuteField;
import fr.hd3d.common.ui.client.widget.SecondField;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskStatusRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNotePanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNotePanel;


@Deprecated
public class ApprovalNotePanel extends ContentPanel implements IApprovalNotePanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    private ApprovalNotePanelModel model;

    private DateField dateField = new DateField();
    private TextField<String> statusField = new TextField<String>();
    private HourField hourField = new HourField();
    private MinuteField minuteField = new MinuteField();
    private SecondField secondField = new SecondField();
    private TextArea commentField;

    /** The task grid, needed to close tasks. */
    protected EditorGrid<TaskModelData> taskGrid;

    public ApprovalNotePanel(ApprovalNotePanelModel model)
    {
        this.model = model;

        this.setStyles();
        this.createForm();

        this.setTaskGrid();
    }

    public String getStatus()
    {
        return this.statusField.getValue();
    }

    public void setStatus(String status)
    {
        this.statusField.setValue(status);
    }

    public String getComment()
    {
        return this.commentField.getValue();
    }

    public void setComment(String comment)
    {
        this.commentField.setValue(comment);
    }

    public Date getDate()
    {
        Date date = this.dateField.getValue();

        DateWrapper dateWrapper = new DateWrapper(date);
        dateWrapper.clearTime();
        dateWrapper = dateWrapper.addHours((Integer) this.hourField.getValue().getValue());
        dateWrapper = dateWrapper.addMinutes((Integer) this.minuteField.getValue().getValue());
        dateWrapper = dateWrapper.addSeconds((Integer) this.secondField.getValue().getValue());

        return dateWrapper.asDate();
    }

    public void setDate(Date date)
    {
        this.dateField.setValue(date);

        DateWrapper dateWrapper = new DateWrapper(date);
        this.hourField.setValueByIntegerValue(dateWrapper.getHours());
        this.minuteField.setValueByIntegerValue(dateWrapper.getMinutes());
        this.secondField.setValueByIntegerValue(dateWrapper.getSeconds());
    }

    public void createForm()
    {
        this.setLayout(new BorderLayout());

        HBoxLayout topLayout = new HBoxLayout();
        topLayout.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);

        dateField.setFieldLabel("Date");
        dateField.setLabelSeparator("");
        dateField.setWidth(100);
        dateField.disable();

        LayoutContainer topContainer = new LayoutContainer();
        topContainer.setLayout(topLayout);
        topContainer.add(dateField);

        topContainer.add(hourField);
        topContainer.add(minuteField);
        topContainer.add(secondField);
        hourField.disable();
        minuteField.disable();
        secondField.disable();

        HBoxLayoutData flex = new HBoxLayoutData(new Margins(0));
        flex.setFlex(1);
        topContainer.add(new Text(), flex);

        statusField.setFieldLabel(COMMON_CONSTANTS.Status());
        statusField.setEnabled(false);
        statusField.setLabelSeparator("");
        statusField.setWidth(50);
        topContainer.add(statusField);

        commentField = new TextArea();
        commentField.setLabelSeparator("");
        commentField.setFieldLabel(COMMON_CONSTANTS.Comment());
        commentField.setHeight(200);
        commentField.setEmptyText("Add comments...");
        commentField.setStyleAttribute("padding", "3px");

        this.add(topContainer, new BorderLayoutData(LayoutRegion.NORTH, 28));
        this.add(commentField, new BorderLayoutData(LayoutRegion.CENTER));
    }

    public void focusComment()
    {
        this.commentField.focus();
    }

    /** Set panel styles. */
    private void setStyles()
    {
        this.setHeaderVisible(false);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setFrame(true);
    }

    private void setTaskGrid()
    {
        taskGrid = new EditorGrid<TaskModelData>(this.model.getTaskStore(), this.getColumnModel());
        taskGrid.setHeight(70);
        taskGrid.setBorders(true);
        taskGrid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        BorderLayoutData layoutData = new BorderLayoutData(LayoutRegion.SOUTH, 80);
        layoutData.setMargins(new Margins(5, 0, 0, 0));

        this.taskGrid.addListener(Events.AfterEdit, new Listener<GridEvent<TaskModelData>>() {

            public void handleEvent(GridEvent<TaskModelData> ge)
            {
                TaskModelData task = ge.getModel();
                task.save(new BaseCallback() {
                    protected void onSuccess(Request request, Response response)
                    {
                        model.getTaskStore().commitChanges();
                    }
                });
            }
        });

        this.add(taskGrid, layoutData);
    }

    /**
     * @return Column list for task grid.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        GridUtils.addColumnConfig(columns, TaskModelData.WORKER_NAME_FIELD, COMMON_CONSTANTS.Worker(), 180);

        ColumnConfig statusColumn = GridUtils.addColumnConfig(columns, TaskModelData.STATUS_FIELD,
                COMMON_CONSTANTS.Status(), 120);
        statusColumn.setRenderer(new TaskStatusRenderer<Hd3dModelData>());

        ColumnConfig startDate = GridUtils.addColumnConfig(columns, TaskModelData.START_DATE_FIELD,
                COMMON_CONSTANTS.StartDate(), 70);
        startDate.setDateTimeFormat(DateFormat.DATE);
        startDate.setAlignment(HorizontalAlignment.CENTER);

        ColumnConfig endDate = GridUtils.addColumnConfig(columns, TaskModelData.END_DATE_FIELD,
                COMMON_CONSTANTS.EndDate(), 70);
        endDate.setDateTimeFormat(DateFormat.DATE);
        endDate.setAlignment(HorizontalAlignment.CENTER);

        ColumnConfig elapsed = GridUtils.addColumnConfig(columns, TaskModelData.TOTAL_ACTIVITY_DURATION_FIELD,
                COMMON_CONSTANTS.EndDate(), 70);
        elapsed.setRenderer(new DurationRenderer());
        elapsed.setAlignment(HorizontalAlignment.CENTER);

        ColumnConfig estimated = GridUtils.addColumnConfig(columns, TaskModelData.DURATION_FIELD,
                COMMON_CONSTANTS.EndDate(), 70);
        estimated.setRenderer(new DurationRenderer());
        estimated.setAlignment(HorizontalAlignment.CENTER);

        return new ColumnModel(columns);
    }
}
