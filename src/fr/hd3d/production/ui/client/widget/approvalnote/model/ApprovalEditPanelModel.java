package fr.hd3d.production.ui.client.widget.approvalnote.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.ApprovalNoteList;


public class ApprovalEditPanelModel extends BaseObservable
{
    private ApprovalNoteModelData approvalNote;
    private ListStore<Hd3dModelData> store;
    private ListStore<ApprovalNoteModelData> approvalStore;
    private String field;
    private Hd3dModelData record;
    private List<Hd3dModelData> records;

    private ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(new TaskReader());

    private FastMap<ApprovalNoteList> approvalNoteListByApprovalNoteType = new FastMap<ApprovalNoteList>();

    private FastMap<List<ApprovalNoteList>> graphicalNoteListByApprovalNoteType = new FastMap<List<ApprovalNoteList>>();

    protected List<GraphicalAnnotationModelData> graphicalNoteList = new ArrayList<GraphicalAnnotationModelData>();

    public void reloadTaskStore()
    {
        if (this.getApprovalNote() != null)
        {
            this.taskStore.clearParameters();
            if (this.getApprovalNote().getId() != null)
            {
                this.taskStore.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
                this.taskStore.setPath(ServicesPath.getApprovalNotePath(this.getApprovalNote().getId()) + "/"
                        + ServicesPath.TASKS);
            }
            else
            {
                String path = ServicesPath.PROJECTS + MainModel.getCurrentProject().getId() + "/";
                path += this.approvalNote.getBoundEntityName().toLowerCase() + "s/"
                        + this.approvalNote.getBoundEntityId();
                path += "/" + ServicesPath.TASKS;

                ApprovalNoteTypeModelData approvalType = ProductionModel.approvalStore.findModel(
                        ApprovalNoteTypeModelData.ID_FIELD, approvalNote.getApprovalNoteType());

                this.taskStore.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
                this.taskStore.addEqConstraint("taskType.id", approvalType.getTaskType());
                this.taskStore.setPath(path);
            }
            this.taskStore.reload();
        }
        else
        {
            this.taskStore.removeAll();
        }
    }

    public ApprovalNoteModelData getApprovalNote()
    {
        return this.approvalNote;
    }

    public void setApprovalNote(ApprovalNoteModelData approvalNote)
    {
        if (approvalNote == null)
        {
            approvalNote = new ApprovalNoteModelData();
            approvalNote.setDate(new Date());
        }
        this.approvalNote = approvalNote;
    }

    public ListStore<Hd3dModelData> getRecordStore()
    {
        return this.store;
    }

    public void setRecordStore(ListStore<Hd3dModelData> store)
    {
        this.store = store;
    }

    public String getField()
    {
        return this.field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public Hd3dModelData getRecord()
    {
        return this.record;
    }

    public void setRecord(Hd3dModelData record)
    {
        this.record = record;
    }

    public List<Hd3dModelData> getRecords()
    {
        return records;
    }

    public void setRecords(List<Hd3dModelData> records)
    {
        this.records = records;
    }

    public ListStore<ApprovalNoteModelData> getApprovalStore()
    {
        return this.approvalStore;
    }

    public void setApprovalStore(ListStore<ApprovalNoteModelData> approvalStore)
    {
        this.approvalStore = approvalStore;
    }

    public ServiceStore<TaskModelData> getTaskStore()
    {
        return this.taskStore;
    }

    public void addApprovalNoteListForApprovalNoteType(ApprovalNoteList approvalNoteList, ItemModelData itemModelData)
    {
        this.approvalNoteListByApprovalNoteType.put(itemModelData.getParameter(), approvalNoteList);
    }

    public ApprovalNoteList getApprovalNoteListByApprovalNoteTypeId(String approvalNoteTypeModelDataId)
    {
        return this.approvalNoteListByApprovalNoteType.get(approvalNoteTypeModelDataId);
    }

    public Collection<ApprovalNoteList> getApprovalNoteLists()
    {
        return this.approvalNoteListByApprovalNoteType.values();
    }

    public void addGraphicalApprovalNoteListForApprovalNoteType(ApprovalNoteList approvalNoteList,
            ItemModelData itemModelData)
    {
        List<ApprovalNoteList> list = this.graphicalNoteListByApprovalNoteType.get(itemModelData.getParameter());
        if (list == null)
        {
            list = new ArrayList<ApprovalNoteList>();
        }
        list.add(approvalNoteList);
        this.graphicalNoteListByApprovalNoteType.put(itemModelData.getParameter(), list);
    }

    public List<ApprovalNoteList> getGraphicalNoteListByApprovalNoteTypeId(String approvalNoteTypeModelDataId)
    {
        return this.graphicalNoteListByApprovalNoteType.get(approvalNoteTypeModelDataId);
    }

    public List<GraphicalAnnotationModelData> getGraphicalNoteList()
    {
        return graphicalNoteList;
    }

}
