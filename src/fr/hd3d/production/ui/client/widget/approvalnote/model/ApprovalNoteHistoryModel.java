package fr.hd3d.production.ui.client.widget.approvalnote.model;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


/**
 * Model for the approval note history widget.
 * 
 * @author HD3D
 */
public class ApprovalNoteHistoryModel
{
    /** The store containing history of approval notes. */
    private ListStore<ApprovalNoteModelData> approvalNoteStore;
    private ListStore<Hd3dModelData> recordStore;
    private Hd3dModelData record;
    private String field;

    /**
     * Default constructor.
     */
    public ApprovalNoteHistoryModel()
    {
        approvalNoteStore = new ListStore<ApprovalNoteModelData>();
    }

    /**
     * @return Store constraint approval history.
     */
    public ListStore<ApprovalNoteModelData> getStore()
    {
        return this.approvalNoteStore;
    }

    public Hd3dModelData getRecord()
    {
        return this.record;
    }

    public void setRecord(Hd3dModelData record)
    {
        this.record = record;
    }

    public ListStore<Hd3dModelData> getRecordStore()
    {
        return this.recordStore;
    }

    public void setRecordStore(ListStore<Hd3dModelData> store)
    {
        this.recordStore = store;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    /**
     * @return Approval note list.
     */
    public List<ApprovalNoteModelData> getApprovals()
    {
        approvalNoteStore.sort(ApprovalNoteModelData.DATE_FIELD, SortDir.DESC);
        List<ApprovalNoteModelData> models = approvalNoteStore.getModels();
        for (int i = 0; i < models.size(); i++)
        {
            if (approvalNoteStore.getRecord(models.get(i)).isDirty())
            {
                ApprovalNoteModelData newApp = new ApprovalNoteModelData();
                Hd3dModelData.copy(models.get(i), newApp);
                models.set(i, newApp);
            }
        }
        return models;
    }

    /**
     * Replace current approval note list by <i>approvals</i>.
     * 
     * @param approvals
     *            The approval note list
     */
    public void setApprovals(List<ApprovalNoteModelData> approvals)
    {
        approvalNoteStore.removeAll();
        approvalNoteStore.add(approvals);
        approvalNoteStore.sort(ApprovalNoteModelData.DATE_FIELD, SortDir.DESC);
    }

    public String getField()
    {
        return this.field;
    }

}
