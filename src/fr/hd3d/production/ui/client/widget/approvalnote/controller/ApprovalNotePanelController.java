package fr.hd3d.production.ui.client.widget.approvalnote.controller;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNotePanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNotePanel;


public class ApprovalNotePanelController
{
    private IApprovalNotePanel view;
    private ApprovalNotePanelModel model;

    public ApprovalNotePanelController(IApprovalNotePanel view, ApprovalNotePanelModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void setApprovalNote(ApprovalNoteModelData approvalNote)
    {
        this.model.setApprovalNote(approvalNote);
        this.view.setDate(approvalNote.getDate());
        this.view.setComment(approvalNote.getComment());
        this.view.setStatus(approvalNote.getStatus());
        this.model.reloadTaskStore();
    }

    public ApprovalNoteModelData getApprovalNote()
    {
        ApprovalNoteModelData note = this.model.getApprovalNote();
        note.setComment(this.view.getComment());
        note.setDate(this.view.getDate());
        return note;
    }
}
