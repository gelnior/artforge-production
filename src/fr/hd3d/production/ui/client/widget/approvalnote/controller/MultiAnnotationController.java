package fr.hd3d.production.ui.client.widget.approvalnote.controller;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.wrappers.SVGShapeWrapper;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxyDialogEvents;
import fr.hd3d.production.ui.client.widget.approvalnote.AnnotationDialog;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.MultiApprovalEditPanel;


public class MultiAnnotationController extends Controller
{

    private final MultiApprovalEditPanel view;

    public MultiAnnotationController(MultiApprovalEditPanel view)
    {
        this.view = view;
        EventDispatcher.get().addController(this);
        registerEvents();
    }

    private void registerEvents()
    {
        this.registerEventTypes(ProxyDialogEvents.PROXY_SELECTED);
        this.registerEventTypes(AnnotationEvents.SHOW_ANNOTATION_DIALOG_EVENT);
        this.registerEventTypes(AnnotationEvents.ADD_ANNOTATION_TO_HISTORY_EVENT);
        this.registerEventTypes(AnnotationEvents.SET_FILE_REVISION_TO_ANNOTATION_EVENT);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        if (ProxyDialogEvents.PROXY_SELECTED.equals(event.getType()))
        {
            onProxySelected((ProxyModelData) event.getData());
        }
        else if (AnnotationEvents.SHOW_ANNOTATION_DIALOG_EVENT.equals(event.getType()))
        {
            showAnnotationDialog((SVGShapeWrapper) event.getData());
        }
        else if (AnnotationEvents.ADD_ANNOTATION_TO_HISTORY_EVENT.equals(event.getType()))
        {
            onAddAnnotationToHistory(event);
        }
        else if (AnnotationEvents.SET_FILE_REVISION_TO_ANNOTATION_EVENT.equals(event.getType()))
        {
            setFileRevisionToAnnotation(event);
        }

    }

    private void setFileRevisionToAnnotation(AppEvent event)
    {
        List<ProxyModelData> proxies = event.getData();
        if (proxies != null && proxies.size() > 0)
        {
            this.view.setApprovalNoteProxy(proxies.get(0));
        }
    }

    private void onAddAnnotationToHistory(AppEvent event)
    {
        if (this.view != null)
        {
            double currentTime = 0;
            double duration = 0;
            if (this.view.getProxyPlayer() != null && this.view.getProxyPlayer().getDisplayer() != null)
            {
                currentTime = this.view.getProxyPlayer().getDisplayer().getCurrentTime();
                duration = this.view.getProxyPlayer().getDisplayer().getDuration();
            }
            GraphicalAnnotationModelData annotationModelData = event.getData();
            annotationModelData.setMarkIn(new Long((long) currentTime));
            annotationModelData.setMarkOut(new Long((long) duration));
            FieldModelData status = event.getData("status");
            ItemModelData approvalNoteType = event.getData("approvalNoteType");
            this.view.addAnnotationToHistoryList(annotationModelData, status, approvalNoteType);
        }
    }

    private void showAnnotationDialog(SVGShapeWrapper svgShapeWrapper)
    {
        if (view != null && view.getApprovalEditPanelModel() != null
                && view.getApprovalEditPanelModel().getSelectedProxy() != null)
        {
            AnnotationDialog annotationDialog = new AnnotationDialog(view.getApprovalEditPanelModel(), svgShapeWrapper);
            annotationDialog.show();
        }
    }

    private void onProxySelected(ProxyModelData proxy)
    {
        if (proxy != null)
        {
            this.view.showProxy(proxy);
        }
    }

}
