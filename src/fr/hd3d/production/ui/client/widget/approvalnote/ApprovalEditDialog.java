package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.controller.ApprovalEditPanelController;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalEditPanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.ApprovalEditPanel;


/**
 * Approval edit dialog is a small dialog box allowing to edit or create an approval note.
 * 
 * @author HD3D
 */
public class ApprovalEditDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    /** The model that handles approval edit dialog data. */
    private ApprovalEditPanelModel model = new ApprovalEditPanelModel();
    /** The view that displays dialog widgets (panel set inside dialog). */
    private ApprovalEditPanel view = new ApprovalEditPanel(model);
    /** Controller that handles dialog events. */
    private ApprovalEditPanelController controller = new ApprovalEditPanelController(view, model);

    /**
     * Constructor. Build widgets, set style and behavior.
     */
    public ApprovalEditDialog()
    {
        this.setStyles();
        this.setLayout(new FitLayout());
        this.add(view);
    }

    /**
     * Display approval edit dialog for edition (single selection only).
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param model
     *            The model to modify.
     * @param title
     *            The title to set on this dialog.
     * @param approvalNote
     *            The approval note to edit.
     * @param field
     *            The field that contains approval list from where <i>approval</i> comes.
     */
    public void show(ListStore<Hd3dModelData> store, Hd3dModelData model, String title,
            ApprovalNoteModelData approvalNote, String field)
    {
        this.setModal(false);
        this.model.setRecordStore(store);
        this.model.setField(field);
        this.model.setRecord(model);
        this.controller.setApprovalNote(approvalNote);
        this.setHeading(title);

        this.setFocusWidget(this.view);
        this.view.focusComment();
        this.model.reloadTaskStore();

        super.show();
        this.view.focusComment();
    }

    /**
     * Display approval edit dialog for creation (multi-selection allowed).
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param selectedModels
     *            The models to modify.
     * @param title
     *            The title to set on this dialog.
     * @param approvalNote
     *            The approval note to edit.
     * @param field
     *            The field that contains approval list from where <i>approval</i> comes.
     * @param approvalType
     *            Type of the approval to update.
     * @param status
     *            The status of the approval to create.
     * @param date
     *            The date for the approval to create.
     */
    public void show(ServicesPagingStore<Hd3dModelData> store, List<Hd3dModelData> selectedModels, String title,
            String field, Long approvalType, String status, Date date)
    {
        this.setModal(false);
        this.model.setRecordStore(store);
        this.model.setField(field);
        this.model.setRecords(selectedModels);

        ApprovalNoteModelData approval = ApprovalUtils.getNewApprovalNote(MainModel.currentUser,
                (Hd3dModelData) CollectionUtils.getFirst(selectedModels), approvalType, date);
        approval.setStatus(status);
        approval.setComment("");

        this.controller.setApprovalNote(approval);

        this.setHeading(title);
        this.setFocusWidget(this.view);

        if (selectedModels.size() == 1)
        {
            this.model.reloadTaskStore();
            this.view.enableTaskGrid();
        }
        else
        {
            this.view.disableTaskGrid();
        }
        super.show();
        this.view.focusComment();
    }

    /**
     * Workaround to display edit dialog for a list of approval note instead that for a list of model having approvals
     * inside one of their field.
     * 
     * @param store
     *            Store containing modified model.
     * @param model
     *            The model to modify.
     * @param title
     *            The title to set on this dialog.
     * @param approvalNote
     *            The approval note to edit.
     * @param field
     *            The field containing the approval.
     */
    public void showForHistory(ListStore<ApprovalNoteModelData> store, Hd3dModelData model, String title,
            ApprovalNoteModelData approvalNote, String field)
    {
        this.model.setApprovalStore(store);
        this.model.setField(field);
        this.model.setRecord(model);
        this.controller.setApprovalNote(approvalNote);
        this.setHeading(title);
        this.model.reloadTaskStore();

        super.show();

        this.view.focus();
        this.view.focusComment();
    }

    /**
     * When OK button is clicked, modification are transfered to the explorer and dialog is hidden.
     */
    @Override
    protected void onButtonPressed(Button button)
    {
        try
        {
            if (button.getItemId().equals(OK))
            {
                this.onOkClick();
            }
            super.onButtonPressed(button);
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    /**
     * Update registered record and store with modified data or newly created approval note.
     * 
     * @throws Hd3dException
     */
    private void onOkClick() throws Hd3dException
    {
        if (this.model.getRecordStore() != null)
        {
            if (this.model.getRecords() != null)
            {
                for (Hd3dModelData model : this.model.getRecords())
                {
                    ApprovalNoteModelData approval = new ApprovalNoteModelData();
                    ApprovalUtils.setTaskTypeInfo(this.controller.getApprovalNote());
                    approval.updateFromModelData(this.controller.getApprovalNote());
                    approval.setBoundEntityId(model.getId());

                    ApprovalUtils.addOrUpdateApprovalForModel(this.model.getRecordStore(), model, approval, this.model
                            .getField());
                }
            }
            else
            {
                ApprovalUtils.addOrUpdateApprovalForModel(this.model.getRecordStore(), this.model.getRecord(),
                        this.controller.getApprovalNote(), this.model.getField());
            }
        }
        else if (this.model.getApprovalStore() != null)
        {
            Record rec = this.model.getApprovalStore().getRecord(this.model.getApprovalNote());
            rec.set(ApprovalNoteModelData.COMMENT_FIELD, this.view.getComment());
            rec.set(ApprovalNoteModelData.STATUS_FIELD, this.view.getStatus());
        }
        if (ExplorerController.autoSave)
        {
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
        }

        this.model.setRecords(null);
        this.model.setRecordStore(null);
        this.model.setApprovalStore(null);
    }

    /**
     * Disable date fields (used to change approval date).
     */
    public void disableDate()
    {
        this.view.disableDate();
    }

    /**
     * Enable date fields (used to change approval date).
     */
    public void enableDate()
    {
        this.view.enableDate();
    }

    /**
     * Set dialog style and behavior.
     */
    private void setStyles()
    {
        this.setModal(false);
        this.setButtons(OKCANCEL);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setSize(600, 300);
        this.setHeading(CONSTANTS.ApprovalNote());
        this.setHideOnButtonClick(true);
        this.setClosable(false);
        this.setOnEsc(true);
    }

}
