package fr.hd3d.production.ui.client.widget.approvalnote.model;

import java.util.Date;

import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Model handling approval note panel data.
 * 
 * @author HD3D
 */
public class ApprovalNotePanelModel extends BaseObservable
{
    private ApprovalNoteModelData approvalNote;

    private ListStore<Hd3dModelData> store;
    private ListStore<ApprovalNoteModelData> approvalStore;

    private String field;
    private Hd3dModelData approvalRecord;

    // private ApprovalNoteTypeModelData approvalType;
    private ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(new TaskReader());

    // private ServiceStore<EntityTaskLinkModelData> linkStore = new ServiceStore<EntityTaskLinkModelData>(
    // new EntityTaskLinkReader());

    public ApprovalNotePanelModel()
    {
        // this.linkStore.addLoadListener(new LoadListener() {
        // public void loaderLoad(LoadEvent le)
        // {
        // onLinkStoreLoaded();
        // }
        // });
    }

    protected void onLinkStoreLoaded()
    {
        // List<Long> taskIds = new ArrayList<Long>();
        // for (EntityTaskLinkModelData link : linkStore.getModels())
        // {
        // taskIds.add(link.getTaskId());
        // }
        //
        // taskStore.clearParameters();
        //
        // if (CollectionUtils.isNotEmpty(taskIds))
        // {
        // Constraints constraints = new Constraints();
        // constraints.add(new Constraint(EConstraintOperator.in, TaskModelData.ID_FIELD, taskIds));
        // constraints.add(new Constraint(EConstraintOperator.eq, "taskType.id", approvalType.getTaskType()));
        // taskStore.addParameter(constraints);
        // taskStore.setPath(ServicesPath.getTasksPath(null));
        // taskStore.reload();
        // }
        // else
        // {
        // taskStore.removeAll();
        // }
    }

    public void reloadTaskStore()
    {
        if (this.getApprovalNote() != null)
        {
            this.taskStore.setPath(ServicesPath.getApprovalNotePath(this.getApprovalNote().getId()) + "/"
                    + ServicesPath.TASKS);
            this.taskStore.reload();
        }
        else
        {
            this.taskStore.removeAll();
        }
    }

    public ApprovalNoteModelData getApprovalNote()
    {
        return this.approvalNote;
    }

    public void setApprovalNote(ApprovalNoteModelData approvalNote)
    {
        if (approvalNote == null)
        {
            approvalNote = new ApprovalNoteModelData();
            approvalNote.setDate(new Date());
        }
        this.approvalNote = approvalNote;
    }

    public ListStore<Hd3dModelData> getRecordStore()
    {
        return this.store;
    }

    public void setRecordStore(ListStore<Hd3dModelData> store)
    {
        this.store = store;
    }

    public String getField()
    {
        return this.field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public Hd3dModelData getRecord()
    {
        return this.approvalRecord;
    }

    public void setRecord(Hd3dModelData record)
    {
        this.approvalRecord = record;
    }

    public ListStore<ApprovalNoteModelData> getApprovalStore()
    {
        return this.approvalStore;
    }

    public void setApprovalStore(ListStore<ApprovalNoteModelData> approvalStore)
    {
        this.approvalStore = approvalStore;
    }

    public ServiceStore<TaskModelData> getTaskStore()
    {
        return this.taskStore;
    }

}
