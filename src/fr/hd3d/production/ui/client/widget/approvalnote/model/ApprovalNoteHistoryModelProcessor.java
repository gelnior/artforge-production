package fr.hd3d.production.ui.client.widget.approvalnote.model;

import com.extjs.gxt.ui.client.data.ModelProcessor;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


/**
 * Approval model processor add another field to approval model data. This field is set to facilitate switch to done
 * status. An approval can be marked as done, when work is done and but not accessible.
 * 
 * @author HD3D
 */
public class ApprovalNoteHistoryModelProcessor extends ModelProcessor<ApprovalNoteModelData>
{
    @Override
    public ApprovalNoteModelData prepareData(ApprovalNoteModelData model)
    {
        if (model.isStatusDone())
        {
            model.setColumnStatusDone();
        }
        else
        {
            model.setColumnStatusUndone();
        }
        return model;
    }
}
