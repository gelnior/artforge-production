package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ComponentPlugin;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.grid.renderer.LongTextRenderer;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.ApprovalEditDisplayer;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNoteHistoryModel;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNoteHistoryModelProcessor;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNoteHistoryPanel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.renderer.ApprovalNoteStatusRenderer;
import fr.hd3d.production.ui.client.widget.approvalnote.view.renderer.DeleteButtonRenderer;
import fr.hd3d.production.ui.client.widget.approvalnote.view.renderer.EditButtonRenderer;


/**
 * Panel displaying a list of approval note ordered by date. This panel is used by production explorer to display all
 * validations set on a work object (its validation history).
 * 
 * @author HD3D
 */
public class ApprovalNoteHistoryPanel extends ContentPanel implements IApprovalNoteHistoryPanel
{
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    private ApprovalNoteHistoryModel model;
    private Grid<ApprovalNoteModelData> grid;

    public ApprovalNoteHistoryPanel(ApprovalNoteHistoryModel model, ListStore<ApprovalNoteModelData> store)
    {
        this.model = model;

        this.setStyles();
        this.setGrid(store, this.getColumnModel());
    }

    private void setGrid(ListStore<ApprovalNoteModelData> store, ColumnModel cm)
    {
        this.grid = new Grid<ApprovalNoteModelData>(store, cm);
        this.grid.setBorders(false);
        this.grid.setTrackMouseOver(false);
        this.grid.getView().setViewConfig(new ApprovalNoteHistoryGridViewConfig());
        this.grid.setModelProcessor(new ApprovalNoteHistoryModelProcessor());

        this.add(grid);

        List<ColumnConfig> columns = cm.getColumns();
        for (ColumnConfig column : columns)
        {
            if (column instanceof CheckColumnConfig)
            {
                grid.addPlugin((ComponentPlugin) column);
            }
        }
    }

    private void setStyles()
    {
        this.setHeaderVisible(false);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setLayout(new FitLayout());
    }

    public void editApprovalNote(ApprovalNoteModelData note)
    {
        String title = CONSTANTS.ApprovalEditor();
        if (model.getRecord().get(ConstituentModelData.CONSTITUENT_LABEL) != null)
        {
            title = model.getRecord().get(ConstituentModelData.CONSTITUENT_LABEL) + " " + CONSTANTS.ApprovalEditor();
        }
        else if (model.getRecord().get(TaskModelData.NAME_FIELD) != null)
        {
            title = model.getRecord().get(TaskModelData.NAME_FIELD) + " " + CONSTANTS.ApprovalEditor();
        }

        ApprovalEditDisplayer.showForHistory(model.getStore(), model.getRecord(), title, note, model.getField());
    }

    /**
     * @return Column model for the history grid.
     */
    public ColumnModel getColumnModel()
    {
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig status = GridUtils.addColumnConfig(configs, ApprovalNoteModelData.STATUS_FIELD,
                COMMON_CONSTANTS.Status(), 50);
        status.setRenderer(new ApprovalNoteStatusRenderer());
        status.setSortable(false);

        ColumnConfig date = GridUtils.addColumnConfig(configs, ApprovalNoteModelData.DATE_FIELD,
                COMMON_CONSTANTS.Date(), 115);
        date.setDateTimeFormat(DateFormat.FRENCH_DATE_TIME);
        date.setSortable(false);

        ColumnConfig user = GridUtils.addColumnConfig(configs, ApprovalNoteModelData.APPROVER_NAME_FIELD,
                COMMON_CONSTANTS.User(), 100);
        user.setSortable(false);

        ColumnConfig comment = GridUtils.addColumnConfig(configs, ApprovalNoteModelData.COMMENT_FIELD,
                COMMON_CONSTANTS.Comment(), 360);
        comment.setRenderer(new LongTextRenderer<ApprovalNoteModelData>());
        comment.setSortable(false);

        ColumnConfig update = GridUtils.addColumnConfig(configs, ApprovalNoteModelData.USER_CAN_UPDATE_FIELD, "", 30);
        update.setRenderer(new EditButtonRenderer());
        update.setSortable(false);

        ColumnConfig delete = GridUtils.addColumnConfig(configs, ApprovalNoteModelData.USER_CAN_DELETE_FIELD, "", 30);
        delete.setHeader("");
        delete.setRenderer(new DeleteButtonRenderer());
        delete.setSortable(false);

        return new ColumnModel(configs);
    }

}
