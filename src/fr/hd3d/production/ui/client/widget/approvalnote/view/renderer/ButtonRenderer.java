package fr.hd3d.production.ui.client.widget.approvalnote.view.renderer;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Display a button in each cell of the column that forwards given event when clicked.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of data concerned by the renderer.
 */
public class ButtonRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>
{

    /** CSS icon style. */
    protected String iconStyle;
    /** Tool tip to display when mouse is over. */
    protected String toolTip;
    /** Event type to forward when button is clicked. */
    protected EventType eventType;

    /**
     * Default constructor
     * 
     * @param iconStyle
     *            CSS icon style.
     * @param toolTip
     *            Tool tip to display when mouse is over.
     * @param eventType
     *            Event type to forward when button is clicked.
     */
    public ButtonRenderer(String iconStyle, String toolTip, EventType eventType)
    {
        this.iconStyle = iconStyle;
        this.toolTip = toolTip;
        this.eventType = eventType;
    }

    public Object render(final M model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<M> store, Grid<M> grid)
    {
        Button b = new Button(null, new ButtonListener(model, this.eventType));
        b.setWidth(grid.getColumnModel().getColumnWidth(colIndex) - 10);
        b.setIconStyle(iconStyle);
        b.setToolTip(toolTip);

        return b;
    }

    /**
     * Specific button listener that forwards a given event with given model as attachment.
     * 
     * @author HD3D
     */
    private class ButtonListener extends SelectionListener<ButtonEvent>
    {
        private M model;
        private EventType event;

        public ButtonListener(M model, EventType event)
        {
            this.model = model;
            this.event = event;
        }

        @Override
        public void componentSelected(ButtonEvent ce)
        {
            EventDispatcher.forwardEvent(new AppEvent(event, model));
        }

    }
}
