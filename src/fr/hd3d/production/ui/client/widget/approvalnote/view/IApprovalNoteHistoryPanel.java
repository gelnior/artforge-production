package fr.hd3d.production.ui.client.widget.approvalnote.view;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


public interface IApprovalNoteHistoryPanel
{
    public void editApprovalNote(ApprovalNoteModelData note);
}
