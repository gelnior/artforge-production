package fr.hd3d.production.ui.client.widget.approvalnote.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;


public class ApprovalNoteStatusRenderer implements GridCellRenderer<ApprovalNoteModelData>
{

    public Object render(ApprovalNoteModelData approval, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ApprovalNoteModelData> store, Grid<ApprovalNoteModelData> grid)
    {
        String status = approval.get(property);
        Element div = DOM.createDiv();
        div.setAttribute("style",
                "padding : 3px; font-weight: bold; background-color:" + TaskStatusMap.getColorForStatus(status) + ";");
        div.setInnerHTML(NoteStatusComboBox.getStatusAbbreviationMap().get(status));
        return div.getString();
    }
}
