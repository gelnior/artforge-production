package fr.hd3d.production.ui.client.widget.approvalnote;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.controller.ApprovalNotePanelController;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNotePanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.ApprovalNotePanel;


@Deprecated
public class ApprovalNoteEditDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    private ApprovalNotePanelModel model = new ApprovalNotePanelModel();
    private ApprovalNotePanel view = new ApprovalNotePanel(model);
    private ApprovalNotePanelController controller = new ApprovalNotePanelController(view, model);

    public ApprovalNoteEditDialog()
    {
        this.setStyles();
        this.setLayout(new FitLayout());
        this.add(view);
        this.setOnEsc(true);
    }

    public void show(ListStore<Hd3dModelData> store, Hd3dModelData record, String title,
            ApprovalNoteModelData approvalNote, String field)
    {
        this.setModal(false);
        this.model.setRecordStore(store);
        this.model.setField(field);
        this.model.setRecord(record);
        this.controller.setApprovalNote(approvalNote);

        // this.view.focusComment();

        this.setFocusWidget(this.view);
        this.setHeading(title);

        super.show();
        this.view.focusComment();
    }

    public void showForHistory(ListStore<ApprovalNoteModelData> store, Hd3dModelData model, String title,
            ApprovalNoteModelData approvalNote, String field)
    {
        this.model.setApprovalStore(store);
        this.model.setField(field);
        this.model.setRecord(model);
        this.controller.setApprovalNote(approvalNote);
        this.setHeading(title);
        this.model.reloadTaskStore();

        super.show();

        this.view.focus();
        this.view.focusComment();
    }

    @Override
    protected void onButtonPressed(Button button)
    {
        super.onButtonPressed(button);
        if (button == getButtonBar().getItemByItemId(OK))
        {
            this.onOkClick();
        }
    }

    private void onOkClick()
    {
        if (this.model.getRecordStore() != null)
        {
            ApprovalUtils.addOrUpdateApprovalForModel(this.model.getRecordStore(), this.model.getRecord(),
                    this.controller.getApprovalNote(), this.model.getField());
        }
        else if (this.model.getApprovalStore() != null)
        {
            Record rec = this.model.getApprovalStore().getRecord(this.model.getApprovalNote());
            rec.set(ApprovalNoteModelData.COMMENT_FIELD, this.view.getComment());
            rec.set(ApprovalNoteModelData.DATE_FIELD, this.view.getDate());
        }

        this.model.setRecordStore(null);
        this.model.setApprovalStore(null);
    }

    private void setStyles()
    {
        setButtons(OKCANCEL);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setSize(500, 300);
        this.setHeading(CONSTANTS.ApprovalNote());
        this.setHideOnButtonClick(true);
        this.setModal(false);
    }

}
