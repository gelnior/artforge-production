package fr.hd3d.production.ui.client.widget.approvalnote.model;

import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyReader;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.proxyplayer.util.ProxyMimeType;


public class MultiApprovalEditPanelModel extends ApprovalEditPanelModel
{
    private ProxyModelData selectedProxy;
    private List<ItemModelData> approvalColumns;
    private Long approvalType;
    private Long fileRevisionId;
    private ServiceStore<ProxyModelData> serviceStore;

    public Long getApprovalType()
    {
        return approvalType;
    }

    public void setApprovalType(Long approvalType)
    {
        this.approvalType = approvalType;
    }

    public ProxyModelData getSelectedProxy()
    {
        return selectedProxy;
    }

    public void setSelectedProxy(ProxyModelData selectedProxy)
    {
        this.selectedProxy = selectedProxy;
    }

    public void setApprovalColumns(List<ItemModelData> approvalColumns)
    {
        this.approvalColumns = approvalColumns;
    }

    public List<ItemModelData> getApprovalColumns()
    {
        return approvalColumns;
    }

    public void setFileRevisionId(Long fileRevisionId)
    {
        if (fileRevisionId != null)
        {
            this.fileRevisionId = fileRevisionId;
            serviceStore = new ServiceStore<ProxyModelData>(new ProxyReader());
            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint("fileRevision", fileRevisionId));
            constraints.add(new InConstraint("proxyType.openType", ProxyMimeType.getTypes()));
            serviceStore.addParameter(constraints);
            serviceStore.addLoadListener(new LoadListener() {
                @Override
                public void loaderLoad(LoadEvent le)
                {
                    if (serviceStore.getModels() != null)
                    {
                        EventDispatcher.forwardEvent(AnnotationEvents.SET_FILE_REVISION_TO_ANNOTATION_EVENT,
                                serviceStore.getModels());
                    }
                }
            });
            serviceStore.reload();
        }
    }

    public Long getFileRevisionId()
    {
        return fileRevisionId;
    }

    public void saveGraphicalAnnotations()
    {
        BulkRequests.bulkPost(this.graphicalNoteList, AnnotationEvents.GRAPHICAL_ANNOTATION_SAVED_EVENT);
    }
}
