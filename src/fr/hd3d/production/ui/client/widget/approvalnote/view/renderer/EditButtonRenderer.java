package fr.hd3d.production.ui.client.widget.approvalnote.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.event.ProductionEvents;


/**
 * Display an edit approval button inside each cell column if user has right to edit the model data.
 * 
 * @author HD3D
 */
public class EditButtonRenderer extends ButtonRenderer<ApprovalNoteModelData>
{
    /** Constants to display. */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    /**
     * Default constructor
     */
    public EditButtonRenderer()
    {
        super(ExplorateurCSS.NOTE_EDIT, CONSTANTS.EditNote(), ProductionEvents.EDIT_APPROVAL_NOTE_CLICKED);
    }

    public Object render(final ApprovalNoteModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ApprovalNoteModelData> store, Grid<ApprovalNoteModelData> grid)
    {
        if (model.getUserCanUpdate())
        {
            return super.render(model, property, config, rowIndex, colIndex, store, grid);
        }
        else
        {
            return "";
        }
    }

}
