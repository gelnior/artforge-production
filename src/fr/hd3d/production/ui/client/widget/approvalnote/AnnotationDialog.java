package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.Date;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.wrappers.SVGShapeWrapper;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.model.MultiApprovalEditPanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.ApprovalEditPanel;


public class AnnotationDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    private ApprovalEditPanel approvalEditPanel;

    private MultiApprovalEditPanelModel model;

    private final SVGShapeWrapper svgShapeWrapper;

    public AnnotationDialog(MultiApprovalEditPanelModel model, SVGShapeWrapper svgShapeWrapper)
    {
        this.model = model;
        this.svgShapeWrapper = svgShapeWrapper;
        setLayout(new FitLayout());
        approvalEditPanel = new ApprovalEditPanel(model, false);
        approvalEditPanel.setApprovalTypes(this.model.getApprovalColumns(), this.model.getApprovalType(), false);
        approvalEditPanel.setDate(new Date());
        this.add(approvalEditPanel);
        setStyles();
    }

    @Override
    protected void onButtonPressed(Button button)
    {
        try
        {
            if (button.getItemId().equals(OK))
            {
                FieldModelData status = this.approvalEditPanel.getStatusField().getValue();
                if (status != null)
                {
                    this.approvalEditPanel.getStatusField().clearInvalid();
                    this.onOkClick();
                    super.onButtonPressed(button);
                }
                else
                {
                    this.approvalEditPanel.getStatusField().forceInvalid("You must select a status");
                }

            }
            else
            {
                super.onButtonPressed(button);
            }
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    private void onOkClick() throws Hd3dException
    {
        if (this.approvalEditPanel != null)
        {
            // create new GraphicalModelData
            GraphicalAnnotationModelData graphicalAnnotationModelData = new GraphicalAnnotationModelData();
            // graphicalAnnotationModelData.setAnnotationSvg(svgShapeWrapper.getGraphicalWidget().toString());
            graphicalAnnotationModelData.setAuthor(MainModel.currentUser.getId());
            graphicalAnnotationModelData.setComment(this.approvalEditPanel.getComment());
            graphicalAnnotationModelData.setDate(approvalEditPanel.getDate());
            graphicalAnnotationModelData.setApprovalNote(approvalEditPanel.getApprovalNoteType().getId());
            if (model.getSelectedProxy() != null)
            {
                graphicalAnnotationModelData.setProxy(model.getSelectedProxy().getId());
            }
            // graphicalAnnotationModelData.save();
            AppEvent appEvent = new AppEvent(AnnotationEvents.ADD_ANNOTATION_TO_HISTORY_EVENT);
            appEvent.setData(graphicalAnnotationModelData);
            appEvent.setData("status", approvalEditPanel.getStatusField().getValue());
            appEvent.setData("approvalNoteType", approvalEditPanel.getApprovalNoteType());
            EventDispatcher.forwardEvent(appEvent);
        }
    }

    /**
     * Set dialog style and behavior.
     */
    private void setStyles()
    {
        this.setModal(false);
        this.setButtons(OKCANCEL);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setSize(600, 300);
        this.setHeading(CONSTANTS.ApprovalNote());
        this.setHideOnButtonClick(true);
        this.setClosable(false);
        this.setOnEsc(true);
    }
}
