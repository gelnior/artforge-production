package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.WindowEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.controller.ApprovalNoteHistoryController;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalNoteHistoryModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.ApprovalNoteHistoryPanel;


/**
 * Dialog displaying note histories for a couple work-object / approval type.
 * 
 * @author HD3D
 */
public class ApprovalNoteHistoryDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    /** Model handling data. */
    private ApprovalNoteHistoryModel model = new ApprovalNoteHistoryModel();
    /** View handling displayed widgets. */
    private ApprovalNoteHistoryPanel view = new ApprovalNoteHistoryPanel(model, model.getStore());
    /** Controller handing events. */
    private ApprovalNoteHistoryController controller = new ApprovalNoteHistoryController(view, model);

    /**
     * Constructor.
     */
    public ApprovalNoteHistoryDialog()
    {
        this.setStyles();

        this.add(view);
        this.addListener(Events.Hide, new Listener<WindowEvent>() {
            public void handleEvent(WindowEvent be)
            {
                onDialogHide();
            }
        });
    }

    /**
     * Set dialog styles : size, headers...
     */
    private void setStyles()
    {
        this.setWidth(720);
        this.setHeight(300);

        this.setHeading(CONSTANTS.ApprovalNoteHistory());
        this.setBodyBorder(true);
        this.setBorders(false);

        this.setHideOnButtonClick(true);
        this.setResizable(false);

        this.setLayout(new FitLayout());
        this.setButtons(OK);
        this.setResizable(true);
    }

    public void show(ListStore<Hd3dModelData> store, Hd3dModelData record, String title,
            List<ApprovalNoteModelData> approvals, String field)
    {
        this.setHeading(title);
        this.model.setRecord(record);
        this.model.setRecordStore(store);
        this.model.setField(field);
        this.model.setApprovals(approvals);

        super.show();
        this.doLayout(true);
    }

    public Boolean isDirty()
    {
        return controller.isDirty();
    }

    private void onDialogHide()
    {
        Record rec = this.model.getRecordStore().getRecord(this.model.getRecord());
        rec.set(this.model.getField(), this.model.getApprovals());
        if (ExplorerController.autoSave)
        {
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
        }
    }
}
