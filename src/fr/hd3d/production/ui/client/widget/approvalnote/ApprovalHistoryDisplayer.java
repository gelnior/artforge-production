package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.production.ui.client.constant.ProductionConstants;


/**
 * Singleton for approval history editor displaying.
 * 
 * @author HD3D
 */
public class ApprovalHistoryDisplayer
{
    /** Constants for displaying. */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    /** Approval history editor dialog. */
    private static ApprovalNoteHistoryDialog historyDialog;

    /**
     * @return Approval history editor dialog.
     */
    private static ApprovalNoteHistoryDialog getDialog()
    {
        if (historyDialog == null)
        {
            historyDialog = new ApprovalNoteHistoryDialog();
            historyDialog.setModal(true);
        }
        return historyDialog;
    }

    /**
     * Show approval history editor dialog.
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param model
     *            The model on which approval is edited.
     * @param title
     *            The title of the dialog.
     * @param approvalNotes
     *            The list of approvals.
     * @param field
     *            The field that contains approval list from where <i>approval</i> comes.
     */
    public static void show(ListStore<Hd3dModelData> store, Hd3dModelData model, String title,
            List<ApprovalNoteModelData> approvalNotes, String field)
    {
        getDialog().show(store, model, title, approvalNotes, field);
    }

    /**
     * Show approval history editor dialog. Title is based on work object name.
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param model
     *            The model on which approval is edited.
     * @param field
     */
    @SuppressWarnings("unchecked")
    public static void show(ListStore<Hd3dModelData> store, Hd3dModelData model, String field)
    {
        List<ApprovalNoteModelData> approvalNotes = (List<ApprovalNoteModelData>) model.get(field);

        String title = "Notes history";
        if (model.get(ConstituentModelData.CONSTITUENT_LABEL) != null)
        {
            title = model.get(ConstituentModelData.CONSTITUENT_LABEL) + " " + "notes history";
        }
        else if (model.get(TaskModelData.NAME_FIELD) != null)
        {
            title = model.get(TaskModelData.NAME_FIELD) + " " + "notes history";
        }

        show(store, model, title, approvalNotes, field);
    }
}
