package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskStatusRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskTypeRenderer;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.PaddingTextRenderer;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalEditPanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNotePanel;


public class ApprovalEditPanel extends ContentPanel implements IApprovalNotePanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    private ApprovalEditPanelModel model;

    private DateField dateField = new DateField();
    private NoteStatusComboBox statusField = new NoteStatusComboBox(true);
    private TextArea commentField;

    /** The task grid, needed to close tasks. */
    protected BaseGrid<TaskModelData> taskGrid;
    private LayoutContainer topContainer;
    private ComboBox<ItemModelData> approvalNoteTypeComboBox;
    private DateWrapper dateWrapper;

    public ApprovalEditPanel(ApprovalEditPanelModel model, boolean withTaskGrid)
    {
        this.model = model;

        this.setStyles();
        this.createForm();

        if (withTaskGrid)
        {
            this.setTaskGrid();
        }

    }

    public ApprovalEditPanel(ApprovalEditPanelModel model)
    {
        this(model, true);
    }

    public ItemModelData getApprovalNoteType()
    {
        if (this.approvalNoteTypeComboBox != null)
        {
            return this.approvalNoteTypeComboBox.getValue();
        }
        return null;
    }

    public void setApprovalTypes(List<ItemModelData> approvalColumns, Long approvalType)
    {
        setApprovalTypes(approvalColumns, approvalType, true);
    }

    public void setApprovalTypes(List<ItemModelData> approvalColumns, Long approvalType, boolean withChangeComment)
    {
        ListStore<ItemModelData> listStore = new ListStore<ItemModelData>();
        listStore.add(approvalColumns);
        approvalNoteTypeComboBox = new ComboBox<ItemModelData>();
        if (withChangeComment)
        {
            approvalNoteTypeComboBox.addSelectionChangedListener(new SelectionChangedListener<ItemModelData>() {
                @Override
                public void selectionChanged(SelectionChangedEvent<ItemModelData> se)
                {
                    ItemModelData modelData = se.getSelectedItem();
                    ApprovalNoteList approvalNoteList = model.getApprovalNoteListByApprovalNoteTypeId(modelData
                            .getParameter());
                    if (approvalNoteList != null)
                    {
                        setComment(approvalNoteList.getText());
                    }
                    else
                    {
                        clearComment();
                    }
                }
            });
        }
        approvalNoteTypeComboBox.setForceSelection(true);
        approvalNoteTypeComboBox.setTriggerAction(TriggerAction.ALL);
        approvalNoteTypeComboBox.setEditable(false);
        approvalNoteTypeComboBox.setValidateOnBlur(false);
        approvalNoteTypeComboBox.setDisplayField(RecordModelData.NAME_FIELD);
        approvalNoteTypeComboBox.setTypeAhead(true);
        approvalNoteTypeComboBox.setMinChars(3);
        approvalNoteTypeComboBox.setStore(listStore);
        topContainer.add(approvalNoteTypeComboBox);
        topContainer.layout(true);
        if (approvalType != null)
        {
            ItemModelData findModel = listStore.findModel(ItemModelData.PARAMETER_FIELD, approvalType.toString());
            approvalNoteTypeComboBox.setValue(findModel);
        }
    }

    public String getStatus() throws Hd3dException
    {
        if (statusField.getValue() == null)
        {
            throw new Hd3dException("Please set a status.");
        }
        return this.statusField.getDirectValue().toString();
    }

    public NoteStatusComboBox getStatusField()
    {
        return statusField;
    }

    public void setStatus(String status)
    {
        if (status == null || status.equals(""))
        {
            this.statusField.setRawValue("");
            return;
        }

        if (ApprovalNoteModelData.STATUS_OK.equals(status))
        {
            this.statusField.removeStyleName("approval-status-nok");
            this.statusField.addStyleName("approval-status-ok");
        }
        else
        {
            this.statusField.removeStyleName("approval-status-ok");
            this.statusField.addStyleName("approval-status-nok");
        }

        this.statusField.setValueByStringValue(status);
    }

    public String getComment()
    {
        return this.commentField.getValue();
    }

    public void setComment(String comment)
    {
        this.commentField.setValue(comment);
    }

    public void clearComment()
    {
        this.commentField.clear();
    }

    public Date getDate()
    {
        Date date = this.dateField.getValue();

        if (dateWrapper != null)
        {
            DateWrapper returnWrapper = new DateWrapper(date);
            returnWrapper = returnWrapper.addSeconds(dateWrapper.getSeconds());
            date = returnWrapper.asDate();
        }

        return date;
    }

    public void setDate(Date date)
    {
        dateWrapper = new DateWrapper(date);
        this.dateField.setValue(date);
    }

    public void createForm()
    {
        this.setLayout(new BorderLayout());

        HBoxLayout topLayout = new HBoxLayout();
        topLayout.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);

        dateField.setFieldLabel("Date");
        dateField.setLabelSeparator("");
        dateField.setWidth(150);
        dateField.getPropertyEditor().setFormat(DateTimeFormat.getFormat("dd-MM-yyyy HH:mm"));

        topContainer = new LayoutContainer();
        topContainer.setLayout(topLayout);
        topContainer.add(dateField);

        HBoxLayoutData flex = new HBoxLayoutData(new Margins(0));
        flex.setFlex(1);
        topContainer.add(new Text(), flex);

        statusField.setFieldLabel(COMMON_CONSTANTS.Status());
        statusField.setLabelSeparator("");
        statusField.setWidth(80);

        topContainer.add(statusField);

        commentField = new TextArea();
        commentField.setLabelSeparator("");
        commentField.setFieldLabel(COMMON_CONSTANTS.Comment());
        commentField.setHeight(200);
        commentField.setEmptyText("Add comments...");

        Menu pushLastNoteMenu = new Menu();
        for (final ItemModelData item : ProductionModel.getApprovalColumns())
        {
            MenuItem menuItem = new MenuItem("Push selection to " + item.getName());
            pushLastNoteMenu.add(menuItem);

            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    // pushApproval("id_" + item.getId());
                }
            });
        }

        commentField.setContextMenu(pushLastNoteMenu);

        this.add(topContainer, new BorderLayoutData(LayoutRegion.NORTH, 28));
        this.add(commentField, new BorderLayoutData(LayoutRegion.CENTER));
    }

    public void focusComment()
    {
        if (this.commentField.isRendered())
        {
            this.commentField.focus();
        }
    }

    public void enableTaskGrid()
    {
        this.taskGrid.enable();
    }

    public void disableTaskGrid()
    {
        this.taskGrid.disable();
    }

    public void disableDate()
    {
        dateField.disable();
    }

    public void enableDate()
    {
        dateField.enable();
    }

    /** Set panel styles. */
    private void setStyles()
    {
        this.setHeaderVisible(false);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setFrame(true);

        this.statusField.setId("approval-status-field");
    }

    private void setTaskGrid()
    {
        taskGrid = new BaseGrid<TaskModelData>(this.model.getTaskStore(), this.getColumnModel());
        taskGrid.setHeight(70);
        taskGrid.setBorders(true);
        // this.taskGrid.addListener(Events.AfterEdit, new AfterEditListener<TaskModelData>(
        // ProductionEvents.APPROVAL_TASK_EDITED));
        BorderLayoutData layoutData = new BorderLayoutData(LayoutRegion.SOUTH, 80);
        layoutData.setMargins(new Margins(5, 0, 0, 0));
        this.add(taskGrid, layoutData);
    }

    /**
     * @return Column list for task grid.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig personColumn = GridUtils.addColumnConfig(columns, TaskModelData.WORKER_NAME_FIELD,
                COMMON_CONSTANTS.Worker(), 120);
        personColumn.setRenderer(new PaddingTextRenderer<TaskModelData>());

        ColumnConfig typeColumn = GridUtils.addColumnConfig(columns, TaskModelData.TASK_TYPE_NAME_FIELD,
                COMMON_CONSTANTS.Type(), 80);
        typeColumn.setRenderer(new TaskTypeRenderer());

        ColumnConfig statusColumn = GridUtils.addColumnConfig(columns, TaskModelData.STATUS_FIELD,
                COMMON_CONSTANTS.Status(), 110);
        statusColumn.setRenderer(new TaskStatusRenderer<Hd3dModelData>());

        ColumnConfig startDate = GridUtils.addColumnConfig(columns, TaskModelData.START_DATE_FIELD,
                COMMON_CONSTANTS.StartDate(), 70);
        startDate.setDateTimeFormat(DateFormat.FRENCH_DATE);
        startDate.setAlignment(HorizontalAlignment.CENTER);
        startDate.setRenderer(new PaddingTextRenderer<TaskModelData>());

        ColumnConfig endDate = GridUtils.addColumnConfig(columns, TaskModelData.END_DATE_FIELD,
                COMMON_CONSTANTS.EndDate(), 70);
        endDate.setDateTimeFormat(DateFormat.FRENCH_DATE);
        endDate.setAlignment(HorizontalAlignment.CENTER);
        endDate.setRenderer(new PaddingTextRenderer<TaskModelData>());

        ColumnConfig elapsed = GridUtils.addColumnConfig(columns, TaskModelData.TOTAL_ACTIVITY_DURATION_FIELD,
                "Elapsed", 50);
        elapsed.setRenderer(new DurationRenderer());
        elapsed.setAlignment(HorizontalAlignment.CENTER);

        ColumnConfig estimated = GridUtils.addColumnConfig(columns, TaskModelData.DURATION_FIELD, "Estimated", 50);
        estimated.setRenderer(new DurationRenderer());
        estimated.setAlignment(HorizontalAlignment.CENTER);

        return new ColumnModel(columns);
    }

}
