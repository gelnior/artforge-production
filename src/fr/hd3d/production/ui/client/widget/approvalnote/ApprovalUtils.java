package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalComparator;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.production.ui.client.model.ProductionModel;


/**
 * ApprovalUtils provides methods to facilitate approval handling inside explorer.
 * 
 * @author HD3D
 */
public class ApprovalUtils
{

    /**
     * @param model
     *            The model from which last approval is needed.
     * @param columnName
     *            The column name containing desired approval.
     * @return Last approval added to <i>model</i>. If model has no approval, <i>null</i> is returned.
     */
    @SuppressWarnings("unchecked")
    public static ApprovalNoteModelData getLastApprovalNote(Hd3dModelData model, String columnName)
    {
        Object note = model.get(columnName);
        if (note instanceof List)
        {
            List<ApprovalNoteModelData> approvals = (List<ApprovalNoteModelData>) note;
            if (!CollectionUtils.isEmpty(approvals))
            {
                return approvals.get(0);
            }
        }
        return null;
    }

    /**
     * Add <i>approval</i> as last added approval to the <i>model</i>. If approval already exists for this model, a new
     * approval with same data is added after <i>approval</i>.
     * 
     * @param store
     *            The explorer store, needed to mark that model has been changed.
     * @param model
     *            The model on which adding approval.
     * @param approval
     *            The approval to add
     * @param field
     *            The field containing approval.
     */
    @SuppressWarnings("unchecked")
    public static void addOrUpdateApprovalForModel(ListStore<Hd3dModelData> store, Hd3dModelData model,
            ApprovalNoteModelData approval, String field)
    {
        Record rec = store.getRecord(model);
        Object val = rec.get(field);
        if (val instanceof List || val == null)
        {
            List<ApprovalNoteModelData> approvals = (List<ApprovalNoteModelData>) val;
            if (approvals == null)
                approvals = new ArrayList<ApprovalNoteModelData>();
            int pos = approvals.indexOf(approval);
            if (pos == -1)
            {
                approvals = new ArrayList<ApprovalNoteModelData>(approvals);
                approvals.add(0, approval);
            }
            else
            {
                ApprovalNoteModelData newApp = new ApprovalNoteModelData();
                Hd3dModelData.copy(approval, newApp);
                approvals = new ArrayList<ApprovalNoteModelData>(approvals);
                approvals.set(pos, newApp);
            }
            Collections.sort(approvals, new ApprovalComparator());
            rec.set(field, approvals);
        }
    }

    /**
     * @param user
     *            The user who makes the approval.
     * @param model
     *            The model concerned by approval.
     * @param approvalTypeId
     *            The approval type.
     * @return A new approval note correctly configured with date set to current date.
     */
    public static ApprovalNoteModelData getNewApprovalNote(PersonModelData user, Hd3dModelData model,
            Long approvalTypeId, Date date)
    {
        if (user != null)
        {
            ApprovalNoteModelData approval = new ApprovalNoteModelData();
            approval.setDate(new Date(date.getTime()));
            approval.setBoundEntityName(model.getSimpleClassName());
            approval.setBoundEntityId(model.getId());
            approval.setComment(new String());
            approval.setApproverId(user.getId());
            approval.setApproverName(user.getName());
            approval.setApprovalNoteType(approvalTypeId);

            return approval;
        }
        else
        {
            return null;
        }
    }

    /**
     * Display approval note editor for given approval and model.
     * 
     * @param store
     *            Store needed to notify that model has changed.
     * @param model
     *            The model on which approval is edited.
     * @param approval
     *            The approval to edit.
     * @param field
     *            The field containing approval.
     * @param fieldName
     *            The human field name.
     */
    public static void editApprovalNote(ListStore<Hd3dModelData> store, final Hd3dModelData model,
            ApprovalNoteModelData approval, String field, String fieldName)
    {
        String title = FieldUtils.getModelName(model);
        if (title == null)
        {
            title = fieldName + " - note - ";
        }
        else
        {
            title = "<span style=\"font-size: 16px\">" + title + "</span> (" + fieldName + ")";
        }
        ApprovalEditDisplayer.show(store, model, title, approval, field);
    }

    /**
     * @param columnId
     *            The column id of which approval note type is requested.
     * @return Return for given type ID, the approval note type corresponding to this column id.
     */
    public static Long getApprovalNoteTypeForColumn(String columnId)
    {
        for (ItemModelData item : ProductionModel.getApprovalColumns())
        {
            if (columnId.equals("id_" + item.getId()))
            {
                if (!Util.isEmptyString(item.getParameter()))
                    return Long.valueOf(item.getParameter());
                else
                    return null;
            }
        }
        return null;
    }

    /**
     * @param columnId
     *            The column id of which task type is requested.
     * @return Return for given type ID, the task type corresponding to this note type.
     */
    public static Long getTaskTypeForColumn(String columnId)
    {
        Long approvalNoteTypeForColumn = getApprovalNoteTypeForColumn(columnId);
        ApprovalNoteTypeModelData approvalNoteType = ProductionModel.approvalStore.findModel(
                ApprovalNoteTypeModelData.ID_FIELD, approvalNoteTypeForColumn);

        if (approvalNoteType == null)
            return null;
        Long taskTypeId = approvalNoteType.getTaskType();
        return taskTypeId;
    }

    /**
     * @param noteTypeId
     *            The note Type of which column name is requested.
     * @return Return for given type ID, the name of the column corresponding to this note type.
     */
    public static String getColumnName(String noteTypeId)
    {
        for (ItemModelData item : ProductionModel.getApprovalColumns())
        {
            if (noteTypeId.equals("id_" + item.getId()))
            {
                return item.getName();
            }
        }
        return "";
    }

    /**
     * Display approval note editor for given models.
     * 
     * @param store
     *            Store needed to notify that model has changed.
     * @param selectedModels
     *            The model data on which approval is edited.
     * @param field
     *            The field containing approval.
     * @param fieldName
     *            The human field name.
     * @param approvalType
     *            The approval note type ID.
     * @param status
     *            Approval status
     */
    @SuppressWarnings("unchecked")
    public static void editApprovalNotes(ServicesPagingStore<Hd3dModelData> store, List<Hd3dModelData> selectedModels,
            String field, String fieldName, Long approvalType, String status, Date date)
    {
        if (!selectedModels.isEmpty())
        {
            String title = fieldName;

            if (selectedModels.size() == 1)
            {
                String workObjectName = FieldUtils.getModelName(selectedModels.get(0));
                if (workObjectName == null)
                {
                    title += " - Approval note - ";
                }
                else
                {
                    title = "<span style=\"font-size: 16px\">" + workObjectName + "</span> (" + title + ")";
                }
                Object obj = selectedModels.get(0).get(field);

                if (!(obj instanceof Boolean))
                {

                    List<ApprovalNoteModelData> model = (List<ApprovalNoteModelData>) obj;
                    if (model != null && !model.isEmpty())
                    {
                        String oldStatus = model.get(0).getStatus();
                        status = oldStatus;
                    }
                    else
                    {
                        status = "";
                    }
                }
            }
            else
            {
                title += " - Multiple edition";

                String oldStatus = "";
                ArrayList<ApprovalNoteModelData> models = selectedModels.get(0).get(field);
                if (models != null && !models.isEmpty())
                {
                    // Get last comment
                    oldStatus = models.get(0).getStatus();
                }

                int i = 1;
                boolean equals = true;
                do
                {
                    models = selectedModels.get(i).get(field);
                    if (models != null && !models.isEmpty())
                    {
                        // Get last comment
                        equals = oldStatus.equals(models.get(0).getStatus());
                    }
                    else
                    {
                        equals = false;
                    }
                    ++i;
                }
                while (i < selectedModels.size() && equals);

                if (equals)
                {
                    status = oldStatus;
                }
                else
                {
                    status = "";
                }
            }

            ApprovalEditDisplayer.show(store, selectedModels, title, field, approvalType, status, date);
        }

    }

    /**
     * Add inside approval model additional information about task type linked to the approval : add task type ID and
     * task type color.
     * 
     * @param approval
     *            The approval to edit.
     */
    public static void setTaskTypeInfo(ApprovalNoteModelData approval)
    {
        ApprovalNoteTypeModelData type = ProductionModel.approvalStore.findModel(ApprovalNoteTypeModelData.ID_FIELD,
                approval.getApprovalNoteType());
        if (type != null)
            approval.setTaskTypeId(type.getTaskType());
        else
            approval.setTaskTypeId(null);
    }
}
