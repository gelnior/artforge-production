package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlowPanel;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.PreWidget;


public class ApprovalNoteList
{
    private DisclosurePanel disclosurePanel;
    private FlowPanel content;
    private ItemModelData itemModelData;
    private FieldModelData status;
    private PreWidget annotationText;
    private Date date;
    private List<GraphicalAnnotationModelData> graphicalAnnotationModelDatas;

    public ApprovalNoteList(ItemModelData itemModelData, FieldModelData status, Date date)
    {
        this.itemModelData = itemModelData;
        this.status = status;
        this.date = date;
        this.graphicalAnnotationModelDatas = new ArrayList<GraphicalAnnotationModelData>();
        this.content = new FlowPanel();
        disclosurePanel = new DisclosurePanel(itemModelData.getName());
        disclosurePanel.add(this.content);
        disclosurePanel.setWidth(100 + Unit.PCT.getType());
        disclosurePanel.setStyleName("");
        updateStatus(status);
    }

    public void addAnnotation(GraphicalAnnotationModelData graphicalAnnotationModelData)
    {
        add(graphicalAnnotationModelData.getComment(), false);
        this.graphicalAnnotationModelDatas.add(graphicalAnnotationModelData);
    }

    private PreWidget add(String text, boolean hasHtml)
    {
        PreWidget annotationText = new PreWidget(text, hasHtml);
        this.content.add(annotationText);
        return annotationText;
    }

    public void updateStatus(FieldModelData fieldModelData)
    {
        this.status = fieldModelData;
        DOM.setStyleAttribute(disclosurePanel.getElement(), "backgroundColor", status.get("color").toString());
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public DisclosurePanel getWidget()
    {
        return disclosurePanel;
    }

    public String getText()
    {
        if (this.annotationText != null)
        {
            return this.annotationText.getText();
        }
        return "";
    }

    public void setText(String text)
    {
        if (this.annotationText == null)
        {
            this.annotationText = add(text, false);
        }
        else
        {
            this.annotationText.setText(text);
        }
    }

    public Date getDate()
    {
        return date;
    }

    public String getStatus()
    {
        return status.getValue().toString();
    }

    public ItemModelData getItemModelData()
    {
        return itemModelData;
    }
}
