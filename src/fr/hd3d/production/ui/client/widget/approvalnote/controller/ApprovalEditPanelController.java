package fr.hd3d.production.ui.client.widget.approvalnote.controller;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.util.HtmlUtils;
import fr.hd3d.production.ui.client.widget.approvalnote.model.ApprovalEditPanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNotePanel;


public class ApprovalEditPanelController
{
    private IApprovalNotePanel view;
    private ApprovalEditPanelModel model;

    public ApprovalEditPanelController(IApprovalNotePanel view, ApprovalEditPanelModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void setApprovalNote(ApprovalNoteModelData approvalNote)
    {
        this.model.setApprovalNote(approvalNote);
        this.view.setDate(approvalNote.getDate());
        this.view.setComment(approvalNote.getComment());
        this.view.setStatus(approvalNote.getStatus());
    }

    public ApprovalNoteModelData getApprovalNote() throws Hd3dException
    {
        ApprovalNoteModelData note = this.model.getApprovalNote();
        String comment = this.view.getComment();
        note.setDate(this.view.getDate());
        note.setStatus(this.view.getStatus());

        if (comment != null)
        {
            comment = HtmlUtils.changeToHyperText(comment);
        }
        else
        {
            comment = "";
        }
        note.setComment(comment);
        return note;
    }
}
