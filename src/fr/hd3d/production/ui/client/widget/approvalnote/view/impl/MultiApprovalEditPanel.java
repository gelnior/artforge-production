package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.RowData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.google.gwt.core.client.GWT;

import fr.hd3d.annotation.client.player.AnnotationPlayer;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.proxytree.SelectProxyWidget;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.widget.approvalnote.controller.MultiAnnotationController;
import fr.hd3d.production.ui.client.widget.approvalnote.model.MultiApprovalEditPanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.IApprovalNotePanel;


public class MultiApprovalEditPanel extends ContentPanel implements IApprovalNotePanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    private MultiApprovalEditPanelModel approvalEditPanelModel;

    /** The task grid, needed to close tasks. */
    private LayoutContainer approvalNoteListContainer;
    private Hd3dModelData modelData;
    private Boolean annotation;
    private ContentPanel leftComponent;
    private ContentPanel rightComponent;
    private SelectProxyWidget selectProxyWidget;
    private AnnotationPlayer proxyPlayer;
    private MultiAnnotationController multiAnnotationController;
    private ApprovalEditPanel approvalCommentPanel;

    public Hd3dModelData getModelData()
    {
        return modelData;
    }

    public void setModelData(Hd3dModelData modelData)
    {
        this.modelData = modelData;
    }

    public MultiApprovalEditPanel(MultiApprovalEditPanelModel model, boolean annotation)
    {
        this.approvalEditPanelModel = model;
        this.annotation = annotation;
        multiAnnotationController = new MultiAnnotationController(this);

        // this.setLayout(new FillLayout(Orientation.HORIZONTAL));
        this.setLayout(new BorderLayout());
        this.setStyles();
        this.createLeftComponent();
        this.createCenterComponent();
        this.createRightComponent();
    }

    private void createCenterComponent()
    {
        if (this.annotation)
        {
            BorderLayoutData borderLayoutData = new BorderLayoutData(LayoutRegion.CENTER);
            borderLayoutData.setMargins(new Margins(0));
            this.leftComponent = new ContentPanel(new FitLayout());
            this.add(this.leftComponent, borderLayoutData);
        }
    }

    private void createRightComponent()
    {
        this.rightComponent = new ContentPanel(new RowLayout(Orientation.VERTICAL));
        BorderLayoutData borderLayoutData = new BorderLayoutData(LayoutRegion.EAST);
        borderLayoutData.setSize(500);
        borderLayoutData.setMargins(new Margins(0));
        configureContentPanel(this.rightComponent);
        this.rightComponent.setFrame(true);
        this.approvalCommentPanel = new ApprovalEditPanel(this.approvalEditPanelModel, true);
        this.rightComponent.add(approvalCommentPanel, new RowData(1, 0.5, new Margins(0, 0, 5, 0)));
        // this.createForm();
        this.createActionButtonBar();
        this.createApprovalNoteList();
        // this.add(this.rightComponent, new FillData(0));
        this.add(this.rightComponent, borderLayoutData);
    }

    private void createLeftComponent()
    {
        if (annotation)
        {
            BorderLayoutData borderLayoutData = new BorderLayoutData(LayoutRegion.WEST);
            borderLayoutData.setSize(250);
            borderLayoutData.setMargins(new Margins(0));
            selectProxyWidget = new SelectProxyWidget();
            this.add(selectProxyWidget, borderLayoutData);
        }
    }

    private void createApprovalNoteList()
    {
        this.approvalNoteListContainer = new LayoutContainer(new RowLayout(Orientation.VERTICAL));
        this.approvalNoteListContainer.setBorders(true);
        this.approvalNoteListContainer.setScrollMode(Scroll.AUTOY);
        this.approvalNoteListContainer.setLayoutOnChange(true);
        this.rightComponent.add(approvalNoteListContainer, new RowData(1, 0.5));
    }

    public void setApprovalTypes(List<ItemModelData> approvalColumns, Long approvalType)
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.setApprovalTypes(approvalColumns, approvalType);
        }
    }

    public MultiApprovalEditPanelModel getApprovalEditPanelModel()
    {
        return approvalEditPanelModel;
    }

    public void addAnnotationToHistoryList()
    {
        if (validApprovalNote())
        {
            FieldModelData status = approvalCommentPanel.getStatusField().getValue();
            if (status.get("color") != null)
            {
                ApprovalNoteList approvalNoteList = approvalEditPanelModel
                        .getApprovalNoteListByApprovalNoteTypeId(approvalCommentPanel.getApprovalNoteType()
                                .getParameter());
                if (approvalNoteList == null)
                {
                    approvalNoteList = new ApprovalNoteList(approvalCommentPanel.getApprovalNoteType(), status,
                            getDate());
                    approvalNoteListContainer.add(approvalNoteList.getWidget(), new RowData(1, -1, new Margins(3, 20,
                            3, 3)));
                    approvalEditPanelModel.addApprovalNoteListForApprovalNoteType(approvalNoteList,
                            approvalCommentPanel.getApprovalNoteType());
                }
                else
                {
                    approvalNoteList.updateStatus(status);
                    approvalNoteList.setDate(getDate());
                }
                approvalNoteList.setText(approvalCommentPanel.getComment());
                clearAnnotationElement();
            }
        }
    }

    public void addAnnotationToHistoryList(GraphicalAnnotationModelData annotationModelData, FieldModelData status,
            ItemModelData approvalNoteType)
    {
        if (status != null && approvalNoteType != null)
        {
            ApprovalNoteList approvalNoteList = approvalEditPanelModel
                    .getApprovalNoteListByApprovalNoteTypeId(approvalNoteType.getParameter());
            if (approvalNoteList == null)
            {
                approvalNoteList = new ApprovalNoteList(approvalNoteType, status, getDate());
                approvalEditPanelModel.addApprovalNoteListForApprovalNoteType(approvalNoteList, approvalCommentPanel
                        .getApprovalNoteType());
            }
            else
            {
                approvalNoteList.updateStatus(status);
                approvalNoteList.setDate(getDate());
            }
            approvalNoteListContainer.add(approvalNoteList.getWidget(), new RowData(1, -1, new Margins(3, 20, 3, 3)));
            approvalEditPanelModel.getGraphicalNoteList().add(annotationModelData);
            approvalEditPanelModel.addGraphicalApprovalNoteListForApprovalNoteType(approvalNoteList, approvalNoteType);
            approvalNoteList.addAnnotation(annotationModelData);
        }
    }

    private boolean validApprovalNote()
    {
        if (approvalCommentPanel != null)
        {
            return approvalCommentPanel.getStatusField().isValid();
        }
        return false;
    }

    private void clearAnnotationElement()
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.getStatusField().clear();
        }
    }

    private void createActionButtonBar()
    {
        HBoxLayout buttonBarLayout = new HBoxLayout();
        buttonBarLayout.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        LayoutContainer buttonBar = new LayoutContainer(buttonBarLayout);
        Button addButton = new Button("Save for sending ...");
        buttonBar.add(addButton);
        addButton.addSelectionListener(new SelectionListener<ButtonEvent>() {

            @Override
            public void componentSelected(ButtonEvent ce)
            {
                if (approvalCommentPanel != null)
                {
                    addAnnotationToHistoryList();
                }
            }
        });
        this.rightComponent.add(buttonBar, new RowData(1, -1, new Margins(0, 0, 5, 0)));
    }

    public String getStatus() throws Hd3dException
    {
        if (approvalCommentPanel != null)
        {
            return approvalCommentPanel.getStatus();
        }
        return null;
    }

    public void setStatus(String status)
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.setStatus(status);
        }
    }

    public String getComment()
    {
        if (approvalCommentPanel != null)
        {
            return approvalCommentPanel.getComment();
        }
        return null;
    }

    public void setComment(String comment)
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.setComment(comment);
        }
    }

    public Date getDate()
    {
        if (approvalCommentPanel != null)
        {
            return approvalCommentPanel.getDate();
        }
        return null;
    }

    public void setDate(Date date)
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.setDate(date);
        }
    }

    public void focusComment()
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.focusComment();
        }
    }

    public void enableTaskGrid()
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.enableTaskGrid();
        }
    }

    public void disableTaskGrid()
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.disableTaskGrid();
        }
    }

    public void disableDate()
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.disableDate();
        }
    }

    public void enableDate()
    {
        if (approvalCommentPanel != null)
        {
            approvalCommentPanel.enableDate();
        }
    }

    private void configureContentPanel(ContentPanel contentPanel)
    {
        contentPanel.setHeaderVisible(false);
        contentPanel.setBodyBorder(false);
        contentPanel.setBorders(false);
        // contentPanel.setFrame(true);
    }

    /** Set panel styles. */
    private void setStyles()
    {
        configureContentPanel(this);
        this.setWidth(800);
        this.setHeight(450);
    }

    public void showProxy(ProxyModelData proxy)
    {
        this.leftComponent.removeAll();
        this.approvalEditPanelModel.setSelectedProxy(proxy);
        if (proxyPlayer != null)
        {
            proxyPlayer.removeControllers();
        }
        proxyPlayer = new AnnotationPlayer(proxy);
        this.leftComponent.add(proxyPlayer);
        this.leftComponent.layout(true);
    }

    public void removeControllers()
    {
        if (this.proxyPlayer != null)
        {
            this.proxyPlayer.removeControllers();
        }
        if (this.multiAnnotationController != null)
        {
            EventDispatcher.get().removeController(this.multiAnnotationController);
        }
    }

    public void setDatas(Long taskTypeId, Long workObjectId, String boundEntityName)
    {
        this.selectProxyWidget.setData(taskTypeId, workObjectId, boundEntityName);
    }

    public void setApprovalNoteProxy(ProxyModelData proxyModelData)
    {
        if (proxyModelData != null)
        {
            showProxy(proxyModelData);
        }
    }

    public AnnotationPlayer getProxyPlayer()
    {
        return proxyPlayer;
    }
}
