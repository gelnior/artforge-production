package fr.hd3d.production.ui.client.widget.approvalnote.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.event.ProductionEvents;


public class DeleteButtonRenderer extends ButtonRenderer<ApprovalNoteModelData>
{
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    public DeleteButtonRenderer()
    {
        super(ExplorateurCSS.NOTE_DELETE, CONSTANTS.DeleteNote(), ProductionEvents.DELETE_APPROVAL_NOTE_CLICKED);
    }

    public Object render(final ApprovalNoteModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ApprovalNoteModelData> store, Grid<ApprovalNoteModelData> grid)
    {
        if (model.getUserCanDelete())
        {
            return super.render(model, property, config, rowIndex, colIndex, store, grid);
        }
        else
        {
            return "";
        }
    }

}
