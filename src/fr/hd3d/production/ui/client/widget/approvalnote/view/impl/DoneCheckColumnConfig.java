package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


/**
 * 
 * 
 * @author HD3D
 */
public class DoneCheckColumnConfig extends CheckColumnConfig
{
    public DoneCheckColumnConfig(String id, String name, int width)
    {
        super(id, name, width);
    }

    protected void onMouseDown(GridEvent<ModelData> ge)
    {
        ModelData m = ge.getModel();
        String status = m.get(ApprovalNoteModelData.STATUS_FIELD);
        if (status.equals(ApprovalNoteModelData.STATUS_NOK) || status.equals(ApprovalNoteModelData.STATUS_DONE))
        {
            boolean b = (Boolean) m.get(getDataIndex());
            if (!b == Boolean.TRUE)
            {
                m.set(ApprovalNoteModelData.STATUS_FIELD, ApprovalNoteModelData.STATUS_DONE);
            }
            else
            {
                m.set(ApprovalNoteModelData.STATUS_FIELD, ApprovalNoteModelData.STATUS_NOK);
            }

            super.onMouseDown(ge);
        }
    }
}
