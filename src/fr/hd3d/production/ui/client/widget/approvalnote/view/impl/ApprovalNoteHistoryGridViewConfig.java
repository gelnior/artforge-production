package fr.hd3d.production.ui.client.widget.approvalnote.view.impl;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.GridViewConfig;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.production.ui.client.config.ProductionConfig;


/**
 * Automatically set Right CSS styles for approval history rows.
 * 
 * @author HD3D
 */
public class ApprovalNoteHistoryGridViewConfig extends GridViewConfig
{
    @Override
    public String getRowStyle(ModelData model, int rowIndex, ListStore<ModelData> ds)
    {
        String colorCss = "";
        if (model instanceof ApprovalNoteModelData)
        {
            ApprovalNoteModelData note = (ApprovalNoteModelData) model;
            if (note.getStatus().equals(ApprovalNoteModelData.STATUS_OK))
            {
                colorCss = ProductionConfig.CSS_APPROVAL_OK;
            }
            else if (note.getStatus().equals(ApprovalNoteModelData.STATUS_DONE))
            {
                colorCss = ProductionConfig.CSS_APPROVAL_DONE;
            }
            else
            {
                colorCss = ProductionConfig.CSS_APPROVAL_NOK;
            }
        }
        String style = super.getRowStyle(model, rowIndex, ds) + colorCss;
        return style;
    }
}
