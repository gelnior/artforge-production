package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;


/**
 * Singleton for approval editor displaying. It handles a list of approval edit dialog to not have infinite dialog
 * creation with allowing to display multiple approval edit dialog at the same time.
 * 
 * @author HD3D
 */
public class ApprovalEditDisplayer
{
    private static final int NUMBER_DIALOG_MAX = 5;

    /** Approval editor dialog list. */
    private static List<ApprovalEditDialog> editDialog = new ArrayList<ApprovalEditDialog>();
    /** Counter to know which dialog to display. */
    private static int dialogCounter = 0;

    /**
     * It creates a new dialog until NUMBER_DIALOG_MAX is reached, then it reuses created dialogs.
     * 
     * @return Approval editor dialog.
     */
    public static ApprovalEditDialog getDialog()
    {
        if (dialogCounter == editDialog.size())
            editDialog.add(new ApprovalEditDialog());

        ApprovalEditDialog dialog = editDialog.get(dialogCounter);

        dialogCounter++;
        dialogCounter = dialogCounter % NUMBER_DIALOG_MAX;

        return dialog;
    }

    /**
     * Show approval editor dialog.
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param model
     *            The model on which approval is edited.
     * @param title
     *            The title of the dialog.
     * @param approval
     *            The edited approval.
     * @param field
     *            The field that contains approval list from where <i>approval</i> comes.
     */
    public static void show(ListStore<Hd3dModelData> store, Hd3dModelData model, String title,
            ApprovalNoteModelData approval, String field)
    {
        getDialog().show(store, model, title, approval, field);
        getDialog().disableDate();
    }

    /**
     * Display approval edit dialog for creation (multi-selection allowed).
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param selectedModels
     *            The models to modify.
     * @param title
     *            The title to set on this dialog.
     * @param approvalNote
     *            The approval note to edit.
     * @param field
     *            The field that contains approval list from where <i>approval</i> comes.
     * @param approvalType
     *            Type of the approval to update.
     * @param status
     *            The status of the approval to create.
     * @param date
     *            The date for the approval to create.
     */
    public static void show(ServicesPagingStore<Hd3dModelData> store, List<Hd3dModelData> selectedModels, String title,
            String field, Long approvalType, String status, Date date)
    {
        ApprovalEditDialog dialog = getDialog();
        dialog.show(store, selectedModels, title, field, approvalType, status, date);
        dialog.enableDate();
    }

    public static void showForHistory(ListStore<ApprovalNoteModelData> store, Hd3dModelData record, String title,
            ApprovalNoteModelData approval, String field)
    {
        ApprovalEditDialog dialog = getDialog();
        dialog.showForHistory(store, record, title, approval, field);
        dialog.disableDate();
    }
}
