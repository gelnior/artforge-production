package fr.hd3d.production.ui.client.widget.approvalnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.widget.approvalnote.controller.ApprovalEditPanelController;
import fr.hd3d.production.ui.client.widget.approvalnote.model.MultiApprovalEditPanelModel;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.ApprovalNoteList;
import fr.hd3d.production.ui.client.widget.approvalnote.view.impl.MultiApprovalEditPanel;


/**
 * Approval edit dialog is a small dialog box allowing to edit or create approval notes on multiple.
 * 
 * @author HD3D
 */
public class MultiApprovalEditDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    /** The model that handles approval edit dialog data. */
    private MultiApprovalEditPanelModel model = new MultiApprovalEditPanelModel();
    /** The view that displays dialog widgets (panel set inside dialog). */
    private MultiApprovalEditPanel view;
    /** Controller that handles dialog events. */
    private ApprovalEditPanelController controller;

    private final boolean annotation;

    /**
     * Constructor. Build widgets, set style and behavior.
     */
    public MultiApprovalEditDialog(boolean annotation)
    {
        this.annotation = annotation;
        this.view = new MultiApprovalEditPanel(model, annotation);
        this.controller = new ApprovalEditPanelController(view, model);
        this.setStyles();
        this.setLayout(new FitLayout());
        this.add(view);
        if (annotation)
        {
            this.setPixelSize(Window.getClientWidth() - 100, Window.getClientHeight() - 50);
        }
        else
        {
            this.setPixelSize(512, 600);
        }
    }

    /**
     * Display approval edit dialog for creation (multi-selection allowed).
     * 
     * @param store
     *            The store that should be notify when approval is changed.
     * @param selectedModels
     *            The models to modify.
     * @param title
     *            The title to set on this dialog.
     * @param approvalNote
     *            The approval note to edit.
     * @param field
     *            The field that contains approval list from where <i>approval</i> comes.
     * @param approvalType
     *            Type of the approval to update.
     * @param status
     *            The status of the approval to create.
     * @param date
     *            The date for the approval to create.
     * @param approvalColumns
     * @param fileRevisionId
     */
    public void show(ServicesPagingStore<Hd3dModelData> store, List<Hd3dModelData> selectedModels, String title,
            String field, Long approvalType, String status, Date date, List<ItemModelData> approvalColumns,
            Long fileRevisionId)
    {
        this.setModal(false);
        this.model.setRecordStore(store);
        this.model.setField(field);
        this.model.setRecords(selectedModels);
        this.model.setApprovalColumns(approvalColumns);
        this.model.setApprovalType(approvalType);
        this.model.setFileRevisionId(fileRevisionId);
        Hd3dModelData firstModel = (Hd3dModelData) CollectionUtils.getFirst(selectedModels);
        this.view.setModelData(firstModel);

        ApprovalNoteModelData approval = ApprovalUtils.getNewApprovalNote(MainModel.currentUser, firstModel,
                approvalType, date);
        approval.setStatus(status);
        approval.setComment("");

        this.controller.setApprovalNote(approval);

        this.setHeading(title);
        this.setFocusWidget(this.view);

        if (selectedModels.size() == 1)
        {
            this.model.reloadTaskStore();
            this.view.enableTaskGrid();
        }
        else
        {
            this.view.disableTaskGrid();
        }
        this.view.focusComment();
        this.view.setApprovalTypes(approvalColumns, approvalType);
        Long taskTypeId = ApprovalUtils.getTaskTypeForColumn(field);
        if (this.annotation)
        {
            this.view.setDatas(taskTypeId, firstModel.getId(), firstModel.getSimpleClassName());
        }
        super.show();
    }

    @Override
    public void hide(Button buttonPressed)
    {
        if (buttonPressed != null)
        {
            this.view.removeControllers();
        }
        super.hide(buttonPressed);
    }

    /**
     * Workaround to display edit dialog for a list of approval note instead that for a list of model having approvals
     * inside one of their field.
     * 
     * @param store
     *            Store containing modified model.
     * @param model
     *            The model to modify.
     * @param title
     *            The title to set on this dialog.
     * @param approvalNote
     *            The approval note to edit.
     * @param field
     *            The field containing the approval.
     */
    public void showForHistory(ListStore<ApprovalNoteModelData> store, Hd3dModelData model, String title,
            ApprovalNoteModelData approvalNote, String field)
    {
        this.model.setApprovalStore(store);
        this.model.setField(field);
        this.model.setRecord(model);
        this.controller.setApprovalNote(approvalNote);
        this.setHeading(title);
        this.model.reloadTaskStore();

        super.show();

        this.view.focus();
        this.view.focusComment();
    }

    /**
     * When OK button is clicked, modification are transfered to the explorer and dialog is hidden.
     */
    @Override
    protected void onButtonPressed(Button button)
    {
        try
        {
            if (button.getItemId().equals(OK))
            {
                this.onOkClick();
            }
            super.onButtonPressed(button);
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    /**
     * Update registered record and store with modified data or newly created approval note.
     * 
     * @throws Hd3dException
     */
    private void onOkClick() throws Hd3dException
    {
        if (model.getApprovalNoteLists() != null)
        {
            saveGraphicalAnnotations();
            List<ApprovalNoteModelData> approvalNoteModelDatas = new ArrayList<ApprovalNoteModelData>();
            for (ApprovalNoteList noteList : model.getApprovalNoteLists())
            {
                ApprovalNoteModelData approvalNote = ApprovalUtils
                        .getNewApprovalNote(MainModel.currentUser, this.view.getModelData(), Long.parseLong(noteList
                                .getItemModelData().getParameter()), noteList.getDate());
                approvalNote.setStatus(noteList.getStatus());
                approvalNote.setComment(noteList.getText());
                if (model.getFileRevisionId() != null)
                {
                    ArrayList<Long> ids = new ArrayList<Long>();
                    ids.add(model.getFileRevisionId());
                    approvalNote.setFileRevision(ids);
                }
                ApprovalNoteTypeModelData findModel = ProductionModel.approvalStore.findModel(
                        ApprovalNoteTypeModelData.ID_FIELD, Long.parseLong(noteList.getItemModelData().getParameter()));
                if (noteList.getItemModelData() != null)
                {
                    approvalNote.setTaskTypeId(findModel.getTaskType());
                }
                else
                {
                    approvalNote.setTaskTypeId(null);
                }
                approvalNoteModelDatas.add(approvalNote);
                ApprovalUtils.addOrUpdateApprovalForModel(this.model.getRecordStore(), this.view.getModelData(),
                        approvalNote, "id_" + noteList.getItemModelData().getId());
            }
            if (ExplorerController.autoSave)
            {
                EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
            }
        }
    }

    private void saveGraphicalAnnotations()
    {
        model.saveGraphicalAnnotations();
    }

    /**
     * Disable date fields (used to change approval date).
     */
    public void disableDate()
    {
        this.view.disableDate();
    }

    /**
     * Enable date fields (used to change approval date).
     */
    public void enableDate()
    {
        this.view.enableDate();
    }

    /**
     * Set dialog style and behavior.
     */
    private void setStyles()
    {
        this.setModal(false);
        this.setButtons(OKCANCEL);
        this.setBodyBorder(false);
        this.setBorders(false);
        this.setHeading(CONSTANTS.ApprovalNote());
        this.setHideOnButtonClick(true);
        this.setClosable(false);
        this.setOnEsc(true);
    }

}
