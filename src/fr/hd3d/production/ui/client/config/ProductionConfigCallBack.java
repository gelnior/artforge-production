package fr.hd3d.production.ui.client.config;

import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.InitServicesConfigurationCallback;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.model.ProductionModel;


/**
 * Production configuration callback parses configuration file to retrieve service path and if playlist should be
 * enabled or not.
 * 
 * @author HD3D
 */
public class ProductionConfigCallBack extends InitServicesConfigurationCallback
{
    private static final String ENABLE_PLAYLIST_UI_NODE = "enablePlaylistUi";

    private static final String ENABLE_STAT_UI_NODE = "enableStatUi";

    private ProductionModel productionModel;

    /**
     * Constructor
     * 
     * @param model
     *            Production model handles production maine data.
     */
    public ProductionConfigCallBack(ProductionModel model)
    {
        super(model);

        this.productionModel = model;
    }

    /**
     * When parsing of common data succeeds, it parses production specific data from config.xml file.
     */
    @Override
    protected void onParsingSucceed(Document configDocument) throws Exception
    {
        super.onParsingSucceed(configDocument);

        this.parseEnablePlaylist(configDocument);
        this.parseEnableStat(configDocument);
    }

    /**
     * Parse configuration document to retrieve enable playlist boolean and saving it to the model. It looks for
     * following tag : <enable-playlist>false|true</enable-playlist>. If no tag is retrieved false is set by default.
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseEnablePlaylist(Document configDocument)
    {
        Node urlNode = configDocument.getElementsByTagName(ENABLE_PLAYLIST_UI_NODE).item(0);
        if (urlNode == null)
        {
            ProductionModel.setEnablePlaylistUi(false);
        }
        else
        {
            String enablePlaylistString = urlNode.getChildNodes().item(0).getNodeValue();
            Boolean enable = Boolean.parseBoolean(enablePlaylistString);
            ProductionModel.setEnablePlaylistUi(enable);
            EventDispatcher.forwardEvent(ProductionEvents.PRODUCTION_CONFIG_INITIALIZED);
        }
    }

    /**
     * Parse configuration document to retrieve enable playlist boolean and saving it to the model. It looks for
     * following tag : \<enableStatUi\>false|true\</enableStatUi\>. If no tag is retrieved false is set by default.
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseEnableStat(Document configDocument)
    {
        Node urlNode = configDocument.getElementsByTagName(ENABLE_STAT_UI_NODE).item(0);
        if (urlNode == null)
        {
            this.productionModel.setEnableStatUi(Boolean.TRUE);
        }
        else
        {
            String enableStatString = urlNode.getChildNodes().item(0).getNodeValue();
            Boolean enable = Boolean.parseBoolean(enableStatString);
            this.productionModel.setEnableStatUi(enable);
            EventDispatcher.forwardEvent(ProductionEvents.PRODUCTION_CONFIG_INITIALIZED);
        }
    }
}
