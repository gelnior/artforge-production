package fr.hd3d.production.ui.client.config;

/**
 * Application constants.
 */
public class ProductionConfig
{
    public static final String CSS_APPROVAL_OK = " approvalCell-Ok";
    public static final String CSS_APPROVAL_DONE = " approvalCell-done";
    public static final String CSS_APPROVAL_NOK = " approvalCell-NOk";
    public static final String CSS_APPROVAL = "approvalCell";

    public static final String APPROVAL_FIELD_EVENT_VAR = "field";
    public static final String APPROVAL_TYPE_EVENT_VAR = "approvalType";
    public static final String WITH_COMMENT_EVENT_VAR = "withComment";
    public static final String APPROVAL_NAME_EVENT_VAR = "columnName";
    public static final String SHEET_COOKIE_VAR_PREFIX = "production-sheet-";
    public static final String TAB_HISTORY_VAR = "tab";
    public static final String CATEGORY_HISTORY_VAR = "category";
    public static final String APPROVAL_STATUS_EVENT_VAR = "approvalStatus";
    public static final String APPROVAL_DESTINATION_EVENT_VAR = "approvalDestination";
    public static final String WITH_STATUS_EVENT_VAR = "withStatus";
    public static final String MULTI = "multi";
    public static final String ANNOTATION = "annotation";
    public static final String FILE_REVISION = "fileRevisionId";

    public static final String TAB_COOKIE_VAR = "production-tab";
}
