package fr.hd3d.production.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/guillaume-maucomble/workspaces/workspace_hd3d/suiviDeProduction/src/fr/hd3d/production/ui/client/constant/ProductionConstants.properties'.
 */
public interface ProductionConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "Approval Editor".
   * 
   * @return translated "Approval Editor"
   */
  @DefaultStringValue("Approval Editor")
  @Key("ApprovalEditor")
  String ApprovalEditor();

  /**
   * Translated "A or more approval notes have no tasks.".
   * 
   * @return translated "A or more approval notes have no tasks."
   */
  @DefaultStringValue("A or more approval notes have no tasks.")
  @Key("ApprovalHaveNoTasks")
  String ApprovalHaveNoTasks();

  /**
   * Translated "Approval History".
   * 
   * @return translated "Approval History"
   */
  @DefaultStringValue("Approval History")
  @Key("ApprovalHistory")
  String ApprovalHistory();

  /**
   * Translated "Approval Note".
   * 
   * @return translated "Approval Note"
   */
  @DefaultStringValue("Approval Note")
  @Key("ApprovalNote")
  String ApprovalNote();

  /**
   * Translated "Approval note history".
   * 
   * @return translated "Approval note history"
   */
  @DefaultStringValue("Approval note history")
  @Key("ApprovalNoteHistory")
  String ApprovalNoteHistory();

  /**
   * Translated "Bad Use".
   * 
   * @return translated "Bad Use"
   */
  @DefaultStringValue("Bad Use")
  @Key("BadUse")
  String BadUse();

  /**
   * Translated "Current selection is empty".
   * 
   * @return translated "Current selection is empty"
   */
  @DefaultStringValue("Current selection is empty")
  @Key("CurrentSelectionIsEmpty")
  String CurrentSelectionIsEmpty();

  /**
   * Translated "Delete Note".
   * 
   * @return translated "Delete Note"
   */
  @DefaultStringValue("Delete Note")
  @Key("DeleteNote")
  String DeleteNote();

  /**
   * Translated "Edit".
   * 
   * @return translated "Edit"
   */
  @DefaultStringValue("Edit")
  @Key("Edit")
  String Edit();

  /**
   * Translated "Edit last note".
   * 
   * @return translated "Edit last note"
   */
  @DefaultStringValue("Edit last note")
  @Key("EditLastNote")
  String EditLastNote();

  /**
   * Translated "Edit note".
   * 
   * @return translated "Edit note"
   */
  @DefaultStringValue("Edit note")
  @Key("EditNote")
  String EditNote();

  /**
   * Translated "Empty Selection".
   * 
   * @return translated "Empty Selection"
   */
  @DefaultStringValue("Empty Selection")
  @Key("EmptySelection")
  String EmptySelection();

  /**
   * Translated "frame number cannot be negative".
   * 
   * @return translated "frame number cannot be negative"
   */
  @DefaultStringValue("frame number cannot be negative")
  @Key("FrameNumberCannotBeNegative")
  String FrameNumberCannotBeNegative();

  /**
   * Translated "Framerate cannot be set to ".
   * 
   * @return translated "Framerate cannot be set to "
   */
  @DefaultStringValue("Framerate cannot be set to ")
  @Key("FramerateCannotBeSetTo")
  String FramerateCannotBeSetTo();

  /**
   * Translated "Framerate has been set to ".
   * 
   * @return translated "Framerate has been set to "
   */
  @DefaultStringValue("Framerate has been set to ")
  @Key("FramerateHasBeenSetTo")
  String FramerateHasBeenSetTo();

  /**
   * Translated "Framerate set".
   * 
   * @return translated "Framerate set"
   */
  @DefaultStringValue("Framerate set")
  @Key("FramerateSet")
  String FramerateSet();

  /**
   * Translated "is not a valid frame number".
   * 
   * @return translated "is not a valid frame number"
   */
  @DefaultStringValue("is not a valid frame number")
  @Key("IsNotValidFrameNumber")
  String IsNotValidFrameNumber();

  /**
   * Translated "Move down".
   * 
   * @return translated "Move down"
   */
  @DefaultStringValue("Move down")
  @Key("MoveDown")
  String MoveDown();

  /**
   * Translated "Move up".
   * 
   * @return translated "Move up"
   */
  @DefaultStringValue("Move up")
  @Key("MoveUp")
  String MoveUp();

  /**
   * Translated "New revision".
   * 
   * @return translated "New revision"
   */
  @DefaultStringValue("New revision")
  @Key("NewRevision")
  String NewRevision();

  /**
   * Translated "No Information".
   * 
   * @return translated "No Information"
   */
  @DefaultStringValue("No Information")
  @Key("NoInformation")
  String NoInformation();

  /**
   * Translated "Note".
   * 
   * @return translated "Note"
   */
  @DefaultStringValue("Note")
  @Key("Note")
  String Note();

  /**
   * Translated "The playlist has been saved".
   * 
   * @return translated "The playlist has been saved"
   */
  @DefaultStringValue("The playlist has been saved")
  @Key("PlaylistHasBeenSavedInfo")
  String PlaylistHasBeenSavedInfo();

  /**
   * Translated "No playlist is currently loaded".
   * 
   * @return translated "No playlist is currently loaded"
   */
  @DefaultStringValue("No playlist is currently loaded")
  @Key("PlaylistNullInfo")
  String PlaylistNullInfo();

  /**
   * Translated "Playlist saved".
   * 
   * @return translated "Playlist saved"
   */
  @DefaultStringValue("Playlist saved")
  @Key("PlaylistSaved")
  String PlaylistSaved();

  /**
   * Translated "Production Tool".
   * 
   * @return translated "Production Tool"
   */
  @DefaultStringValue("Production Tool")
  @Key("ProductionTool")
  String ProductionTool();

  /**
   * Translated "Retake".
   * 
   * @return translated "Retake"
   */
  @DefaultStringValue("Retake")
  @Key("Retake")
  String Retake();

  /**
   * Translated "Revision".
   * 
   * @return translated "Revision"
   */
  @DefaultStringValue("Revision")
  @Key("Revision")
  String Revision();

  /**
   * Translated "Save".
   * 
   * @return translated "Save"
   */
  @DefaultStringValue("Save")
  @Key("Save")
  String Save();

  /**
   * Translated "Save as new version".
   * 
   * @return translated "Save as new version"
   */
  @DefaultStringValue("Save as new version")
  @Key("SaveAsNewVersion")
  String SaveAsNewVersion();

  /**
   * Translated "Save failure".
   * 
   * @return translated "Save failure"
   */
  @DefaultStringValue("Save failure")
  @Key("SaveFailure")
  String SaveFailure();

  /**
   * Translated "Selection needs to be contiguous".
   * 
   * @return translated "Selection needs to be contiguous"
   */
  @DefaultStringValue("Selection needs to be contiguous")
  @Key("SelectionNeedsToBeContiguous")
  String SelectionNeedsToBeContiguous();

  /**
   * Translated "Show history".
   * 
   * @return translated "Show history"
   */
  @DefaultStringValue("Show history")
  @Key("ShowHistory")
  String ShowHistory();

  /**
   * Translated "Slip".
   * 
   * @return translated "Slip"
   */
  @DefaultStringValue("Slip")
  @Key("Slip")
  String Slip();

  /**
   * Translated "Please select a tool (slip/trim) before editing".
   * 
   * @return translated "Please select a tool (slip/trim) before editing"
   */
  @DefaultStringValue("Please select a tool (slip/trim) before editing")
  @Key("ToolSelectionRequired")
  String ToolSelectionRequired();

  /**
   * Translated "Trim".
   * 
   * @return translated "Trim"
   */
  @DefaultStringValue("Trim")
  @Key("Trim")
  String Trim();

  /**
   * Translated "Validate".
   * 
   * @return translated "Validate"
   */
  @DefaultStringValue("Validate")
  @Key("Validate")
  String Validate();

  /**
   * Translated "Validate with a comment".
   * 
   * @return translated "Validate with a comment"
   */
  @DefaultStringValue("Validate with a comment")
  @Key("ValidateComment")
  String ValidateComment();

  /**
   * Translated "A value is required".
   * 
   * @return translated "A value is required"
   */
  @DefaultStringValue("A value is required")
  @Key("ValueRequired")
  String ValueRequired();

  /**
   * Translated "Waiting for approval".
   * 
   * @return translated "Waiting for approval"
   */
  @DefaultStringValue("Waiting for approval")
  @Key("WaitingForApproval")
  String WaitingForApproval();
}
