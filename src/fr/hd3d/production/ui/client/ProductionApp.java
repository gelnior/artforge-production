package fr.hd3d.production.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.production.ui.client.view.ProductionView;


/**
 * Production Tool entry point.
 * 
 * @author HD3D
 */
public class ProductionApp implements EntryPoint
{
    /**
     * This is the entry point method. It sets up the viewport and start application configuration
     */
    public void onModuleLoad()
    {
        ProductionView mainView = new ProductionView();
        mainView.init();

        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
