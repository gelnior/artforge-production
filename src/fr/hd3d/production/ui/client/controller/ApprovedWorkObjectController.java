package fr.hd3d.production.ui.client.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerColumnConfig;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.Hd3dCheckColumnConfig;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.workobject.browser.controller.WorkObjectController;
import fr.hd3d.common.ui.client.widget.workobject.browser.model.WorkObjectModel;
import fr.hd3d.production.ui.client.config.ProductionConfig;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.view.ApprovedWorkObjectView;
import fr.hd3d.production.ui.client.view.explorer.renderer.ApprovalNoteRenderer;
import fr.hd3d.production.ui.client.widget.approvalnote.ApprovalHistoryDisplayer;
import fr.hd3d.production.ui.client.widget.approvalnote.ApprovalUtils;
import fr.hd3d.production.ui.client.widget.approvalnote.MultiApprovalEditDialog;


/**
 * Controller that handles approvable work object view.
 * 
 * @author HD3D
 */
public class ApprovedWorkObjectController extends WorkObjectController
{
    private ApprovedWorkObjectView view;

    public ApprovedWorkObjectController(ApprovedWorkObjectView view, WorkObjectModel model)
    {
        super(view, model);
        this.view = view;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == ProductionEvents.APPROVAL_EDIT)
        {
            this.onEditApprovalNote(event);
        }
        else if (type == ProductionEvents.APPROVAL_HISTORY)
        {
            this.onShowApprovalHistory(event);
        }
        else if (type == ProductionEvents.APPROVAL_ADD_CLICKED)
        {
            this.onApprovalAddOk(event);
        }
        else if (type == ProductionEvents.APPROVAL_PUSH_CLICKED)
        {
            this.onNotePushClicked(event);
        }
        else if (type == ProductionEvents.SHOW_PROXY_CLICKED)
        {
            this.onShowProxyClicked(event);
        }
        else if (type == ProductionEvents.SEE_FILES_CLICKED)
        {
            this.onSeeFilesClicked(event);
        }
        // else if (type == ProductionEvents.PROXY_MODE_CLICKED)
        // {
        // this.onProxyModeClicked(event);
        // }
        else
        {
            super.handleEvent(event);
        }
    }

    // private void onProxyModeClicked(AppEvent event)
    // {
    // if (this.view.getDisplayMode() == DisplayMode.PROXY_MODE)
    // {
    // this.view.setDisplayMode(DisplayMode.NORMAL);
    // }
    // else
    // {
    // this.view.setDisplayMode(DisplayMode.PROXY_MODE);
    // }
    //
    // this.view.getExplorer().reconfigureGrid();
    // }

    private void onSeeFilesClicked(AppEvent event)
    {
        Long fileRevisionId = event.getData(ProductionConfig.FILE_REVISION);
        if (fileRevisionId == null)
        {
            return;
        }
        String fieldName = event.getData(ProductionConfig.APPROVAL_NAME_EVENT_VAR);
        String workObjectName = FieldUtils.getModelName(this.view.getFirstSelected());
        ArrayList<Long> fileRevisionIds = new ArrayList<Long>();
        fileRevisionIds.add(fileRevisionId);
        this.view.showFileRevisionDialog(fileRevisionIds, workObjectName, fieldName);

    }

    private void onShowProxyClicked(AppEvent event)
    {
        List<Hd3dModelData> models = this.view.getSelection();
        String boundEntityName = this.model.getEntityName();
        this.view.displayProxies(models, boundEntityName);
    }

    /**
     * When enter key is pressed upon explorer, if selected cell is boolean its value is inversed. If the cell is an
     * approval cell, the approval note editor opens in OK mode.
     * 
     * @param event
     *            Enter key pressed event.
     */
    @Override
    protected void onEnterKeyPressed(AppEvent event)
    {
        Integer colIndex = event.getData();

        ColumnConfig column = this.view.getColumn(colIndex);
        Object renderer = column.getRenderer();

        if (column instanceof Hd3dCheckColumnConfig)
        {
            for (Hd3dModelData modelSelected : this.view.getSelection())
            {
                Boolean bool = modelSelected.get(column.getDataIndex());
                Record record = model.getExplorerStore().getRecord(modelSelected);

                record.set(column.getDataIndex(), !bool);
            }
        }
        else if (renderer instanceof ApprovalNoteRenderer)
        {
            AppEvent appEvent = new AppEvent(ProductionEvents.APPROVAL_ADD_OK);
            appEvent.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, column.getDataIndex());
            appEvent.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, column.getHeader());
            appEvent.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, ((ExplorerColumnConfig) column).getParameter());
            appEvent.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, true);

            EventDispatcher.forwardEvent(appEvent);
        }
    }

    /**
     * When show approval history is raised, if selection is not empty, approval history panel is displayed for first
     * selected model and given (event parameter) validation.
     * 
     * @param event
     *            The show approval history event.
     */
    private void onShowApprovalHistory(AppEvent event)
    {
        String field = event.getData();

        Hd3dModelData record = this.view.getFirstSelected();
        ApprovalHistoryDisplayer.show(this.model.getExplorerStore(), record, field);
    }

    /**
     * When edit approval note is raised, it retrieve last approval note from first selected model then show edit
     * approval dialog.
     * 
     * @param event
     *            The event edit approval note event.
     */
    private void onEditApprovalNote(AppEvent event)
    {
        String field = event.getData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR);
        String fieldName = event.getData(ProductionConfig.APPROVAL_NAME_EVENT_VAR);
        Hd3dModelData model = this.view.getFirstSelected();
        ApprovalNoteModelData approval = ApprovalUtils.getLastApprovalNote(model, field);

        ApprovalUtils.editApprovalNote(this.model.getExplorerStore(), model, approval, field, fieldName);
    }

    /**
     * When a OK approval is asked for adding, a new approval with status at OK is added for current user to selected
     * models. If the withComment parameter is set to true, a dialog is displayed before adding approval.
     * 
     * @param event
     *            Add OK approval event.
     */
    private void onApprovalAddOk(AppEvent event)
    {
        if (!this.model.isServerTimeUpdate())
        {
            this.model.refreshServerTime(event);
            return;
        }

        String field = event.getData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR);
        String fieldName = event.getData(ProductionConfig.APPROVAL_NAME_EVENT_VAR);
        String approvalNoteTypeId = event.getData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR);
        String status = event.getData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR);
        Boolean withComment = event.getData(ProductionConfig.WITH_COMMENT_EVENT_VAR);
        Boolean multi = event.getData(ProductionConfig.MULTI);
        Boolean annotation = event.getData(ProductionConfig.ANNOTATION);
        Long fileRevisionId = event.getData(ProductionConfig.FILE_REVISION);

        if (withComment)
        {
            if (multi != null && multi)
            {
                MultiApprovalEditDialog dialog = new MultiApprovalEditDialog(annotation);
                dialog.show(this.model.getExplorerStore(), this.view.getSelection(), fieldName, field,
                        Long.valueOf(approvalNoteTypeId), status, this.model.getServerTime(),
                        ProductionModel.getApprovalColumns(), fileRevisionId);
                dialog.enableDate();
            }
            else
            {
                ApprovalUtils.editApprovalNotes(this.model.getExplorerStore(), this.view.getSelection(), field,
                        fieldName, Long.valueOf(approvalNoteTypeId), status, this.model.getServerTime());
            }
        }
        else
        {
            for (Hd3dModelData model : this.view.getSelection())
            {
                ApprovalNoteModelData approval = ApprovalUtils.getNewApprovalNote(MainModel.currentUser, model,
                        Long.valueOf(approvalNoteTypeId), this.model.getServerTime());
                approval.setStatus(status);

                ApprovalUtils.setTaskTypeInfo(approval);
                ApprovalUtils.addOrUpdateApprovalForModel(this.model.getExplorerStore(), model, approval, field);
            }
            if (ExplorerController.autoSave)
            {
                EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
            }
        }
        this.model.setServerTimeUpdate(false);
    }

    /**
     * Before column configuration is set, if an aggregation row configuration is needed, it is added to the model
     * configuration.
     * 
     * @param event
     *            Before set column model event.
     */
    protected void onBeforeSetColumnModel(AppEvent event)
    {
        // boolean isApprovalRenderer = false;
        // AggregationRowConfig<Hd3dModelData> validationSummary = new AggregationRowConfig<Hd3dModelData>();
        // ColumnModel cm = event.getData();
        // SheetModelData sheet = this.view.getSelectedSheet();
        //
        // for (IColumn column : sheet.getColumns())
        // {
        // if (column.getRenderer().equals(Renderer.APPROVAL))
        // {
        // validationSummary.setRenderer(column.getDataIndex(), new ApprovalNoteAggregationRenderer());
        // isApprovalRenderer = true;
        // }
        // }
        //
        // if (isApprovalRenderer)
        // cm.addAggregationRow(validationSummary);
    }

    /**
     * When note push is requested, it copies the selected note to the given column and changes all its type date to
     * make it corresponds to the arrival column.
     * 
     * @param event
     *            The "note push" clicked event.
     */
    private void onNotePushClicked(AppEvent event)
    {
        String field = event.getData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR);
        String destinationNoteId = event.getData(ProductionConfig.APPROVAL_DESTINATION_EVENT_VAR);
        Boolean withStatus = event.getData(ProductionConfig.WITH_STATUS_EVENT_VAR);

        if (!this.model.isServerTimeUpdate())
        {
            this.model.refreshServerTime(event);
            return;
        }

        for (Hd3dModelData workObject : this.view.getSelection())
        {
            ApprovalNoteModelData approvalToPush = ApprovalUtils.getLastApprovalNote(workObject, field);
            if (approvalToPush != null)
            {
                ApprovalNoteModelData pushedApproval = this.getApprovalFrom(approvalToPush, field, destinationNoteId);

                ApprovalUtils.setTaskTypeInfo(pushedApproval);

                ApprovalNoteModelData lastNote = ApprovalUtils.getLastApprovalNote(workObject, destinationNoteId);
                if (lastNote != null || withStatus)
                {
                    if (withStatus)
                        pushedApproval.setStatus(approvalToPush.getStatus());
                    else
                        pushedApproval.setStatus(lastNote.getStatus());
                }

                ApprovalUtils.addOrUpdateApprovalForModel(this.model.getExplorerStore(), workObject, pushedApproval,
                        destinationNoteId);
            }
        }
        if (ExplorerController.autoSave)
        {
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
        }
        this.model.setServerTimeUpdate(false);
    }

    /**
     * @param approval
     *            The approval to copy.
     * @param destinationNoteId
     *            The id of column used to get approval note data.
     * @return Approval with same data as given one and with date set with current date.
     */
    private ApprovalNoteModelData getApprovalFrom(ApprovalNoteModelData approvalToPush, String field,
            String destinationNoteId)
    {
        ApprovalNoteModelData pushedApproval = new ApprovalNoteModelData();

        Hd3dModelData.copy(approvalToPush, pushedApproval);
        pushedApproval.setId(null);
        pushedApproval.setApprovalNoteType(ApprovalUtils.getApprovalNoteTypeForColumn(destinationNoteId));
        pushedApproval.setDate(new Date(this.model.getServerTime().getTime()));
        pushedApproval.setComment("From " + ApprovalUtils.getColumnName(field) + ": " + approvalToPush.getComment());

        return pushedApproval;
    }

    /**
     * Register supported events.
     */
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(ProductionEvents.APPROVAL_EDIT);
        this.registerEventTypes(ProductionEvents.APPROVAL_HISTORY);
        this.registerEventTypes(ProductionEvents.APPROVAL_ADD_CLICKED);
        this.registerEventTypes(ProductionEvents.APPROVAL_PUSH_CLICKED);
        this.registerEventTypes(ProductionEvents.SHOW_PROXY_CLICKED);
        // this.registerEventTypes(ProductionEvents.PROXY_MODE_CLICKED);
        this.registerEventTypes(ProductionEvents.SEE_FILES_CLICKED);
    }
}
