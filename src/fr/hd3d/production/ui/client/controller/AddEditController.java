package fr.hd3d.production.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.view.AddEditDialog;


public class AddEditController extends MaskableController
{
    /** Model handling constituent view data (explorer and sheet combo stores). */
    @SuppressWarnings("unused")
    private final AddEditDialog view;

    public AddEditController(AddEditDialog view)
    {
        this.view = view;
        this.registerEvents();
    }

    private void registerEvents()
    {
        this.registerEventTypes(PlaylistUiEvents.FILE_REVISION_CHANGED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        final EventType type = event.getType();
        if (type == PlaylistUiEvents.FILE_REVISION_CHANGED)
        {
            System.out.println("FILE_REVISION_CHANGED");
        }
    }

}
