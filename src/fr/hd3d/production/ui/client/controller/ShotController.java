package fr.hd3d.production.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.model.ShotModel;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListRevisionGroupReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionGridModelData;
import fr.hd3d.production.ui.client.config.ProductionConfig;
import fr.hd3d.production.ui.client.error.ProductionErrors;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.playlist.model.PlayListRevisionManagerModel;
import fr.hd3d.production.ui.client.view.ShotView;


/**
 * Controller handling events raised by or for shots view.
 * 
 * @author HD3D
 */
public class ShotController extends ApprovedWorkObjectController
{
    /** Model handling constituent view data (explorer and sheet combo stores). */
    private ShotModel model;
    /** Constituent view containing tree, explorer and identity widget for constituents. */
    private ShotView view;

    private String plName;
    private List<ProxySelectionGridModelData> plCollection;

    /**
     * Default constructor.
     * 
     * @param view
     *            Shot view containing tree, explorer and identity widget for constituents.
     * @param model
     *            Model handling shot view data (explorer and sheet combo stores).
     */
    public ShotController(ShotModel model, ShotView view)
    {
        super(view, model);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == CommonEvents.SEQUENCES_SELECTED)
        {
            this.onSequenceSelected(event);
        }
        else if (type == CommonEvents.SHOTS_SELECTED)
        {
            this.onShotSelected(event);
        }
        else if (type == CommonEvents.PLAYLIST_CREATION_REQUIRED)
        {
            this.onPlaylistCreationRequired(event);
        }
        else if (type == ProductionEvents.PLAYLIST_CREATION_SUCCESS)
        {
            this.onPlayListCreated(event);
        }
    }

    @Override
    protected void onError(AppEvent event)
    {
        super.onError(event);
        this.view.enableTree();
    }

    @Override
    protected void onExplorerDataLoaded(AppEvent event) 
    {
    	this.view.enableTree();
	}
    
    private void setPlName(String plName)
    {
        this.plName = plName;
    }

    private void setPlCollection(List<ProxySelectionGridModelData> plCollection)
    {
        this.plCollection = plCollection;
    }

    private void resetPlFields()
    {
        setPlName(null);
        setPlCollection(null);
    }

    /**
     * When project changes, all data are cleared. Then first sheet of combo box is selected. Explorer filter is updated
     * to retrieve only sequences from this project. Project id is save to cookie for reselecting at next launching.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();
        EqConstraint constraint = new EqConstraint("sequence.project.id", MainModel.currentProject.getId());
        this.model.getFilter().setLeftMember(constraint);
        this.model.removeSequenceConstraint();
        this.model.removeShotConstraint();

        if (project != null)
        {
            this.view.setExplorerView(null);

            this.view.reloadTree(project);

            String cookieKey = ProductionConfig.SHEET_COOKIE_VAR_PREFIX + this.model.getEntityName() + "-"
                    + MainModel.currentProject.getId();
            this.view.setSheetCookie(cookieKey);
            this.view.reloadSheets(project, ShotModelData.SIMPLE_CLASS_NAME.toLowerCase());
        }
    }

    /**
     * When a sequence is selected, explorer filter is updated. If the all sequence is selected (id = -1) the default
     * filter is set (filter on whole project). If another sequence is selected a lucene constraint is set. It will make
     * the explorer to retrieve only shots from the selected sequence and from its children.
     * 
     * @param event
     *            The sequence selected event.
     */
    private void onSequenceSelected(AppEvent event)
    {
        List<SequenceModelData> sequences = event.getData();
        if (CollectionUtils.isNotEmpty(sequences))
        {
            SequenceModelData sequence = sequences.get(0);
            Constraint constraint = new Constraint(EConstraintOperator.eq, "sequence.project.id",
                    MainModel.currentProject.getId());

            this.model.removeSequenceConstraint();
            this.model.removeShotConstraint();
            this.model.getFilter().setLeftMember(constraint);
            if (sequence.getId() != -1)
            {
                this.model.setConstraintSequence(sequences);
            }

            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * Modify explorer store parameters, then reload store data to display only selected shot inside explorer.
     * 
     * @param event
     *            The shot selected event.
     */
    private void onShotSelected(AppEvent event)
    {
        List<ShotModelData> shots = event.getData();
        if (CollectionUtils.isNotEmpty(shots))
        {
            this.model.removeSequenceConstraint();
            this.model.removeShotConstraint();
            this.model.setConstraintShot(shots);

            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * 
     * @param event
     */
    private void onPlaylistCreationRequired(AppEvent event)
    {
        if (MainModel.getCurrentProject() == null)
        {
            ErrorDispatcher.sendError(ProductionErrors.NO_CURRENT_PROJECT_OPENED_ERROR);
            return;
        }

        final String plName = event.getData("name");
        if (plName == null || "".equals(plName))
            return;
        final List<ProxySelectionGridModelData> plCollection;
        try
        {
            plCollection = event.getData("collection");
        }
        catch (ClassCastException exception)
        {
            return;
        }
        if (plCollection == null || plCollection.isEmpty())
            return;

        final ServiceStore<PlayListRevisionGroupModelData> playlistRevisionGroupsStore = new ServiceStore<PlayListRevisionGroupModelData>(
                new PlayListRevisionGroupReader());

        Constraints pconstraints = new Constraints(new EqConstraint("project", MainModel.getCurrentProject().getId()));
        playlistRevisionGroupsStore.addParameter(pconstraints);

        playlistRevisionGroupsStore.setPath(ServicesPath.getPath(PlayListRevisionGroupModelData.SIMPLE_CLASS_NAME));
        playlistRevisionGroupsStore.getLoader().addLoadListener(new LoadListener() {

            @Override
            public void loaderLoad(LoadEvent e)
            {
                onPlaylistsLoaded(e, playlistRevisionGroupsStore);
            }

        });

        playlistRevisionGroupsStore.getLoader().load();

    }

    private void onPlaylistsLoaded(LoadEvent e, ServiceStore<PlayListRevisionGroupModelData> playlistRevisionGroupsStore)
    {
        Long parentId = null;
        getparent: for (PlayListRevisionGroupModelData plGroup : playlistRevisionGroupsStore.getModels())
        {
            Long pId = ((PlayListRevisionGroupModelData) plGroup).getParentId();
            if (pId == null)
            {
                parentId = ((PlayListRevisionGroupModelData) plGroup).getId();
                break getparent;
            }
        }

        PlayListRevisionModelData newPlaylistRevision = PlayListRevisionManagerModel.createPlayListRevisionModelData(
                plName, 0, parentId, MainModel.getCurrentProject().getId());

        // This sends PLAYLIST_CREATION_SUCCESS Event when id is loaded.
        setPlName(plName);
        setPlCollection(plCollection);
        newPlaylistRevision.save(ProductionEvents.PLAYLIST_CREATION_SUCCESS);
    }

    private void onPlayListCreated(AppEvent event)
    {
        // Check predicates
        if (plName == null || "".equals(plName) || plCollection == null || plCollection.size() == 0)
        {
            resetPlFields();
            return;
        }

        PlayListRevisionModelData newPlaylistRevision = event.getData("model");

        // Create edits
        for (ProxySelectionGridModelData psgmd : plCollection)
        {
            int duration = 0;
            RecordModelData workObj = psgmd.getWorkObjectModelData();
            if (workObj != null && ShotModelData.SIMPLE_CLASS_NAME.equals(workObj.getSimpleClassName()))
            {
                duration = ((ShotModelData) workObj).getNbFrame();
            }
            PlayListEditModelData mPlayListEditModelData = PlayListRevisionManagerModel.createPlayListEditModelData(
                    psgmd.getWorkObjectModelData().getName(), 0, duration, 0, duration, psgmd.getProxyModelData(),
                    newPlaylistRevision.getId());
            mPlayListEditModelData.save();
        }

        resetPlFields();
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(CommonEvents.SEQUENCES_SELECTED);
        this.registerEventTypes(CommonEvents.SHOTS_SELECTED);
        this.registerEventTypes(CommonEvents.PLAYLIST_CREATION_REQUIRED);
        this.registerEventTypes(ProductionEvents.PLAYLIST_CREATION_SUCCESS);
    }
}
