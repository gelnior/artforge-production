package fr.hd3d.production.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.model.ConstituentModel;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.config.ProductionConfig;
import fr.hd3d.production.ui.client.view.ConstituentView;


/**
 * Controller handling events raised by or for constituents view.
 * 
 * @author HD3D
 */
public class ConstituentController extends ApprovedWorkObjectController
{

    /** Model handling constituent view data (explorer and sheet combo stores). */
    private final ConstituentModel model;
    /** Constituent view containing tree, explorer and identity widget for constituents. */
    private final ConstituentView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            Constituent view containing tree, explorer and identity widget for constituents.
     * @param model
     *            Model handling constituent view data (explorer and sheet combo stores).
     */
    public ConstituentController(ConstituentModel model, ConstituentView view)
    {
        super(view, model);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == CommonEvents.CATEGORIES_SELECTED)
        {
            this.onCategorySelected(event);
        }
        else if (type == CommonEvents.CONSTITUENTS_SELECTED)
        {
            this.onConstituentSelected(event);
        }
        else if (type == CommonEvents.HISTORY_CHANGED)
        {
            // this.onHistoryChanged(event);
        }
    }

    @Override
    protected void onError(AppEvent event)
    {
        super.onError(event);
        this.view.enableTree();
    }

    @Override
    protected void onExplorerDataLoaded(AppEvent event) 
    {
    	this.view.enableTree();
	}
    
    @SuppressWarnings("unused")
    private void onHistoryChanged(AppEvent event)
    {
        // String categoryPath = HistoryUtils.getValue(ProductionConfig.CATEGORY_HISTORY_VAR);
        // Stack<Long> idPath = new Stack<Long>();
        // if (!Util.isEmptyString(categoryPath))
        // {
        // for (String id : categoryPath.split(ConstituentModel.PATH_SEPARATOR))
        // {
        // idPath.push(Long.valueOf(id));
        // }
        // this.view.selectCategory(idPath);
        // }
    }

    /**
     * When project changes, all data are cleared. Then first sheet of combo box is selected. Explorer filter is updated
     * to retrieve only categories from this project. Project id is save to cookie for reselecting at next launching.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();
        Constraint constraint = new Constraint(EConstraintOperator.eq, "category.project.id", project.getId());
        this.model.getFilter().setLeftMember(constraint);
        this.model.removeCategoryConstraint();
        this.model.removeConstituentConstraint();

        if (project != null)
        {
            this.view.setExplorerView(null);
            this.view.reloadTree(project);

            String cookieKey = ProductionConfig.SHEET_COOKIE_VAR_PREFIX + this.model.getEntityName() + "-"
                    + MainModel.currentProject.getId();
            this.view.setSheetCookie(cookieKey);
            this.view.reloadSheets(project, ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase());
        }
    }

    /**
     * When a category is selected, explorer filter is updated. If the all category is selected (id = -1) the default
     * filter is set (filter on whole project). If another category is set, all its children are retrieved. Then a new
     * filter is set on explorer, it retrieves only constituents from the selected category and from its children.
     * 
     * @param event
     *            The category selected event.
     */
    private void onCategorySelected(AppEvent event)
    {
        List<CategoryModelData> categories = event.getData();
        if (CollectionUtils.isNotEmpty(categories))
        {
            CategoryModelData category = categories.get(0);
            // HistoryUtils.putValue(ProductionConfig.CATEGORY_HISTORY_VAR, this.model.getCategoryPath(category));

            this.model.removeCategoryConstraint();
            this.model.removeConstituentConstraint();

            if (category.getId() != -1)
            {
                this.model.setConstraintCategory(categories);
            }

            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * Modify explorer store parameters, then reload store data to display only selected constituent inside explorer.
     * 
     * @param event
     *            The constituent selected event.
     */
    private void onConstituentSelected(AppEvent event)
    {
        List<ConstituentModelData> constituents = event.getData();
        if (CollectionUtils.isNotEmpty(constituents))
        {
            this.model.removeCategoryConstraint();
            this.model.removeConstituentConstraint();
            this.model.setConstraintConstituent(constituents);
            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(CommonEvents.CONSTITUENTS_SELECTED);
        this.registerEventTypes(CommonEvents.CATEGORIES_SELECTED);
        this.registerEventTypes(CommonEvents.HISTORY_CHANGED);
    }
}
