package fr.hd3d.production.ui.client.controller;

public enum ApprovalStatus
{
    OK, NOK, DONE
}
