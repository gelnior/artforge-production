package fr.hd3d.production.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.history.HistoryUtils;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.config.ProductionConfig;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.event.StatDetailsEvents;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.playlist.view.PlaylistView;
import fr.hd3d.production.ui.client.stat.StatEvents;
import fr.hd3d.production.ui.client.stat.StatView;
import fr.hd3d.production.ui.client.stat.details.StatDetailsView;
import fr.hd3d.production.ui.client.view.ConstituentView;
import fr.hd3d.production.ui.client.view.IProductionView;
import fr.hd3d.production.ui.client.view.ShotView;


/**
 * Controller handling Production Tool events.
 */
public class ProductionController extends MainController
{

    /** Main model handling production view data. */
    private ProductionModel model;
    /** Main view, containing main layout and all widgets. */
    private IProductionView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            Production view
     * @param model
     *            Production model
     */
    public ProductionController(IProductionView view, ProductionModel model)
    {
        super(model, view);

        this.view = view;
        this.model = model;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == ProductionEvents.APPROVAL_TYPES_LOADED)
        {
            this.onApprovalTypesLoaded(event);
        }
        else if (type == ExplorerEvents.SHEET_INFORMATIONS_LOADED)
        {
            this.onSheetInformationsLoaded(event);
        }
        else if (type == ProductionEvents.CONSTITUENT_TAB_CLICKED)
        {
            this.onConstituentTabClicked();
        }
        else if (type == ProductionEvents.STAT_TAB_CLICKED)
        {
            this.onStatTabClicked();
        }
        else if (type == ProductionEvents.STAT_DETAIL_TAB_CLICKED)
        {
            this.onStatDetailsTabClicked();
        }
        else if (type == ProductionEvents.SHOT_TAB_CLICKED)
        {
            this.onShotTabClicked();
        }
        else if (type == ProductionEvents.PLAYLIST_TAB_CLICKED)
        {
            this.onPlaylistTabClicked();
        }
        else if (type == ExplorerEvents.SELECT_CHANGE)
        {
            this.onExplorerSelectChange(event);
        }
        else if (type == ExplorerEvents.AUTO_SAVE_TOGGLE)
        {
            this.forwardToChildForced(event);
        }
        else if (type == CommonEvents.HISTORY_CHANGED)
        {
            this.onHistoryChanged(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When user settings are initialized, view is built, loading panel is hidden, controller hierarchy is made then
     * last environment is rebuilt (last selected project since last connection...).
     * 
     * @param event
     *            The setting initialized event.
     */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        super.onSettingInitialized(event);

        ExcludedField.addExcludedField("activitiesDuration");
        ExcludedField.addExcludedField("boundTasks");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskDurationCollectionQuery");
        ExcludedField.addExcludedField("Approval Notes");
        // ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery");
        ExcludedField.addExcludedField("tasksActivitiesDuration");
        ExcludedField.addExcludedField("tasksEstimation");
        ExcludedField.addExcludedField("tasksStatus");
        ExcludedField.addExcludedField("approvalNotes");
        ExcludedField.addExcludedField("ApprovalNote");
        ExcludedField.addExcludedField("approvable");
        ExcludedField.addExcludedField("path");

        ExcludedField.addExcludedField("modeling");
        ExcludedField.addExcludedField("hairs");
        ExcludedField.addExcludedField("setup");
        ExcludedField.addExcludedField("shading");
        ExcludedField.addExcludedField("texturing");
        ExcludedField.addExcludedField("tracking");
        ExcludedField.addExcludedField("rendering");
        ExcludedField.addExcludedField("layout");
        ExcludedField.addExcludedField("compositing");
        ExcludedField.addExcludedField("dressing");
        ExcludedField.addExcludedField("lighting");
        ExcludedField.addExcludedField("mattepainting");
        ExcludedField.addExcludedField("export");
        ExcludedField.addExcludedField("animation");
        ExcludedField.addExcludedField("layout");
        ExcludedField.addExcludedField("design");
        // ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery");
        ExcludedField.addExcludedField("tasksDurationGap");
        ExcludedField.addExcludedField("tasksWorkers");

        this.getLastEnvironement();
        this.view.initWidgets();
        this.view.setControllers();
        this.buildEnvironment();
        this.view.hideStartPanel();
    }

    @Override
    protected void onConfigInitialized(AppEvent event)
    {
        super.onConfigInitialized(event);
        // History.addValueChangeHandler(new HistoryChangedHandler());
    }

    /**
     * Retrieve last environment variable from cookie.
     */
    private void getLastEnvironement()
    {
        String tab = HistoryUtils.getValue(ProductionConfig.TAB_HISTORY_VAR);
        if (tab == null)
        {
            tab = this.view.getFavValue(ProductionConfig.TAB_COOKIE_VAR);
        }
        this.model.setLastSelectedTab(tab);
        Boolean isAutoSave = UserSettings.getSettingAsBoolean(ExplorerConfig.SETTING_AUTOSAVE);
        if (isAutoSave != null)
        {
            ExplorerController.autoSave = isAutoSave;
        }
    }

    private void onHistoryChanged(AppEvent event)
    {
        String tab = HistoryUtils.getValue(ProductionConfig.TAB_HISTORY_VAR);
        if (tab == null)
        {
            tab = ConstituentView.NAME;
        }
        this.model.setLastSelectedTab(tab);
        this.view.selectTab(this.model.getLastSelectedTab());

        String projectId = HistoryUtils.getValue(CommonConfig.PROJECT_HISTORY_VAR);
        if (projectId != null && MainModel.currentProject != null
                && !projectId.equals(MainModel.currentProject.getId().toString()))
            this.view.selectProject(Long.valueOf(projectId));

        this.forwardToChild(event);
    }

    /** Reload last selected project, and select last selected tab. */
    private void buildEnvironment()
    {
        if (ExplorerController.autoSave)
            this.view.toggleAutoSave();
        this.view.selectTab(this.model.getLastSelectedTab());
        this.view.selectLastProject();
        this.view.enablePlaylistUi(ProductionModel.isEnablePlaylistUi());
        this.view.enableStatUi(model.isEnableStatUi());
    }

    /**
     * When explorer selection changes, tag editor is updated with currently selected record tags.
     * 
     * @param event
     *            Explorer selection change event.
     */
    private void onExplorerSelectChange(AppEvent event)
    {
        Hd3dModelData selectedModel = event.getData(ExplorerEvents.EVENT_VAR_MODELDATA);
        if (selectedModel != null)
        {
            if (ConstituentModelData.SIMPLE_CLASS_NAME.equals(ProductionModel.getCurrentEntity()))
            {
                selectedModel.setSimpleClassName(ConstituentModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(ConstituentModelData.CLASS_NAME);
            }
            else if (ShotModelData.SIMPLE_CLASS_NAME.equals(ProductionModel.getCurrentEntity()))
            {
                selectedModel.setSimpleClassName(ShotModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(ShotModelData.CLASS_NAME);
            }
            else if (SequenceModelData.SIMPLE_CLASS_NAME.equals(ProductionModel.getCurrentEntity()))
            {
                selectedModel.setSimpleClassName(ShotModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(ShotModelData.CLASS_NAME);
            }
            else if (CategoryModelData.SIMPLE_CLASS_NAME.equals(ProductionModel.getCurrentEntity()))
            {
                selectedModel.setSimpleClassName(CategoryModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(CategoryModelData.CLASS_NAME);
            }
            // refreshTagEditor(selectedModel);
        }
    }

    /**
     * When shot tab is clicked, shot tab is unidled, constituent tab is idled (controller is masked) then if project
     * has changed since last tab display, it refreshes data. Current entity is set to shot. The tab cookie variable
     * value is set to "shot".
     */
    private void onShotTabClicked()
    {
        FavoriteCookie.putFavValue(ProductionConfig.TAB_COOKIE_VAR, ShotView.NAME);
        HistoryUtils.putValue(ProductionConfig.TAB_HISTORY_VAR, ShotView.NAME);
        this.view.idleConstituentView();
        this.view.idlePlaylistView();
        this.view.idleStatView();
        this.view.idleStatDetailsView();
        this.view.unidleShotView();
        if (this.model.isProjectChanged())
        {
            this.model.setIsProjectChanged(false);
            this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
        }

        this.view.refreshTagEditor(null);
        ProductionModel.setCurrentEntity(ShotModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * When playlist tab is clicked, shot tab and constituent tab is unidled, constituent tab is idled (controller is
     * masked) then if project has changed since last tab display, it refreshes data. Current entity is set to shot. The
     * tab cookie variable value is set to "shot".
     */
    private void onPlaylistTabClicked()
    {
        FavoriteCookie.putFavValue(ProductionConfig.TAB_COOKIE_VAR, PlaylistView.NAME);
        HistoryUtils.putValue(ProductionConfig.TAB_HISTORY_VAR, PlaylistView.NAME);
        this.view.idleConstituentView();
        this.view.idleShotView();
        this.view.idleStatView();
        this.view.idleStatDetailsView();
        this.view.unidlePlaylistView();
        if (this.model.isProjectChanged())
        {
            this.model.setIsProjectChanged(false);
            this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
        }
        // this.view.refreshTagEditor(null);
        // this.model.setCurrentEntity(ShotModelData.SIMPLE_CLASS_NAME); TODO
    }

    /**
     * When constituent tab is clicked, constituent tab is unidled, shot tab is idled (controller is masked) then if
     * project has changed since last tab display, it refreshes data. Current entity is set to constituent. The tab
     * cookie variable value is set to "constituent".
     */
    private void onConstituentTabClicked()
    {
        FavoriteCookie.putFavValue(ProductionConfig.TAB_COOKIE_VAR, ConstituentView.NAME);
        HistoryUtils.putValue(ProductionConfig.TAB_HISTORY_VAR, ConstituentView.NAME);
        this.view.idleShotView();
        this.view.idlePlaylistView();
        this.view.idleStatView();
        this.view.idleStatDetailsView();
        this.view.unidleConstituentView();
        if (this.model.isProjectChanged())
        {
            this.model.setIsProjectChanged(false);
            this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
        }
        this.view.refreshTagEditor(null);
        ProductionModel.setCurrentEntity(ConstituentModelData.SIMPLE_CLASS_NAME);
    }

    private void onStatTabClicked()
    {
        FavoriteCookie.putFavValue(ProductionConfig.TAB_COOKIE_VAR, StatView.NAME);
        HistoryUtils.putValue(ProductionConfig.TAB_HISTORY_VAR, StatView.NAME);
        this.view.idleShotView();
        this.view.idlePlaylistView();
        this.view.idleConstituentView();
        this.view.idleStatDetailsView();
        this.view.unidleStatView();

        this.forwardToChild(new AppEvent(StatEvents.PROJECT_CHANGED, MainModel.currentProject));
        this.model.setIsProjectChanged(true);
    }

    private void onStatDetailsTabClicked()
    {
        FavoriteCookie.putFavValue(ProductionConfig.TAB_COOKIE_VAR, StatDetailsView.NAME);
        HistoryUtils.putValue(ProductionConfig.TAB_HISTORY_VAR, StatDetailsView.NAME);
        this.view.idleShotView();
        this.view.idlePlaylistView();
        this.view.idleConstituentView();
        this.view.idleStatView();
        this.view.unidleStatDetailsView();

        this.forwardToChild(new AppEvent(StatDetailsEvents.PROJECT_CHANGED, MainModel.getCurrentProject()));
        this.model.setIsProjectChanged(true);
    }

    /**
     * When project changes, all data are cleared. Currently selected tab is refreshed. Explorer will display data when
     * a constituent or a shot will be selected.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        // if (null == MainModel.currentProject)
        // HistoryUtils.removeValue(ProductionConfig.CATEGORY_HISTORY_VAR);

        MainModel.setCurrentProject((ProjectModelData) event.getData());
        // HistoryUtils.putValue(CommonConfig.PROJECT_HISTORY_VAR, MainModel.currentProject.getId().toString());
        ProductionModel.approvalStore.clearParameters();
        ProductionModel.approvalStore.setPath(ProductionModel.getCurrentProject().getDefaultPath() + "/"
                + ServicesPath.APPROVAL_NOTE_TYPES);
        ProductionModel.approvalStore.addEqConstraint("project.id", ProductionModel.currentProject.getId());
        ProductionModel.approvalStore.reload();
        ProductionModel.setCurrentProjectSelected(MainModel.currentProject);

    }

    private void onApprovalTypesLoaded(AppEvent event)
    {
        this.model.setIsProjectChanged(true);
        this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
    }

    private void onSheetInformationsLoaded(AppEvent event)
    {
        List<ItemModelData> items = event.getData();
        ProductionModel.getApprovalColumns().clear();

        for (ItemModelData item : items)
        {
            if (FieldUtils.isApprovalColumn(item.getQuery()))
            {
                ProductionModel.getApprovalColumns().add(item);
            }
        }

        this.forwardToChild(event);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(ProductionEvents.APPROVAL_TYPES_LOADED);
        this.registerEventTypes(ProductionEvents.CONSTITUENT_TAB_CLICKED);
        this.registerEventTypes(ProductionEvents.SHOT_TAB_CLICKED);
        this.registerEventTypes(ProductionEvents.STAT_TAB_CLICKED);
        this.registerEventTypes(ProductionEvents.PLAYLIST_TAB_CLICKED);
        this.registerEventTypes(ProductionEvents.STAT_DETAIL_TAB_CLICKED);
        this.registerEventTypes(CommonEvents.HISTORY_CHANGED);

        this.registerEventTypes(ExplorerEvents.BEFORE_SAVE);
        this.registerEventTypes(ExplorerEvents.AFTER_SAVE);
        this.registerEventTypes(ExplorerEvents.AUTO_SAVE_TOGGLE);
        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);

        this.registerEventTypes(CommonEvents.ERROR);
    }
}
