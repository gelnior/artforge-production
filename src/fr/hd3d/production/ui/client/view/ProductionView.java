package fr.hd3d.production.ui.client.view;

import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerModel;
import fr.hd3d.common.ui.client.widget.identitysheet.IdentitySheetWidget;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.constant.ProductionMessages;
import fr.hd3d.production.ui.client.controller.ProductionController;
import fr.hd3d.production.ui.client.error.ProductionErrors;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.playlist.view.PlaylistView;
import fr.hd3d.production.ui.client.stat.StatView;
import fr.hd3d.production.ui.client.stat.details.StatDetailsView;
import fr.hd3d.production.ui.client.view.column.ProductionEditorProvider;
import fr.hd3d.production.ui.client.view.toolbar.ProductionToolbar;


/**
 * ProductionView manages layout and widget top functionality : constituent view and shot view.
 */
public class ProductionView extends MainView implements IProductionView
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static ProductionMessages MESSAGES = GWT.create(ProductionMessages.class);

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Controller that handles main events. */
    private ProductionController controller;
    /** Model handling application data. */
    private ProductionModel model = new ProductionModel();

    /** Panel used for making tabs (constituents, shots...). */
    private TabPanel tabPanel = new TabPanel();

    /** The panel hosting the application. */
    private ContentPanel panel = new ContentPanel();
    /** The main tool bar */
    private ProductionToolbar toolbar;

    /** The constituent view composed of a navigation tree, an explorer and an identity sheet. */
    private ConstituentView constituentView;
    /** Tab containing constituent view. */
    final TabItem constituentTabItem = new TabItem(COMMON_CONSTANTS.Constituents());
    /** The shot view composed of a navigation tree, an explorer and an identity sheet. */
    private ShotView shotView;
    /** Tab containing shot view. */
    final TabItem shotTabItem = new TabItem(COMMON_CONSTANTS.Shots());
    /** The play list view */
    private PlaylistView playlistView;
    /** Tab containing shot view. */
    final TabItem plTabItem = new TabItem(COMMON_CONSTANTS.Playlists());
    /** The play Stat view */
    private StatView statView;
    /** Tab containing stat view. */
    final TabItem statisticTabItem = new TabItem("Statistics");// TODO manage internationalization

    /** The play Stat view */
    private StatDetailsView statDetailsView;
    // /** Tab containing stat view. */
    final TabItem statisticDetailsTabItem = new TabItem("Statistics Details");// TODO manage internationalization

    /**
     * Default constructor
     * 
     */
    public ProductionView()
    {
        super(CONSTANTS.ProductionTool());
    }

    /** Registers all controller. */
    @Override
    public void init()
    {
        controller = new ProductionController(this, model);
        EventDispatcher.get().addController(controller);
    }

    /** Initialize production widgets and add them to viewport. Selects automatically last selected tab. */
    public void initWidgets()
    {
        this.toolbar = new ProductionToolbar();
        this.panel.setTopComponent(this.toolbar);

        this.constituentView = new ConstituentView();
        this.constituentTabItem.setLayout(new FitLayout());
        this.constituentTabItem.add(this.constituentView);
        this.constituentTabItem.setIcon(Hd3dImages.getConsituentIcon());
        this.constituentTabItem.addListener(Events.Select, new EventBaseListener(
                ProductionEvents.CONSTITUENT_TAB_CLICKED));

        this.shotView = new ShotView();
        this.shotTabItem.setLayout(new FitLayout());
        this.shotTabItem.add(this.shotView);
        this.shotTabItem.setIcon(Hd3dImages.getShotIcon());
        this.shotTabItem.addListener(Events.Select, new EventBaseListener(ProductionEvents.SHOT_TAB_CLICKED));
        this.shotView.idle();

        this.tabPanel.add(this.constituentTabItem);
        this.tabPanel.add(this.shotTabItem);

        if (ProductionModel.isEnablePlaylistUi())
        {
            this.playlistView = new PlaylistView();
            this.plTabItem.setLayout(new FitLayout());
            this.plTabItem.add(this.playlistView);
            this.plTabItem.setIcon(ProductionImages.getPlaylistSymbol());
            this.plTabItem.addListener(Events.Select, new EventBaseListener(ProductionEvents.PLAYLIST_TAB_CLICKED));
            this.playlistView.idle();
            this.tabPanel.add(this.plTabItem);
        }

        if (model.isEnableStatUi())
        {
            this.statView = new StatView();
            this.statisticTabItem.setLayout(new FitLayout());
            this.statisticTabItem.add(this.statView);
            this.statisticTabItem.setIcon(Hd3dImages.getStatsIcon());
            this.statisticTabItem.addListener(Events.Select, new EventBaseListener(ProductionEvents.STAT_TAB_CLICKED));
            this.statView.idle();
            this.tabPanel.add(this.statisticTabItem);

            this.statDetailsView = new StatDetailsView();
            this.statisticDetailsTabItem.setLayout(new FitLayout());
            this.statisticDetailsTabItem.add(this.statDetailsView);
            this.statisticDetailsTabItem.setIcon(Hd3dImages.getStatDetailsIcon());
            this.statisticDetailsTabItem.addListener(Events.Select, new EventBaseListener(
                    ProductionEvents.STAT_DETAIL_TAB_CLICKED));
            this.statDetailsView.idle();
            this.tabPanel.add(this.statisticDetailsTabItem);
        }

        this.panel.setLayout(new FitLayout());
        this.panel.setHeaderVisible(false);
        this.panel.setBorders(false);
        this.panel.setBodyBorder(false);
        this.panel.add(tabPanel);
        this.addToViewport(panel);

        // Identity sheet configuration for Production application.
        IdentitySheetWidget.itemInfoProvider = new ProductionEditorProvider();
        ExplorerModel.setIsIdentityDialogEnabled(true);
    }

    /** Set tab controller as child of Production controller. */
    public void setControllers()
    {
        this.controller.addChild(constituentView.getController());
        this.controller.addChild(shotView.getController());
        if (statView != null)
        {
            this.controller.addChild(statView.getController());
        }
        if (statDetailsView != null)
        {
            this.controller.addChild(statDetailsView.getController());
        }

        if (playlistView == null)
        {
            return;
        }
        controller.addChild(playlistView.getController());
    }

    /** Refresh tag displayed inside tag editor. */
    public void refreshTagEditor(List<Hd3dModelData> selection)
    {
        this.toolbar.refreshTagEditor(selection);
    }

    /**
     * Select last selected project. The last selected project is the one of which id is stored in the favorite cookie.
     */
    public void selectLastProject()
    {
        this.toolbar.selectLastSelectedProject();
    }

    /**
     * Make the constituent view inactive (to not react to event from similar view such as shot view).
     */
    public void idleConstituentView()
    {
        this.constituentView.idle();
    }

    /**
     * Make the constituent view active.
     */
    public void unidleConstituentView()
    {
        this.constituentView.unidle();
    }

    /**
     * Make the shot view inactive (to not react to event from similar view such as constituent view).
     */
    public void idleShotView()
    {
        this.shotView.idle();
    }

    /**
     * Make the shot view active.
     */
    public void unidleShotView()
    {
        this.shotView.unidle();
    }

    /**
     * Make the Playlist view inactive (to not react to event from similar view such as constituent view).
     */
    public void idlePlaylistView()
    {
        if (playlistView == null)
        {
            return;
        }
        this.playlistView.idle();
    }

    /**
     * Make the Playlist view active.
     */
    public void unidlePlaylistView()
    {
        if (playlistView == null)
        {
            return;
        }
        this.playlistView.unidle();
    }

    /**
     * Make the stat view inactive
     */
    public void idleStatView()
    {
        if (statView == null)
        {
            return;
        }
        this.statView.idle();
    }

    /**
     * Make the stat view active.
     */
    public void unidleStatView()
    {
        if (statView == null)
        {
            return;
        }
        this.statView.unidle();
    }

    /**
     * Make the stat view inactive
     */
    public void idleStatDetailsView()
    {
        if (statDetailsView == null)
        {
            return;
        }
        this.statDetailsView.idle();
    }

    /**
     * Make the stat view active.
     */
    public void unidleStatDetailsView()
    {
        if (statDetailsView == null)
        {
            return;
        }
        this.statDetailsView.unidle();
    }

    public void initStatSheets()
    {
        // this.statView.initSheets();
    }

    /**
     * @return Favorite cookie value corresponding to <i>key</i>.
     * @param key
     *            Key corresponding to requested value.
     */
    public String getFavValue(String key)
    {
        return FavoriteCookie.getFavParameterValue(key);
    }

    /**
     * Select tab whose name is equal to string given in parameter.
     * 
     * @param name
     *            Name of the tab to select
     */
    public void selectTab(String name)
    {
        if (name == null)
        {
            return;
        }
        if (ShotView.NAME.toLowerCase().equals(name.toLowerCase()))
        {
            tabPanel.setSelection(shotTabItem);
        }
        else if (ConstituentView.NAME.toLowerCase().equals(name))
        {
            tabPanel.setSelection(constituentTabItem);
        }
        else if (StatView.NAME.toLowerCase().equals(name))
        {
            tabPanel.setSelection(statisticTabItem);
        }
        else if (StatDetailsView.NAME.toLowerCase().equals(name))
        {
            tabPanel.setSelection(statisticDetailsTabItem);
        }
        else if (PlaylistView.NAME.equalsIgnoreCase(name))
        {
            tabPanel.setSelection(plTabItem);
        }
    }

    /**
     * Disable play list view if <i>enable</i> is set to false, else it enables it.
     * 
     * @param enable
     *            True to enable play list view, false either.
     */
    public void enablePlaylistUi(boolean enable)
    {
        plTabItem.setVisible(enable);
        if (enable)
        {
            unidlePlaylistView();
        }
        else
        {
            // ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE);
            idlePlaylistView();
        }
    }

    public void enableStatUi(boolean enable)
    {
        plTabItem.setVisible(enable);
        if (enable)
        {
            // unidleStatDetailsView();
            // unidleStatView();
        }
        else
        {
            idleStatDetailsView();
            idleStatView();
        }
    }

    protected void displayErrorMessageBox(int error, String userMsg, String stack, Listener<MessageBoxEvent> callback)
    {
        switch (error)
        {
            case ProductionErrors.NO_CURRENT_PROJECT_OPENED_ERROR:
                MessageBox.alert(COMMON_CONSTANTS.Error(), "Open a project first", callback);
                break;
            default:
                super.displayErrorMessageBox(error, userMsg, stack, callback);
        }
    }

    /**
     * In project combobox select project of which ID is equal to <i>projectId</i>.
     */
    public void selectProject(Long projectId)
    {
        this.toolbar.selectProject(projectId);
    }

    /**
     * Press toggle button if it is not pressed.
     */
    public void toggleAutoSave()
    {
        this.toolbar.toggleAutoSave();
    }
}
