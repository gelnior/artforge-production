package fr.hd3d.production.ui.client.view.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.production.ui.client.playlist.event.CreatePlayListListener;


/**
 * This button allows the creation of a playlist entity, if a list of WorkObject is not null.
 */
public class CreatePlayListFromSelectionButton extends Button
{

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    private CreatePlayListListener createPlayListListener = new CreatePlayListListener();

    /**
     * Default constructor
     */
    public CreatePlayListFromSelectionButton(List<? extends Hd3dModelData> collection)
    {
        super();
        initButton();
        createPlayListListener.setCollection(collection);
        this.addListener(Events.Select, createPlayListListener);
    }

    public void setCollection(List<? extends Hd3dModelData> collection)
    {
        createPlayListListener.setCollection(collection);
    }

    /**
     * initialize the class button and listeners
     * 
     */
    // TODO internationalization
    private void initButton()
    {
        this.setIconStyle(ExplorateurCSS.EDIT_FOLDER);
        this.getElement().setNodeValue(ExplorateurCSS.EDIT_FOLDER);
        this.setToolTip("Create Playlist");
    }
}
