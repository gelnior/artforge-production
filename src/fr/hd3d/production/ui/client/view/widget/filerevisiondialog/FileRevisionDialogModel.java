package fr.hd3d.production.ui.client.view.widget.filerevisiondialog;

import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.FileRevisionReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyTypeReader;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


public class FileRevisionDialogModel
{

    private FileRevisionModelData fileRevisionSelected;

    private ServiceStore<ProxyTypeModelData> proxyTypeStore = new ServiceStore<ProxyTypeModelData>(
            new ProxyTypeReader());

    private Constraint proxyTypeFileRevisionConstraint = new Constraint(EConstraintOperator.eq,
            "proxies.fileRevision.id");

    private Constraint proxyFileRevisionConstraint = new Constraint(EConstraintOperator.eq, "fileRevision.id");

    private Constraint proxyProxyTypeConstraint = new Constraint(EConstraintOperator.eq, "proxyType.id");

    private ServiceStore<ProxyModelData> proxyStore = new ServiceStore<ProxyModelData>(new ProxyReader());

    private Long proxyTypeId;

    private ServiceStore<FileRevisionModelData> fileRevisionStore = new ServiceStore<FileRevisionModelData>(
            new FileRevisionReader());

    private Constraint fileRevisionConstraint = new Constraint(EConstraintOperator.in, "id");

    public FileRevisionDialogModel()
    {
        proxyTypeStore.addParameter(proxyTypeFileRevisionConstraint);
        fileRevisionStore.addParameter(fileRevisionConstraint);
    }

    public void reset()
    {
        this.proxyTypeId = null;
        this.fileRevisionSelected = null;
        this.proxyStore.removeAll();
        this.proxyTypeStore.removeAll();
    }

    public void setFileRevisionSelected(Long fileRevision)
    {

        this.fileRevisionSelected = new FileRevisionModelData();
        this.fileRevisionSelected.setId(fileRevision);
        this.fileRevisionSelected.refresh();

        this.proxyFileRevisionConstraint.setLeftMember(fileRevision);
        this.proxyTypeFileRevisionConstraint.setLeftMember(fileRevision);
    }

    public void setProxyType(Long proxyType)
    {
        this.proxyTypeId = proxyType;
        this.proxyProxyTypeConstraint.setLeftMember(proxyType);
    }

    public ServiceStore<ProxyTypeModelData> getProxyTypeStore()
    {
        return proxyTypeStore;
    }

    public ServiceStore<ProxyModelData> getProxyStore()
    {
        return proxyStore;
    }

    public void reload()
    {
        this.proxyTypeStore.reload();
        this.fileRevisionStore.reload();
        reloadProxyStore();
    }

    public void reloadProxyTypeStore(Long proxyTypeId)
    {
        setProxyType(proxyTypeId);
        reloadProxyStore();
    }

    public void reloadProxyStore()
    {
        proxyStore.clearParameters();
        Constraints constraints = new Constraints();

        constraints.add(proxyFileRevisionConstraint);
        if (proxyTypeId != null)
        {
            constraints.add(proxyProxyTypeConstraint);
        }
        proxyStore.addParameter(constraints);
        this.proxyStore.reload();
    }

    public FileRevisionModelData getFileRevisionSelected()
    {
        return fileRevisionSelected;
    }

    public BaseStore<FileRevisionModelData> getFileRevisionStore()
    {
        return fileRevisionStore;
    }

    public void setFileRevisions(List<Long> fileRevisions) throws Hd3dException
    {
        if (fileRevisions == null || fileRevisions.isEmpty())
        {
            throw new Hd3dException("No file revisions!!");
        }
        fileRevisionConstraint.setLeftMember(fileRevisions);
        setFileRevisionSelected(fileRevisions.get(0));
    }
}
