package fr.hd3d.production.ui.client.view.widget.filerevisiondialog;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.dom.client.Element;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.proxycreatordialog.AutoProxyTypeComboBox;
import fr.hd3d.common.ui.client.widget.proxyplayer.ProxyPlayer;


public class FileRevisionDialog extends Dialog
{
    private static FileRevisionDialog fileRevisionDialog;
    private final FileRevisionDialogModel model;

    private final BaseGrid<ProxyModelData> proxyGrid;

    private final LayoutContainer imgPanel;
    private AutoProxyTypeComboBox proxyTypeCombobox = new AutoProxyTypeComboBox();
    private final BaseGrid<FileRevisionModelData> fileGrid;

    private FileRevisionDialog()
    {
        model = new FileRevisionDialogModel();
        proxyGrid = new BaseGrid<ProxyModelData>(this.model.getProxyStore(), getColumnModelProxyStore());
        fileGrid = new BaseGrid<FileRevisionModelData>(this.model.getFileRevisionStore(), getColumnModelFileStore());
        imgPanel = new LayoutContainer();
        imgPanel.setLayoutOnChange(true);
        this.setButtons(Dialog.CLOSE);
        setStyles();
        setListeners();
    }

    private ColumnModel getColumnModelFileStore()
    {
        ArrayList<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        GridUtils.addColumnConfig(columns, FileRevisionModelData.KEY_FIELD, "File name", 200);
        GridUtils.addColumnConfig(columns, FileRevisionModelData.VARIATION_FIELD, "variation", 200);
        GridUtils.addColumnConfig(columns, FileRevisionModelData.RELATIVE_PATH_FIELD, "relative path name", 200);
        GridUtils.addColumnConfig(columns, FileRevisionModelData.REVISION_FIELD, "revision", 200);

        return new ColumnModel(columns);
    }

    private void setListeners()
    {
        proxyTypeCombobox.addSelectionChangedListener(new SelectionChangedListener<ProxyTypeModelData>() {

            @Override
            public void selectionChanged(SelectionChangedEvent<ProxyTypeModelData> se)
            {
                ProxyTypeModelData proxyType = se.getSelectedItem();
                if (proxyType == null)
                {
                    return;
                }
                model.reloadProxyTypeStore(proxyType.getId());
            }

        });
        proxyGrid.addListener(Events.RowClick, new Listener<GridEvent<ProxyModelData>>() {

            public void handleEvent(GridEvent<ProxyModelData> be)
            {
                if (be.getRowIndex() == -1)
                {
                    return;
                }
                ProxyModelData proxy = be.getGrid().getStore().getAt(be.getRowIndex());
                imgPanel.removeAll();
                ProxyPlayer pp = new ProxyPlayer(proxy);
                pp.setPixelSize(720, 576);
                imgPanel.add(pp);
                // imgPanel.getElement().setInnerHTML(
                // "<center><img width='600px'  src='" + proxy.getDownloadPath() + "'/></center>");
                // imgPanel.removeAll();
                // imgPanel.add(new ProxyPlayer(proxy));
            }

        });

        fileGrid.addListener(Events.RowClick, new Listener<GridEvent<FileRevisionModelData>>() {

            public void handleEvent(GridEvent<FileRevisionModelData> be)
            {
                if (be.getRowIndex() == -1)
                {
                    return;
                }
                FileRevisionModelData fileRevision = be.getGrid().getStore().getAt(be.getRowIndex());
                model.setFileRevisionSelected(fileRevision.getId());
            }

        });
    }

    private void setStyles()
    {
        this.setPixelSize(902, 670);
        this.setLayout(new FitLayout());

        VerticalPanel globalContainer = new VerticalPanel();
        globalContainer.setSpacing(5);
        globalContainer.setAutoHeight(true);
        globalContainer.setAutoWidth(true);

        createFilePanel(globalContainer);

        createProxyPanel(globalContainer);

        this.add(globalContainer);
        this.setHideOnButtonClick(true);
    }

    private void createFilePanel(VerticalPanel globalContainer)
    {
        LayoutContainer filePanel = new LayoutContainer();
        filePanel.setLayout(new FitLayout());
        filePanel.setHeight(50);
        filePanel.setWidth(880);

        fileGrid.setBorders(true);
        filePanel.add(fileGrid);

        globalContainer.add(filePanel);
    }

    private void createProxyPanel(VerticalPanel globalContainer)
    {
        HorizontalPanel proxyPanel = new HorizontalPanel();

        proxyPanel.setHeight(600);
        proxyPanel.setWidth(900);
        proxyPanel.setScrollMode(Scroll.AUTO);

        VerticalPanel panel = new VerticalPanel();
        panel.setSpacing(5);

        LayoutContainer topPanel = new LayoutContainer();
        proxyTypeCombobox.setStore(this.model.getProxyTypeStore());
        topPanel.add(proxyTypeCombobox);
        panel.add(topPanel);

        proxyGrid.setAutoHeight(true);
        proxyGrid.setAutoWidth(true);
        proxyGrid.setBorders(true);
        panel.add(proxyGrid);

        proxyPanel.add(panel);

        imgPanel.setBorders(false);
        imgPanel.setLayout(new FitLayout());

        proxyPanel.add(imgPanel);

        globalContainer.add(proxyPanel);
    }

    private ColumnModel getColumnModelProxyStore()
    {
        ArrayList<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        GridUtils.addColumnConfig(columns, ProxyModelData.FILENAME_FIELD, "proxy file name");

        return new ColumnModel(columns);
    }

    public static FileRevisionDialog getInstance()
    {
        if (fileRevisionDialog == null)
        {
            fileRevisionDialog = new FileRevisionDialog();
        }
        return fileRevisionDialog;
    }

    public void show(List<Long> fileRevisions, String workObjectName, String approvalNoteTypeName) throws Hd3dException
    {
        this.reset();
        this.model.reset();
        this.model.setFileRevisions(fileRevisions);
        this.model.reload();
        setHeading(workObjectName + " (" + approvalNoteTypeName + ")");
        super.show();
    }

    private void reset()
    {
        this.proxyTypeCombobox.reset();
        Element el = imgPanel.getElement();
        if (el != null)
        {
            el.setInnerHTML("");
        }
    }
}
