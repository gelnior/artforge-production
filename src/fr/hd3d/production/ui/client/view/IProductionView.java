package fr.hd3d.production.ui.client.view;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.mainview.IMainView;


public interface IProductionView extends IMainView
{
    public void refreshTagEditor(List<Hd3dModelData> arrayList);

    public void initWidgets();

    public void selectLastProject();

    public void idleConstituentView();

    public void unidleShotView();

    public void idleShotView();

    public void unidleConstituentView();

    public void idlePlaylistView();

    public void unidlePlaylistView();

    public void unidleStatView();

    public void idleStatView();

    public void setControllers();

    public String getFavValue(String key);

    public void selectTab(String lastSelectedTab);

    public void enablePlaylistUi(boolean enable);

    public void enableStatUi(boolean enable);

    public void selectProject(Long valueOf);

    public void toggleAutoSave();

    public void idleStatDetailsView();

    public void unidleStatDetailsView();

}
