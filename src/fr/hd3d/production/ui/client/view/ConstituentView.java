package fr.hd3d.production.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.StoreFilterField;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.model.ConstituentModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerSelectionModel;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;
import fr.hd3d.production.ui.client.controller.ConstituentController;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.widget.approvalnote.ApprovalUtils;


/**
 * ConstituentView displays constituent data for validation.
 * 
 * @author HD3D
 */
public class ConstituentView extends ApprovedWorkObjectView
{
    /** View name to tab distinction purpose. */
    public static final String NAME = "constituent";

    /** Model that handles displayed data. */
    private final ConstituentModel model = new ConstituentModel();
    /** Controller that handles constituent view related events. */
    private final ConstituentController controller = new ConstituentController(model, this);

    /** Constituent navigation tree. */
    private final ConstituentTree constituentTree = new ConstituentTree();

    /**
     * Default constructor : initialize widgets then register controllers.
     */
    public ConstituentView()
    {
        super();

        this.setSheetCombo();
        this.setTree();
        this.configureExplorer();

        // this.controller.addChild(this.identityPortal.getController());
        this.controller.addChild(this.explorer.getController());
        this.setTreeFilter();
        setPlayListCreationActions();
    }

    /**
     * @return view controller
     */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Refresh navigation tree for given project.
     * 
     * @param project
     *            The project used to set data inside tree.
     */
    public void reloadTree(ProjectModelData project)
    {
        this.constituentTree.setCurrentProject(project);
    }

    /**
     * Idle every widget contains inside view and mask view controller.
     */
    @Override
    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    /**
     * Unidle every widget contains inside view and unmask view controller.
     */
    @Override
    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    /** Configure sheet combo box and register its store to model. */
    private void setSheetCombo()
    {
        this.model.setSheetStore(this.sheetCombo.getServiceStore());
        this.sheetCombo.setType(ESheetType.PRODUCTION);
        this.sheetCombo.addAvailableEntity(ConstituentModelData.SIMPLE_CLASS_NAME);
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                WorkObjectEvents.SHEET_CHANGED));
    }

    /**
     * Add navigation tree to west panel.
     */
    private void setTree()
    {
        this.constituentTree.setBodyBorder(false);
        this.constituentTree.setBorders(false);
        this.addToWestPanel(constituentTree);
    }

    private void setTreeFilter()
    {
        StoreFilterField<RecordModelData> storeFilter = new StoreFilterField<RecordModelData>() {
            @Override
            protected boolean doSelect(Store<RecordModelData> store, RecordModelData parent, RecordModelData record,
                    String property, String filter)
            {
                if (constituentTree.getStore().getLoader().hasChildren(record))
                {
                    return true;
                }
                String name = record.get(ConstituentModelData.CONSTITUENT_LABEL);
                name = name.toLowerCase();
                if (name.startsWith(filter.toLowerCase()))
                {
                    return true;
                }
                return false;
            }
        };
        storeFilter.bind(constituentTree.getStore());
        storeFilter.setWidth(45);
        // this.westPanel.getHeader().addTool(storeFilter);
    }

    /** Configure explorer and sheet editor for constituent browsing. */
    private void configureExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        String entityName = ConstituentModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);

        this.model.setExplorerStore(explorer.getStore(), ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase());
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());
    }

    // public Record getTreeConstituentRecord(Long id)
    // {
    // RecordModelData constituent = this.constituentTree.getStore().findModel(Hd3dModelData.ID_FIELD, id);
    // return this.constituentTree.getStore().getRecord(constituent);
    // }
    //
    // public void updateTreeConstituentRecord(Record constituent)
    // {
    // this.constituentTree.getStore().update((RecordModelData) constituent.getModel());
    // }

    /**
     * Adds CreatePlayListFromSelectionButton button to explorerBar.
     */
    private void setPlayListCreationActions()
    {
        if (!ProductionModel.isEnablePlaylistUi())
            return;

        Button createPlaylistButton = new Button();
        createPlaylistButton.setIcon(ProductionImages.getPlaylistSymbol());
        createPlaylistButton.getElement().setNodeValue(ExplorateurCSS.EDIT_FOLDER);
        createPlaylistButton.setToolTip("Create Playlist");
        createPlaylistButton.addListener(Events.Select, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                List<Hd3dModelData> selectedModels = explorer.getSelectedModels();
                if (selectedModels == null || selectedModels.isEmpty())
                    return;

                Long taskTypeId = null;
                String columnId = "";
                final ExplorerSelectionModel sm = (ExplorerSelectionModel) explorer.getGrid().getSelectionModel();
                if (sm != null)
                {
                    final int columnIndex = sm.getSelectionColumnIndex();
                    columnId = explorer.getGrid().getColumnModel().getColumnId(columnIndex);
                    taskTypeId = ApprovalUtils.getTaskTypeForColumn(columnId);
                }

                List<Long> collectionIds = new ArrayList<Long>();
                FastMap<Long> frCollection = new FastMap<Long>();

                for (Hd3dModelData model : selectedModels)
                {
                    if (ApprovalUtils.getLastApprovalNote(model, columnId).getFileRevision() != null
                            && !ApprovalUtils.getLastApprovalNote(model, columnId).getFileRevision().isEmpty())
                        frCollection.put(model.getId().toString(), ApprovalUtils.getLastApprovalNote(model, columnId)
                                .getFileRevision().get(0));
                    collectionIds.add(model.getId());
                }
                ProxySelectionDisplayer.show(collectionIds, ConstituentModelData.SIMPLE_CLASS_NAME, taskTypeId,
                        frCollection);
            }
        });
        explorerBar.insert(new SeparatorToolItem(), 7);
        this.explorerBar.insert(createPlaylistButton, 8);
    }

    /**
     * Enable navigation Tree.
     */
	public void enableTree() 
	{
		this.constituentTree.enable();
	}

    /**
     * Disable navigation Tree.
     */
	public void disableTree() 
	{
		this.constituentTree.disable();
	}
}
