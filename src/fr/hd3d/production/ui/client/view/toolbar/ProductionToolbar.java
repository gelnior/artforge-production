package fr.hd3d.production.ui.client.view.toolbar;

import java.util.List;

import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.widget.ProjectCombobox;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AutoSaveButton;
import fr.hd3d.common.ui.client.widget.tageditor.TagEditor;


/**
 * Production view toolbar.
 */
public class ProductionToolbar extends ToolBar
{

    /** Combo box needed to select project. */
    private ProjectCombobox projectCombo;
    /** Tools needed to set tags on shots and constituents. */
    private TagEditor tagEditor = new TagEditor();

    /** Explorer auto-save button. */
    private AutoSaveButton autoSaveButton = new AutoSaveButton();

    /**
     * Default Constructor
     */
    public ProductionToolbar()
    {
        this.projectCombo = new ProjectCombobox();

        this.add(projectCombo);

        this.add(new SeparatorToolItem());
        this.add(autoSaveButton);
        this.add(new FillToolItem());
        this.add(tagEditor);
        this.tagEditor.hide();

        this.setStyles();
        this.registerListeners();
    }

    public void refreshTagEditor(List<Hd3dModelData> selection)
    {
        this.tagEditor.refreshSelection(selection);
    }

    /** Set toolbar styles such as padding. */
    private void setStyles()
    {
        this.projectCombo.setWidth(200);
        this.setStyleAttribute("padding", "5px");
    }

    /** Register widget listeners. */
    private void registerListeners()
    {
        EventDispatcher.get().addController(tagEditor.getController());

        projectCombo.addSelectionChangedListener(new EventSelectionChangedListener<ProjectModelData>(
                CommonEvents.PROJECT_CHANGED));
    }

    /**
     * In project combobox select project which were selected last time user select a projet in project combo box.
     */
    public void selectLastSelectedProject()
    {
        this.projectCombo.setData();
    }

    /**
     * In project combobox select project of which ID is equal to <i>projectId</i>.
     */
    public void selectProject(Long projectId)
    {
        this.projectCombo.setValueById(projectId);
    }

    /**
     * Press toggle button if it is not pressed.
     */
    public void toggleAutoSave()
    {
        this.autoSaveButton.toggle();
    }
}
