package fr.hd3d.production.ui.client.view.column;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.constraint.IEditorProvider;


/**
 * Field used by constraint panel to filter specific data used in production app.
 * 
 * @author HD3D
 */
public class ConstraintEditorProvider implements IEditorProvider
{
    /**
     * @return Constraint editor corresponding to editor string set by user.
     */
    public Field<?> getEditor(String name, EConstraintOperator operator, BaseModelData baseModel)
    {
        if (Editor.DURATION.equals(name))
        {
            DurationField field = new DurationField();
            return field;
        }
        return null;
    }

    public String getItemConstraintColumnName(String name)
    {
        return null;
    }
}
