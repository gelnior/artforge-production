package fr.hd3d.production.ui.client.view.column;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.widget.AutoCompleteModelCombo;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.HoursRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.RatingCellRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ThumbnailRenderer;
import fr.hd3d.common.ui.client.widget.grid.editor.ModelDataComboBoxEditor;
import fr.hd3d.production.ui.client.view.DisplayMode;
import fr.hd3d.production.ui.client.view.explorer.renderer.ApprovalNoteCompactRenderer;
import fr.hd3d.production.ui.client.view.explorer.renderer.ApprovalNoteRenderer;
import fr.hd3d.production.ui.client.view.explorer.renderer.LastPublishRenderRenderer;
import fr.hd3d.production.ui.client.view.explorer.renderer.TaskOkRenderer;


public class ProductionColumnInfoProvider extends BaseColumnInfoProvider
{

    private Grid<Hd3dModelData> grid;
    private DisplayMode mode;

    public ProductionColumnInfoProvider(Grid<Hd3dModelData> grid, DisplayMode mode)
    {
        super();
        this.grid = grid;
        this.mode = mode;
    }

    @Override
    public GridCellRenderer<Hd3dModelData> getRenderer(String name)
    {
        GridCellRenderer<Hd3dModelData> renderer = super.getRenderer(name);

        if (renderer == null)
        {
            if (name.equals(Renderer.ACTITVITY_DURATION) || (name.equals(Renderer.HOURS_DURATION)))
            {
                renderer = new HoursRenderer<Hd3dModelData>();
            }
            else if (name.equals(Renderer.NB_OK_TASKS))
            {
                renderer = new TaskOkRenderer<Hd3dModelData>();
            }
            else if (name.equals(Renderer.RATING))
            {
                renderer = new RatingCellRenderer();
            }
            else if (name.equals(Renderer.THUMBNAIL))
            {
                renderer = new ThumbnailRenderer<Hd3dModelData>();
            }
            else if (name.equals(Renderer.APPROVAL))
            {
                renderer = new ApprovalNoteRenderer(grid, mode);
            }
            else if (name.equals(Renderer.APPROVAL_COMPACT))
            {
                renderer = new ApprovalNoteCompactRenderer(grid);
            }
            else if (name.equals(Renderer.LAST_PUBLISHED))
            {
                renderer = new LastPublishRenderRenderer();
            }
        }
        return renderer;
    }

    @Override
    public CellEditor getEditor(String name, IColumn column)
    {
        CellEditor editor = super.getEditor(name, column);

        if (editor == null)
        {
            if (name.toLowerCase().equals(PersonModelData.SIMPLE_CLASS_NAME.toLowerCase()))
            {
                final AutoCompleteModelCombo<PersonModelData> combo = new AutoCompleteModelCombo<PersonModelData>(
                        new PersonReader());
                combo.setOrderColumn(PersonModelData.PERSON_FIRST_NAME_FIELD);
                combo.setFilterColumn(PersonModelData.PERSON_FIRST_NAME_FIELD);

                ModelDataComboBoxEditor<PersonModelData> ce = new ModelDataComboBoxEditor<PersonModelData>(combo) {

                    public Object preProcessValue(Object value)
                    {
                        PersonModelData object;

                        if (value == null)
                        {
                            return null;
                        }
                        else if (value instanceof PersonModelData)
                        {
                            return value;
                        }
                        else
                        {
                            PersonReader reader = new PersonReader();
                            object = reader.readObject((JSONObject) value, PersonModelData.getModelType());

                            return object;
                        }
                    }

                    public Object postProcessValue(Object value)
                    {
                        return value;
                    }
                };

                return ce;
            }
        }

        return editor;
    }
}
