package fr.hd3d.production.ui.client.view.column;

import java.util.List;

import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.ApprovalField;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.IdentitySheetEditorProvider;


/**
 * Editor provider provides edition fields for identity sheet widget.
 * 
 * @author HD3D
 */
public class ProductionEditorProvider extends IdentitySheetEditorProvider
{
    /**
     * @param name
     *            The editor name.
     * @return Field corresponding to <i>name</i>.
     */
    @SuppressWarnings("unchecked")
    public Field<?> getEditor(String name, Object value, ItemModelData itemModelData)
    {
        if (Renderer.ACTIVITIES_DURATION.equals(name))
        {
            DurationField field = new DurationField();
            if (value != null)
                field.setValue(((Number) value));
            return field;
        }
        else if (Renderer.APPROVAL.equals(name))
        {
            ApprovalField field = new ApprovalField();
            if (value != null && value instanceof List)
                field.setValue(((List<ApprovalNoteModelData>) value));
            return field;
        }
        return super.getEditor(name, value, itemModelData);
    }
}
