package fr.hd3d.production.ui.client.view;

public class DisplayMode
{
    public static final int NORMAL = 0;

    public static final int PROXY_MODE = 1;

    private int mode;

    /**
     * Default constructor.
     * 
     * @param mode
     *            mode to display (static in this class);
     */
    public DisplayMode(int mode)
    {
        this.mode = mode;
    }

    /**
     * Return the mode.
     * 
     * @return the mode
     */
    public int getMode()
    {
        return mode;
    }

    /**
     * Set the mode.
     * 
     * @param mode
     *            the new mode.
     */
    public void setMode(int mode)
    {
        this.mode = mode;
    }

}
