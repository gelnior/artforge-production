package fr.hd3d.production.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.model.ShotModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.widget.FilterStoreField;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerSelectionModel;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;
import fr.hd3d.production.ui.client.controller.ShotController;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.widget.approvalnote.ApprovalUtils;


public class ShotView extends ApprovedWorkObjectView
{
    public static final String NAME = "shot";

    private ShotTree shotTree = new ShotTree();
    private ShotModel model = new ShotModel();

    private ShotController controller = new ShotController(model, this);

    /**
     * Default constructor : initialize widgets then register controllers.
     */
    public ShotView()
    {
        super();

        this.setSheetCombo();
        this.setTree();
        this.setTreeFilter();
        this.configureExplorer();

        // this.controller.addChild(this.identityPortal.getController());
        this.controller.addChild(this.explorer.getController());
    }

    public MaskableController getController()
    {
        return this.controller;
    }

    public void reloadTree(ProjectModelData project)
    {
        this.shotTree.setCurrentProject(project);
    }

    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    private void setSheetCombo()
    {
        this.model.setSheetStore(this.sheetCombo.getServiceStore());
        this.sheetCombo.setType(ESheetType.PRODUCTION);
        this.sheetCombo.addAvailableEntity(ShotModelData.SIMPLE_CLASS_NAME);
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                WorkObjectEvents.SHEET_CHANGED));
    }

    private void setTree()
    {
        this.shotTree.setBodyBorder(false);
        this.shotTree.setBorders(false);
        this.addToWestPanel(shotTree);
    }

    private void setTreeFilter()
    {
        FilterStoreField<RecordModelData> filter = new FilterStoreField<RecordModelData>(shotTree.getStore());
        filter.setFieldLabel(ShotModelData.SHOT_LABEL);
        filter.setWidth(45);
        // this.explorerBar.add(new FillToolItem());
        // this.explorerBar.add(filter);
    }

    private void configureExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        String entityName = ShotModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);

        this.model.setExplorerStore(explorer.getStore(), ShotModelData.SIMPLE_CLASS_NAME.toLowerCase());
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());

        setPlayListCreationActions();
    }

    /**
     * Adds CreatePlayListFromSelectionButton button to explorerBar.
     */
    private void setPlayListCreationActions()
    {
        if (!ProductionModel.isEnablePlaylistUi())
            return;

        explorerBar.insert(new SeparatorToolItem(), 5);
        Button createPlaylistButton = new Button();
        createPlaylistButton.setIcon(ProductionImages.getPlaylistSymbol());
        createPlaylistButton.getElement().setNodeValue(ExplorateurCSS.EDIT_FOLDER);
        createPlaylistButton.setToolTip("Create Playlist");
        createPlaylistButton.addListener(Events.Select, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                List<Hd3dModelData> selectedModels = explorer.getSelectedModels();
                if (selectedModels == null || selectedModels.isEmpty())
                    return;

                Long taskTypeId = null;
                String columnId = "";
                final ExplorerSelectionModel sm = (ExplorerSelectionModel) explorer.getGrid().getSelectionModel();
                if (sm != null)
                {
                    final int columnIndex = sm.getSelectionColumnIndex();
                    columnId = explorer.getGrid().getColumnModel().getColumnId(columnIndex);
                    taskTypeId = ApprovalUtils.getTaskTypeForColumn(columnId);
                }

                List<Long> collectionIds = new ArrayList<Long>();
                FastMap<Long> frCollection = new FastMap<Long>();

                for (Hd3dModelData model : selectedModels)
                {
                    ApprovalNoteModelData lastApprovalNote = ApprovalUtils.getLastApprovalNote(model, columnId);
                    if (lastApprovalNote != null && lastApprovalNote.getFileRevision() != null
                            && !lastApprovalNote.getFileRevision().isEmpty())
                        frCollection.put(model.getId().toString(), lastApprovalNote.getFileRevision().get(0));
                    collectionIds.add(model.getId());
                }
                ProxySelectionDisplayer.show(collectionIds, ShotModelData.SIMPLE_CLASS_NAME, taskTypeId, frCollection);
            }
        });
        this.explorerBar.insert(createPlaylistButton, 6);
    }

    /**
     * Enable navigation Tree.
     */
	public void enableTree() 
	{
		this.shotTree.enable();
	}

    /**
     * Disable navigation Tree.
     */
	public void disableTree() 
	{
		this.shotTree.disable();
	}
}
