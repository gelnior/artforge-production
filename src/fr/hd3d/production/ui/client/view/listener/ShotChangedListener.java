package fr.hd3d.production.ui.client.view.listener;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;


public class ShotChangedListener extends SelectionChangedListener<RecordModelData>
{

    @Override
    public void selectionChanged(SelectionChangedEvent<RecordModelData> se)
    {
        RecordModelData item = se.getSelectedItem();

        if (item != null && item.getClassName().equals(ShotModelData.CLASS_NAME))
        {
            ShotModelData shot = new ShotModelData();
            Hd3dModelData.copy(item, shot);

            AppEvent event = new AppEvent(CommonEvents.SHOTS_SELECTED);
            event.setData(shot);
            EventDispatcher.forwardEvent(event);
        }

        if (item != null && item.getClassName().equals(SequenceModelData.CLASS_NAME))
        {
            SequenceModelData sequence = new SequenceModelData();
            Hd3dModelData.copy(item, sequence);

            AppEvent event = new AppEvent(CommonEvents.SEQUENCES_SELECTED);
            event.setData(sequence);
            EventDispatcher.forwardEvent(event);
        }
    }

}
