package fr.hd3d.production.ui.client.view.listener;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;


public class ConstituentChangedListener extends SelectionChangedListener<RecordModelData>
{

    @Override
    public void selectionChanged(SelectionChangedEvent<RecordModelData> se)
    {
        RecordModelData item = se.getSelectedItem();

        if (item != null && item.getClassName().equals(ConstituentModelData.CLASS_NAME))
        {
            ConstituentModelData constituent = new ConstituentModelData();
            Hd3dModelData.copy(item, constituent);

            AppEvent event = new AppEvent(CommonEvents.CONSTITUENTS_SELECTED);
            event.setData(constituent);
            EventDispatcher.forwardEvent(event);
        }

        if (item != null && item.getClassName().equals(CategoryModelData.CLASS_NAME))
        {
            CategoryModelData category = new CategoryModelData();
            Hd3dModelData.copy(item, category);

            AppEvent event = new AppEvent(CommonEvents.CATEGORIES_SELECTED);
            event.setData(category);
            EventDispatcher.forwardEvent(event);
        }
    }

}
