package fr.hd3d.production.ui.client.view.listener;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerColumnConfig;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGrid;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.Hd3dCheckColumnConfig;
import fr.hd3d.production.ui.client.config.ProductionConfig;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.view.explorer.renderer.ApprovalNoteRenderer;


/**
 * Double click listener dedicated to explorer grid. Handle double click on validations and checkbox columns.
 * 
 * @author HD3D
 */
public class ValidationListener implements Listener<BaseEvent>
{
    /** The explorer grid. */
    private ExplorerGrid grid;
    /** The explorer store. */
    private ServicesPagingStore<Hd3dModelData> explorerStore;

    /**
     * Default constructor
     * 
     * @param grid
     *            Grid which has been double clicked.
     * @param explorerStore
     *            Data store of the grid which has been double clicked.
     */
    public ValidationListener(ExplorerGrid grid, ServicesPagingStore<Hd3dModelData> explorerStore)
    {
        this.grid = grid;
        this.explorerStore = explorerStore;
    }

    /**
     * Set grid on which double click occur.
     * 
     * @param grid
     *            the grid to set.
     * */
    public void setGrid(ExplorerGrid grid)
    {
        this.grid = grid;
    }

    /**
     * Check if the column clicked is a validation column or a check box column.
     * 
     * @param be
     *            Double click event.
     */
    @SuppressWarnings("unchecked")
    public void handleEvent(BaseEvent be)
    {
        GridEvent<Hd3dModelData> ge = (GridEvent<Hd3dModelData>) be;

        ColumnConfig column = this.grid.getColumnModel().getColumn(ge.getColIndex());
        Object renderer = column.getRenderer();

        if (renderer instanceof ApprovalNoteRenderer)
        {
            this.onValidationCellDoubleClick(ge, column);
        }
        else if (column instanceof Hd3dCheckColumnConfig)
        {
            this.onCheckBoxCellDoubleClick(ge, column);
        }
    }

    /**
     * When validation cell is double clicked the validation OK editor is opened. If the shift key is pressed, the cell
     * is validated. If alt-shift keys are pressed, the NOK validation editor is displayed.
     * 
     * @param ge
     *            Double click event.
     * @param column
     *            Column description.
     */
    private void onValidationCellDoubleClick(GridEvent<Hd3dModelData> ge, ColumnConfig column)
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_ADD_CLICKED);
        event.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, column.getDataIndex());
        event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, column.getHeader());
        event.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, ((ExplorerColumnConfig) column).getParameter());

        // ALT + Shift + double click.
        if (ge.isShiftKey() && ge.isAltKey())
        {
            event.setData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR, EApprovalNoteStatus.RETAKE.toString());
            event.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, Boolean.FALSE);
            EventDispatcher.forwardEvent(event);
        }
        // Shift + double click.
        else if (ge.isShiftKey())
        {
            event.setData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR, EApprovalNoteStatus.OK.toString());
            event.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, Boolean.FALSE);
            EventDispatcher.forwardEvent(event);
        }
        // double click.
        else
        {
            event.setData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR, EApprovalNoteStatus.OK.toString());
            event.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, Boolean.TRUE);
            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * When check box column is double clicked, the value is change to opposite.
     * 
     * @param ge
     *            Double click event.
     * @param column
     *            Column description.
     */
    private void onCheckBoxCellDoubleClick(GridEvent<Hd3dModelData> ge, ColumnConfig column)
    {
        Hd3dModelData modelClicked = (Hd3dModelData) ge.getModel();
        Boolean bool = modelClicked.get(column.getDataIndex());
        if (bool == null)
            bool = false;
        Record record = explorerStore.getRecord(modelClicked);

        record.set(column.getDataIndex(), !bool);
    }

}
