package fr.hd3d.production.ui.client.view;

import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;


/**
 * Images used by production application widgets.
 * 
 * @author HD3D
 */
public class ProductionImages
{

    public final static String PLAYLIST_ICON_URL = "images/playlists/playlist.png";

    public final static Image playlistSymbol()
    {
        Image playlistImage = new Image();
        playlistImage.setUrl(PLAYLIST_ICON_URL);

        return playlistImage;
    }

    public static AbstractImagePrototype getPlaylistSymbol()
    {
        return IconHelper.create(PLAYLIST_ICON_URL);
    }
}
