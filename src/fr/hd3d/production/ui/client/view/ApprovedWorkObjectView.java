package fr.hd3d.production.ui.client.view;

import java.util.List;

import com.extjs.gxt.ui.client.event.Events;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxyDisplayer;
import fr.hd3d.common.ui.client.widget.workobject.browser.view.WorkObjectView;
import fr.hd3d.production.ui.client.view.column.ProductionColumnInfoProvider;
import fr.hd3d.production.ui.client.view.listener.ValidationListener;
import fr.hd3d.production.ui.client.view.widget.filerevisiondialog.FileRevisionDialog;


public class ApprovedWorkObjectView extends WorkObjectView
{
    
    /** Double click listener to enhance validation usage. */
    private ValidationListener clicListener;

    private DisplayMode mode;

    public ApprovedWorkObjectView()
    {
        super();
    }

    /** When grid is rebuilt double click listeners are added to grid if they are no more set on it. */
    @Override
    public void setListeners()
    {
        if (!this.explorer.getGrid().getListeners(Events.CellDoubleClick).contains(this.clicListener))
        {
            if (this.clicListener == null)
                this.clicListener = new ValidationListener(explorer.getGrid(), this.explorer.getStore());
            this.explorer.getGrid().addListener(Events.CellDoubleClick, clicListener);
        }
    }

    @Override
    protected void setFieldProviders()
    {
        this.mode = new DisplayMode(DisplayMode.NORMAL);
        this.explorer.setColumnInfoProvider(new ProductionColumnInfoProvider(explorer.getGrid(), mode));
        this.explorer.setConstraintEditorProvider(new ProductionConstraintEditorProvider());
    }

    public void displayProxies(List<Hd3dModelData> models, String boundEntityName)
    {
        try
        {
            if (MainModel.getCurrentProject() != null)
            {
                ProjectModelData project = MainModel.getCurrentProject();
                ProxyDisplayer.show(project.getId(), models, boundEntityName);
            }
        }
        catch (Hd3dException he)
        {
            he.print();
        }
    }

    @Override
    public void setExplorerToolBar()
    {
        super.setExplorerToolBar();
    }

    public void setDisplayMode(int mode)
    {
        this.mode.setMode(mode);
    }

    public int getDisplayMode()
    {
        return this.mode.getMode();
    }

    public void showFileRevisionDialog(List<Long> fileRevisionId, String workObjectName, String approvalNoteTypeName)
    {
        try
        {
            FileRevisionDialog.getInstance().show(fileRevisionId, workObjectName, approvalNoteTypeName);
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }
}
