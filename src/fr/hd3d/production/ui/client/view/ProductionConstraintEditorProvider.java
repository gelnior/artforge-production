package fr.hd3d.production.ui.client.view;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.widget.MultiNoteStatusComboBox;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;
import fr.hd3d.production.ui.client.view.column.ConstraintEditorProvider;


public class ProductionConstraintEditorProvider extends ConstraintEditorProvider
{

    @Override
    public Field<?> getEditor(String name, EConstraintOperator operator, BaseModelData baseModel)
    {
        if (Editor.APPROVAL.equals(name))
        {
            if (EConstraintOperator.in == operator)
                return new MultiNoteStatusComboBox();
            else
                return new NoteStatusComboBox(true);
        }

        return super.getEditor(name, operator, baseModel);
    }

    public String getItemConstraintColumnName(String name)
    {
        if (Editor.APPROVAL.equals(name))
        {
            return ApprovalNoteModelData.STATUS_FIELD;
        }

        return super.getItemConstraintColumnName(name);
    }
}
