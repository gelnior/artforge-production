package fr.hd3d.production.ui.client.view;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;
import fr.hd3d.production.ui.client.controller.AddEditController;


/**
 * Customized FormDialog that handles creation of a new Edit on Playlist.
 * 
 * @author guillaume-maucomble
 * @version 1.1
 * @since 1.0
 */
public class AddEditDialog extends FormDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Controller for this view */
    @SuppressWarnings("unused")
    private AddEditController controller;

    private Field<String> fileRevision;
    private Field<String> name;
    private Field<String> srcin;
    private Field<String> srcout;
    private Field<String> recin;
    private Field<String> recout;

    /**
     * Customized FormDialog that handles creation of a new Edit on Playlist. The fields are all required.
     * 
     * @param okEvent
     *            EventType to be sent when Ok button is pressed
     * @param header
     *            Sting displayed on the dialog title bar
     */
    public AddEditDialog(EventType okEvent, String header)
    {
        super(okEvent, header);
        controller = new AddEditController(this);
        this.setWidth(330);
        addFields();
    }

    @Override
    public void show()
    {
        super.show();
        this.panel.reset();
        setFocusWidget(name);
    }

    @Override
    protected void onOkClicked() throws Hd3dException
    {
        AppEvent event = new AppEvent(okEvent);
        event.setData("name", name.getValue());
        event.setData("srcIn", srcin.getValue());
        event.setData("srcOut", srcout.getValue());
        event.setData("recIn", recin.getValue());
        event.setData("recOut", recout.getValue());
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Creates and adds fields to panel.
     */
    private void addFields()
    {
        fileRevision = createField(COMMON_CONSTANTS.FileRevision());

        name = createField(COMMON_CONSTANTS.Name());
        srcin = createField(COMMON_CONSTANTS.SrcIn());
        srcout = createField(COMMON_CONSTANTS.SrcOut());
        recin = createField(COMMON_CONSTANTS.RecIn());
        recout = createField(COMMON_CONSTANTS.RecOut());

        addField(name);
        addField(fileRevision);
        addField(srcin);
        addField(srcout);
        addField(recin);
        addField(recout);
    }

    /**
     * Returns a field with input name. The field is set to match new edit requirements. TODO Validator, Timecode
     * 
     * @param name
     *            String defining the name of the field
     * @return created field
     */
    private Field<String> createField(String name)
    {
        TextField<String> field = new TextField<String>();
        field.setFieldLabel(name);
        field.setEmptyText(name);
        field.setAllowBlank(false);
        field.setValidateOnBlur(true);
        field.setValidator(new Validator() {

            public String validate(Field<?> field, String value)
            {
                return null;
            }
        });
        return field;
    }

}
