package fr.hd3d.production.ui.client.view.explorer.aggregation;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.AggregationRenderer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;


/**
 * Count how many work objects are validated then display it in this way : count / total.
 * 
 * @author HD3D
 */
public class ApprovalNoteAggregationRenderer implements AggregationRenderer<Hd3dModelData>
{

    public Object render(Number value, int colIndex, Grid<Hd3dModelData> grid, ListStore<Hd3dModelData> store)
    {
        int approved = 0;
        List<Hd3dModelData> models = store.getModels();
        ColumnConfig column = grid.getColumnModel().getColumn(colIndex);
        if (column != null)
        {
            String property = column.getDataIndex();
            for (Hd3dModelData model : models)
            {
                Object val = model.get(property);
                if (val instanceof List)
                {
                    List<ApprovalNoteModelData> objs = model.get(property);
                    if (CollectionUtils.isNotEmpty(objs))
                    {
                        if (objs.get(0).getStatus().equals(ApprovalNoteModelData.STATUS_OK))
                        {
                            approved++;
                        }
                    }
                }
            }
        }

        return "<div style=\"text-align:center;\">" + approved + " / " + models.size() + "</div>";
    }
}
