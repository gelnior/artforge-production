package fr.hd3d.production.ui.client.view.explorer.renderer;

import java.util.Date;

import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Render that shows the minimum of data on displayed approval note (date and status).
 * 
 * @author HD3D
 */
public class ApprovalNoteCompactRenderer extends ApprovalNoteRenderer
{

    /**
     * Default constructor : register grid.
     * 
     * @param grid
     *            The grid on which the renderer will apply.
     */
    public ApprovalNoteCompactRenderer(Grid<Hd3dModelData> grid)
    {
        super(grid);
    }

    /**
     * @return HTML representation of compact note.
     */
    @Override
    protected String getRenderedValue(Date date, String text, int count, String color, String status,
            String borderColor, boolean hasfileRevision)
    {
        String stringDate = DateFormat.FRENCH_DATE.format(date);

        String html = getOpenDivTagString(text, stringDate, color, borderColor);

        Element span = DOM.createSpan();
        span.addClassName("approval-title");
        span.setInnerHTML(stringDate);
        html += span.getString();// + " (" + count + ")";
        // span.setInnerHTML(" " + status);
        // html += span.getString();
        if (hasfileRevision)
        {
            html += Hd3dImages.getFileIcon().createImage();
        }
        html += "</div>";

        return html;
    }

    /**
     * @param text
     *            The text to display inside tool tip.
     * @param stringDate
     *            The date to display.
     * @param color
     *            The background color.
     * @return The string corresponding to div tag opening. It settles styles and tooltip for this rendering.
     */
    protected String getOpenDivTagString(String text, String stringDate, String color, String borderColor)
    {
        if (text != null)
        {
            if (!Util.isEmptyString(borderColor))
                return "<div qtip='" + text.replace("'", "&#39;").replace("\n", "<br />") + "' qtitle='" + stringDate
                        + "'style=\" padding: 4px;  background-color:" + color + "; border: 3px solid " + borderColor
                        + ";\">";
            else
                return "<div qtip='" + text.replace("'", "&#39;").replace("\n", "<br />") + "' qtitle='" + stringDate
                        + "'style=\" padding: 4px;  background-color:" + color + ";\">";
        }
        else
        {
            if (!Util.isEmptyString(borderColor))
                return "<div qtip='' qtitle='" + stringDate + "'style=\" padding: 4px;  background-color:" + color
                        + "; border: 3px solid " + borderColor + ";\">";
            else
                return "<div qtip='' qtitle='" + stringDate + "'style=\" padding: 4px;  background-color:" + color
                        + "; \">";
        }
    }

    protected String getOpenDivTagString(String color, String borderColor)
    {
        if (!Util.isEmptyString(borderColor))
            return "<div style=\"white-space: normal; padding: 4px;  background-color:" + color
                    + "; border: 3px solid " + borderColor + ";\">";
        return "<div style=\"white-space: normal; padding: 4px;  background-color:" + color + ";\">";
    }
}
