package fr.hd3d.production.ui.client.view.explorer.renderer;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.extjs.gxt.ui.client.widget.tips.QuickTip;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.HtmlUtils;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerColumnConfig;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.production.ui.client.config.ProductionConfig;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.event.ProductionEvents;
import fr.hd3d.production.ui.client.model.ProductionModel;
import fr.hd3d.production.ui.client.view.DisplayMode;


/**
 * Approval note renderer provides utility to display clearly when approval is OK or not OK. It also provides a context
 * menu to forward events like approval editing, setting approval to OK...
 * 
 * @author HD3D
 */
public class ApprovalNoteRenderer implements GridCellRenderer<Hd3dModelData>,
        IExplorateurColumnContextMenu<Hd3dModelData>
{
    /** Constants to display. */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    /** Context menu for approval columns. */
    private Menu contextMenu;

    private final MenuItem okItem = new MenuItem("OK");
    private final MenuItem retakeItem = new MenuItem(CONSTANTS.Retake());
    private final MenuItem todoItem = new MenuItem("To Do");;
    private final MenuItem standByItem = new MenuItem("Stand By");
    private final MenuItem wipItem = new MenuItem("Work In Progress");
    private final MenuItem wapItem = new MenuItem(CONSTANTS.WaitingForApproval());
    private final MenuItem cancelItem = new MenuItem("Cancelled");
    private final MenuItem closeItem = new MenuItem("Closed");

    private final MenuItem validationNote = new MenuItem("Comment");
    private final MenuItem validationMultiNote = new MenuItem("Comment Multi.");
    private final MenuItem validationMultiAnnotationNote = new MenuItem("Multi Annot");
    private final MenuItem pushLastNote = new MenuItem("Push last note");
    private final MenuItem pushLastNoteStatus = new MenuItem("Push last note with status");
    private final MenuItem history = new MenuItem(CONSTANTS.ShowHistory());
    private final MenuItem editLast = new MenuItem(CONSTANTS.EditLastNote());

    private final MenuItem seeFiles = new MenuItem("Show files");

    // private final MenuItem createPlayList = new MenuItem("Create Playlist");

    /** Amount of approval currently selected. */
    private Integer nbSelected;
    /** True if there is at least one approval for current model and current property. */
    private Boolean isApproval;
    /** The column id in which data are stored. */
    private String property;
    /** The column in which data are stored. */
    private String columnName;
    /**  */
    private String approvalNoteTypeId;

    /** Tool tip configuration. */
    private ToolTipConfig config = new ToolTipConfig();
    /** Tool tip displayed on each cell. */
    private QuickTip quickTip;
    /** True if approval is typed and tasks corresponding to its type exists */
    private Boolean isTasks = false;

    private Long fileRevisionId = null;

    /**
     * Constructor
     * 
     * @param grid
     *            Build tool tip on grid.
     */
    public ApprovalNoteRenderer(Grid<Hd3dModelData> grid)
    {
        this(grid, new DisplayMode(DisplayMode.NORMAL));
    }

    public ApprovalNoteRenderer(Grid<Hd3dModelData> grid, DisplayMode mode)
    {
        this.setQuickTip(grid);
    }

    /**
     * Set tool tip to display approval data.
     * 
     * @param grid
     *            The grid on which tool tip should be set.
     */
    private void setQuickTip(Grid<Hd3dModelData> grid)
    {
        quickTip = new QuickTip(grid);

        config.setShowDelay(200);
        config.setHideDelay(0);
        quickTip.setToolTip(config);
        quickTip.setInterceptTitles(false);
        quickTip.setShadow(false);
        quickTip.setBodyBorder(false);
        quickTip.setBorders(false);
    }

    @SuppressWarnings("unchecked")
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        ColumnConfig cc = grid.getColumnModel().getColumn(colIndex);
        this.columnName = cc.getHeader();
        this.property = property;
        this.quickTip.initTarget(grid);

        String html = "";
        Object val = model.get(property);
        if (val instanceof List)
        {
            html = this.getApprovalsHtml((List<ApprovalNoteModelData>) val, cc);
        }
        else
        {
            html = "<div class=\"approval-cell\">No task</div>";
        }

        return html;
    }

    /**
     * @param approvals
     *            The approvals to render.
     * @param cc
     *            The column configuration data.
     * @return HTML representation of approval inside cell.
     */
    private String getApprovalsHtml(List<ApprovalNoteModelData> approvals, ColumnConfig cc)
    {
        String html = "";

        ExplorerColumnConfig ecc = (ExplorerColumnConfig) cc;
        this.approvalNoteTypeId = ecc.getParameter();

        int nbApproval = 0;
        if (approvals != null)
        {
            for (ApprovalNoteModelData approval : approvals)
                if (ETaskStatus.RETAKE.toString().equals(approval.getStatus()))
                    nbApproval++;
        }

        if (approvals.size() > 0)
        {
            ApprovalNoteModelData approval = approvals.get(0);
            html = getApprovalHtml(approval, nbApproval, approval.getStatus(), approval.getTaskTypeId());
        }
        else
        {
            html = "&nbsp;";
        }
        return html;
    }

    /**
     * @param approval
     *            The approval to render.
     * @param nbApproval
     *            The amount of approval with status equal to retake for this cell.
     * @param status
     *            The status of the the approval to render.
     * @param approvalTypeId
     *            The ID of the type of rendered approval.
     * 
     * @return HTML representation of an approval. Representation is bordered if approval is linked to task type.
     */
    private String getApprovalHtml(ApprovalNoteModelData approval, int nbApproval, String status, Long approvalTypeId)
    {
        ApprovalNoteTypeModelData approvalType = ProductionModel.approvalStore.findModel(
                ApprovalNoteTypeModelData.TASK_TYPE_FIELD, approvalTypeId);

        String borderColor = "";
        if (approvalType != null)
            borderColor = approvalType.getTaskTypeColor();

        return getRenderedValue(approval.getDate(), approval.getComment(), nbApproval,
                TaskStatusMap.getColorForStatus(status), NoteStatusComboBox.getStatusAbbreviationMap().get(status),
                borderColor, approval.getFileRevision() != null && !approval.getFileRevision().isEmpty());
    }

    /**
     * @param date
     *            The approval date.
     * @param text
     *            The approval comment.
     * @param color
     * @param count
     *            The number of action on this approval.
     * @param borderColor
     * 
     * @return Approval note to display at HTML format.
     */
    protected String getRenderedValue(Date date, String text, int count, String color, String status,
            String borderColor, boolean hasfileRevision)
    {
        String[] stringDate = DateFormat.FRENCH_DATE_TIME.format(date).split(" ");

        String html = getOpenDivTagString(color, borderColor);

        Element span = DOM.createSpan();
        span.addClassName("approval-title");
        span.setInnerHTML(stringDate[0]);
        html += span.getString() + " " + stringDate[1];
        if (count > 0)
            html += " (" + count + ")";
        span.setInnerHTML(" " + status);
        html += span.getString();

        if (text != null)
        {
            Element div = DOM.createDiv();
            div.addClassName("approval-text");
            div.setInnerHTML(HtmlUtils.changeToHyperText(text.replace("\n", "<br />")));
            html += div.getString();
        }

        if (hasfileRevision)
        {
            Image fileImage = Hd3dImages.getFileIcon().createImage();
            html += fileImage;
        }

        html += "</div>";

        return html;
    }

    /**
     * @param color
     *            The background color of the DIV.
     * @param borderColor
     *            The border color of the DIV.
     * 
     * @return HTML opening DIV tag for HTML representation of the approval.
     */
    protected String getOpenDivTagString(String color, String borderColor)
    {
        if (!Util.isEmptyString(borderColor))
            return "<div class=\"approval-cell\" style=\"background-color:" + color + "; border: 3px solid "
                    + borderColor + ";\">";
        return "<div class=\"approval-cell\" style=\"background-color:" + color + ";\">";
    }

    /**
     * Check for context menu event to configure context menu before displaying it.
     */
    public void handleGridEvent(GridEvent<Hd3dModelData> ge)
    {
        if (ge.getType() == Events.ContextMenu)
        {
            this.onContextMenu(ge);
        }
    }

    private void onContextMenu(GridEvent<Hd3dModelData> ge)
    {
        Grid<Hd3dModelData> grid = ge.getGrid();
        if (grid.getSelectionModel().getSelectedItems() != null)
        {
            this.isApproval = false;
            this.isTasks = false;
            fileRevisionId = null;

            List<Hd3dModelData> items = grid.getSelectionModel().getSelectedItems();
            this.nbSelected = items.size();
            if (this.nbSelected == 1)
            {
                isTasks = items.get(0).get(this.property) instanceof List;
                this.isApproval = items.get(0).get(this.property) instanceof List
                        && !CollectionUtils.isEmpty((List<?>) items.get(0).get(this.property));

                // if (this.isApproval)
                // {
                // @SuppressWarnings("unchecked")
                // ApprovalNoteModelData approval = ((List<ApprovalNoteModelData>) items.get(0).get(this.property))
                // .get(0);
                // if (approval.getFileRevision() != null)
                // {
                // fileRevisionId = approval.getFileRevision();
                // }
                // }
            }
            else
            {
                for (Hd3dModelData item : items)
                {
                    isTasks = item.get(this.property) instanceof List;
                    if (!isTasks)
                        break;
                }
            }

            this.createValidationContextMenu(isApproval);

            this.contextMenu.removeAllListeners();
            // final Long fileRevisionFinal = fileRevisionId;
            this.contextMenu.addListener(Events.BeforeShow, new Listener<BaseEvent>() {
                public void handleEvent(BaseEvent be)
                {
                    okItem.setEnabled(isTasks);
                    retakeItem.setEnabled(isTasks);
                    todoItem.setEnabled(isTasks);
                    standByItem.setEnabled(isTasks);
                    wipItem.setEnabled(isTasks);
                    wapItem.setEnabled(isTasks);
                    cancelItem.setEnabled(isTasks);
                    validationNote.setEnabled(isTasks);
                    validationMultiNote.setEnabled(isTasks);
                    validationMultiAnnotationNote.setEnabled(isTasks);
                    closeItem.setEnabled(isTasks);
                    pushLastNote.setEnabled(isTasks);
                    pushLastNoteStatus.setEnabled(isTasks);

                    // seeFiles.setEnabled(fileRevisionFinal != null);

                    editLast.setEnabled(nbSelected == 1 && isApproval);
                    history.setEnabled(nbSelected == 1 && isApproval);
                    if (!isTasks)
                    {
                        contextMenu.setTitle(CONSTANTS.ApprovalHaveNoTasks());
                    }
                    else
                    {
                        contextMenu.setTitle("");
                    }
                }
            });

            ge.getGrid().setContextMenu(contextMenu);
        }
    }

    private void createValidationContextMenu(final boolean isApproval)
    {
        if (contextMenu == null)
        {
            contextMenu = new Menu();

            contextMenu.add(todoItem);
            contextMenu.add(okItem);
            contextMenu.add(retakeItem);
            contextMenu.add(cancelItem);
            contextMenu.add(closeItem);
            contextMenu.add(new SeparatorMenuItem());
            contextMenu.add(standByItem);
            contextMenu.add(wipItem);
            contextMenu.add(wapItem);
            contextMenu.add(new SeparatorMenuItem());
            contextMenu.add(validationNote);
            contextMenu.add(validationMultiNote);
            if (MainModel.getAssetManager())
            {
                contextMenu.add(validationMultiAnnotationNote);
            }
            contextMenu.add(editLast);
            contextMenu.add(pushLastNote);
            contextMenu.add(pushLastNoteStatus);
            contextMenu.add(history);

            // if (MainModel.getAssetManager())
            // {
            // contextMenu.add(seeFiles);
            // }

            okItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.OK.toString()));
            retakeItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.RETAKE.toString()));
            todoItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.TO_DO.toString()));
            standByItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.STAND_BY.toString()));
            wipItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.WORK_IN_PROGRESS.toString()));
            wapItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.WAIT_APP.toString()));
            cancelItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.CANCELLED.toString()));
            closeItem.addSelectionListener(new AddApprovalItemListener(EApprovalNoteStatus.CLOSE.toString()));
            validationNote.addSelectionListener(new AddApprovalItemListener(Boolean.TRUE, EApprovalNoteStatus.OK
                    .toString()));
            validationMultiNote.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    multiComment();
                }
            });

            validationMultiAnnotationNote.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    annotate();
                }
            });

            history.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    showHistory();
                }
            });

            editLast.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    editLastApproval();
                }
            });

            // seeFiles.addSelectionListener(new SelectionListener<MenuEvent>() {
            //
            // @Override
            // public void componentSelected(MenuEvent ce)
            // {
            // seeFiles();
            // }
            //
            // });

            this.buildPushValidationNoteMenus();
        }
    }

    private void buildPushValidationNoteMenus()
    {
        Menu pushLastNoteMenu = new Menu();
        Menu pushLastNoteStatusMenu = new Menu();

        for (final ItemModelData item : ProductionModel.getApprovalColumns())
        {
            MenuItem menuItem = new MenuItem(item.getName());
            pushLastNoteMenu.add(menuItem);
            if (property.equals("id_" + item.getId()))
                menuItem.disable();

            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    pushApproval("id_" + item.getId(), Boolean.FALSE);
                }
            });

            menuItem = new MenuItem(item.getName());
            pushLastNoteStatusMenu.add(menuItem);
            if (property.equals("id_" + item.getId()))
                menuItem.disable();

            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    pushApproval("id_" + item.getId(), Boolean.TRUE);
                }
            });
        }
        this.pushLastNote.setSubMenu(pushLastNoteMenu);
        this.pushLastNoteStatus.setSubMenu(pushLastNoteStatusMenu);
    }

    private void editLastApproval()
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_EDIT);
        event.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, property);
        event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, columnName);
        event.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, approvalNoteTypeId);
        EventDispatcher.forwardEvent(event);
    }

    // private void seeFiles()
    // {
    // AppEvent event = new AppEvent(ProductionEvents.SEE_FILES_CLICKED);
    // event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, columnName);
    // event.setData(ProductionConfig.FILE_REVISION, this.fileRevisionId);
    // EventDispatcher.forwardEvent(event);
    // }

    private void addApproval(Boolean withComment, String status)
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_ADD_CLICKED);
        event.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, property);
        event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, columnName);
        event.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, approvalNoteTypeId);
        event.setData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR, status);
        event.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, withComment);
        EventDispatcher.forwardEvent(event);
    }

    private void pushApproval(String destination, Boolean withStatus)
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_PUSH_CLICKED);
        event.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, property);
        event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, columnName);
        event.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, approvalNoteTypeId);
        event.setData(ProductionConfig.APPROVAL_DESTINATION_EVENT_VAR, destination);
        event.setData(ProductionConfig.WITH_STATUS_EVENT_VAR, withStatus);
        EventDispatcher.forwardEvent(event);
    }

    private void showHistory()
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_HISTORY, property);
        EventDispatcher.forwardEvent(event);
    }

    private void multiComment()
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_ADD_CLICKED);
        event.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, property);
        event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, columnName);
        event.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, approvalNoteTypeId);
        event.setData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR, EApprovalNoteStatus.OK.toString());
        event.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, Boolean.TRUE);
        event.setData(ProductionConfig.MULTI, Boolean.TRUE);
        event.setData(ProductionConfig.ANNOTATION, Boolean.FALSE);
        EventDispatcher.forwardEvent(event);
    }

    private void annotate()
    {
        AppEvent event = new AppEvent(ProductionEvents.APPROVAL_ADD_CLICKED);
        event.setData(ProductionConfig.APPROVAL_FIELD_EVENT_VAR, property);
        event.setData(ProductionConfig.APPROVAL_NAME_EVENT_VAR, columnName);
        event.setData(ProductionConfig.APPROVAL_TYPE_EVENT_VAR, approvalNoteTypeId);
        event.setData(ProductionConfig.APPROVAL_STATUS_EVENT_VAR, EApprovalNoteStatus.OK.toString());
        event.setData(ProductionConfig.WITH_COMMENT_EVENT_VAR, Boolean.TRUE);
        event.setData(ProductionConfig.MULTI, Boolean.TRUE);
        event.setData(ProductionConfig.ANNOTATION, Boolean.TRUE);
        event.setData(ProductionConfig.FILE_REVISION, fileRevisionId);
        EventDispatcher.forwardEvent(event);
    }

    private class AddApprovalItemListener extends SelectionListener<MenuEvent>
    {
        private Boolean isComment;
        private String status;

        public AddApprovalItemListener(String status)
        {
            this.isComment = Boolean.FALSE;
            this.status = status;
        }

        public AddApprovalItemListener(Boolean isComment, String status)
        {
            this.isComment = isComment;
            this.status = status;
        }

        @Override
        public void componentSelected(MenuEvent ce)
        {
            addApproval(isComment, status);
        }
    }
}
