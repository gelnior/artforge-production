package fr.hd3d.production.ui.client.view.explorer.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;


public class TaskOkRenderer<M extends ModelData> implements GridCellRenderer<M>
{
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String stringValue = "";
        String sValue = model.get(property);
        if (sValue != null)
        {

            String[] values = sValue.split("/");
            if (values.length == 2)
            {
                Long ok = new Long(values[0]);
                Long total = new Long(values[1]);
                String color = getColor(ok, total);
                stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:" + color
                        + ";'>" + sValue + "</div>";
            }
        }
        return stringValue;
    }

    private String getColor(Long ok, Long total)
    {
        String color = "";
        if (ok.equals(total))
        {
            if (total > 0)
            {
                color = "#9BD46D";
            }
        }
        else
        {
            color = "#F4C431";
        }
        return color;
    }
}
