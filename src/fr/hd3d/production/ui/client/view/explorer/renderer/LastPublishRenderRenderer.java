package fr.hd3d.production.ui.client.view.explorer.renderer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.XDOM;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.dom.client.Element;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.Timer;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


public class LastPublishRenderRenderer implements GridCellRenderer<Hd3dModelData>
{
    private static final List<String> blinking = new ArrayList<String>();
    private static Boolean blinkStart = false;
    private static final String blinkCss = " approvalCell-WFA ";

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        makeItBlink();
        ColumnModel cm = grid.getColumnModel();
        List<ColumnConfig> columns = cm.getColumns();
        Date approvalDate = null;
        for (ColumnConfig column : columns)
        {
            GridCellRenderer<?> renderer = column.getRenderer();
            if (renderer instanceof ApprovalNoteRenderer)
            {
                Object value = model.get(column.getDataIndex());
                if (value instanceof ArrayList<?>)
                {
                    List<?> approvals = ((List<?>) value);
                    if (approvals.size() > 0)
                    {
                        if (approvals.get(0) instanceof JSONObject)
                        {
                            JSONObject obj = (JSONObject) approvals.get(0);
                            JSONString dateString = (JSONString) obj.get(ApprovalNoteModelData.DATE_FIELD);
                            approvalDate = DateFormat.TIMESTAMP_FORMAT.parse(dateString.stringValue());
                            break;
                        }
                        else
                        {
                            ApprovalNoteModelData obj = (ApprovalNoteModelData) approvals.get(0);
                            if (obj.getStatus().equals(ApprovalNoteModelData.STATUS_NOK))
                            {
                                approvalDate = obj.getDate();
                                break;
                            }
                        }
                    }
                }
            }
        }
        String id = XDOM.getUniqueId();
        String content = "<div id=" + id + ">";
        Date publishDate = model.get(property);
        config.css = config.css.replaceAll(blinkCss, "");
        if (publishDate != null)
        {

            if (approvalDate != null && publishDate.after(approvalDate))
            {
                blinking.add(id);
                content += DateFormat.DATE.format(publishDate);
                config.css += blinkCss;
            }
            else
            {
                content += DateFormat.DATE.format(publishDate);
            }
        }
        content += "</div>";
        return content;
    }

    private void makeItBlink()
    {
        if (blinkStart == false)
        {
            blinkStart = true;
            Timer tmr = new Timer() {
                private boolean onOff = false;

                @Override
                public void run()
                {
                    if (LastPublishRenderRenderer.blinking.size() > 0)
                    {
                        for (String id : LastPublishRenderRenderer.blinking)
                        {
                            Element elChildChild = XDOM.getElementById(id);
                            if (elChildChild == null)
                            {
                                LastPublishRenderRenderer.blinking.remove(id);
                            }
                            else
                            {
                                Element elChild = elChildChild.getParentElement();
                                Element el = elChild.getParentElement();
                                String css = el.getClassName();
                                if (onOff)
                                {
                                    css += blinkCss;
                                }
                                else
                                {
                                    css = css.replaceAll(blinkCss, "");
                                }
                                el.setClassName(css);
                            }
                        }
                        if (LastPublishRenderRenderer.blinking.size() == 0)
                        {
                            blinkStart = false;
                            this.cancel();
                        }
                        onOff = !onOff;
                    }
                }
            };
            tmr.scheduleRepeating(800);
        }
    }
}
