package fr.hd3d.production.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raise by the application.
 * 
 * @author HD3D
 */
public class ProductionEvents
{
    public static final EventType PRODUCTION_CONFIG_INITIALIZED = new EventType();
    public static final EventType EDIT_APPROVAL_NOTE_CLICKED = new EventType();
    public static final EventType DELETE_APPROVAL_NOTE_CLICKED = new EventType();
    public static final EventType DELETE_APPROVAL_NOTE_CONFIRMED = new EventType();
    public static final EventType APPROVAL_NOTE_DELETED = new EventType();
    public static final EventType APPROVAL_ADD_CLICKED = new EventType();
    public static final EventType APPROVAL_EDIT = new EventType();
    public static final EventType APPROVAL_HISTORY = new EventType();
    public static final EventType APPROVAL_ADD_OK = new EventType();
    public static final EventType APPROVAL_ADD_NOK = new EventType();
    public static final EventType APPROVAL_TASK_EDITED = new EventType();

    public static final EventType CONSTITUENT_TAB_CLICKED = new EventType();
    public static final EventType SHOT_TAB_CLICKED = new EventType();
    public static final EventType PLAYLIST_TAB_CLICKED = new EventType();
    public static final EventType APPROVAL_TYPES_LOADED = new EventType();
    public static final EventType APPROVAL_PUSH_CLICKED = new EventType();
    public static final EventType STAT_TAB_CLICKED = new EventType();
    public static final EventType STAT_DETAIL_TAB_CLICKED = new EventType();
    public static final EventType SHOW_PROXY_CLICKED = new EventType();
    public static final EventType PROXY_MODE_CLICKED = new EventType();
    public static final EventType SEE_FILES_CLICKED = new EventType();

    public static final EventType PLAYLIST_CREATION_SUCCESS = new EventType();

}
