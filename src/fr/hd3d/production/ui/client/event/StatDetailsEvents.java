package fr.hd3d.production.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


public class StatDetailsEvents
{
    public static final EventType COMPOSITION_LOADED = new EventType();
    public static final EventType SHOTS_LOADED = new EventType();
    public static final EventType CONSTITUENT_LOADED = new EventType();
    public static final EventType CATEGORIES_LOADED = new EventType();
    public static final EventType SEQUENCE_ALL_CHILDREN_LOADED = new EventType();
    public static final EventType CONSTITUENT_DROPPED = new EventType();
    public static final EventType COPY_KEYPRESS = new EventType();
    public static final EventType PASTE_KEYPRESS = new EventType();
    public static final EventType DELETE_KEYPRESS = new EventType();
    public static final EventType REFRESH_CLICKED = new EventType();
    public static final EventType CATEGORIES_ROOT_LOADED = new EventType();
    public static final EventType AFTER_EDIT = new EventType();
    public static final EventType SHOT_SAVE_SUCCESS = new EventType();
    public static final EventType REFRESH_CATEGORIE_CLICKED = new EventType();

    public static final EventType APPROVAL_NOTES_LOADED = new EventType();
    public static final EventType LOAD_STEP = new EventType();
    public static final EventType LOAD_STAT = new EventType();
    public static final EventType STAT_REFRESH_CLICKED = new EventType();
    public static final EventType DATA_LOADED = new EventType();
    public static final EventType REFRESH_STAT_CHART = new EventType();
    public static final EventType PROJECT_CHANGED = new EventType();
    public static final EventType TASKS_LOADED = new EventType();
    public static final EventType STATS_PRINT = new EventType();
    public static final EventType APPROVAL_TYPES_LOADED = new EventType();
    public static final EventType APPROVAL_TYPES_LOADED_ADD_STAT = new EventType();
    public static final EventType RECONFIG_GRID = new EventType();
    public static final EventType WITH_TOTAL = new EventType();

}
