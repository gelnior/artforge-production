package fr.hd3d.production.ui.client.playlist.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.math.PlaylistUtils;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.playlist.model.PlayListEditorModel;
import fr.hd3d.production.ui.client.playlist.view.PlayListEditor;


/**
 * Controller of MVC architecture (PlayListEditorModel, PlayListEditor, PlayListEditorController). It handles events and
 * notifies model and view.
 * 
 * @version 1.0
 * @since 1.0
 * 
 */
public class PlayListEditorController extends MaskableController
{

    private final PlayListEditorModel model;
    private final PlayListEditor view;

    /**
     * Controller constructor.
     * 
     * @param model
     *            Model of MVC architecture (PlayListEditorModel, PlayListEditor, PlayListEditorController)
     * @param view
     *            View of MVC architecture (PlayListEditorModel, PlayListEditor, PlayListEditorController)
     */
    public PlayListEditorController(PlayListEditorModel model, PlayListEditor view)
    {
        this.model = model;
        this.view = view;
        registerEvents();
    }

    private void registerEvents()
    {
        registerEventTypes(PlaylistUiEvents.NEW_PLAYLIST_EDIT,//
                PlaylistUiEvents.DELETE_PLAYLIST_EDIT,//
                PlaylistUiEvents.LOAD_PLAYLIST,//
                PlaylistUiEvents.NEW_PLAYLIST,//
                PlaylistUiEvents.SAVE_PLAYLIST,//
                PlaylistUiEvents.MOVE_EDITING_TOOL,//
                PlaylistUiEvents.RIPPLE_EDITING_TOOL,//
                PlaylistUiEvents.PLAYLIST_FRAMERATE_CHANGED, //
                PlaylistUiEvents.REFRESH_PLAYLIST, //
                CommonEvents.MODEL_DATA_DELETE_SUCCESS,//
                CommonEvents.MODEL_DATA_CREATION_SUCCESS,//
                CommonEvents.MODEL_DATA_UPDATE_SUCCESS,//
                PlaylistUiEvents.PROXIES_MAP_UPDATED); //

    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == PlaylistUiEvents.LOAD_PLAYLIST)
        {
            onLoadPlaylist(event);
        }
        else if (type == PlaylistUiEvents.NEW_PLAYLIST_EDIT)
        {
            onNewEdit(event);
        }
        else if (type == PlaylistUiEvents.SAVE_PLAYLIST)
        {
            onSavePlaylist();
        }
        else if (type == PlaylistUiEvents.DELETE_PLAYLIST_EDIT)
        {
            onDeleteEdits(event);
        }
        else if (type == PlaylistUiEvents.MOVE_EDITING_TOOL)
        {
            onMove(event);
        }
        else if (type == PlaylistUiEvents.RIPPLE_EDITING_TOOL)
        {
            onRipple(event);
        }
        else if (type == PlaylistUiEvents.REFRESH_PLAYLIST)
        {
            onRefresh(event);
        }
        else if (type == PlaylistUiEvents.PLAYLIST_FRAMERATE_CHANGED)
        {
            onPlayListFrameRateChanged(event);
        }
        else if (type == PlaylistUiEvents.PROXIES_MAP_UPDATED)
        {
            onProxiesMapUpdated(event);
        }
    }

    /**
     * onDeleteEdits is called whenever the delete button is triggered. It will remove the edit from the store and add
     * the edit to a to-be-deleted list on the model (the action will be performed whenever the save action is
     * performed).
     */
    private void onDeleteEdits(AppEvent event)
    {
        List<PlayListEditModelData> editsToDelete = event.getData();
        final ServiceStore<PlayListEditModelData> editStore = model.getEditStore();
        for (PlayListEditModelData edit : editsToDelete)
        {
            editStore.remove(edit);
            model.deleteEdit(edit);
        }
    }

    private void onSavePlaylist()
    {
        if (model.getCurrentPlaylist() != null)
        {
            model.saveEdits();
            model.saveComments(view.getComments());
            model.getCurrentPlaylist().save();
            view.onPlaylistSaved();
        }
        else
        {
            view.display(PlayListEditor.CONSTANTS.SaveFailure(), PlayListEditor.CONSTANTS.PlaylistNullInfo());
        }
    }

    private void onNewEdit(AppEvent event)
    {
        int srcIn = Integer.parseInt((String) event.getData("srcIn"));
        int srcOut = Integer.parseInt((String) event.getData("srcOut"));
        int recIn = Integer.parseInt((String) event.getData("recIn"));
        int recOut = Integer.parseInt((String) event.getData("recOut"));

        model.addNewEdit((String) event.getData("name"), srcIn, srcOut, recIn, recOut);
        updateStore(model.getEditStore().getModels());
        view.resort();
    }

    private void onLoadPlaylist(AppEvent event)
    {
        final PlayListRevisionModelData currentPlaylist = event.getData();
        model.setCurrentPlaylist(currentPlaylist);
        view.onLoadPlaylist();
    }

    /**
     * Reaction to Ripple command.
     * 
     * @param event
     *            Transmitted AppEvent containint a list of edits to move
     */
    private void onRipple(AppEvent event)
    {
        List<PlayListEditModelData> editsToRipple = event.getData();
        if (PlaylistUtils.ripple(editsToRipple))
            updateAll();
    }

    /**
     * Reaction to Move command.
     * 
     * @param event
     *            Transmitted AppEvent containint a list of edits to move
     */
    private void onMove(AppEvent event)
    {
        Integer offset = event.getData("offset");
        List<PlayListEditModelData> editsToMove = event.getData("list");
        if (PlaylistUtils.move(editsToMove, model.getEditStore().getModels(), offset))
            updateAll();
    }

    /**
     * If a playlist is asked for refreshing, its content is asked for reload.
     * 
     * @param event
     *            The refresh constituent node event.
     */
    private void onRefresh(AppEvent event)
    {
        model.getEditStore().reload();
    }

    /**
     * If an asset is asked for refreshing, all views are refreshed by raising a SELECTED_ASSET event. If a shot is
     * asked for refreshed, its content is reloaded.
     * 
     * @param event
     *            The refresh constituent node event.
     */
    // private void onRefreshShotNode(AppEvent event)
    // {
    // List<RecordModelData> selection = this.view.getShotSelection();
    // if (selection != null && selection.size() > 0
    // && AssetRevisionModelData.CLASS_NAME.equals(selection.get(0).getClassName()))
    // {
    // AppEvent assetEvent = new AppEvent(DataflowEvent.ASSET_SELECTED);
    // assetEvent.setData(selection.get(0));
    // EventDispatcher.forwardEvent(assetEvent);
    // }
    // else
    // {
    // this.view.refreshSelectedShot();
    // }
    // }
    private void onPlayListFrameRateChanged(AppEvent event)
    {
        EFrameRate newFrameRate = event.getData();
        model.getCurrentPlaylist().setFrameRate(newFrameRate.toString());
        model.getCurrentPlaylist().save();
    }

    private void onProxiesMapUpdated(AppEvent event)
    {
        // Info.display("PLAYLIST LOADED", "onProxiesMapUpdated");
        view.loadPlayer(model.getProxies());
    }

    /**
     * Updates the specified edits on the the current model' store
     * 
     * @param edits
     *            Edits to update
     */
    private void updateStore(List<PlayListEditModelData> edits)
    {
        for (PlayListEditModelData edit : edits)
            model.getEditStore().update(edit);
    }

    /**
     * Ensures the view has correct data by redrawing the grid with modified store.
     */
    public void updateAll()
    {
        updateStore(model.getEditStore().getModels());
        view.resort();
        view.reselect();
    }
}
