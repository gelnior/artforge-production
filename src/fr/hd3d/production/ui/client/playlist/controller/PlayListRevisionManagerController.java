package fr.hd3d.production.ui.client.playlist.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.store.StoreListener;
import com.extjs.gxt.ui.client.store.TreeStoreEvent;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.playlist.event.PlayListTreeEvents;
import fr.hd3d.production.ui.client.error.ProductionErrors;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.playlist.model.PlayListRevisionManagerModel;
import fr.hd3d.production.ui.client.playlist.view.PlaylistRevisionManagerView;


/**
 * 
 * Controller for PlayListRevisionManager.
 * 
 * @author guillaume-maucomble
 * @version 1.0
 * @since 1.0
 * 
 *        TODO Check INTERNATIONALISATION FIXME Replace Info.Display by Error.Dispatcher
 * 
 */
public class PlayListRevisionManagerController extends MaskableController
{
    private final PlayListRevisionManagerModel model;
    private final PlaylistRevisionManagerView view;

    public PlayListRevisionManagerController(PlayListRevisionManagerModel model, PlaylistRevisionManagerView view)
    {
        this.model = model;
        this.view = view;
        this.registerEvents();
        this.setUpStoreListener();
    }

    protected PlayListRevisionManagerModel getModel()
    {
        return model;
    }

    protected PlaylistRevisionManagerView getView()
    {
        return view;
    }

    /**
     * Sets up (if required) a StoreListener that listens for add events. This updates the model data.
     */
    protected void setUpStoreListener()
    {
        if (getView() == null || getView().getTreePanel() == null || getView().getTreePanel().getStore() == null)
        {
            return;
        }

        getView().getTreePanel().getStore().addStoreListener(new StoreListener<RecordModelData>() {

            /**
             * Sets playListRevisionGroupId value for PlayListRevisionModelData and saves it. Does nothing if
             * object'current playListRevisionGroupId value is parentId.
             * 
             * @param object
             *            Object to which the parentId is applied
             * @param parentId
             *            New value for playListRevisionGroupId
             */
            private void setParentId(PlayListRevisionModelData object, Long parentId)
            {
                if (object.getPlayListRevisionGroupId() == parentId)
                    return;
                object.setPlayListRevisionGroupId(parentId);
                object.save();
            }

            /**
             * Sets parentId value for PlayListRevisionGroupModelData and saves it. Does nothing if object'current
             * parentId value is parentId.
             * 
             * @param object
             *            Object to which the parentId is applied
             * @param parentId
             *            New value for parentId
             */
            private void setParentId(PlayListRevisionGroupModelData object, Long parentId)
            {
                if (object.getParentId() == parentId)
                    return;
                object.setParentId(parentId);
                object.save();
            }

            @Override
            public void storeAdd(StoreEvent<RecordModelData> se)
            {
                TreeStoreEvent<RecordModelData> tse = (TreeStoreEvent<RecordModelData>) se;
                Long parentId = (tse == null || tse.getParent() == null) ? null : tse.getParent().getId();
                List<RecordModelData> children = tse.getChildren();
                for (RecordModelData child : children)
                {
                    if (child instanceof PlayListRevisionModelData)
                    {
                        setParentId((PlayListRevisionModelData) child, parentId);
                    }
                    else if (child instanceof PlayListRevisionGroupModelData)
                    {
                        setParentId((PlayListRevisionGroupModelData) child, parentId);
                    }

                }
            }
        });
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        /** PlayListRevisionGroup */
        if (PlaylistUiEvents.NEW_PLAYLIST_GROUP.equals(type))
        {
            onNewPlaylistRevisionGroup(event);
        }
        else if (PlaylistUiEvents.DELETE_PLAYLIST_GROUP.equals(type))
        {
            onDeletePlayListRevisionGroup(event);
        }
        else if (PlaylistUiEvents.DELETE_PLAYLIST_GROUPS.equals(type))
        {
            onDeletePlayListRevisionGroups(event);
        }
        else if (PlaylistUiEvents.CHANGE_PLAYLIST_GROUP.equals(type))
        {

        }
        /** PLayListRevision */
        else if (PlaylistUiEvents.NEW_PLAYLIST.equals(type))
        {
            onNewPlaylist(event);
        }
        else if (PlaylistUiEvents.NEW_PLAYLIST_REVISION.equals(type))
        {
            onPlayListRevisionCreate(event);
        }
        else if (PlaylistUiEvents.DELETE_PLAYLIST.equals(type))
        {
            onPlayListRevisionDelete(event);
        }
        else if (PlaylistUiEvents.DELETE_PLAYLISTS.equals(type))
        {
            onDeleteManyPlaylists(event);
        }
        else if (PlaylistUiEvents.DELETE_PLAYLIST_REVISION.equals(type))
        {
            onDeleteRevision(event);
        }
        else if (PlaylistUiEvents.DELETE_PLAYLIST_REVISIONS.equals(type))
        {
            onDeleteManyRevisions(event);
        }

        else if (PlaylistUiEvents.RENAME_PLAYLIST_ITEM.equals(type))
        {
            onPlayListItemRename(event);
        }
        else if (PlayListTreeEvents.PLAYLISTREVISION_SELECTED.equals(type))
        {

        }
        else if (PlayListTreeEvents.PLAYLISTREVISION_GROUP_SELECTED.equals(type))
        {

        }
        else if (PlaylistUiEvents.DELETE_BOTH_PLAYLIST_AND_GROUP.equals(type))
        {

        }
        /** Project */
        else if (CommonEvents.PROJECT_CHANGED.equals(type))
        {
            onProjectChanged(event);
        }
        /** Operations */
        else if (CommonEvents.MODEL_DATA_CREATION_SUCCESS.equals(type))
        {
            onCreationSuccess(event);
        }
        else if (CommonEvents.MODEL_DATA_UPDATE_SUCCESS.equals(type))
        {

        }
        else if (CommonEvents.MODEL_DATA_DELETE_SUCCESS.equals(type))
        {
            onDeleteSuccess(event);
        }
        else if (PlaylistUiEvents.REFRESH_PLAYLIST_NODE.equals(type))
        {
            onRefresh(event);
        }
    }

    private void onPlayListRevisionCreate(AppEvent event)
    {
        PlayListRevisionModelData data = event.getData();
        if (data == null)
        {
            return;
        }
        boolean createNewRevisionFrom = model.createNewRevisionFrom(data);
        if (!createNewRevisionFrom)
        {
            Info.display("Error", "Error while creating new version from " + data.getPlaylistString()
                    + ". Playlist has not been saved");
        }

    }

    private void onDeleteSuccess(AppEvent event)
    {
        model.removeFromStoreById((Long) event.getData(CommonConfig.ID_EVENT_VAR_NAME));
    }

    private void onDeleteManyRevisions(AppEvent event)
    {
        List<PlayListRevisionModelData> revisionsToRemove = event.getData();
        deleteRevisions(revisionsToRemove);
    }

    private void onDeleteRevision(AppEvent event)
    {
        PlayListRevisionModelData plToRemove = event.getData();
        plToRemove.delete();

    }

    private void onDeleteManyPlaylists(AppEvent event)
    {
        List<PlayListRevisionModelData> selection = event.getData();
        List<PlayListRevisionModelData> revisionsToDelete = model.getAllRevisions(selection);
        deleteRevisions(revisionsToDelete);
    }

    private void deleteRevisions(List<PlayListRevisionModelData> revisionsToDelete)
    {
        if (revisionsToDelete == null || revisionsToDelete.isEmpty())
            return;
        for (PlayListRevisionModelData pl : revisionsToDelete)
        {
            pl.delete();
        }
    }

    private void onPlayListRevisionDelete(AppEvent event)
    {
        PlayListRevisionModelData playListToDelete = event.getData();
        List<PlayListRevisionModelData> revisionsToDelete = model.getAllRevisions(playListToDelete);
        deleteRevisions(revisionsToDelete);
    }

    /**
     * Global event that notifies the successful creation of a RecordModelData. The created item is available through
     * CommonConfig.MODEL_EVENT_VAR_NAME key in event data.
     * 
     * @param event
     *            Event triggered by the successful creation of a RecordModelData
     */
    private void onCreationSuccess(AppEvent event)
    {
        RecordModelData item = (RecordModelData) event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        PlayListRevisionGroupModelData parentItem = model.getParent(item);
        model.addToStore(parentItem, item);
        view.getTreePanel().setExpanded(parentItem, true);
    }

    private void onPlayListItemRename(AppEvent event)
    {
        RecordModelData object = view.getSelectedItem();
        String name = event.getData("name");
        object.setName(name);
        object.save();
        model.update(object);

    }

    /**
     * Called whenever a new PlayListRevision object is required.
     * 
     * @param event
     *            should contain the "name" entry, and can contain a "collection" of Hd3dModelData entries.
     */
    private void onNewPlaylist(AppEvent event)
    {
        if (MainModel.getCurrentProject() == null)
        {
            ErrorDispatcher.sendError(ProductionErrors.NO_CURRENT_PROJECT_OPENED_ERROR);
            return;
        }

        String name = event.getData("name");
        List<? extends Hd3dModelData> collection = event.getData("collection");

        Long playListRevisionGroupId = -1L;
        PlayListRevisionGroupModelData parentItem;
        try
        {
            parentItem = (PlayListRevisionGroupModelData) view.getSelectedItem();
            playListRevisionGroupId = parentItem.getId();
        }
        catch (Exception e)
        {
            return;
        }

        if (model.playlistExists(name, playListRevisionGroupId))
        {
            Info.display("Adding playlist revision", name + " already exists");
            return;
        }

        playListRevisionGroupId = playListRevisionGroupId > 0 ? playListRevisionGroupId : 0;

        PlayListRevisionModelData newPlaylistRevision = PlayListRevisionManagerModel.createPlayListRevisionModelData(
                name, 0, playListRevisionGroupId, MainModel.getCurrentProject().getId());
        newPlaylistRevision.save();

        if (!(collection == null || collection.isEmpty()))
            for (Hd3dModelData modelData : collection)
            {
                if (ShotModelData.CLASS_NAME.equals(modelData.getClassName()))
                {
                    final int nbFrame = ((Double) modelData.get(ShotModelData.SHOT_NBFRAME)).intValue();
                    PlayListEditModelData createPlayListEditModelData = PlayListRevisionManagerModel
                            .createPlayListEditModelData((String) modelData.get(ShotModelData.SHOT_HOOK), 0, nbFrame,
                                    0, nbFrame, newPlaylistRevision.getId());
                    createPlayListEditModelData.save();

                }
            }
    }

    /**
     * Reaction to new PlayListRevisionGroup action.
     * 
     * @param event
     */
    private void onNewPlaylistRevisionGroup(AppEvent event)
    {
        if (MainModel.getCurrentProject() == null)
        {
            ErrorDispatcher.sendError(ProductionErrors.NO_CURRENT_PROJECT_OPENED_ERROR);
            return;
        }
        String newName = event.getData("name");
        Long parentId = new Long(0);
        RecordModelData selectedItem = getPlayListRevisionGroupId();
        if (selectedItem != null)
        {
            parentId = selectedItem.getId();
            parentId = parentId > 0 ? parentId : 0;
        }

        PlayListRevisionGroupModelData createPlayListRevisionGroupModelData = PlayListRevisionManagerModel
                .createPlayListRevisionGroupModelData(newName, parentId, MainModel.currentProject.getId());
        createPlayListRevisionGroupModelData.save();
    }

    /**
     * Returns current selectedItem
     * 
     * @return
     */
    private RecordModelData getPlayListRevisionGroupId()
    {
        return view.getSelectionModel().getSelectedItem();
    }

    // /**
    // * Returns current selectedItems
    // *
    // * @return
    // */
    // private List<RecordModelData> getPlayListRevisionGroupIds()
    // {
    // return view.getSelectionModel().getSelectedItems();
    // }

    private void deletePlayListRevisionGroup(PlayListRevisionGroupModelData item)
    {
        for (RecordModelData child : model.getTreeStore().getChildren(item))
            child.delete();

        item.delete();
    }

    private void onDeletePlayListRevisionGroup(AppEvent event)
    {
        deletePlayListRevisionGroup((PlayListRevisionGroupModelData) event.getData());
    }

    private void onDeletePlayListRevisionGroups(AppEvent event)
    {
        List<PlayListRevisionGroupModelData> collection = event.getData();
        for (PlayListRevisionGroupModelData item : collection)
        {
            deletePlayListRevisionGroup(item);
        }
    }

    /**
     * When project changes, all data are cleared. Then first sheet of combo box is selected. Explorer filter is updated
     * to retrieve only sequences from this project. Project id is save to cookie for reselecting at next launching.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        this.view.getTreePanel().getSelectionModel().deselectAll();
        ProjectModelData project = event.getData();
        if (project == null)
        {
            onProjectChanged();
        }
        else
        {
            onProjectChanged(project);
        }
    }

    /**
     * Resets the project to MainModel value.
     */
    public void onProjectChanged()
    {
        onProjectChanged(MainModel.currentProject);
    }

    /**
     * Resets the project to parameter value.
     */
    private void onProjectChanged(ProjectModelData project)
    {

        EqConstraint constraint = new EqConstraint("playlistrevisiongroup.project.id", project.getId());
        this.model.getFilter().setLeftMember(constraint);

        if (project != null)
        {
            this.view.reloadTree(project);
            view.setEnabled(project != null);

        }
    }

    /**
     * Refreshes selected nodes by calling {@link fr.hd3d.common.ui.client.widget.basetree.BaseTree#refreshSelection()}.
     * 
     * @param event
     *            REFRESH_PLAYLIST_NODE Event
     */
    private void onRefresh(AppEvent event)
    {
        this.view.refreshSelection();
    }

    /**
     * Register events to Controller#supportedEvents (List<EventType>)
     */
    private void registerEvents()
    {
        /** PlayListRevisionGroup */
        this.registerEventTypes(PlaylistUiEvents.NEW_PLAYLIST_GROUP, //
                PlaylistUiEvents.DELETE_PLAYLIST_GROUP,//
                PlaylistUiEvents.DELETE_PLAYLIST_GROUPS,//
                PlaylistUiEvents.CHANGE_PLAYLIST_GROUP, //
                PlayListTreeEvents.PLAYLISTREVISION_GROUP_SELECTED,//
                /** PlayListRevision */
                PlaylistUiEvents.NEW_PLAYLIST,//
                PlaylistUiEvents.NEW_PLAYLIST_REVISION, PlaylistUiEvents.DELETE_PLAYLIST,//
                PlaylistUiEvents.DELETE_PLAYLISTS,//
                PlaylistUiEvents.RENAME_PLAYLIST_ITEM,//
                PlaylistUiEvents.DELETE_PLAYLIST_REVISION,//
                PlaylistUiEvents.DELETE_PLAYLIST_REVISIONS, //
                PlayListTreeEvents.PLAYLISTREVISION_SELECTED,//
                PlaylistUiEvents.DELETE_BOTH_PLAYLIST_AND_GROUP,//
                CommonEvents.PROJECT_CHANGED,//
                CommonEvents.MODEL_DATA_CREATION_SUCCESS,//
                CommonEvents.MODEL_DATA_UPDATE_SUCCESS,//
                CommonEvents.MODEL_DATA_DELETE_SUCCESS,//
                PlaylistUiEvents.REFRESH_PLAYLIST_NODE);
    }
}
