package fr.hd3d.production.ui.client.playlist.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.production.ui.client.playlist.model.PlaylistUiModel;
import fr.hd3d.production.ui.client.playlist.view.PlaylistView;


public class PlaylistController extends MaskableController
{

    @SuppressWarnings("unused")
    private final PlaylistUiModel model;
    @SuppressWarnings("unused")
    private final PlaylistView view;

    public PlaylistController(PlaylistUiModel model, PlaylistView view)
    {
        this.model = model;
        this.view = view;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
    // forwardToChild(event);
    }

}
