package fr.hd3d.production.ui.client.playlist.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.math.PlaylistUtils;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyReader;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListEditReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.production.ui.client.playlist.editor.EEditTool;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;


/**
 * Model for PlayList Editor associated with PlayListEditor (View) and PlayListEditorController (Controller). It
 * includes the current playlist revison as well as a bunch of PlayListEditModelData wrapped onto a ServiceStore.
 * 
 * @version 1.0
 * @since 1.0
 * 
 */
public class PlayListEditorModel
{
    /**
     * Default duration (number of frames) of a new Edit Object
     */
    private static final int NEW_EDIT_DEFAULT_DURATION = 1;
    /**
     * Current PlayListRevisionModelData object to which this editor is associated
     */
    private PlayListRevisionModelData currentPlaylist = null;
    /**
     * Encapsulates a client side cache of PlayListEditModelData (interface for GXT model objects which supports
     * introspection)
     */
    private final ServiceStore<PlayListEditModelData> editStore = new ServiceStore<PlayListEditModelData>(
            new PlayListEditReader());

    private final FastMap<ProxyModelData> proxyModelsMap = new FastMap<ProxyModelData>();

    /**
     * List of edits that are to be deleted on next save action
     */
    private final List<PlayListEditModelData> editsToDelete = new ArrayList<PlayListEditModelData>();

    /**
     * Creates a new PlayListEditorModel.
     */
    public PlayListEditorModel()
    {}

    /**
     * Get the EFrameRate enum value of the framerate for the current playlist. If {@link #currentPlaylist} is null,
     * {@link #getFrameRate()} returns null.
     * 
     * @return EFrameRate value of the current playlist framerate, or null
     */
    public EFrameRate getFrameRate()
    {
        if (currentPlaylist == null)
            return null;
        return Enum.valueOf(EFrameRate.class, currentPlaylist.getFrameRate());
    }

    /**
     * Returns the store associated with this model.
     * 
     * @return ServiceStore
     */
    public ServiceStore<PlayListEditModelData> getEditStore()
    {
        return editStore;
    }

    public ProxyModelData getProxy(PlayListEditModelData edit)
    {
        if (proxyModelsMap.isEmpty() || edit == null)
            return null;

        return proxyModelsMap.get(edit.getId().toString());
    }

    // private List<Long> getEditsIds()
    // {
    // List<Long> collection = new ArrayList<Long>();
    // for (PlayListEditModelData edit : editStore.getModels())
    // {
    // collection.add(edit.getId());
    // }
    // return collection;
    // }

    private List<Long> getProxiesIds()
    {
        List<Long> collection = new ArrayList<Long>();
        for (PlayListEditModelData edit : editStore.getModels())
        {
            collection.add(edit.getProxy());
        }
        return collection;
    }

    private void updateProxiesMap()
    {
        proxyModelsMap.clear();
        // launch proxy store
        final ServiceStore<ProxyModelData> proxyStore = new ServiceStore<ProxyModelData>(new ProxyReader());
        proxyStore.setPath(ServicesPath.getPath(ProxyModelData.SIMPLE_CLASS_NAME));
        proxyStore.addInConstraint("id", getProxiesIds());
        proxyStore.getLoader().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                for (PlayListEditModelData edit : editStore.getModels())
                {
                    Long proxyId = edit.getProxy();
                    for (ProxyModelData proxy : proxyStore.getModels())
                    {
                        if (proxyId == proxy.getId())
                            proxyModelsMap.put(edit.getId().toString(), proxy);
                    }
                }
                EventDispatcher.forwardEvent(new AppEvent(PlaylistUiEvents.PROXIES_MAP_UPDATED));
            }
        });
        proxyStore.getLoader().load();

    }

    public List<ProxyModelData> getProxies()
    {
        if (proxyModelsMap.isEmpty())
            return null;

        List<ProxyModelData> collection = new ArrayList<ProxyModelData>();
        for (ProxyModelData proxy : proxyModelsMap.values())
            collection.add(proxy);

        return collection;
    }

    /**
     * Returns the current PlayListRevisionModelData object.
     * 
     * @return PlayListRevisionModelData
     */
    public PlayListRevisionModelData getCurrentPlaylist()
    {
        return currentPlaylist;
    }

    /**
     * <p>
     * Sets the current PlayListRevisionModelData (this changes the playlist revision loaded on PlayListEditor
     * interface).
     * </p>
     * <p>
     * If PlayListRevisionModelData is null, then the current editStore is emptied and the currentPlaylist is set to
     * null.
     * </p>
     * 
     * @param currentPlaylist
     */
    public void setCurrentPlaylist(PlayListRevisionModelData currentPlaylist)
    {
        this.currentPlaylist = currentPlaylist;
        editStore.removeAll();
        editStore.clearParameters();
        if (currentPlaylist == null)
        {
            return;
        }
        final Constraint constraint = new EqConstraint(PlayListEditModelData.PLAYLIST_REVISION_FIELD + '.'
                + Hd3dModelData.ID_FIELD, currentPlaylist.getId());
        final List<String> orderFields = new ArrayList<String>(1);
        orderFields.add(PlayListEditModelData.RECIN_FIELD);
        final OrderBy order = new OrderBy(orderFields);
        editStore.addParameter(constraint);
        editStore.addParameter(order);
        editStore.getLoader().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                updateProxiesMap();
            }
        });
        editStore.getLoader().load();
    }

    /**
     * Adds the PlayListEditModelData parameter to the {@link #editsToDelete} list. When the save action is performed,
     * the list content will be deleted, and the list will be cleared.
     * 
     * @param edit
     *            PlayListEditModelData to be deleted
     */
    public void deleteEdit(PlayListEditModelData edit)
    {
        editsToDelete.add(edit);
    }

    /**
     * Creates a new edit (if the {@link #currentPlaylist} is non null) and adds it to the {@link #editStore}.
     */
    public void addNewEdit()
    {
        addNewEdit(null, 0, NEW_EDIT_DEFAULT_DURATION, 0, NEW_EDIT_DEFAULT_DURATION);
    }

    public void addNewEdit(PlayListEditModelData edit)
    {
        slipAllAfter(editStore.getModels(), edit);
        editStore.add(edit);
    }

    public void addNewEdit(List<PlayListEditModelData> editCollection)
    {
        if (editCollection.isEmpty())
            return;
        Iterator<PlayListEditModelData> iterator = editCollection.iterator();
        while (iterator.hasNext())
            addNewEdit(iterator.next());
        // slipAllAfter(editStore.getModels(), editCollection.get(editCollection.size()-1));
        // editStore.add(editCollection);
    }

    /**
     * Creates a new Edit with given parameters. For now, timecode values are considered to be integer values (expressed
     * in number of frames). If the new PlayListEditModelData object overlaps existing PlayListEditModelData objects,
     * then those last ones will be slipped to stay on a good configuration.
     * 
     * @param name
     *            String representation of the Edit
     * @param srcIn
     *            Source in timecode value
     * @param srcOut
     *            Source Out timecode value
     * @param recIn
     *            Record In timecode value
     * @param recOut
     *            Record Out timecode value
     */
    public void addNewEdit(String name, int srcIn, int srcOut, int recIn, int recOut)
    {
        if (currentPlaylist == null)
            return;
        if (srcOut - srcIn < recOut - recIn)
            return;
        if (srcOut - srcIn < 0 || recOut - recIn < 0)
            return;

        PlayListEditModelData newEdit = new PlayListEditModelData();
        newEdit.setName(name);
        newEdit.setPlayListRevision(currentPlaylist.getId());

        final int duration = (recOut - recIn) <= 0 ? NEW_EDIT_DEFAULT_DURATION : recOut - recIn;
        newEdit.setSrcIn(srcIn);
        // Keep source and record ranges with same duration
        newEdit.setSrcOut(srcIn + duration);

        // newEdit.setAssetRevision(assetRevisionId)
        newEdit.setRecIn(recIn);
        newEdit.setRecOut(recIn + duration);

        addNewEdit(newEdit);
    }

    private boolean slipAllAfter(List<PlayListEditModelData> collection, PlayListEditModelData edit)
    {

        final int firstOverlappedEdit = getFirstOverlappedEdit(edit.getRecIn(), edit.getRecOut());
        if (firstOverlappedEdit != -1)
        {
            List<PlayListEditModelData> subList = editStore.getModels().subList(firstOverlappedEdit,
                    editStore.getModels().size());
            final int offset = edit.getRecOut() - subList.get(0).getRecIn();
            if (!PlaylistUtils.slipRec(subList, offset))
                return false;
        }

        return true;
    }

    /**
     * Returns the index of the first overlapping edit (if so). This is used to move newly created edits that might
     * overlap already present ones. If not edit is affected, the method returns -1.
     * 
     * @param recIn
     *            New edit record in frame number
     * @return Index of the first overlaped edit on the editStore. -1 if none is affected.
     */
    private int getFirstOverlappedEdit(int recIn, int recOut)
    {
        int first = -1;
        // overlapping
        for (PlayListEditModelData edit : editStore.getModels())
        {
            if (recIn >= edit.getRecIn() && (recIn < edit.getRecOut() || recOut < edit.getRecOut()))
            {
                first = editStore.getModels().indexOf(edit);
                return first;
            }
        }
        return first;
    }

    /**
     * Returns the previous edit on the playlist, if such exists.
     * 
     * @return previous PlayListEditModelData of the current playlist
     */
    @SuppressWarnings("unused")
    private PlayListEditModelData getLastEdit()
    {
        PlayListEditModelData lastEdit = null;
        for (PlayListEditModelData edit : editStore.getModels())
        {
            if (lastEdit == null || edit.getRecIn() > lastEdit.getRecIn())
            {
                lastEdit = edit;
            }
        }
        return lastEdit;
    }

    /**
     * This method saves the playlist changes on database. It will delete all the to-be-deleted edits, and save the
     * edits data.
     */
    public void saveEdits()
    {
        editStore.commitChanges();
        for (PlayListEditModelData edit : editsToDelete)
        {
            edit.delete();
        }
        editsToDelete.clear();
        for (PlayListEditModelData edit : editStore.getModels())
        {
            edit.save();
        }
    }

    public void saveComments(String comment)
    {
        currentPlaylist.setComment(comment);
    }

    /**
     * The perfomEditAction is called whenever a tool action call has been triggered.
     * 
     * @param edit
     *            PlayListEditModelData on which the action is performed
     * @param fieldName
     *            Field to which the action is performed, this can be PlayListEditModelData.SRCIN_FIELD, etc.
     * @param oldValue
     *            Previous value of the field
     * @param newValue
     *            New value of the field
     * @param tool
     *            EEditTool used to perform the action
     * @return The action has been performed
     */
    public boolean performEditAction(PlayListEditModelData edit, String fieldName, int oldValue, int newValue,
            EEditTool tool)
    {
        final int offset = newValue - oldValue;
        switch (tool)
        {
            case SLIP:
            {
                return performSlipOperation(edit, fieldName, offset);
            }
            case TRIM:
            {
                if (PlayListEditModelData.SRCIN_FIELD.equals(fieldName)
                        || PlayListEditModelData.RECIN_FIELD.equals(fieldName))
                {
                    return performTrimInOperation(edit, offset);
                }
                else if (PlayListEditModelData.SRCOUT_FIELD.equals(fieldName)
                        || PlayListEditModelData.RECOUT_FIELD.equals(fieldName))
                {
                    return performTrimOutOperation(edit, offset);
                }
            }
        }
        return false;
    }

    private boolean performTrimOutOperation(PlayListEditModelData edit, int offset)
    {
        boolean trimOut = PlaylistUtils.trimOut(edit, editStore.getModels(), offset);
        if (trimOut)
        {
            editStore.commitChanges();
        }
        return trimOut;
    }

    private boolean performTrimInOperation(PlayListEditModelData edit, int offset)
    {
        boolean trimIn = PlaylistUtils.trimIn(edit, editStore.getModels(), offset);
        if (trimIn)
        {
            editStore.commitChanges();
        }
        return trimIn;
    }

    private boolean performSlipOperation(PlayListEditModelData edit, String fieldName, int offset)
    {
        if (PlayListEditModelData.SRCIN_FIELD.equals(fieldName) || PlayListEditModelData.SRCOUT_FIELD.equals(fieldName))
        {
            boolean slipSrc = PlaylistUtils.slipSrc(edit, offset);
            if (slipSrc)
            {
                editStore.commitChanges();
            }
            return slipSrc;
        }
        else if (PlayListEditModelData.RECIN_FIELD.equals(fieldName)
                || PlayListEditModelData.RECOUT_FIELD.equals(fieldName))
        {

        }
        return false;
    }

}
