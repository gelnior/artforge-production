package fr.hd3d.production.ui.client.playlist.model;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.model.TreeModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class PlayListRevisionManagerModel extends TreeModel
{
    /**
     * Constructor for PlayListRevisionManagerModel.
     */
    public PlayListRevisionManagerModel(TreeStore<RecordModelData> store)
    {
        setTreeStore(store);
    }

    /**
     * Creates a PlayListEditModelData object with parameters.
     * 
     * @param name
     * @param srcIn
     * @param srcOut
     * @param recIn
     * @param recOut
     * @param playListRevisionId
     * @return PlayListEditModelData object
     */
    public static PlayListEditModelData createPlayListEditModelData(String name, int srcIn, int srcOut, int recIn,
            int recOut, Long playListRevisionId)
    {
        return createPlayListEditModelData(name, srcIn, srcOut, recIn, recOut, 0L, playListRevisionId);
    }

    /**
     * Creates a PlayListEditModelData object with parameters.
     * 
     * @param name
     * @param srcIn
     * @param srcOut
     * @param recIn
     * @param recOut
     * @param proxyId
     * @param playListRevisionId
     * @return PlayListEditModelData object
     */
    public static PlayListEditModelData createPlayListEditModelData(String name, int srcIn, int srcOut, int recIn,
            int recOut, Long proxyId, Long playListRevisionId)
    {
        PlayListEditModelData object = new PlayListEditModelData();
        object.setName(name);
        object.setSrcIn(srcIn);
        object.setSrcOut(srcOut);
        object.setRecIn(recIn);
        object.setRecOut(recOut);
        object.setProxy(proxyId);
        object.setPlayListRevision(playListRevisionId);
        return object;
    }

    /**
     * Creates a PlayListRevisionModelData object with parameters.
     * 
     * @param name
     * @param revision
     * @param playListRevisionGroupId
     * @param projectId
     * @return PlayListRevisionModelData object
     */
    public static PlayListRevisionModelData createPlayListRevisionModelData(String name, int revision,
            Long playListRevisionGroupId, Long projectId)
    {
        PlayListRevisionModelData object = new PlayListRevisionModelData();
        object.setName(name);
        object.setRevision(revision);
        object.setPlayListRevisionGroupId(playListRevisionGroupId);
        object.setProjectId(projectId);
        return object;
    }

    /**
     * Creates a PlayListRevisionGroupModelData object with parameters.
     * 
     * @param name
     * @param parentId
     * @param projectId
     * @return PlayListRevisionGroupModelData object
     */
    public static PlayListRevisionGroupModelData createPlayListRevisionGroupModelData(String name, Long parentId,
            Long projectId)
    {
        PlayListRevisionGroupModelData object = new PlayListRevisionGroupModelData();
        object.setName(name);
        object.setParentId(parentId);
        object.setProjectId(projectId);
        return object;
    }

    /**
     * Checks if a playlistRevisionGroup named @newName exists for the current project
     * */
    public boolean playlistRevisionGroupExists(String newName)
    {

        RecordModelData model = getTreeStore().findModel(PlayListRevisionGroupModelData.NAME_FIELD, newName);
        return model != null;
    }

    /**
     * Checks if a playlistRevisionGroup named @newName exists for the current project
     * */
    public boolean playlistRevisionGroupExists(String newName, Long parentId)
    {
        List<RecordModelData> models = getTreeStore().findModels(PlayListRevisionGroupModelData.NAME_FIELD, newName);
        for (RecordModelData model : models)
        {
            if (((PlayListRevisionGroupModelData) model).getParentId() == parentId)
                return true;
        }

        return false;
    }

    /** Checks if a playlist named @newName exists for the current project */
    public boolean playlistExists(String newName, Long parentId)
    {
        PlayListRevisionModelData model = (PlayListRevisionModelData) (getTreeStore().findModel(
                PlayListRevisionModelData.NAME_FIELD, newName));
        return (model != null) && (model.getPlayListRevisionGroupId() == parentId);
    }

    /** Checks if a playlist named @newName exists for the current project */
    public boolean playlistExists(String newName)
    {
        RecordModelData model = getTreeStore().findModel(PlayListRevisionModelData.NAME_FIELD, newName);
        return model != null;
    }

    public PlayListRevisionModelData getLatestRevision(PlayListRevisionModelData playListRevisionModelData)
    {
        PlayListRevisionModelData latest = playListRevisionModelData;
        List<PlayListRevisionModelData> allRevisions = getAllRevisions(playListRevisionModelData);
        for (PlayListRevisionModelData revision : allRevisions)
        {
            if (revision.getRevision() > latest.getRevision())
            {
                latest = revision;
            }
        }
        return latest;
    }

    public List<PlayListRevisionModelData> getAllRevisions(PlayListRevisionModelData playlist)
    {
        final ArrayList<PlayListRevisionModelData> revisions = new ArrayList<PlayListRevisionModelData>();
        final String name = playlist.getName();

        for (RecordModelData playlistRevision : getTreeStore().getModels())
        {
            if (!PlayListRevisionModelData.CLASS_NAME.equals(playlistRevision.getClassName()))
                return null;
            if (name.equals(playlistRevision.getName()))
            {
                revisions.add((PlayListRevisionModelData) playlistRevision);
            }
        }
        return revisions;
    }

    public List<PlayListRevisionModelData> getAllRevisions(List<PlayListRevisionModelData> revisions)
    {
        final ArrayList<PlayListRevisionModelData> result = new ArrayList<PlayListRevisionModelData>();
        for (RecordModelData playlistRevision : getTreeStore().getModels())
        {
            if (!PlayListRevisionModelData.CLASS_NAME.equals(playlistRevision.getClassName()))
                continue;

            List<PlayListRevisionModelData> allRevisions = getAllRevisions((PlayListRevisionModelData) playlistRevision);
            for (PlayListRevisionModelData allPlaylist : allRevisions)
            {
                if (result.contains(allPlaylist))
                {
                    continue;
                }
                result.add(allPlaylist);
            }
        }
        return result;
    }

    public void addToStore(PlayListRevisionGroupModelData parent, RecordModelData child)
    {
        getTreeStore().add(parent, child, false);
        getTreeStore().update(parent);
    }

    /**
     * Pops a RecordModelData (sets its internalStatus to -1) from the treeStore. This won't delete any of its subitems.
     * 
     * @param ObjectIdToBeDeleted
     *            Long ID of the object to be deleted
     * @return Deleted Object
     */
    public RecordModelData removeFromStoreById(Long ObjectIdToBeDeleted)
    {
        RecordModelData model = (RecordModelData) getTreeStore().findModel(Hd3dModelData.ID_FIELD, ObjectIdToBeDeleted);
        RecordModelData parent = getTreeStore().getParent(model);
        getTreeStore().remove(model);
        getTreeStore().update(parent);
        return model;
    }

    public void update(RecordModelData modelData)
    {
        if (modelData != null && treeStore.contains(modelData))
            treeStore.update(modelData);
    }

    /**
     * Returns the parent of the parameter object
     * 
     * @param item
     *            object whose parent (PlayListRevisionGroupModelData) is to be returned
     * @return PlayListRevisionGroupModelData object
     */
    public PlayListRevisionGroupModelData getParent(RecordModelData item)
    {
        if (item instanceof PlayListRevisionGroupModelData)
            return (PlayListRevisionGroupModelData) treeStore.findModel(RecordModelData.ID_FIELD,
                    ((PlayListRevisionGroupModelData) item).getParentId());
        else if (item instanceof PlayListRevisionModelData)
            return (PlayListRevisionGroupModelData) treeStore.findModel(RecordModelData.ID_FIELD,
                    ((PlayListRevisionModelData) item).getPlayListRevisionGroupId());
        else
            return null;
    }

    public boolean createNewRevisionFrom(final PlayListRevisionModelData data)
    {
        if (data.getId() == null)
        {
            return false;
        }
        final RecordModelData parent = getTreeStore().getParent(data);

        String copyPath = ServicesURI.PLAYLISTREVISIONS + '/' + data.getId() + "/copy";
        RestRequestHandlerSingleton.getInstance().postRequest(copyPath, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                AppEvent event = new AppEvent(CommonEvents.MODEL_DATA_CREATION_SUCCESS);
                event.setData(CommonConfig.MODEL_EVENT_VAR_NAME, data);
                EventDispatcher.forwardEvent(event);
                getTreeStore().update(parent);
            }
        }, data.toPOSTJson());
        return true;
    }
}
