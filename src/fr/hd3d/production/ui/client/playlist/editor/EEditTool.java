package fr.hd3d.production.ui.client.playlist.editor;

public enum EEditTool
{
    SLIP("Slip"), TRIM("Trim");

    public final String name;

    EEditTool(String name)
    {
        this.name = name;

    }
}
