package fr.hd3d.production.ui.client.playlist;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.widget.AutoTaskTypeComboBox;


public class EditablePlaylistGrid extends LayoutContainer
{

    @Override
    protected void onRender(Element parent, int index)
    {
        super.onRender(parent, index);
        setLayout(new FlowLayout(10));

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig column = new ColumnConfig();
        column.setId("name");
        column.setHeader("Column name");
        column.setWidth(200);
        configs.add(column);

        final AutoTaskTypeComboBox combo = new AutoTaskTypeComboBox();
        combo.setForceSelection(true);
        combo.setTriggerAction(TriggerAction.ALL);
        CellEditor editor = new CellEditor(combo) {
            @Override
            public Object preProcessValue(Object value)
            {
                if (value == null)
                {
                    return value;
                }
                return combo.getName();
            }
        };

        column = new ColumnConfig();
        column.setId("combo");
        column.setHeader("combo header");
        column.setWidth(50);
        column.setEditor(editor);
        configs.add(column);

        final ListStore<PlaylistCreationElement> store = new ListStore<PlaylistCreationElement>();
        store.add(createData());

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setHeading("Select Proxies");
        cp.setFrame(true);
        cp.setSize(200, 300);
        cp.setLayout(new FitLayout());

        final EditorGrid<PlaylistCreationElement> grid = new EditorGrid<PlaylistCreationElement>(store, cm);
        grid.setAutoExpandColumn("name");
        grid.setBorders(true);
        cp.add(grid);

        add(cp);

    }

    private List<PlaylistCreationElement> createData()
    {
        List<PlaylistCreationElement> list = new ArrayList<PlaylistCreationElement>();

        list.add(new PlaylistCreationElement("nom1", 1L, 1L, 1L, 1L));
        list.add(new PlaylistCreationElement("nom2", 1L, 1L, 1L, 1L));
        list.add(new PlaylistCreationElement("nom3", 1L, 1L, 1L, 1L));

        return list;

    }

    @SuppressWarnings({ "serial" })
    private static class PlaylistCreationElement extends BaseModel
    {

        public PlaylistCreationElement(String name, Long taskTypeID, Long assetID, Long fileRevisionID, Long proxyID)
        {
            set("name", name);
            set("taskTypeID", taskTypeID);
            set("assetID", assetID);
            set("fileRevisionID", fileRevisionID);
            set("proxyID", proxyID);
        }

        public String getName()
        {
            return get("name");
        }

        // public void setName(String name)
        // {
        // set("name", name);
        // }

        public String toString()
        {
            return getName();
        }
    }

}
