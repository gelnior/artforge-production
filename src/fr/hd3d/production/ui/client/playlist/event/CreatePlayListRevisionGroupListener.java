package fr.hd3d.production.ui.client.playlist.event;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;


/**
 * Listener of CreatePlayListRevisionGroupListener buttons. Displays a prompt dialog and creates a group with it.
 * 
 * @author guillaume-maucomble
 * @version 1.0
 * @since 1.0
 */
public class CreatePlayListRevisionGroupListener implements Listener<BaseEvent>
{
    /** Can be used to set initial PlayListRevisionModelData collection into new PlayListRevisionGroup */
    List<PlayListRevisionModelData> collection;

    public void handleEvent(BaseEvent be)
    {
        // TODO internationalization
        final MessageBox newPlaylistPrompt = MessageBox.prompt("Add new directory", "Enter new directory name");
        newPlaylistPrompt.addCallback(dialog);
    }

    public void setCollection(List<PlayListRevisionModelData> collection)
    {
        this.collection = collection;
    }

    private Listener<MessageBoxEvent> dialog = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent be)
        {
            String buttonClicked = be.getButtonClicked().getText().toLowerCase();
            if (MessageBox.OK.toLowerCase().equals(buttonClicked) == false)
            {
                return;
            }
            final String newName = be.getValue();
            AppEvent event = new AppEvent(PlaylistUiEvents.NEW_PLAYLIST_GROUP);
            event.setData("name", newName);
            event.setData("collection", collection);
            EventDispatcher.forwardEvent(event);
        };
    };

}
