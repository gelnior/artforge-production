package fr.hd3d.production.ui.client.playlist.event;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.event.EventDispatcher;


public class RenamePlayListRevisionItemListener implements Listener<BaseEvent>
{
    private String originalName;

    protected void setName(String name)
    {
        originalName = name;
    }

    public void handleEvent(BaseEvent be)
    {
        final MessageBox newPlaylistPrompt = MessageBox.prompt("Rename " + originalName, "Enter new name");
        newPlaylistPrompt.getTextBox().setValue(originalName);
        newPlaylistPrompt.addCallback(dialog);
    }

    private Listener<MessageBoxEvent> dialog = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent be)
        {
            String buttonClicked = be.getButtonClicked().getText().toLowerCase();
            if (MessageBox.OK.toLowerCase().equals(buttonClicked) == false)
            {
                return;
            }
            final String newName = be.getValue();
            AppEvent event = new AppEvent(PlaylistUiEvents.RENAME_PLAYLIST_ITEM);
            event.setData("name", newName);
            EventDispatcher.forwardEvent(event);
        };
    };

}
