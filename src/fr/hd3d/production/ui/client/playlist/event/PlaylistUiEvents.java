package fr.hd3d.production.ui.client.playlist.event;

import com.extjs.gxt.ui.client.event.EventType;


public class PlaylistUiEvents
{

    /** Playlist Revision Group */
    public static final EventType NEW_PLAYLIST_GROUP = new EventType();
    public static final EventType DELETE_PLAYLIST_GROUP = new EventType();
    public static final EventType DELETE_PLAYLIST_GROUPS = new EventType();
    public static final EventType CHANGE_PLAYLIST_GROUP = new EventType();
    /** Playlist Revision */
    public static final EventType LOAD_PLAYLIST = new EventType();
    public static final EventType NEW_PLAYLIST = new EventType();
    public static final EventType NEW_PLAYLIST_REVISION = new EventType();
    public static final EventType DELETE_PLAYLIST = new EventType();
    public static final EventType RENAME_PLAYLIST_ITEM = new EventType();
    public static final EventType DELETE_PLAYLIST_REVISION = new EventType();
    public static final EventType DELETE_PLAYLISTS = new EventType();
    public static final EventType DELETE_PLAYLIST_REVISIONS = new EventType();
    public static final EventType REFRESH_PLAYLIST = new EventType();
    public static final EventType PLAYLIST_FRAMERATE_CHANGED = new EventType();
    public static final EventType FILE_REVISION_CHANGED = new EventType();
    /** Edits */
    public static final EventType LOAD_PLAYLIST_EDITS = new EventType();
    public static final EventType NEW_PLAYLIST_EDIT = new EventType();
    public static final EventType SAVE_PLAYLIST = new EventType();
    public static final EventType DELETE_PLAYLIST_EDIT = new EventType();
    public static final EventType DELETE_PLAYLIST_EDIT_EXIST = new EventType();
    /** Move Editing Tool */
    public static final EventType MOVE_EDITING_TOOL = new EventType();
    /** Ripple Editing Tool */
    public static final EventType RIPPLE_EDITING_TOOL = new EventType();
    /** Both */
    public static final EventType DELETE_BOTH_PLAYLIST_AND_GROUP = new EventType();
    /** General events */
    public static final EventType REFRESH_PLAYLIST_NODE = new EventType();

    public static final EventType PROXIES_MAP_UPDATED = new EventType(500);

}
