package fr.hd3d.production.ui.client.playlist.event;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Listener of CreatePlayList buttons. Displays a prompt dialog and creates a playlist with
 * 
 * @TODO internationalization
 * @author guillaume-maucomble
 */
public class CreatePlayListListener implements Listener<BaseEvent>
{
    List<? extends Hd3dModelData> collection;

    public void handleEvent(BaseEvent be)
    {
        final MessageBox newPlaylistPrompt = MessageBox.prompt("Add new playlist", "Enter new playlist name");
        newPlaylistPrompt.addCallback(dialog);
    }

    public void setCollection(List<? extends Hd3dModelData> collection)
    {
        this.collection = collection;
    }

    private Listener<MessageBoxEvent> dialog = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent be)
        {
            String buttonClicked = be.getButtonClicked().getText().toLowerCase();
            if (MessageBox.OK.toLowerCase().equals(buttonClicked) == false)
            {
                return;
            }
            final String newName = be.getValue();
            if (newName == null || "".equals(newName))
            {
                // TODO display error message
                return;
            }
            AppEvent event = new AppEvent(PlaylistUiEvents.NEW_PLAYLIST);
            event.setData("name", newName);
            event.setData("collection", collection);
            EventDispatcher.forwardEvent(event);
        };
    };

}
