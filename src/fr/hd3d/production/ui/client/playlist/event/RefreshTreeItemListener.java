package fr.hd3d.production.ui.client.playlist.event;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Listener for events that trigger {@link PlaylistUiEvents#REFRESH_PLAYLIST_NODE} event.
 * 
 * @author guillaume-maucomble
 * 
 */
public class RefreshTreeItemListener implements Listener<BaseEvent>
{

    public void handleEvent(BaseEvent be)
    {
        AppEvent event = new AppEvent(PlaylistUiEvents.REFRESH_PLAYLIST_NODE);
        EventDispatcher.forwardEvent(event);
    }

}
