package fr.hd3d.production.ui.client.playlist.view;

import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.util.hotkeys.HotKeysUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.production.ui.client.playlist.controller.PlaylistController;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.playlist.model.PlaylistUiModel;


/**
 * General Interface for Playlist tab on Production. It contains both {@linkplain PlaylistRevisionManagerView} and
 * {@linkplain PlayListEditor}.
 * 
 * This contains a HotKeyPanel (from
 * {@link fr.hd3d.common.ui.client.util.hotkeys.HotKeysUtils#addHotKeys(int, com.extjs.gxt.ui.client.event.EventType)}).
 * 
 */
public class PlaylistView extends BorderedPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** View name to tab distinction purpose. */
    public static final String NAME = "Playlists";
    /** Prefered size (in pixels) for playlistRevisionManager */
    public static final int PREFERED_LEFT_WIDTH = 250;
    /** Model for hmvc purpose */
    private final PlaylistUiModel model = new PlaylistUiModel();
    /** Controller for hmvc purpose */
    private final PlaylistController controller = new PlaylistController(model, this);
    /** Left Part of the screen : this contains a tree view */
    private final PlaylistRevisionManagerView playlistRevisionManager = new PlaylistRevisionManagerView();
    /** Right part of the screen : contains a grid */
    private final PlayListEditor playlistEditor = new PlayListEditor();
    /** Hidable Panel */
    protected ContentPanel westPanel = new ContentPanel();

    /**
     * Default constructor. This places both {@link #playlistRevisionManager} and {@link #playlistEditor} into a
     * focusable panel that can handle hot keys.
     */
    public PlaylistView()
    {
        this.initControllers();
        this.initWestPanel();

        // Makes a focusable panel
        BorderedPanel focusablePanel = new BorderedPanel();

        BorderLayoutData data = focusablePanel.addWest(westPanel);
        data.setMinSize(PlaylistView.PREFERED_LEFT_WIDTH);
        data.setCollapsible(true);

        focusablePanel.addCenter(playlistEditor);

        Component addHotKeysPanel = fr.hd3d.common.ui.client.util.hotkeys.HotKeysUtils.addHotKeys(focusablePanel,
                HotKeysUtils.CTRL_S, PlaylistUiEvents.SAVE_PLAYLIST);
        this.addCenter(addHotKeysPanel);
    }

    /**
     * Initializes {@link #controller}.
     */
    private void initControllers()
    {
        controller.addChild(playlistRevisionManager.getController());
        controller.addChild(playlistEditor.getController());
        EventDispatcher.get().addController(controller);
    }

    /**
     * Returns the current controller.
     * 
     * @return current controller
     */
    public PlaylistController getController()
    {
        return controller;
    }

    /**
     * Idle every widget contains inside view and mask view controller.
     */
    public void idle()
    {
        this.playlistRevisionManager.getController().mask();
        this.controller.mask();
        this.playlistEditor.getController().mask();
    }

    /**
     * Unidle every widget contains inside view and unmask view controller.
     */
    public void unidle()
    {
        this.playlistRevisionManager.getController().unMask();
        this.controller.unMask();
        this.playlistEditor.getController().unMask();
    }

    /**
     * Initializes Left Panel with {@link #playlistRevisionManager}.
     */
    private void initWestPanel()
    {
        this.westPanel.setHeaderVisible(true);
        this.westPanel.setLayout(new FitLayout());
        this.westPanel.setBorders(false);

        BorderLayoutData data = this.addWest(westPanel, PlaylistView.PREFERED_LEFT_WIDTH);
        data.setMinSize(PlaylistView.PREFERED_LEFT_WIDTH);
        data.setCollapsible(true);

        this.westPanel.setHeading(CONSTANTS.Playlists());
        this.westPanel.add(playlistRevisionManager);
    }
}
