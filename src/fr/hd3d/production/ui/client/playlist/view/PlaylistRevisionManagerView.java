package fr.hd3d.production.ui.client.playlist.view;

import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.TreePanelEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanelSelectionModel;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.playlist.view.PlayListRevisionTree;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.playlist.controller.PlayListRevisionManagerController;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.playlist.model.PlayListRevisionManagerModel;


/**
 * Widget displayed for PlayList global UI.
 * 
 * @author jules-pajot, guillaume-maucomble
 * @version 1.1 Enchanced to handle multiple RecordModelData (PlayListRevisionModelData and
 *          PlayListRevisionGroupModelData) in {@link #playListRevisionTree}
 * @since 1.0
 */
public class PlaylistRevisionManagerView extends BorderedPanel
{
    /** Internationalization purpose */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Internationalization purpose */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);
    /** Model on hmvc */
    private final PlayListRevisionManagerModel model;
    /** Controller on hmvc */
    private final PlayListRevisionManagerController controller;
    /** Tree widget */
    private final PlayListRevisionTree playListRevisionTree = new PlayListRevisionTree();
    /** Specific contextual menu for {@link #playListRevisionTree} */
    private final PlayListContextMenu contextMenu = new PlayListContextMenu();

    /**
     * Default constructor for PlaylistRevisionManager view.
     */
    public PlaylistRevisionManagerView()
    {
        setHeading(COMMON_CONSTANTS.Playlists());
        initTree();
        model = new PlayListRevisionManagerModel(playListRevisionTree.getStore());
        controller = new PlayListRevisionManagerController(model, this);
        EventDispatcher.get().addController(controller);
        initListeners();
    }

    /**
     * @return Tree wrapped in this panel.
     */
    public TreePanel<RecordModelData> getTreePanel()
    {
        if (playListRevisionTree != null)
            return playListRevisionTree.getTree();
        else
            return null;
    }

    /**
     * Returns the tree's selection model.
     * 
     * @see {@link TreePanel#getSelectionModel()}
     * @return the tree's selection model.
     */
    public TreePanelSelectionModel<RecordModelData> getSelectionModel()
    {
        if (playListRevisionTree != null)
            return playListRevisionTree.getTree().getSelectionModel();

        return null;
    }

    /**
     * Returns the selected item.
     * 
     * @return the selected item.
     */
    public RecordModelData getSelectedItem()
    {
        if (playListRevisionTree != null)
            return playListRevisionTree.getTree().getSelectionModel().getSelectedItem();
        return null;
    }

    /**
     * 
     * @return the list of selected RecordModelData objects
     */
    public List<RecordModelData> getSelectedItems()
    {
        if (playListRevisionTree != null)
            return playListRevisionTree.getTree().getSelectionModel().getSelectedItems();
        return null;
    }

    /**
     * @see BaseTree#refreshSelection()
     */
    public void refreshSelection()
    {
        this.playListRevisionTree.refreshSelection();
    }

    /**
     * 
     * @return PlayListRevisionManagerController controller object for PlaylistRevisionManager
     */
    public PlayListRevisionManagerController getController()
    {
        return controller;
    }

    /**
     * Private method to initialize {@link #playListRevisionTree} context.
     */
    private void initTree()
    {
        ProjectModelData currentProject = MainModel.getCurrentProject();
        boolean enabled = currentProject != null;
        String name;
        if (enabled)
        {
            playListRevisionTree.setCurrentProject(currentProject);
            name = currentProject.getName();
        }
        else
        {
            name = COMMON_CONSTANTS.Playlists();
        }
        playListRevisionTree.setRootName(name);
        playListRevisionTree.setBodyBorder(false);
        playListRevisionTree.setBorders(false);
        playListRevisionTree.setContextMenu(contextMenu);
        setEnabled(enabled);
        addCenter(playListRevisionTree);
    }

    /**
     * Enables or Disables Tree widget
     */
    public void setEnabled(boolean enabled)
    {
        if (playListRevisionTree != null)
            playListRevisionTree.setEnabled(enabled);
    }

    /**
     * Reset the project so the tree is reloaded
     * 
     * @param project
     */
    public void reloadTree(ProjectModelData project)
    {
        setEnabled(false);
        playListRevisionTree.setCurrentProject(project);
        setEnabled(project != null);
    }

    /**
     * Sets the listeners for {@link #playListRevisionTree} : adds support for {@link Events#SelectionChange} and
     * {@link Events#OnDoubleClick} events.
     */
    private void initListeners()
    {
        playListRevisionTree.getTree().getSelectionModel()
                .addListener(Events.SelectionChange, new Listener<SelectionChangedEvent<RecordModelData>>() {
                    public void handleEvent(SelectionChangedEvent<RecordModelData> be)
                    {
                        contextMenu.setSelection(be.getSelection());
                    }
                });

        playListRevisionTree.getTree().addListener(Events.OnDoubleClick,
                new Listener<TreePanelEvent<RecordModelData>>() {

                    public void handleEvent(TreePanelEvent<RecordModelData> be)
                    {
                        RecordModelData selectedItem = be.getItem();
                        if (selectedItem == null)
                            return;
                        if (PlayListRevisionModelData.CLASS_NAME.equals(selectedItem.getClassName()))
                        {
                            EventDispatcher.forwardEvent(PlaylistUiEvents.LOAD_PLAYLIST,
                                    (PlayListRevisionModelData) selectedItem);
                        }

                    }
                });
    }
}
