package fr.hd3d.production.ui.client.playlist.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.playlist.event.CreatePlayListListener;
import fr.hd3d.production.ui.client.playlist.event.CreatePlayListRevisionGroupListener;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.playlist.event.RefreshTreeItemListener;
import fr.hd3d.production.ui.client.playlist.event.RenamePlayListRevisionItemListener;


/**
 * Contextual menu for PlayListRevisionManager List items.
 * 
 * @author jules-pajot, guillaume-maucomble
 * 
 * @since 1.0
 * @version 1.1
 */
public class PlayListContextMenu extends EasyMenu
{
    /** Internationalization purpose */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Internationalization purpose */
    public static ProductionConstants PRODUCTION_CONSTANTS = GWT.create(ProductionConstants.class);

    /** Current selection contains PlayListRevisionModelData object(s) */
    public final static int PLAYLIST_REVISION = 0;
    /** Current selection contains PlayListRevisionGroupModelData object(s) */
    public final static int PLAYLIST_REVISION_GROUP = 1;
    /** Current selection contains both PlayListRevisionModelData and PlayListRevisionGroupModelData objects */
    public final static int BOTH = 2;

    /** Add PlayList item */
    private final MenuItem newPlayListRevisionItem = new MenuItem(COMMON_CONSTANTS.Add() + ' '
            + COMMON_CONSTANTS.Playlist(), Hd3dImages.getAddIcon());
    /** Add PlayList group item */
    private final MenuItem newPlayListRevisionGroupItem = new MenuItem(COMMON_CONSTANTS.Add() + ' '
            + COMMON_CONSTANTS.Group(), Hd3dImages.getAddIcon());
    /** Rename item */
    private final MenuItem renamePlaylistItem = new MenuItem(COMMON_CONSTANTS.Rename());
    /** Delete item, used for PlayListRevision */
    private final MenuItem deletePlaylistItem = new MenuItem(COMMON_CONSTANTS.Delete());
    /** Add Revision Item */
    private final MenuItem addRevisionItem = new MenuItem(PRODUCTION_CONSTANTS.NewRevision());
    /** Delete Revision item */
    private final MenuItem deleteRevisionItem = new MenuItem(COMMON_CONSTANTS.Delete() + " rev");
    /** Delete group item */
    private final MenuItem deletePlayListRevisionGroup = new MenuItem(COMMON_CONSTANTS.Delete());
    /** Delete multiple item */
    private final MenuItem deleteMultiple = new MenuItem(COMMON_CONSTANTS.Delete());
    /** Refresh item */
    private final MenuItem refreshItem = new MenuItem("Refresh", Hd3dImages.getRefreshIcon());
    /** The selection can be either PlayListRevisionModelData or PlayListRevisionGroupModelData */
    private List<RecordModelData> selection;

    public PlayListContextMenu()
    {
        super();

        initPlayListRevisionListeners();
        initPlayListRevisionGroupListeners();
        initBothListeners();
        buildEmpty();

        newPlayListRevisionItem.addListener(Events.Select, new CreatePlayListListener());

        newPlayListRevisionGroupItem.addListener(Events.Select, new CreatePlayListRevisionGroupListener());

        refreshItem.addListener(Events.Select, new RefreshTreeItemListener());

        renamePlaylistItem.addListener(Events.Select, new RenamePlayListRevisionItemListener() {
            @Override
            public void handleEvent(BaseEvent be)
            {
                setName(selection.get(0).getName());
                super.handleEvent(be);
            }
        });
    }

    /**
     * Initializes the context menu for PLayListRevision items
     */
    private void buildPlayListRevisionMenu()
    {
        renamePlaylistItem.setText(COMMON_CONSTANTS.Rename());
        deleteRevisionItem.setText(COMMON_CONSTANTS.Delete() + " rev");

        add(renamePlaylistItem);
        add(deletePlaylistItem);
        add(new SeparatorMenuItem());
        add(addRevisionItem);
        add(deleteRevisionItem);
        add(new SeparatorMenuItem());
        add(refreshItem);
    }

    /**
     * Initializes the context menu for PlayListRevisionGroup items
     */
    private void buildPlayListRevisionGroupMenu()
    {
        add(renamePlaylistItem);
        add(deletePlayListRevisionGroup);
        add(new SeparatorMenuItem());
        add(newPlayListRevisionGroupItem);
        add(newPlayListRevisionItem);
        add(new SeparatorMenuItem());
        add(refreshItem);
    }

    /**
     * Initializes the context menu for both PlayListRevision and PlayListRevisionGroup items
     */
    private void buildBothMenu()
    {
        add(deleteMultiple);
        add(new SeparatorMenuItem());
        add(refreshItem);
    }

    /**
     * Initializes the context menu for no valid selection
     */
    private void buildEmpty()
    {
        add(refreshItem);
    }

    private void initPlayListRevisionListeners()
    {
        deletePlaylistItem.addListener(Events.Select, createSelectionListener(PlaylistUiEvents.DELETE_PLAYLIST,
                PlaylistUiEvents.DELETE_PLAYLISTS));
        deleteRevisionItem.addListener(Events.Select, createSelectionListener(
                PlaylistUiEvents.DELETE_PLAYLIST_REVISION, PlaylistUiEvents.DELETE_PLAYLIST_REVISIONS));
        addRevisionItem.addListener(Events.Select,
                createSelectionListener(PlaylistUiEvents.NEW_PLAYLIST_REVISION, null));

    }

    private void initPlayListRevisionGroupListeners()
    {
        deletePlayListRevisionGroup.addListener(Events.Select, createSelectionListener(
                PlaylistUiEvents.DELETE_PLAYLIST_GROUP, PlaylistUiEvents.DELETE_PLAYLIST_GROUPS));
    }

    private void initBothListeners()
    {
        deleteMultiple.addListener(Events.Select, createSelectionListener(
                PlaylistUiEvents.DELETE_BOTH_PLAYLIST_AND_GROUP, PlaylistUiEvents.DELETE_BOTH_PLAYLIST_AND_GROUP));
    }

    /**
     * builds a selection listener
     * 
     * @param eventToDispatch
     *            the eventType to dispatch if the selection contains one element only
     * @param eventToDispatchIfMany
     *            the eventType to dispatch if the selection has many items
     * 
     * */
    private Listener<BaseEvent> createSelectionListener(final EventType eventToDispatch,
            final EventType eventToDispatchIfMany)
    {
        return new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (selection == null || selection.isEmpty())
                {
                    return;
                }
                if (selection.size() == 1)
                {
                    if (eventToDispatch == null)
                    {
                        return;
                    }
                    EventDispatcher.forwardEvent(eventToDispatch, selection.get(0));
                }
                else
                {
                    if (eventToDispatchIfMany == null)
                    {
                        return;
                    }
                    EventDispatcher.forwardEvent(eventToDispatchIfMany, selection);
                }
            }
        };
    }

    /**
     * Gives the kind of selection that is currently at stake. Returns -1 if the type is unknown.
     * 
     * @return either -1, {@link #PLAYLIST_REVISION_GROUP}, {@link #PLAYLIST_REVISION} or {@link #BOTH}
     */
    public int getSelectionType()
    {
        if (selection == null || selection.isEmpty())
            return -1;

        final RecordModelData firstPlaylist = selection.get(0);
        if (!(PlayListRevisionModelData.CLASS_NAME.equals(firstPlaylist.getClassName()) || PlayListRevisionGroupModelData.CLASS_NAME
                .equals(firstPlaylist.getClassName())))
            return -1;

        for (RecordModelData playList : selection)
        {
            if (firstPlaylist.getClassName().equals(playList.getClassName()))
                continue;
            else
                return BOTH;
        }

        return (PlayListRevisionModelData.CLASS_NAME.equals(firstPlaylist.getClassName())) ? PLAYLIST_REVISION
                : PLAYLIST_REVISION_GROUP;
    }

    public void setSelection(List<RecordModelData> selection)
    {
        if (selection == null || selection.isEmpty())
        {
            buildEmpty();
            return;
        }

        this.selection = selection;

        removeAll();
        int type = getSelectionType();
        final RecordModelData model = selection.get(0);
        final String plName = model.getName();

        if (type == PLAYLIST_REVISION)
        {
            buildPlayListRevisionMenu();

            if (selection.size() == 1)
            {
                renamePlaylistItem.setEnabled(true);
                renamePlaylistItem.setText(COMMON_CONSTANTS.Rename() + ' ' + plName);
                deletePlaylistItem.setText(COMMON_CONSTANTS.Delete() + ' ' + plName);
                addRevisionItem.setText(PRODUCTION_CONSTANTS.NewRevision() + COMMON_CONSTANTS.from()
                        + ((PlayListRevisionModelData) model).getPlaylistString());
                deleteRevisionItem.setText(COMMON_CONSTANTS.Delete() + ' '
                        + ((PlayListRevisionModelData) model).getPlaylistString());
            }
            else
            {
                renamePlaylistItem.setEnabled(false);
                deletePlaylistItem.setText(COMMON_CONSTANTS.Delete() + ' ' + getPlaylistsCount()
                        + COMMON_CONSTANTS.Playlist() + "(s)");
                deleteRevisionItem.setText(COMMON_CONSTANTS.Delete() + ' ' + selection.size() + ' '
                        + COMMON_CONSTANTS.Revision() + "s");
            }
        }
        else if (type == PLAYLIST_REVISION_GROUP)
        {
            buildPlayListRevisionGroupMenu();
            if (selection.size() == 1)
            {
                renamePlaylistItem.setEnabled(true);
                renamePlaylistItem.setText("Rename " + plName);
                deletePlayListRevisionGroup.setText(COMMON_CONSTANTS.Delete() + ' ' + plName);
            }
            else
            {
                renamePlaylistItem.setEnabled(false);
                deletePlayListRevisionGroup.setText(COMMON_CONSTANTS.Delete() + ' ' + selection.size() + " groups");
            }
        }
        else if (type == BOTH)
        {
            buildBothMenu();
            deleteMultiple.setText(COMMON_CONSTANTS.Delete());
        }
        else
        {
            buildEmpty();
        }

    }

    /**
     * Returns the number of selected items.
     * 
     * @return Number of selected items
     */
    private int getPlaylistsCount()
    {
        int count = 0;
        final ArrayList<String> plNames = new ArrayList<String>();
        for (RecordModelData playlist : selection)
        {
            String name = playlist.getName();
            if (containsString(name, plNames) == false)
            {
                count++;
                plNames.add(name);
            }
        }
        return count;
    }

    /**
     * Returns true if the parameter string (@str) is in collection.
     * 
     * @param str
     *            Value to look for
     * @param collection
     *            Values to look into
     * @return Success of the search
     */
    private boolean containsString(String str, List<String> collection)
    {
        if (str == null)
        {
            return false;
        }
        for (String current : collection)
        {
            if (str.equals(current))
            {
                return true;
            }
        }
        return false;
    }
}
