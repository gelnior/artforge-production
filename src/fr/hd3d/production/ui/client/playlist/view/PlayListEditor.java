package fr.hd3d.production.ui.client.playlist.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EditorEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.extjs.gxt.ui.client.widget.custom.Portal;
import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.RowNumberer;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.VerticalSplitPanel;

import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.EFrameRateModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.math.PlaylistUtils;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.grid.BaseEditorGrid;
import fr.hd3d.common.ui.client.widget.grid.MultiLineCellSelectionModel;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;
import fr.hd3d.common.ui.client.widget.proxyplayer.ProxyPlayer;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.constant.ProductionMessages;
import fr.hd3d.production.ui.client.playlist.controller.PlayListEditorController;
import fr.hd3d.production.ui.client.playlist.editor.EEditTool;
import fr.hd3d.production.ui.client.playlist.event.PlaylistUiEvents;
import fr.hd3d.production.ui.client.playlist.model.PlayListEditorModel;
import fr.hd3d.production.ui.client.view.AddEditDialog;


/**
 * PlayListEditor manages layout and widgets functionnalities editing playlist.
 * 
 * @version 1.1 New layout
 * @since 1.0
 */
public class PlayListEditor extends BorderedPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);
    /** Parameterizable messages to display */
    public static ProductionMessages MESSAGES = GWT.create(ProductionMessages.class);
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Container for {@link #playListPortlet} and {@link #videoPortlet} */
    Portal portal;
    /** Container for video portlet */
    Portlet commentPortlet;
    /** Container for video portlet */
    Portlet videoPortlet;
    /** The model for the mvc triplet (PlayListEditorModel, PlayListEditor, PlayListEditorController) */
    private final PlayListEditorModel editorModel;
    /** The controller for the mvc triplet (PlayListEditorModel, PlayListEditor, PlayListEditorController) */
    private final PlayListEditorController controller;
    /** Editing tool (Trim/Slip) */
    private EEditTool currentTool = null;
    /** Grid property : width of asset column */
    private static final int PROXY_COLUMN_WIDTH = 300;
    /** Grid property : width of timecode column */
    private static final int TC_COLUMN_WIDTH = 80;
    /** Column model used to enhance events */
    private ColumnModel columnModel;
    /** Actual Grid to display PlayListEditModelData objects */
    private BaseEditorGrid<PlayListEditModelData> editorGrid;
    /** Add Edit Dialog widget */
    private AddEditDialog addEditDialog;
    /** Create a new edit Button */
    private Button addEditButton;
    /** Delete edit button */
    private Button deleteEditBtn;
    /** Save playlist button */
    private Button saveButton;
    /** Save as new version button */
    private Button saveAsNewVersionButton;
    /** playlist' framerate combo widget */
    private ComboBox<EFrameRateModelData> framerateCombo;
    /** Store of EFrameRateModelData objects used to fill {@link #framerateCombo} */
    private ListStore<EFrameRateModelData> frameRateStore;
    /** This keeps current selection for editing purpose. This is also convenient when edits needs to be re-selected. */
    private List<PlayListEditModelData> selection = new ArrayList<PlayListEditModelData>();

    /** Comments area, to be displayed whenever a plalist is displayed */
    private TextArea commentArea;
    /** */
    private BorderedPanel playListPanel = new BorderedPanel();
    /** Enhanced gui */
    private VerticalSplitPanel verticalSplitPane = new VerticalSplitPanel();

    /**
     * Creates a PlayListEditor object with new PlayListEditorModel and PlayListEditorController. This also initializes
     * the user interface.
     */
    public PlayListEditor()
    {
        this.editorModel = new PlayListEditorModel();
        this.controller = new PlayListEditorController(editorModel, this);
        EventDispatcher.get().addController(controller);
        addCenter(verticalSplitPane);
        verticalSplitPane.setWidth("100%");
        verticalSplitPane.setVisible(true);
        initUi();

    }

    /**
     * Returns the editor model associated with this view.
     * 
     * @return editor model associated with this view
     */
    public PlayListEditorModel getEditorModel()
    {
        return editorModel;
    }

    /**
     * Returns the controller associated with this view.
     * 
     * @return controller associated with this view
     */
    public PlayListEditorController getController()
    {
        return controller;
    }

    /**
     * Launches User interface initialization.
     */
    private void initUi()
    {
        initGrid();
        initFormView();
        initPortalView();

        playListPanel = new BorderedPanel();
        playListPanel.setHeight(300);
        initToolBar();
        playListPanel.setLayout(new FitLayout());
        playListPanel.addCenter(editorGrid);
        verticalSplitPane.setBottomWidget(playListPanel);

        // enableAllButton(false);
    }

    /**
     * Initializes {@link #addEditDialog}.
     */
    private void initFormView()
    {
        addEditDialog = new AddEditDialog(PlaylistUiEvents.NEW_PLAYLIST_EDIT, COMMON_CONSTANTS.New() + " "
                + CONSTANTS.Edit());
        addEditDialog.okText = COMMON_CONSTANTS.Add();
    }

    private void initPortalView()
    {
        portal = new Portal(2);
        portal.setBorders(true);
        portal.setColumnWidth(0, .5);
        portal.setColumnWidth(1, .5);
        verticalSplitPane.setTopWidget(portal);
    }

    /**
     * Inits a column with a number editor.
     * 
     * @param config
     * @return number formatted ColumnConfig
     */
    private ColumnConfig prepareTimecodeColumn(ColumnConfig config)
    {
        NumberField numberField = new NumberField();
        // The field wraps only Integer values
        numberField.setPropertyEditorType(Integer.class);

        numberField.setValidator(new Validator() {
            public String validate(Field<?> field, String value)
            {
                try
                {
                    Integer frame = Integer.valueOf(value);
                    if (frame < 0)
                        return CONSTANTS.FrameNumberCannotBeNegative();
                    return null;
                }
                catch (NumberFormatException nbfe)
                {
                    return value + CONSTANTS.IsNotValidFrameNumber();
                }
            }
        });
        NumberFormat integerFormat = NumberFormat.getFormat("#");
        numberField.setFormat(integerFormat);
        numberField.setAllowBlank(false);
        numberField.setAutoValidate(true);

        CellEditor editor = new CellEditor(numberField);
        editor.addListener(Events.BeforeComplete, new Listener<EditorEvent>() {
            public void handleEvent(EditorEvent editorEvent)
            {
                onBeforeEditComplete(editorEvent);
            }
        });
        config.setEditor(editor);
        config.setSortable(false);
        return config;
    }

    /**
     * Initializes {@link #editorGrid}.
     */
    private void initGrid()
    {
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        RowNumberer rowNumber = new RowNumberer();
        configs.add(rowNumber);

        ColumnConfig nameCol = new ColumnConfig(PlayListEditModelData.NAME_FIELD, COMMON_CONSTANTS.Name(),
                TC_COLUMN_WIDTH);
        nameCol.setSortable(false);
        nameCol.setRowHeader(true);
        nameCol.setEditor(new CellEditor(new TextField<String>()));

        // TODO Double Click on (or edit) it would launch the very same dialog from which the user created the PL.
        ColumnConfig fileRevCol = new ColumnConfig(PlayListEditModelData.PROXY_FIELD, "Proxy", PROXY_COLUMN_WIDTH);
        fileRevCol.setSortable(false);

        configs.add(fileRevCol);
        configs.add(nameCol);
        configs.add(prepareTimecodeColumn(new ColumnConfig(PlayListEditModelData.SRCIN_FIELD, COMMON_CONSTANTS.SrcIn(),
                TC_COLUMN_WIDTH)));
        configs.add(prepareTimecodeColumn(new ColumnConfig(PlayListEditModelData.SRCOUT_FIELD, COMMON_CONSTANTS
                .SrcOut(), TC_COLUMN_WIDTH)));
        configs.add(prepareTimecodeColumn(new ColumnConfig(PlayListEditModelData.RECIN_FIELD, COMMON_CONSTANTS.RecIn(),
                TC_COLUMN_WIDTH)));
        configs.add(prepareTimecodeColumn(new ColumnConfig(PlayListEditModelData.RECOUT_FIELD, COMMON_CONSTANTS
                .RecOut(), TC_COLUMN_WIDTH)));

        columnModel = new ColumnModel(configs);

        editorGrid = new BaseEditorGrid<PlayListEditModelData>(editorModel.getEditStore(), columnModel);
        editorGrid.setAutoExpandColumn(fileRevCol.getId());
        editorGrid.setStripeRows(true);
        editorGrid.setClicksToEdit(ClicksToEdit.TWO);

        MultiLineCellSelectionModel<PlayListEditModelData> sm = new MultiLineCellSelectionModel<PlayListEditModelData>(
                (SelectableGridView) editorGrid.getView(), null);
        editorGrid.setSelectionModel(sm);
        editorGrid.addPlugin(rowNumber);
        editorGrid.setBorders(true);

        playListPanel.addCenter(editorGrid);
        playListPanel.setLayout(new FitLayout());
        verticalSplitPane.setBottomWidget(playListPanel);

        // handleDragAndDrop();
    }

    // /**
    // * Sets Drag And Drop feature to editorGrid
    // */
    // private void handleDragAndDrop()
    // {
    // if (editorGrid == null)
    // return;
    //
    // // TODO DEBUG THIS PART
    // new GridDragSource(editorGrid);
    // GridDropTarget target = new GridDropTarget(editorGrid) {
    // @Override
    // protected void onDragDrop(DNDEvent e)
    // {
    // ArrayList<PlayListEditModelData> data = (ArrayList<PlayListEditModelData>) e.getDragSource().getData();
    //
    // // re-order data
    // Collections.sort(data, PlaylistUtils.EditComparator);
    // // get offset
    // int indexOf = PlayListEditor.this.editorGrid.getServiceStore().getModels().indexOf(activeItem);
    // System.out.println("new index : " + insertIndex);
    //
    // FrameRange recRange = ((PlayListEditModelData) activeItem).getRecRange();
    // FrameRange dataRecRange = data.get(0).getRecRange();
    //
    // int dataDuration = 0;
    //
    // Iterator<PlayListEditModelData> iterator = data.iterator();
    // while (iterator.hasNext())
    // dataDuration += iterator.next().getRecRange().getLength();
    //
    // if (dataRecRange.getGap(recRange) > 0)
    // {
    // // Move down
    // // final int dataDuration
    // // recRange.frameIn = dataRecRange.frameIn
    // EditingMathUtils.slip(recRange, -1 * dataDuration);
    //
    // }
    // else
    // {
    // // Move up
    // EditingMathUtils.slip(recRange, -1 * dataDuration);
    // }
    //
    // editorModel.addNewEdit(data);
    // }
    // };
    // target.setAllowSelfAsSource(true);
    // target.setFeedback(Feedback.INSERT);
    // }

    /**
     * Resets playList panel to display editorGrid with current playlist.
     */
    private void showPlayListPortlet()
    {
        if (this.editorModel.getCurrentPlaylist() == null)
        {
            enableAllButton(false);
            return;
        }
        enableAllButton(true);
        showVideoPortlet();
        showCommentPortlet();
    }

    private void checkCommentAreaSize()
    {
        if (commentArea == null || commentArea.getValue() == null)
            return;
        if (commentArea.getValue().length() >= 255)
            commentArea.setValue(commentArea.getValue().substring(0, 254));
    }

    private void showCommentPortlet()
    {
        if (commentPortlet != null)
            commentPortlet.removeFromParent();
        commentPortlet = new Portlet();
        commentPortlet.setHeading("Description of " + this.editorModel.getCurrentPlaylist().getEnhancedName());
        commentPortlet.setLayout(new FitLayout());
        commentPortlet.setCollapsible(false);
        commentPortlet.setAnimCollapse(false);
        commentPortlet.setExpanded(true);
        commentArea = new TextArea();
        commentArea.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyDown(ComponentEvent event)
            {
                checkCommentAreaSize();
            }
        });

        commentArea.setHeight(100);
        commentArea.setMaxLength(255);

        if (editorModel.getCurrentPlaylist() != null)
            commentArea.setValue(editorModel.getCurrentPlaylist().getComment());
        commentPortlet.add(commentArea);
        portal.add(commentPortlet, 1);
    }

    /**
     * Display video portlet and play Proxies.
     */
    public void loadPlayer(List<ProxyModelData> collection)
    {

        if (videoPortlet != null)
            videoPortlet.removeFromParent();
        videoPortlet = new Portlet();
        videoPortlet.setHeading("Preview of " + this.editorModel.getCurrentPlaylist().getName() + " (with Player)");
        videoPortlet.setLayout(new FitLayout());
        videoPortlet.setCollapsible(false);
        videoPortlet.setAnimCollapse(false);
        videoPortlet.setExpanded(true);
        if (collection != null && collection.isEmpty() == false)
        {
            ProxyPlayer pp = new ProxyPlayer(collection);
            pp.setPixelSize(300, 300);
            videoPortlet.add(pp);
        }
        portal.add(videoPortlet, 0);

    }

    /**
     * Display video portlet.
     */
    private void showVideoPortlet()
    {
        loadPlayer(null);
    }

    /**
     * Called when the user changes a cell.
     * 
     * @code{ editor.addListener(Events.BeforeComplete, new Listener<EditorEvent>() { public void
     *        handleEvent(EditorEvent editorEvent) { onBeforeEditComplete(editorEvent); } }); }
     * @param editorEvent
     */
    private void onBeforeEditComplete(EditorEvent editorEvent)
    {
        CellEditor cellEditor = (CellEditor) editorEvent.getEditor();
        final int oldValue = (Integer) editorEvent.getStartValue();
        final int newValue = (Integer) editorEvent.getValue();
        if (oldValue == newValue)
            return;
        if (currentTool == null)
        {
            Info.display(COMMON_CONSTANTS.Error(), CONSTANTS.ToolSelectionRequired());
            cellEditor.cancelEdit();
            return;
        }
        if (editorEvent.getValue() == null)
        {
            Info.display(COMMON_CONSTANTS.Error(), CONSTANTS.ValueRequired());
            cellEditor.cancelEdit();
            return;
        }

        ColumnConfig column = columnModel.getColumn(cellEditor.col);
        PlayListEditModelData edit = editorGrid.getStore().getAt(cellEditor.row);
        boolean canPerform = editorModel.performEditAction(edit, column.getId(), oldValue, newValue, currentTool);
        if (!canPerform)
        {
            cellEditor.cancelEdit();
        }
        else
        {
            // Triggers an update of the store for the edit. This is required to update the renderer of the grid.
            controller.updateAll();
        }
    }

    /**
     * Initialize playlist framerate combo box
     */
    private void initFrameRateComboBox()
    {
        List<EFrameRateModelData> frameRateList = new ArrayList<EFrameRateModelData>();
        for (EFrameRate frameRate : EFrameRate.values())
        {
            frameRateList.add(new EFrameRateModelData(frameRate));
        }

        frameRateStore = new ListStore<EFrameRateModelData>();
        frameRateStore.add(frameRateList);
        framerateCombo = new ComboBox<EFrameRateModelData>();
        framerateCombo.setEmptyText("Select Framerate...");
        framerateCombo.setDisplayField("framerate");
        framerateCombo.setStore(frameRateStore);
        framerateCombo.setTypeAhead(false);
        framerateCombo.setTriggerAction(TriggerAction.ALL);
        framerateCombo.setWidth(80);
        framerateCombo.addSelectionChangedListener(new SelectionChangedListener<EFrameRateModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<EFrameRateModelData> frameRateModelData)
            {
                if (editorModel.getCurrentPlaylist() != null)
                {
                    AppEvent evt = new AppEvent(PlaylistUiEvents.PLAYLIST_FRAMERATE_CHANGED);
                    evt.setData(frameRateModelData.getSelectedItem().getFrameRate());
                    EventDispatcher.forwardEvent(evt);
                }
                else
                    display(COMMON_CONSTANTS.Error(), CONSTANTS.PlaylistNullInfo());
            }
        });
    }

    /**
     * Sets the framerate combo box to specified value.
     * 
     * @param value
     *            String representation of EFrameRate to apply to {@link #framerateCombo}
     */
    public void setFrameRateComboBoxValue(String value)
    {
        if (value != null)
            try
            {
                setFrameRateComboBoxValue(Enum.valueOf(EFrameRate.class, value));
            }
            catch (IllegalArgumentException iae)
            {
                framerateCombo.reset();
            }
        else
            framerateCombo.reset();
    }

    /**
     * Sets the framerate combo box to specified value.
     * 
     * @param value
     *            EFrameRate to apply to {@link #framerateCombo}
     */
    public void setFrameRateComboBoxValue(EFrameRate value)
    {
        if (editorModel.getCurrentPlaylist() == null)
            return;

        if (framerateCombo == null)
            initFrameRateComboBox();

        final List<EFrameRateModelData> models = frameRateStore.getModels();
        Iterator<EFrameRateModelData> iterator = models.iterator();
        while (iterator.hasNext())
        {
            EFrameRateModelData next = iterator.next();
            if (next.getFrameRate() == value)
            {
                framerateCombo.setValue(next);
                return;
            }
        }
        display(COMMON_CONSTANTS.Error(), CONSTANTS.FramerateCannotBeSetTo() + value.toString());
    }

    private void initToolBar()
    {
        playListPanel.setToolBar();
        ToolBar playListToolBar = playListPanel.getToolBar();
        addEditButton = new Button(COMMON_CONSTANTS.New() + " " + CONSTANTS.Edit(), Hd3dImages.getAddIcon());
        saveButton = new Button(CONSTANTS.Save(), Hd3dImages.getSaveIcon());
        saveAsNewVersionButton = new Button(CONSTANTS.SaveAsNewVersion(), Hd3dImages.getSaveIcon());
        deleteEditBtn = new Button(COMMON_CONSTANTS.Delete() + " " + CONSTANTS.Edit(), Hd3dImages.getDeleteIcon());
        deleteEditBtn.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                AppEvent event = new AppEvent(PlaylistUiEvents.DELETE_PLAYLIST_EDIT);
                List<PlayListEditModelData> selection = editorGrid.getSelectionModel().getSelectedItems();
                if (selection.isEmpty())
                    return;
                event.setData(selection);
                EventDispatcher.forwardEvent(event);
            }
        });
        // add listeners
        addEditButton.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                addEditDialog.show();
            }
        });
        addSelectEventListenerToBtn(saveButton, PlaylistUiEvents.SAVE_PLAYLIST);

        // build toolbar
        playListToolBar.add(new FillToolItem());
        playListToolBar.add(addEditButton);
        playListToolBar.add(deleteEditBtn);
        // add edit mode buttons
        playListToolBar.add(new SeparatorToolItem());

        for (EEditTool mode : EEditTool.values())
        {
            playListToolBar.add(createModificationModeBtn(mode));
        }

        final String rippleStr = "Ripple";
        Button rippleButton = new Button(rippleStr);
        rippleButton.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                selection = editorGrid.getSelectionModel().getSelectedItems();
                if (selection.isEmpty())
                {
                    PlayListEditor.this.display(CONSTANTS.EmptySelection(), CONSTANTS.CurrentSelectionIsEmpty());
                    return;
                }
                if (!CollectionUtils.isContiguous(selection, editorModel.getEditStore().getModels()))
                {
                    PlayListEditor.this.display(CONSTANTS.BadUse(), CONSTANTS.SelectionNeedsToBeContiguous());
                    return;
                }
                AppEvent event = new AppEvent(PlaylistUiEvents.RIPPLE_EDITING_TOOL);
                event.setData(selection);
                EventDispatcher.forwardEvent(event);
            }
        });
        playListToolBar.add(rippleButton);

        // add move edits buttons
        final String moveUp = CONSTANTS.MoveUp();
        Button moveUpButton = new Button(moveUp, Hd3dImages.getMoveUpIcon());

        moveUpButton.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                AppEvent event = new AppEvent(PlaylistUiEvents.MOVE_EDITING_TOOL);
                selection = editorGrid.getSelectionModel().getSelectedItems();
                if (selection.isEmpty())
                    return;
                event.setData("list", selection);
                event.setData("offset", PlaylistUtils.MOVE_UP);
                EventDispatcher.forwardEvent(event);
            }
        });
        playListToolBar.add(moveUpButton);

        final String moveDown = CONSTANTS.MoveDown();
        Button moveDownButton = new Button(moveDown, Hd3dImages.getMoveDownIcon());
        moveDownButton.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                AppEvent event = new AppEvent(PlaylistUiEvents.MOVE_EDITING_TOOL);
                selection = editorGrid.getSelectionModel().getSelectedItems();
                if (selection.isEmpty())
                    return;
                event.setData("list", selection);
                event.setData("offset", PlaylistUtils.MOVE_DOWN);
                EventDispatcher.forwardEvent(event);
            }
        });
        playListToolBar.add(moveDownButton);

        playListToolBar.add(new SeparatorToolItem());
        playListToolBar.add(saveButton);
        playListToolBar.add(saveAsNewVersionButton);
        playListToolBar.add(new SeparatorToolItem());
        initFrameRateComboBox();
        playListToolBar.add(framerateCombo);
        enableButtons(false, saveAsNewVersionButton);

    }

    /**
     * Forces to re-select the lost selection
     */
    public void reselect()
    {
        editorGrid.getSelectionModel().select(selection, true);
    }

    /**
     * Forces to select the given selection
     * 
     * @param selection
     *            List of {@link PlayListEditModelData} to select
     */
    public void reselect(List<PlayListEditModelData> selection)
    {
        this.selection = selection;
        reselect();
    }

    /**
     * Sorts the ServiceStore among recIn field.
     */
    public void resort()
    {
        editorGrid.getServiceStore().sort(PlayListEditModelData.RECIN_FIELD, SortDir.ASC);
    }

    private Button createModificationModeBtn(final EEditTool tool)
    {
        final ToggleButton button = new ToggleButton(tool.name);
        button.setToggleGroup("editMode");
        button.addListener(Events.Select, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                currentTool = tool;
            }
        });
        return button;
    }

    private void addSelectEventListenerToBtn(final Button btn, final EventType eventType)
    {
        btn.addListener(Events.Select, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(new AppEvent(eventType));
            }
        });

    }

    /**
     * Enables a certain number of buttons.
     * 
     * @param enable
     *            Enable the buttons or not
     * @param buttons
     *            Buttons to enable or disable
     */
    private void enableButtons(boolean enable, Button... buttons)
    {
        for (Button button : buttons)
        {
            button.setEnabled(enable);
        }
    }

    /**
     * Sets Enabled state to all the buttons of the toolbar
     * 
     * @param enabled
     *            Enable the buttons or not
     */
    private void enableAllButton(boolean enabled)
    {
        for (Component c : playListPanel.getToolBar().getItems())
            c.setEnabled(enabled);
    }

    /**
     * Returns value of {@link #commentArea} field.
     * 
     * @return String representation of {@link #commentArea} field
     */
    public String getComments()
    {
        return (commentArea != null) ? commentArea.getValue() : null;
    }

    /**
     * The controller calls this function whenever a new PlayListRevisionModelData is loaded by
     * PlayListEditorController.
     * 
     * @see {@link PlaylistUiEvents.LOAD_PLAYLIST}
     */
    public void onLoadPlaylist()
    {
        PlayListRevisionModelData currentPlaylist = editorModel.getCurrentPlaylist();
        final boolean playlistNotNull = currentPlaylist != null;
        showPlayListPortlet();
        setFrameRateComboBoxValue(currentPlaylist.getFrameRate());
        enableButtons(playlistNotNull, addEditButton, saveButton, saveAsNewVersionButton);
    }

    /**
     * The view reacts when "save" action is performed.
     */
    public void onPlaylistSaved()
    {
        Info.display(CONSTANTS.PlaylistSaved(), CONSTANTS.PlaylistHasBeenSavedInfo());
    }

    /**
     * The view displays a message on a small box on right bottom corner.
     * 
     * @param title
     *            bold title of the box
     * @param message
     *            displayed on the box
     */
    public void display(String title, String message)
    {
        Info.display(title, message);
    }
}
