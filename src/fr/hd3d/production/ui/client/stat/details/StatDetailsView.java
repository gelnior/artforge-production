package fr.hd3d.production.ui.client.stat.details;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel.Cell;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ColumnHiddenChangeListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ColumnWidthChangeListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.BoldRenderer;
import fr.hd3d.common.ui.client.widget.grid.MultiLineCellSelectionModel;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.event.StatDetailsEvents;


/**
 * View displaying project sequences/categories statistics. For each task type a column is displayed. Inside each cell
 * is displayed the task progression for link sequence/task type.
 * 
 * @author HD3D
 */
public class StatDetailsView extends BorderedPanel
{
    private static final int TC_COLUMN_WIDTH = 200;

    /** View name used by main controller to idle controller. */
    public static final String NAME = "statisticDetails";

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    public static List<String> getListStatus()
    {
        if (listStatus.isEmpty())
        {
            listStatus.add("All");
            listStatus.add(EApprovalNoteStatus.OK.toString());
            listStatus.add(EApprovalNoteStatus.WAIT_APP.toString());
            listStatus.add(EApprovalNoteStatus.WORK_IN_PROGRESS.toString());
            listStatus.add(EApprovalNoteStatus.RETAKE.toString());
            listStatus.add(EApprovalNoteStatus.TO_DO.toString());
            listStatus.add(EApprovalNoteStatus.STAND_BY.toString());
            listStatus.add(EApprovalNoteStatus.NEW.toString());
            listStatus.add(EApprovalNoteStatus.CLOSE.toString());
            listStatus.add(EApprovalNoteStatus.CANCELLED.toString());
        }

        return listStatus;
    }

    /** Model handling casting data. */
    private final StatDetailsModel model = new StatDetailsModel();
    /** Controller handling casting events. */
    private final StatDetailsController controller = new StatDetailsController(model, this);

    private final LabelField labelWeek = new LabelField(" Week : ");
    private final LabelField labelDate = new LabelField(" Current State : ");
    private final Label nbWeek = new Label();
    private final CheckBox withTotal = new CheckBox();
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh",
            StatDetailsEvents.STAT_REFRESH_CLICKED);
    private final ToolBarButton printButton = new ToolBarButton(Hd3dImages.getPrinterIcon(), COMMON_CONSTANTS.Print(),
            StatDetailsEvents.STATS_PRINT);

    /** Constituent navigation tree. */
    private final ConstituentTree constituentTree = new ConstituentTree();
    /** Shot navigation tree. */
    private final ShotTree shotTree = new ShotTree("");

    /** Grid displaying statistics. */
    private Grid<StatDetailsModelData> grid;

    private boolean isShotView = true;

    private static List<String> listStatus = new ArrayList<String>();

    /**
     * Constructor : set layout.
     */
    public StatDetailsView()
    {
        super();

        getListStatus();

        this.initToolBar();
        this.initGrid();
        this.setGrids();
        this.setTrees();
    }

    public boolean isShotView()
    {
        return (isShotView);
    }

    public void setIsShotView(boolean shotview)
    {
        isShotView = shotview;
    }

    public void deselectConstituents()
    {
        this.constituentTree.getTree().getSelectionModel().deselectAll();
    }

    public void deselectShots()
    {
        this.shotTree.getTree().getSelectionModel().deselectAll();
    }

    public MaskableController getController()
    {
        return this.controller;
    }

    public void setTextDate(String date)
    {
        this.labelDate.setText("<b>Current State " + date + "</b>&#160;&#160;&#160;");
    }

    public void setTextWeek(int week)
    {
        this.labelWeek.setText("<b>Week " + week + "</b>&#160;&#160;&#160;");
    }

    public void reconfigureGrid()
    {
        this.grid.reconfigure((ListStore<StatDetailsModelData>) this.model.getListStore(), getStatDetailsColumnModel());
    }

    public ColumnModel getStatDetailsColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        ColumnConfig nameCol = new ColumnConfig(StatDetailsModelData.WO_PATH, "Path", TC_COLUMN_WIDTH);
        columns.add(nameCol);
        nameCol.setRenderer(new BoldRenderer<StatDetailsModelData>());

        if (this.isShotView)
        {
            ColumnConfig nbWoCol = new ColumnConfig(StatDetailsModelData.NB_WO, "NbShot", 50);
            ColumnConfig nbFrameCol = new ColumnConfig(StatDetailsModelData.NB_FRAME, "NbFrame", 60);
            columns.add(nbWoCol);
            columns.add(nbFrameCol);
        }
        else
        {
            ColumnConfig nbWoCol = new ColumnConfig(StatDetailsModelData.NB_WO, "NbObject", 60);
            columns.add(nbWoCol);
        }

        int size = 150;
        if (this.model.getApprovalNoteTypesStore().getCount() > 0)
        {
            size = 1200 / this.model.getApprovalNoteTypesStore().getCount();
            if (size < 150)
                size = 150;
        }

        for (final ApprovalNoteTypeModelData approvalNoteTypeModelData : this.model.getApprovalNoteTypesStore()
                .getModels())
        {
            ColumnConfig approvalTypeColumn = new ColumnConfig(approvalNoteTypeModelData.getId().toString(),
                    approvalNoteTypeModelData.getName(), size);
            approvalTypeColumn.setAlignment(HorizontalAlignment.CENTER);
            approvalTypeColumn.setSortable(false);
            approvalTypeColumn.setRenderer(new StatsDetailRenderer());
            approvalTypeColumn.setId(approvalNoteTypeModelData.getId().toString());
            approvalTypeColumn.setResizable(false);
            approvalTypeColumn.setToolTip(approvalNoteTypeModelData.getName());

            columns.add(approvalTypeColumn);
        }

        ColumnModel columnModel = new ColumnModel(columns);

        if (withTotal.getValue())
        {

            AggregationRowConfig<StatDetailsModelData> totaux = new AggregationRowConfig<StatDetailsModelData>();
            totaux.setHtml(StatDetailsModelData.WO_PATH, "<small><b>Total</b></small>");
            totaux.setSummaryType(StatDetailsModelData.NB_WO.toString(), StatDetailsSummaryType.SUM);
            totaux.setSummaryFormat(StatDetailsModelData.NB_WO, NumberFormat.getDecimalFormat());
            totaux.setSummaryType(StatDetailsModelData.NB_FRAME.toString(), StatDetailsSummaryType.SUM);
            totaux.setSummaryFormat(StatDetailsModelData.NB_FRAME.toString(), NumberFormat.getDecimalFormat());

            for (String status : listStatus)
            {
                AggregationRowConfig<StatDetailsModelData> calcul = new AggregationRowConfig<StatDetailsModelData>();
                if (status.compareTo("All") != 0)
                {
                    calcul.setHtml(StatDetailsModelData.WO_PATH, "<small> Total " + status + "</small>");
                }

                for (ApprovalNoteTypeModelData approvalNoteTypeModelData : this.model.getApprovalNoteTypesStore()
                        .getModels())
                {

                    if (status.compareTo("All") == 0)
                    {
                        totaux.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.TOTAL_ALL);
                    }
                    else if (status.compareTo("OK") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(), StatDetailsSummaryType.OK);
                    }
                    else if (status.compareTo("RETAKE") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.RETAKE);
                    }
                    else if (status.compareTo("WAIT_APP") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.WAIT_APP);
                    }
                    else if (status.compareTo("WORK_IN_PROGRESS") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.WORK_IN_PROGRESS);
                    }
                    else if (status.compareTo("TO_DO") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.TO_DO);
                    }
                    else if (status.compareTo("STAND_BY") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.STAND_BY);
                    }
                    else if (status.compareTo("CANCELLED") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.CANCELLED);
                    }
                    else if (status.compareTo("NEW") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(), StatDetailsSummaryType.NEW);
                    }
                    else if (status.compareTo("CLOSE") == 0)
                    {
                        calcul.setSummaryType(approvalNoteTypeModelData.getId().toString(),
                                StatDetailsSummaryType.CLOSE);
                    }

                    if (status.compareTo("All") == 0)
                        totaux.setSummaryFormat(approvalNoteTypeModelData.getId().toString(), NumberFormat
                                .getDecimalFormat());
                    else
                    {
                        calcul.setSummaryFormat(approvalNoteTypeModelData.getId().toString(), NumberFormat
                                .getDecimalFormat());
                    }
                }

                if (status.compareTo("All") == 0)
                {
                    columnModel.addAggregationRow(totaux);
                }

                columnModel.addAggregationRow(calcul);
            }
        }

        setColumnModelListener(columnModel);

        StatsDetailRenderer.setSize(size);
        return columnModel;
    }

    private void setColumnModelListener(ColumnModel columnModel)
    {
        columnModel.addListener(Events.WidthChange, new ColumnWidthChangeListener());
        columnModel.addListener(Events.HiddenChange, new ColumnHiddenChangeListener());
    }

    public void maskLoading()
    {
        this.grid.mask(GXT.MESSAGES.loadMask_msg());
    }

    public void unmaskLoading()
    {
        this.grid.unmask();
    }

    public List<Cell> getCellSelected()
    {
        MultiLineCellSelectionModel<StatDetailsModelData> selection = (MultiLineCellSelectionModel<StatDetailsModelData>) this.grid
                .getSelectionModel();
        List<Cell> cellList = selection.getSeletedCells();
        return cellList;
    }

    public void updateRow(int index)
    {
        this.grid.getStore().update(this.grid.getStore().getAt(index));
    }

    public void unidle()
    {
        this.controller.unMask();
    }

    public void idle()
    {
        this.controller.mask();
    }

    public void print()
    {
        if (rendered)
        {
            JavaScriptObject jsobj = open("", COMMON_CONSTANTS.Print(), "");
            String css = getCss();

            El grid = this.grid.getView().getBody();
            grid = grid.getChild(0);
            String html = css + "<div class='x-grid3-body' style='height:100%; width:100%;'>" + grid.getInnerHtml()
                    + "</div>";

            setDocInnerHtml(jsobj, html);
            printDoc(jsobj);
        }
    }

    /**
     * Set data grid to the center of the view.
     */
    private void setGrids()
    {
        BorderedPanel centerPanel = new BorderedPanel();
        centerPanel.add(grid);
        centerPanel.setHeaderVisible(false);
        centerPanel.setLayout(new FitLayout());

        centerPanel.setBorders(true);
        this.addCenter(centerPanel);
    }

    private void initGrid()
    {
        final ColumnModel cm = getStatDetailsColumnModel();
        this.grid = new Grid<StatDetailsModelData>(this.model.getListStore(), cm);
    }

    /**
     * Set navigation trees to the west of the view.
     */
    private void setTrees()
    {
        BorderedPanel treePanel = new BorderedPanel();
        treePanel.addNorth(constituentTree, 300, 0, 0, 0, 0);
        treePanel.addCenter(shotTree, 5, 0, 0, 0);

        treePanel.setHeaderVisible(true);
        treePanel.setBorders(false);

        BorderLayoutData data = this.addWest(treePanel, 200);
        data.setCollapsible(true);
        treePanel.setHeading("");

        this.constituentTree.setLocalEventsOn();
        this.constituentTree.addController(this.controller);
        this.shotTree.setLocalEventsOn();
        this.shotTree.addController(this.controller);

        this.model.setShotNavModel(this.shotTree.getTreeModel());
        this.model.setConstituentNavModel(this.constituentTree.getTreeModel());
    }

    private void initToolBar()
    {
        setToolBar();

        DateWrapper date = new DateWrapper(DatetimeUtil.today());
        this.setTextDate(Hd3dDateUtil.dateToString(date.asDate()));
        this.toolBar.add(labelDate);

        int weekNumber = Hd3dDateUtil.getWeekOfYear(date, 1);
        this.setTextWeek(weekNumber);

        this.toolBar.add(labelWeek);
        this.toolBar.add(printButton);

        this.toolBar.add(new SeparatorMenuItem());
        this.toolBar.add(new Label(" total "));
        this.withTotal.setValue(true);

        this.withTotal.addListener(Events.Change, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(StatDetailsEvents.STAT_REFRESH_CLICKED);
            }
        });

        this.toolBar.add(withTotal);
        this.toolBar.add(new SeparatorMenuItem());
        this.toolBar.add(refreshButton);
    }

    private static native Document open(String url, String name, String features) /*-{
        var wnd = $wnd.open(url, name, features);
        return wnd;
    }-*/;

    private static native void setDocInnerHtml(JavaScriptObject wnd, String innerHtml) /*-{
        //doc.body.innerHTML = innerHtml;
        wnd.document.write(innerHtml);
    }-*/;

    private static native void printDoc(JavaScriptObject wnd) /*-{
        var javascript = " <script> "
        javascript += "function printFunction(){";
        javascript += "    window.print();";
        javascript += "}";
        javascript += "t = setTimeout('printFunction()', 600);";
        javascript += "</script>";
        wnd.document.write(javascript);
    }-*/;

    /**
     * @return All CSS sheets needed.
     */
    private String getCss()
    {
        String css = "";

        String href = Window.Location.getHref();
        String[] urlPart = href.split("/");
        StringBuilder url = new StringBuilder();
        for (int i = 0; i < urlPart.length - 1; i++)
        {
            url.append(urlPart[i] + "/");
        }
        NodeList<Element> cssLinks = Document.get().getElementsByTagName("link");
        int size = cssLinks.getLength();
        for (int i = 0; i < size; i++)
        {
            css += "<link rel='stylesheet' type='text/css' href='" + url.toString()
                    + cssLinks.getItem(i).getAttribute("href") + "' />";
        }
        return css;
    }

}
