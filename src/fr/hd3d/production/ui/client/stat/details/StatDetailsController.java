package fr.hd3d.production.ui.client.stat.details;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.production.ui.client.constant.ProductionConstants;
import fr.hd3d.production.ui.client.event.StatDetailsEvents;


public class StatDetailsController extends MaskableController
{

    private final StatDetailsModel model;
    private final StatDetailsView view;

    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);

    public StatDetailsController(StatDetailsModel model, StatDetailsView view)
    {
        this.model = model;
        this.view = view;

        registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (CommonEvents.PROJECT_CHANGED == type)
        {
            this.onProjectChanged(event);
        }
        if (StatDetailsEvents.PROJECT_CHANGED == type)
        {
            this.onProjectChanged(event);
        }
        else if (CommonEvents.SEQUENCES_SELECTED == type)
        {
            this.onSequenceSelected(event);
        }
        else if (CommonEvents.CATEGORIES_SELECTED == type)
        {
            this.onCategorySelected(event);
        }
        else if (type == StatDetailsEvents.SHOTS_LOADED)
        {
            this.onShotLoaded();
        }
        else if (type == StatDetailsEvents.CONSTITUENT_LOADED)
        {
            this.onConstituentLoaded();
        }
        else if (type == StatDetailsEvents.STAT_REFRESH_CLICKED)
        {
            this.onRefresh();
        }
        else if (type == StatDetailsEvents.APPROVAL_TYPES_LOADED)
        {
            this.onApprovalTypesLoaded();
        }
        else if (type == StatDetailsEvents.RECONFIG_GRID)
        {
            this.onGridReconfigured();
        }
        else if (type == StatDetailsEvents.APPROVAL_NOTES_LOADED)
        {
            this.onApprovalLoaded();
        }
        else if (type == StatDetailsEvents.APPROVAL_TYPES_LOADED_ADD_STAT)
        {
            this.onApprovalTypesLoadedAddStat();
        }
        else if (type == StatDetailsEvents.DATA_LOADED)
        {
            this.onDataLoaded();
        }
        else if (type == StatDetailsEvents.STATS_PRINT)
        {
            this.view.print();
        }
    }

    private void onSequenceSelected(AppEvent event)
    {
        List<SequenceModelData> selectedSequences = event.getData();
        if (CollectionUtils.isNotEmpty(selectedSequences))
        {
            this.view.deselectConstituents();
            this.model.clearSelectedCategories();

            this.model.setSelectedSequences(selectedSequences);
            this.model.loadShots();

            this.view.maskLoading();
        }
    }

    private void onCategorySelected(AppEvent event)
    {
        List<CategoryModelData> selectedCategories = event.getData();
        if (CollectionUtils.isNotEmpty(selectedCategories))
        {
            this.view.deselectShots();
            this.model.clearSelectedSequences();

            this.model.setSelectedCategories(selectedCategories);
            this.model.loadConstituents();

            this.view.maskLoading();
        }
    }

    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();

        if (project != null && project.getId() != this.model.getProjectId())
        {
            this.model.setProject(project);
            this.model.initialize(project);
        }
    }

    private void onRefresh()
    {
        this.view.maskLoading();
        this.model.statLoadApprovalNotes(this.view.isShotView());
    }

    private void onApprovalTypesLoaded()
    {
        this.model.initApprovalTypes();
        this.view.reconfigureGrid();

        if (this.model.isDataSelected())
        {
            EventDispatcher.forwardEvent(StatDetailsEvents.DATA_LOADED);
        }
    }

    private void onGridReconfigured()
    {
        this.view.reconfigureGrid();
    }

    private void onApprovalTypesLoadedAddStat()
    {
        this.view.reconfigureGrid();
        EventDispatcher.forwardEvent(StatDetailsEvents.DATA_LOADED);
    }

    private void onShotLoaded()
    {
        this.view.setIsShotView(true);
        if (this.model.getShotStore().getCount() != 0)
        {
            EventDispatcher.forwardEvent(StatDetailsEvents.STAT_REFRESH_CLICKED);
        }
        else
        {
            this.model.statLoadStats();
            this.view.unmaskLoading();
        }
    }

    private void onConstituentLoaded()
    {
        this.view.setIsShotView(false);
        if (this.model.getConstituentStore().getCount() != 0)
        {
            EventDispatcher.forwardEvent(StatDetailsEvents.STAT_REFRESH_CLICKED);
        }
        else
        {
            this.model.statLoadStats();
            this.view.unmaskLoading();
        }
    }

    private void onDataLoaded()
    {
        this.view.unmaskLoading();
    }

    private void onApprovalLoaded()
    {
        this.model.statLoadStats();
    }

    private void registerEvents()
    {
        registerEventTypes(CommonEvents.PROJECT_CHANGED);
        registerEventTypes(StatDetailsEvents.PROJECT_CHANGED);
        registerEventTypes(StatDetailsEvents.SHOTS_LOADED);
        registerEventTypes(StatDetailsEvents.CONSTITUENT_LOADED);
        registerEventTypes(StatDetailsEvents.RECONFIG_GRID);

        registerEventTypes(StatDetailsEvents.STAT_REFRESH_CLICKED);
        registerEventTypes(StatDetailsEvents.DATA_LOADED);
        registerEventTypes(StatDetailsEvents.REFRESH_STAT_CHART);
        registerEventTypes(StatDetailsEvents.APPROVAL_TYPES_LOADED);
        registerEventTypes(StatDetailsEvents.APPROVAL_NOTES_LOADED);
        registerEventTypes(StatDetailsEvents.STATS_PRINT);
        registerEventTypes(StatDetailsEvents.APPROVAL_TYPES_LOADED_ADD_STAT);
    }

}
