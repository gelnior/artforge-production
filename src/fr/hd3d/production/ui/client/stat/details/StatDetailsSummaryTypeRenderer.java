package fr.hd3d.production.ui.client.stat.details;

import java.util.Map;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;


public class StatDetailsSummaryTypeRenderer extends SummaryType<Long>
{
    String modelField;

    public StatDetailsSummaryTypeRenderer(String modelField)
    {
        this.modelField = modelField;
    }

    @Override
    public Long render(Object v, ModelData m, String field, Map<String, Object> data)
    {
        if (v == null)
        {
            v = 0L;
        }
        StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
        FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
        StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
        if (detailModel == null)
        {
            return (Long) v;
        }

        Long res = (Long) v + new Long((Integer) detailModel.get(modelField));
        return (Long) res;
    }

}
