package fr.hd3d.production.ui.client.stat.details;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ApprovalNoteReader;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.service.store.AllLoadListener;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;
import fr.hd3d.production.ui.client.event.StatDetailsEvents;


/**
 * Model handling statistics detail data. It prepares data /** widgets.
 * 
 * @author HD3D
 */

public class StatDetailsModel
{
    /** Store containing activities to display in the TimeRecap grid. */
    private ListStore<StatDetailsModelData> statsStore = new ListStore<StatDetailsModelData>();

    final ServiceStore<ApprovalNoteModelData> approvalNoteStore = new ServiceStore<ApprovalNoteModelData>(
            new ApprovalNoteReader());

    /** Shot navigation store tree. */
    private BaseTreeDataModel<ShotModelData, SequenceModelData> shotNavModel;
    /** Constituent navigation store tree. */
    private BaseTreeDataModel<ConstituentModelData, CategoryModelData> constituentNavModel;

    /** Store containing shots from a sequence or from a project. */
    private final ServiceStore<ShotModelData> shotStore = new ServiceStore<ShotModelData>(ReaderFactory.getReader(
            ShotModelData.class, ShotModelData.getModelType()));
    /** Store containing constituents from a category or from a project. */
    private final ServiceStore<ConstituentModelData> constituentStore = new ServiceStore<ConstituentModelData>(
            ReaderFactory.getReader(ConstituentModelData.class, ConstituentModelData.getModelType()));

    /** LuceneConstraint to reload all shots from list of sequences ( included the sub sequences ). */
    private final LuceneConstraint sequenceConstraintId = new LuceneConstraint(EConstraintOperator.in, "sequences_id");

    /** LuceneConstraint to reload all constituents from list of categories ( included the sub categories ). */
    private final LuceneConstraint categoryConstraintId = new LuceneConstraint(EConstraintOperator.in, "categories_id");

    /** Store containing all approvalNoteTypes from a project. */
    private final ServiceStore<ApprovalNoteTypeModelData> approvalNoteTypesStore = new ServiceStore<ApprovalNoteTypeModelData>(
            ReaderFactory.getReader(ApprovalNoteTypeModelData.class, ApprovalNoteTypeModelData.getModelType()));

    private final ListStore<ApprovalNoteTypeModelData> currentApprovalNoteTypesStore = new ListStore<ApprovalNoteTypeModelData>();

    private final AndConstraint rootFilter = new AndConstraint();

    private final EqConstraint approvalWoTypeConstraint = new EqConstraint("boundEntityName");

    private final Constraint approvalWoIdsConstraint;

    /** Containing the current project selected. */
    private ProjectModelData selectedProject = null;
    /** Containing the current sequence selected. */
    private ArrayList<SequenceModelData> selectedSequences = new ArrayList<SequenceModelData>();
    private ArrayList<CategoryModelData> selectedCategories = new ArrayList<CategoryModelData>();

    public FastMap<String> fastMapApprovalNoteTypes = new FastMap<String>();
    private FastMap<ShotModelData> fastMapShots = new FastMap<ShotModelData>();
    private FastMap<ConstituentModelData> fastMapConstituents = new FastMap<ConstituentModelData>();

    public StatDetailsModel()
    {
        this.shotStore.addLoadListener(new AllLoadListener<ShotModelData>(shotStore, StatDetailsEvents.SHOTS_LOADED));

        this.constituentStore.addLoadListener(new AllLoadListener<ConstituentModelData>(constituentStore,
                StatDetailsEvents.CONSTITUENT_LOADED));

        this.approvalWoIdsConstraint = new Constraint(EConstraintOperator.in, ApprovalNoteModelData.BOUND_ENTITY_ID);
        this.rootFilter.setLeftMember(approvalWoIdsConstraint);
        this.rootFilter.setRightMember(approvalWoTypeConstraint);

        this.approvalNoteTypesStore.addLoadListener(new AllLoadListener<ApprovalNoteTypeModelData>(
                approvalNoteTypesStore, StatDetailsEvents.APPROVAL_TYPES_LOADED));

        this.approvalNoteStore.addEventLoadListener(StatDetailsEvents.APPROVAL_NOTES_LOADED);
    }

    public Long getProjectId()
    {
        if (selectedProject == null)
            return null;
        return selectedProject.getId();
    }

    public void setProject(ProjectModelData project)
    {
        this.selectedProject = project;
    }

    public ProjectModelData getProject()
    {
        return this.selectedProject;
    }

    public ServiceStore<ApprovalNoteTypeModelData> getApprovalNoteTypesStore()
    {
        return approvalNoteTypesStore;
    }

    public ListStore<ApprovalNoteTypeModelData> getCurrentApprovalNoteTypesStore()
    {
        return currentApprovalNoteTypesStore;
    }

    public ServiceStore<ShotModelData> getShotStore()
    {
        return shotStore;
    }

    public ServiceStore<ConstituentModelData> getConstituentStore()
    {
        return constituentStore;
    }

    public void initialize(ProjectModelData project)
    {
        if (project != null)
        {
            this.selectedProject = project;

            this.selectedSequences = new ArrayList<SequenceModelData>();
            this.selectedCategories = new ArrayList<CategoryModelData>();
            this.shotStore.removeAll();
            this.constituentStore.removeAll();
            this.statsStore.removeAll();

            this.currentApprovalNoteTypesStore.removeAll();
            this.loadApprovalNoteTypes(project);

            if (this.shotNavModel != null && this.constituentNavModel != null)
            {
                this.shotNavModel.setCurrentProject(project);
                this.constituentNavModel.setCurrentProject(project);
            }
        }
    }

    public void initApprovalTypes()
    {
        this.currentApprovalNoteTypesStore.removeAll();
        this.currentApprovalNoteTypesStore.add(approvalNoteTypesStore.getModels());
        this.approvalNoteTypesStore.removeAll();
    }

    public void statLoadApprovalNotes(final boolean shotView)
    {
        if (!isDataSelected())
        {
            EventDispatcher.forwardEvent(StatDetailsEvents.DATA_LOADED);
            return;
        }
        this.approvalNoteStore.removeAll();
        this.approvalNoteStore.clearParameters();
        this.fastMapShots.clear();
        this.fastMapConstituents.clear();
        this.fastMapApprovalNoteTypes.clear();

        String path = ServicesURI.APPROVALNOTES_BY_SEQUENCES_IDS_BY_DATE;
        this.approvalNoteStore.setPath(path);

        ArrayList<Long> idsWo = new ArrayList<Long>();
        if (shotView)
        {
            for (ShotModelData shot : this.shotStore.getModels())
            {
                idsWo.add(shot.getId());
                this.fastMapShots.put(shot.getId().toString(), shot);
            }
            this.approvalWoTypeConstraint.setLeftMember(ShotModelData.SIMPLE_CLASS_NAME);
        }
        else
        {
            path = ServicesURI.APPROVALNOTES_BY_CATEGORIES_IDS_BY_DATE;
            for (ConstituentModelData constituent : this.constituentStore.getModels())
            {
                idsWo.add(constituent.getId());
                this.fastMapConstituents.put(constituent.getId().toString(), constituent);
            }
            this.approvalWoTypeConstraint.setLeftMember(ConstituentModelData.SIMPLE_CLASS_NAME);
        }

        if (idsWo.isEmpty())
        {
            EventDispatcher.forwardEvent(StatDetailsEvents.DATA_LOADED);
            return;
        }

        DateWrapper currentDate = new DateWrapper(DatetimeUtil.tomorrow());

        path = path + "?date=" + DateFormat.TIMESTAMP_FORMAT.format(currentDate.asDate()) + "&ids=";
        if (shotView)
        {
            for (SequenceModelData sequence : selectedSequences)
            {
                path = path + sequence.getId().toString() + ',';
            }
        }
        else
        {
            for (CategoryModelData category : selectedCategories)
            {
                path = path + category.getId().toString() + ',';
            }
        }

        final FastMap<String> approvalNoteTypesAll = new FastMap<String>();
        for (ApprovalNoteTypeModelData noteType : currentApprovalNoteTypesStore.getModels())
        {
            approvalNoteTypesAll.put(noteType.getId().toString(), noteType.getName());
        }

        this.statsStore.removeAll();
        final List<StatDetailsModelData> statsStoreList = new ArrayList<StatDetailsModelData>();

        RestRequestHandlerSingleton.getInstance().getRequest(path, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                try
                {
                    String json = null;
                    try
                    {
                        json = response.getEntity().getText();

                        JSONValue object = JSONParser.parse(json);
                        JSONArray resultMap = object.isObject().get(Hd3dModelData.ROOT).isArray();
                        JSONObject listParentsId = resultMap.get(0).isObject();

                        if (listParentsId.size() != 0)
                        {
                            List<String> lstStatus = this.getStatus();

                            if (shotView)
                            {
                                for (SequenceModelData sequence : selectedSequences)
                                {
                                    if (listParentsId.get(sequence.getId().toString()) != null)
                                    {
                                        JSONArray result = listParentsId.get(sequence.getId().toString()).isArray();
                                        int nbFrame = 0;
                                        int nbWo = 0;
                                        int size = result.size();

                                        JSONObject data = result.get(result.size() - 2).isObject();
                                        if (data.get("nbFrame") != null)
                                        {
                                            JSONValue nbFrameVal = data.get("nbFrame");
                                            String value = nbFrameVal.isNumber().toString();
                                            nbFrame = Integer.parseInt(value);
                                        }
                                        data = result.get(result.size() - 1).isObject();
                                        if (data.get("nbWo") != null)
                                        {
                                            JSONValue nbFrameVal = data.get("nbWo");
                                            String value = nbFrameVal.isNumber().toString();
                                            nbWo = Integer.parseInt(value);
                                        }

                                        String path = sequence.getName();
                                        StatDetailsModelData statDetailsRow = new StatDetailsModelData(
                                                sequence.getId(), path, nbWo, nbFrame);

                                        for (int i = 0; i < size - 2; i++)
                                        {
                                            JSONObject listNoteTypesId = result.get(i).isObject();
                                            String noteTypeId = listNoteTypesId.keySet().toString().substring(1)
                                                    .replace("]", "");
                                            String noteTypeName = approvalNoteTypesAll.get(noteTypeId);

                                            JSONValue val = listNoteTypesId.get(noteTypeId);
                                            StatRowDetailModelData model = new StatRowDetailModelData(noteTypeName,
                                                    Long.parseLong(noteTypeId));

                                            for (String statusName : lstStatus)
                                            {
                                                this.setStatusValue(statusName, val.isObject(), model);
                                            }
                                            model.setTheTotal();
                                            model.setTotalWo(nbWo);
                                            statDetailsRow.getPathResult().put(noteTypeId, model);
                                            if (fastMapApprovalNoteTypes.get(noteTypeId) == null)
                                            {
                                                fastMapApprovalNoteTypes.put(noteTypeId, noteTypeName);
                                            }
                                        }
                                        statsStoreList.add(statDetailsRow);
                                    }
                                }
                            }
                            else
                            {
                                for (CategoryModelData category : selectedCategories)
                                {
                                    if (listParentsId.get(category.getId().toString()) != null)
                                    {
                                        JSONArray result = listParentsId.get(category.getId().toString()).isArray();
                                        int nbWo = 0;
                                        int size = result.size();

                                        JSONObject data = result.get(result.size() - 1).isObject();
                                        if (data.get("nbWo") != null)
                                        {
                                            JSONValue nbFrameVal = data.get("nbWo");
                                            String value = nbFrameVal.isNumber().toString();
                                            nbWo = Integer.parseInt(value);
                                        }

                                        String path = category.getName();
                                        StatDetailsModelData statDetailsRow = new StatDetailsModelData(
                                                category.getId(), path, nbWo, 0);

                                        for (int i = 0; i < size - 1; i++)
                                        {
                                            JSONObject listNoteTypesId = result.get(i).isObject();
                                            String noteTypeId = listNoteTypesId.keySet().toString().substring(1)
                                                    .replace("]", "");
                                            String noteTypeName = approvalNoteTypesAll.get(noteTypeId);

                                            JSONValue val = listNoteTypesId.get(noteTypeId);
                                            StatRowDetailModelData model = new StatRowDetailModelData(noteTypeName,
                                                    Long.parseLong(noteTypeId));

                                            for (String statusName : lstStatus)
                                            {
                                                this.setStatusValue(statusName, val.isObject(), model);
                                            }
                                            model.setTheTotal();
                                            model.setTotalWo(nbWo);
                                            statDetailsRow.getPathResult().put(noteTypeId, model);
                                            if (fastMapApprovalNoteTypes.get(noteTypeId) == null)
                                            {
                                                fastMapApprovalNoteTypes.put(noteTypeId, noteTypeName);
                                            }
                                        }
                                        statsStoreList.add(statDetailsRow);
                                    }
                                }
                            }
                        }
                        statsStore.add(statsStoreList);
                        EventDispatcher.forwardEvent(StatDetailsEvents.APPROVAL_NOTES_LOADED);
                    }
                    catch (NumberFormatException nfe)
                    {
                        Logger.error("ServerStatCallback pb : " + json, nfe);
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            private void setStatusValue(String status, JSONObject obj, StatRowDetailModelData model)
            {
                if (obj.get(status) != null)
                {
                    String value = obj.get(status).isString().toString();
                    value = value.replace("\"", "");
                    int nb = Integer.parseInt(value);
                    model.set(status, nb);
                }
            }

            public List<String> getStatus()
            {
                List<String> status = new ArrayList<String>();

                status.add(EApprovalNoteStatus.OK.toString());
                status.add(EApprovalNoteStatus.RETAKE.toString());
                status.add(EApprovalNoteStatus.TO_DO.toString());
                status.add(EApprovalNoteStatus.STAND_BY.toString());
                status.add(EApprovalNoteStatus.CANCELLED.toString());
                status.add(EApprovalNoteStatus.WORK_IN_PROGRESS.toString());
                status.add(EApprovalNoteStatus.WAIT_APP.toString());
                status.add(EApprovalNoteStatus.CLOSE.toString());
                status.add(EApprovalNoteStatus.NEW.toString());

                return status;
            }

        });
    }

    public void statLoadStats()
    {

        this.approvalNoteTypesStore.removeAll();

        for (int i = this.currentApprovalNoteTypesStore.getModels().size(); i > 0; i--)
        {
            if (this.fastMapApprovalNoteTypes.get(this.currentApprovalNoteTypesStore.getModels().get(i - 1).getId()
                    .toString()) != null)
            {
                this.approvalNoteTypesStore.add(this.currentApprovalNoteTypesStore.getModels().get(i - 1));
            }
        }

        approvalNoteTypesStore.sort(ApprovalNoteTypeModelData.NAME_FIELD, SortDir.ASC);

        if ((this.shotStore.getCount() == 0 && this.constituentStore.getCount() == 0)
                || this.statsStore.getCount() == 0)
        {
            EventDispatcher.forwardEvent(StatDetailsEvents.RECONFIG_GRID);
            EventDispatcher.forwardEvent(StatDetailsEvents.DATA_LOADED);
            return;
        }

        EventDispatcher.forwardEvent(StatDetailsEvents.APPROVAL_TYPES_LOADED_ADD_STAT);
    }

    public void loadShots()
    {
        if (this.selectedSequences.isEmpty())
        {
            initialize(this.selectedProject);
        }
        else
        {
            this.shotStore.removeParameter(this.sequenceConstraintId);
            ArrayList<String> idList = new ArrayList<String>();
            for (SequenceModelData sequence : selectedSequences)
            {
                idList.add(sequence.getId().toString());
            }
            this.sequenceConstraintId.setLeftMember(idList);
            this.shotStore.addParameter(this.sequenceConstraintId);
            this.shotStore.reload();

            this.statsStore.removeAll();
            this.fastMapApprovalNoteTypes.clear();
        }
    }

    public void loadConstituents()
    {
        if (selectedCategories.isEmpty())
        {
            initialize(selectedProject);
        }
        else
        {
            constituentStore.removeParameter(categoryConstraintId);
            ArrayList<String> idList = new ArrayList<String>();
            for (CategoryModelData category : selectedCategories)
            {
                idList.add(category.getId().toString());
            }
            categoryConstraintId.setLeftMember(idList);
            constituentStore.addParameter(categoryConstraintId);
            constituentStore.setPath(ServicesPath.PROJECTS + selectedProject.getId() + "/" + ServicesPath.CONSTITUENTS);
            constituentStore.reload();

            this.statsStore.removeAll();
            this.fastMapApprovalNoteTypes.clear();
        }
    }

    public void loadApprovalNoteTypes(ProjectModelData project)
    {
        String path = ServicesPath.PROJECTS + project.getId() + "/" + ServicesURI.APPROVALNOTETYPES;
        approvalNoteTypesStore.setPath(path);
        approvalNoteTypesStore.reload();
    }

    public void setSelectedSequences(List<SequenceModelData> sequences)
    {
        selectedSequences.clear();
        selectedSequences.addAll(sequences);
    }

    public void setSelectedCategories(List<CategoryModelData> categories)
    {
        selectedCategories.clear();
        selectedCategories.addAll(categories);
    }

    public void clearSelectedSequences()
    {
        selectedSequences.clear();
    }

    public void clearSelectedCategories()
    {
        selectedCategories.clear();
    }

    public boolean isDataSelected()
    {
        if (selectedCategories.isEmpty() && selectedSequences.isEmpty())
        {
            return false;
        }
        return true;
    }

    public ListStore<StatDetailsModelData> getListStore()
    {
        return this.statsStore;
    }

    public void setConstituentNavModel(BaseTreeDataModel<ConstituentModelData, CategoryModelData> dataModel)
    {
        this.constituentNavModel = dataModel;
    }

    public void setShotNavModel(BaseTreeDataModel<ShotModelData, SequenceModelData> dataModel)
    {
        this.shotNavModel = dataModel;
    }
}
