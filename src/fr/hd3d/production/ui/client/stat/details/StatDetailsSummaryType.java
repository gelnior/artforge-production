package fr.hd3d.production.ui.client.stat.details;

import java.util.Map;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;


public abstract class StatDetailsSummaryType extends SummaryType<Long>
{
    public static final SummaryType<Long> TOTAL_ALL = new StatDetailsSummaryTypeRenderer(StatRowDetailModelData.TOTAL);

    public static final SummaryType<Long> OK = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getOk());
            return (Long) res;
        }
    };

    public static final SummaryType<Long> RETAKE = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getRetake());
            return (Long) res;
        }
    };

    public static final SummaryType<Long> WORK_IN_PROGRESS = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getWorkInProgress());
            return (Long) res;
        }
    };
    public static final SummaryType<Long> CANCELLED = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getCancelled());
            return (Long) res;
        }
    };

    public static final SummaryType<Long> CLOSE = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getClose());
            return (Long) res;
        }
    };
    public static final SummaryType<Long> NEW = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getNew());
            return (Long) res;
        }
    };

    public static final SummaryType<Long> STAND_BY = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getStandBy());
            return (Long) res;
        }
    };
    public static final SummaryType<Long> WAIT_APP = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getWaitApp());
            return (Long) res;
        }
    };
    public static final SummaryType<Long> TO_DO = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            StatDetailsModelData statDetailsmodelData = (StatDetailsModelData) m;
            FastMap<StatRowDetailModelData> statMap = statDetailsmodelData.getPathResult();
            StatRowDetailModelData detailModel = (StatRowDetailModelData) statMap.get(field);
            if (detailModel == null)
            {
                return (Long) v;
            }
            Long res = (Long) v + new Long(detailModel.getToDo());
            return (Long) res;
        }
    };

}
