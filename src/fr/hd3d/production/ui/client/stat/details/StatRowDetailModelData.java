package fr.hd3d.production.ui.client.stat.details;

import java.util.Date;

import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.util.field.DateDataField;
import fr.hd3d.common.ui.client.util.field.IntegerDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


public class StatRowDetailModelData extends RecordModelData
{
    private static final long serialVersionUID = 6784105897633013765L;

    public static String TASKSTYPE_NAME = "tasktype_name";

    public static String STAND_BY = "STAND_BY";
    public static String WORK_IN_PROGRESS = "WORK_IN_PROGRESS";
    public static String WAIT_APP = "WAIT_APP";
    public static String CANCELLED = "CANCELLED";
    public static String CLOSE = "CLOSE";
    public static String TO_DO = "TO_DO";
    public static String RETAKE = "RETAKE";
    public static String NEW = "NEW";
    public static String OK = "OK";
    public static String TOTAL = "Total";
    public static String TOTAL_WO = "Total_Wo";

    public static String DATE = "Date";

    public static String DIFFWEEK = "DiffWeek";
    public static String ESTIMATE = "Estimate";
    public static String ELAPSED = "Elapsed";
    public static String DIFF = "Diff";
    public static String TYPE_FIELD = "type_wo";

    public static String TASK_TYPE = "task-type";

    public static String CLASS_NAME = "Statistics";

    public static ModelType getModelType()
    {
        final ModelType type = RecordModelData.getModelType();
        type.addField(new LongDataField(STAND_BY));
        type.addField(new LongDataField(WORK_IN_PROGRESS));
        type.addField(new LongDataField(WAIT_APP));
        type.addField(new LongDataField(CANCELLED));
        type.addField(new LongDataField(CLOSE));
        type.addField(new LongDataField(TO_DO));
        type.addField(new LongDataField(RETAKE));
        type.addField(new IntegerDataField(NEW));
        type.addField(new IntegerDataField(OK));
        type.addField(new IntegerDataField(TOTAL));
        type.addField(new IntegerDataField(TOTAL_WO));

        type.addField(new DateDataField(DATE));

        type.addField(new IntegerDataField(DIFFWEEK));
        type.addField(new LongDataField(ESTIMATE));
        type.addField(new LongDataField(ELAPSED));
        type.addField(new LongDataField(DIFF));

        type.addField(TYPE_FIELD);

        type.addField(new LongDataField(TASK_TYPE, TaskTypeModelData.SIMPLE_CLASS_NAME));
        return type;
    }

    /**
     * Default constructor, set a person in the model data (its full name) with no presence informations.
     * 
     * @param persons
     *            The person concerned by the absence.
     */
    public StatRowDetailModelData(String noteType, Long id)
    {
        super(noteType);
        this.setId(id);
        this.setOk(0);
        this.setWorkInProgress(0);
        this.setWaitApp(0);
        this.setRetake(0);
        this.setToDo(0);
        this.setStandBy(0);
        this.setClose(0);
        this.setCancelled(0);
        this.setNew(0);
        this.setType(noteType);
    }

    public void setTheTotal()
    {
        this.setTotal(this.getOk() + this.getWorkInProgress() + this.getWaitApp() + this.getRetake() + this.getToDo()
                + this.getStandBy() + this.getClose() + this.getCancelled() + this.getNew());
    }

    public void setStandBy(Integer standBy)
    {
        this.set(STAND_BY, standBy);
        this.setTheTotal();
    }

    public Integer getStandBy()
    {
        return this.get(STAND_BY, 0);
    }

    public static String getTASKSTYPE_NAME()
    {
        return TASKSTYPE_NAME;
    }

    public static void setTASKSTYPE_NAME(String taskstype_name)
    {
        TASKSTYPE_NAME = taskstype_name;
    }

    public String getType()
    {
        return TYPE_FIELD;
    }

    public void setType(String type)
    {
        this.set(TYPE_FIELD, type);
    }

    public void setWorkInProgress(Integer workInProgress)
    {
        this.set(WORK_IN_PROGRESS, workInProgress);
        this.setTheTotal();
    }

    public Integer getWorkInProgress()
    {
        return this.get(WORK_IN_PROGRESS, 0);
    }

    public void setWaitApp(Integer waitApp)
    {
        this.set(WAIT_APP, waitApp);
        this.setTheTotal();
    }

    public Integer getWaitApp()
    {
        return this.get(WAIT_APP, 0);
    }

    public void setCancelled(Integer cancelled)
    {
        this.set(CANCELLED, cancelled);
        this.setTheTotal();
    }

    public Integer getCancelled()
    {
        return this.get(CANCELLED, 0);
    }

    public void setClose(Integer close)
    {
        this.set(CLOSE, close);
        this.setTheTotal();
    }

    public Integer getClose()
    {
        return this.get(CLOSE, 0);
    }

    public void setToDo(Integer toDo)
    {
        this.set(TO_DO, toDo);
        this.setTheTotal();
    }

    public Integer getToDo()
    {
        return this.get(TO_DO, 0);
    }

    public void setRetake(Integer retake)
    {
        this.set(RETAKE, retake);
        this.setTheTotal();
    }

    public Integer getRetake()
    {
        return this.get(RETAKE, 0);
    }

    public void setNew(Integer new1)
    {
        this.set(NEW, new1);
        this.setTheTotal();
    }

    public Integer getNew()
    {
        return this.get(NEW, 0);
    }

    public void setOk(Integer ok)
    {
        this.set(OK, ok);
        this.setTheTotal();
    }

    public Integer getOk()
    {
        return this.get(OK, 0);
    }

    public void setTotal(Integer total)
    {
        this.set(TOTAL, total);
    }

    public Integer getTotal()
    {
        this.setTheTotal();
        return this.get(TOTAL, 0);
    }

    public void setTotalWo(Integer totalWo)
    {
        this.set(TOTAL_WO, totalWo);
    }

    public Integer getTotalWo()
    {
        return this.get(TOTAL_WO, 0);
    }

    public void setDate(Date date)
    {
        this.set(DATE, date);

    }

    public Date getDate()
    {
        return this.get(DATE, null);
    }

    public Long getEstimate()
    {
        Long duration = get(ESTIMATE);
        if (duration == null)
        {
            duration = 0L;
        }
        return duration;
    }

    public void setEstimate(Long duration)
    {
        set(ESTIMATE, duration);
    }

    public Long getElapsed()
    {
        Long duration = get(ELAPSED);
        if (duration == null)
        {
            duration = 0L;
        }
        return duration;
    }

    public void setElapsed(Long duration)
    {
        set(ELAPSED, duration);
    }

    public Long getDiff()
    {
        Long duration = get(DIFF);
        if (duration == null)
        {
            duration = 0L;
        }
        duration = this.getEstimate() - this.getElapsed();

        return duration;
    }

    public void setDiff(Long duration)
    {
        duration = this.getEstimate() - this.getElapsed();
        set(DIFF, duration);
    }

    public Long getDiffWeek()
    {
        Long duration = get(DIFFWEEK);
        if (duration == null)
        {
            duration = 0L;
        }
        return duration;
    }

    public void setDiffWeek(Long duration)
    {
        set(DIFFWEEK, duration);
    }

    /**
     * Create an duration key-value (date-P) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an duration.
     */
    public void addDuration(Date _date, long _duration)
    {
        DateWrapper date = new DateWrapper(_date);
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, String.valueOf(_duration));
    }

    public void updateFromApprovalNote(String status)
    {
        if (status != null && status.length() > 0)
        {
            set(status, getStatus(status) + 1);
        }
    }

    public Integer getStatus(String status)
    {
        return get(status);
    }
}
