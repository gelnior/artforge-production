package fr.hd3d.production.ui.client.stat.details;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;


public class StatsDetailRenderer implements GridCellRenderer<StatDetailsModelData>
{

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    public static int size = 150;

    public Object render(StatDetailsModelData modelDetail, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<StatDetailsModelData> store, Grid<StatDetailsModelData> grid)
    {

        StatRowDetailModelData model = modelDetail.getPathResult().get(property);
        if (model == null)
        {
            return null;
        }

        ContentPanel cp = new ContentPanel();
        cp.setHeaderVisible(false);
        cp.setBodyBorder(true);

        HBoxLayout layout = new HBoxLayout();
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);

        LayoutContainer container = new LayoutContainer();
        container.setLayout(layout);

        double res = (size - 12) / (float) model.getTotal();

        float pourcent = 0;
        float f = 0;
        LayoutContainer cont = null;

        if (model.getOk() != 0)
        {
            pourcent = (float) model.getOk() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            if (f >= 100)
            {
                cont.setWidth(size - 12);
                cont.addText("<div style=' padding: 1px; border: 1px; border-style:solid; border-color:green ;  white-space: normal; height: 14; text-align:left; background-color:"
                        + TaskStatusMap.getColorForStatus(ETaskStatus.OK.toString())
                        + ";'> <small>"
                        + model.getOk()
                        + "  (100 %) " + "</small></div>");
            }
            else
            {
                cont.setWidth((int) Math.round(model.getOk() * res));
                cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14; text-align:left; background-color:"
                        + TaskStatusMap.getColorForStatus(ETaskStatus.OK.toString())
                        + ";'> <small>"
                        + model.getOk()
                        + "</small></div>");
            }
            cont.setToolTip("<div style='font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.OK.toString()) + ";'>" + model.getOk() + "/"
                    + model.getTotal() + "  (" + f + "%) " + " Ok" + "</div>");

            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getWaitApp() != 0)
        {
            pourcent = (float) model.getWaitApp() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getWaitApp() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14;  text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.WAIT_APP.toString())
                    + ";'><small>"
                    + model.getWaitApp() + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.WAIT_APP.toString())
                    + ";'>"
                    + model.getWaitApp()
                    + "/" + model.getTotal() + "  (" + f + "%) " + " WFA" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getWorkInProgress() != 0)
        {
            pourcent = (float) model.getWorkInProgress() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getWorkInProgress() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14; text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.WORK_IN_PROGRESS.toString())
                    + ";'><small>"
                    + model.getWorkInProgress() + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.WORK_IN_PROGRESS.toString())
                    + ";'>"
                    + model.getWorkInProgress() + "/" + model.getTotal() + "  (" + f + "%) " + " WIP" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getRetake() != 0)
        {
            pourcent = (float) model.getRetake() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getRetake() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14; text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.RETAKE.toString())
                    + ";'><small>"
                    + model.getRetake()
                    + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold;  text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.RETAKE.toString())
                    + ";'>"
                    + model.getRetake()
                    + "/"
                    + model.getTotal() + "  (" + f + "%) " + " Retake" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getToDo() != 0)
        {
            pourcent = (float) model.getToDo() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getToDo() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14; text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.TO_DO.toString())
                    + ";'><small>"
                    + model.getToDo()
                    + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.TO_DO.toString())
                    + ";'>"
                    + model.getToDo()
                    + "/"
                    + model.getTotal() + "  (" + f + "%) " + " ToDo" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getStandBy() != 0)
        {
            pourcent = (float) model.getStandBy() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getStandBy() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal;height: 14; text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.STAND_BY.toString())
                    + ";'><small>"
                    + model.getStandBy() + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.STAND_BY.toString())
                    + ";'>"
                    + model.getStandBy()
                    + "/" + model.getTotal() + "  (" + f + "%) " + " Stand By" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getNew() != 0)
        {
            pourcent = (float) model.getNew() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getNew() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; text-align:left;; height: 14; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.NEW.toString())
                    + ";'><small>"
                    + model.getNew()
                    + " New</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.NEW.toString())
                    + ";'>"
                    + model.getNew()
                    + "/"
                    + model.getTotal() + "  (" + f + "%)" + " New </div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getClose() != 0)
        {
            pourcent = (float) model.getClose() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getClose() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14; text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.CLOSE.toString())
                    + ";'><small>"
                    + model.getClose()
                    + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.CLOSE.toString())
                    + ";'>"
                    + model.getClose()
                    + "/"
                    + model.getTotal() + "  (" + f + "%) " + " Closed" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        if (model.getCancelled() != 0)
        {
            pourcent = (float) model.getCancelled() / (float) model.getTotal() * 100;
            f = ((float) ((int) (pourcent * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round(model.getCancelled() * res));
            cont.addText("<div style=' padding: 1px;  white-space: normal; height: 14; text-align:left; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.CANCELLED.toString())
                    + ";'><small>"
                    + model.getCancelled() + "</small></div>");
            cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
                    + TaskStatusMap.getColorForStatus(ETaskStatus.CANCELLED.toString())
                    + ";'>"
                    + model.getCancelled()
                    + "/" + model.getTotal() + "  (" + f + "%) " + " Cancel" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        // Without approvalNotes
        // if (model.getTotalWo() - model.getTotal() != 0)
        // {
        // int reste = model.getTotalWo() - model.getTotal();
        // pourcent = (float) reste / (float) model.getTotalWo() * 100;
        // f = ((float) ((int) (pourcent * 100))) / 100;
        // cont = new LayoutContainer();
        // cont.setWidth((int) Math.round(reste * res));
        // cont.addText("<div style=' padding: 1px; border: 1px; border-style:dotted;  border-color: blue ; white-space: normal; text-align:left; height: 12;"
        // + ";'><small>" + reste + "</small></div>");
        // cont.setToolTip("<div style=' padding: 3px;  white-space: normal; font-weight: bold; text-align:center; background-color:"
        // + "edeff1" + ";'>" + reste + "/" + model.getTotalWo() + "  (" + f + "%) " + " Without" + "</div>");
        // container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        // }

        cp.add(container);
        return cp;
    }

    public static void setSize(int aSize)
    {
        size = aSize;
    }

}
