package fr.hd3d.production.ui.client.stat.details;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


public class StatDetailsModelData extends RecordModelData

{
    private static final long serialVersionUID = 6784105897633013765L;

    public static String WO_PATH = "wo_path";
    public static String NB_FRAME = "nbframe";
    public static String NB_WO = "nb_wo";
    public static String WO_APPROVAL_NOTES = "wo_approvalNotes";
    public static String PATH_RESULT = "path_result";
    public static String WO_LIST = "wo_list";

    public static String CLASS_NAME = "StatDetail";

    public static String NB_RECORDS_REMAIN = "nb_records_remain";

    /**
     * Default constructor,
     * 
     * @param
     */
    public StatDetailsModelData(Long woId, String path, int nbWo, int nbFrame)
    {
        super(woId.toString());

        this.setId(woId);
        this.setPathResult(new FastMap<StatRowDetailModelData>());

        this.setWoPath(path);
        this.setNbFrame(nbFrame);
        this.setNbWO(nbWo);
    }

    public void setWoPath(String WoPath)
    {
        this.set(WO_PATH, WoPath);
    }

    public String getWoPath()
    {
        return this.get(WO_PATH);
    }

    public void setNbFrame(int WoNbFrame)
    {
        this.set(NB_FRAME, WoNbFrame);
    }

    public int getNbFrame()
    {
        return (Integer) this.get(NB_FRAME);
    }

    public void setNbWO(int nbWo)
    {
        this.set(NB_WO, nbWo);
    }

    public int getNbWo()
    {
        return (Integer) this.get(NB_WO);
    }

    public void setWoApprovalNotes(FastMap<ApprovalNoteModelData> woApprovalNotes)
    {
        this.set(WO_APPROVAL_NOTES, woApprovalNotes);
    }

    @SuppressWarnings("unchecked")
    public FastMap<ApprovalNoteModelData> getWoApprovalNotes()
    {
        return (FastMap<ApprovalNoteModelData>) this.get(WO_APPROVAL_NOTES);
    }

    public void setPathResult(FastMap<StatRowDetailModelData> pathRes)
    {
        this.set(PATH_RESULT, pathRes);
    }

    @SuppressWarnings("unchecked")
    public FastMap<StatRowDetailModelData> getPathResult()
    {
        return (FastMap<StatRowDetailModelData>) this.get(PATH_RESULT);
    }

    @SuppressWarnings("unchecked")
    public FastMap<Long> getWoList()
    {
        return (FastMap<Long>) this.get(WO_LIST);
    }

    public void setWoList(FastMap<Long> wo)
    {
        this.set(WO_LIST, wo);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        DataField nbFrame = new DataField(NB_FRAME);
        nbFrame.setType(Integer.class);

        DataField nbWo = new DataField(NB_WO);
        nbWo.setType(Integer.class);

        DataField approvalNotes = new DataField(WO_APPROVAL_NOTES);
        approvalNotes.setType(FastMap.class);

        DataField pathResult = new DataField(PATH_RESULT);
        pathResult.setType(FastMap.class);

        modelType.addField(WO_PATH);
        modelType.addField(nbFrame);
        modelType.addField(nbWo);
        modelType.addField(approvalNotes);
        modelType.addField(pathResult);

        return modelType;
    }

}
