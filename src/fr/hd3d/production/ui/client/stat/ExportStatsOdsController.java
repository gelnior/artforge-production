package fr.hd3d.production.ui.client.stat;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Hidden;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;


/**
 * Controller handling export button clicks.
 * 
 * @author HD3D
 */
public class ExportStatsOdsController extends Controller
{

    private StatModel model;
    // private StatView view;
    private String jsonToExport;

    /**
     * Default controller
     * 
     * @param model
     *            Model for data.
     * @param view
     *            View for widgets.
     */
    public ExportStatsOdsController(StatModel model, StatView view)
    {
        super();
        this.model = model;
        // this.view = view;
        registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        if (StatEvents.ODS_EXPORT_CLICKED.equals(event.getType()))
        {
            this.onOdsExplortClicked(event);
        }
    }

    private void onOdsExplortClicked(AppEvent event)
    {
        String type = (String) event.getData();
        // jsonToExport = "{" + createData(type) + "}";
        this.jsonToExport = createData(type);

        String request = "export-stat-ods";
        if (type.equals("Detail"))
        {
            request = "export-task-ods";
        }

        Hidden hidden = new Hidden("json");
        hidden.setValue(jsonToExport);

        StatView.exportOdsFormPanel.setAction(RestRequestHandlerSingleton.getInstance().getServicesUrl() + request);
        StatView.exportOdsFormPanel.clear();
        StatView.exportOdsFormPanel.add(hidden);
        StatView.exportOdsFormPanel.submit();
    }

    private String createData(String type)
    {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
        String dateStart = dateTimeFormat.format(new DateWrapper(DatetimeUtil.today()).asDate());
        FastMap<String> map = new FastMap<String>();

        if (type.equals("Detail"))
        {
            map.put("TaskRawData", dateStart);
            map.put("type", type);
            map.put("project-id", this.model.getProjectId().toString());

            // return "\"TaskRawData\":\"" + dateStart + "\", \"type\": \"" + type + "\", \"project-id\":"
            // + this.model.getProjectId();
        }
        else
        {
            map.put("stats", dateStart);
            map.put("type", type);
            map.put("project-id", this.model.getProjectId().toString());

        }
        // return "\"stats\":\"" + dateStart + "\", \"type\": \"" + type + "\", \"project-id\":"
        // + this.model.getProjectId();

        JSONWriter writer = new JSONWriter();
        return writer.write(map);
    }

    private void registerEvents()
    {
        this.registerEventTypes(StatEvents.ODS_EXPORT_CLICKED);
    }
}
