package fr.hd3d.production.ui.client.stat;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonGroup;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.listener.MenuButtonClickListener;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.grid.renderer.BigTextRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DiffDurationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.PaddingTextRenderer;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;
import fr.hd3d.production.ui.client.constant.ProductionConstants;


/**
 * View displaying task type statistics : percentage of task done, total elapsed time...
 * 
 * @author HD3D
 */
public class StatView extends BorderedPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static ProductionConstants CONSTANTS = GWT.create(ProductionConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Name of the view, needed for history token. */
    public static final String NAME = "Statistics";

    private static final int COLUMN_WIDTH = 920; // 1000
    private static final int TC_COLUMN_WIDTH = 150;
    private static final int TC_COLUMN_WIDTH_2 = 100;

    /** Panel displayed when ODS export is requested. */
    public static FormPanel exportOdsFormPanel;

    /** Model handling task type statistic data. */
    private final StatModel model = new StatModel();
    /** Controller handing task type statistic events. */
    private final StatController controller = new StatController(this.model, this);

    /** Button that refreshes task type grid when clicked. */
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh",
            StatEvents.REFRESH_STAT);
    /** Button to send ODS export for categories. */
    private final ToolBarButton odsButtonCat = new ToolBarButton(Hd3dImages.getCsvExportIcon(), COMMON_CONSTANTS
            .ExportToOds(), null);
    /** Button to send ODS export for sequences. */
    private final ToolBarButton odsButtonSeq = new ToolBarButton(Hd3dImages.getCsvExportIcon(), COMMON_CONSTANTS
            .ExportToOds(), null);

    /** Button to send ODS export for tasks. */
    private final Button odsTaskButton = new Button("Tasks ", Hd3dImages.getCsvExportIcon());
    /** Button to send full ODS export. */
    private final Button odsButtonAll = new Button("Approval Notes ", Hd3dImages.getCsvExportIcon());
    /** Button to send ODS export that gives the retake list. */
    private final Button odsButtonRetake = new Button("Approval Notes Retake ", Hd3dImages.getCsvExportIcon());

    /** When clicked, this button refreshes the TimeRecap grid. */
    private final ToolBarButton printButton = new ToolBarButton(Hd3dImages.getPrinterIcon(), COMMON_CONSTANTS.Print(),
            StatEvents.STATS_PRINT);

    /** Description of grid columns. */
    private ColumnModel columnModel;
    /** Grid displaying stats data. */
    private Grid<StatRowModelData> grid;

    /** Start date field needed by date filter. */
    private final DateField startDateFilter = new DateField();
    /** End date field needed by date filter. */
    private final DateField endDateFilter = new DateField();

    private final LabelField labelWeek = new LabelField(" Week : ");
    private final LabelField labelDate = new LabelField(" Current State : ");
    private final Label nbWeek = new Label();
    private final Button prevButton = new Button("<<");
    private final Button nextButton = new Button(">>");

    /** Default constructor : build widgets. */
    public StatView()
    {
        this.initToolBar();
        this.initGrid();

        this.setBodyBorder(true);
        this.addCenter(grid);
    }

    /**
     * @return Controller that handles statistic events.
     */
    public StatController getController()
    {
        return controller;
    }

    /**
     * Change date displayed to tell for which day stats are displayed.
     * 
     * @param date
     *            The date to display
     */
    public void setTextDate(String date)
    {
        this.labelDate.setText("<b> " + date + "</b>&#160;&#160;&#160;");
    }

    /**
     * Display current week number.
     * 
     * @param week
     *            The week to display.
     */
    public void setTextWeek(int week)
    {
        this.labelWeek.setText("<b>Week " + week + "</b>&#160;&#160;&#160;");
    }

    /** Unidle controller. */
    public void unidle()
    {
        this.controller.unMask();
    }

    /** Idle controller. */
    public void idle()
    {
        this.controller.mask();
    }

    /** Send grid to for printing inside browser. */
    public void print()
    {
        if (rendered)
        {
            JavaScriptObject jsobj = open("", COMMON_CONSTANTS.Print(), "");
            String css = getCss();

            El grid = this.grid.getView().getBody();
            grid = grid.getChild(0);
            // String html = css + "<div class='x-grid3-body' style='height:100%; width:100%;'>" + grid.getInnerHtml()
            // + "</div>";

            El headers = this.grid.getView().getHeader().getContainer().el();
            headers.setStyleAttribute("height", "100%");
            headers.setStyleAttribute("position", "static");
            headers.getChild(0).setStyleAttribute("height", "100%");
            headers.getChild(0).setStyleAttribute("position", "static");
            headers.getChild(0).setStyleAttribute("overflow", "visible");
            headers.getChild(0).getChild(0).setStyleAttribute("height", "100%");
            headers.getChild(0).getChild(0).setStyleAttribute("position", "static");
            headers.getChild(0).getChild(0).setStyleAttribute("overflow", "visible");
            headers.getChild(0).getChild(0).getChild(1).setStyleAttribute("height", "100%");
            headers.getChild(0).getChild(0).getChild(1).setStyleAttribute("position", "static");
            headers.getChild(0).getChild(0).getChild(1).setStyleAttribute("overflow", "visible");
            String html = "<html><head>" + css + "</head><body >" + headers.getInnerHtml() + "</body>";
            setDocInnerHtml(jsobj, html);
            printDoc(jsobj);
        }
    }

    public void createOdsExportPanel()
    {
        exportOdsFormPanel = new FormPanel();
        exportOdsFormPanel.setMethod(FormPanel.METHOD_POST);
        RootPanel.get().add(exportOdsFormPanel);
    }

    public void displayLoadingMask()
    {
        this.mask(COMMON_CONSTANTS.Loading());
    }

    /** Build statistic grid. */
    private void initGrid()
    {
        this.grid = new Grid<StatRowModelData>(model.getListStore(), this.getColumnModel());
        this.grid.setBorders(true);
        this.grid.setAutoExpandColumn(StatRowModelData.NAME_FIELD);
    }

    /** Initialize toolbar. */
    private void initToolBar()
    {
        this.setToolBar();

        DateWrapper date = new DateWrapper(DatetimeUtil.today());
        this.setTextDate(Hd3dDateUtil.dateToString(date.asDate()));
        int weekNumber = Hd3dDateUtil.getWeekOfYear(date, 1);
        this.setTextWeek(weekNumber);

        ButtonGroup group = new ButtonGroup(5);
        group.setHeading("Current State ");
        group.add(this.labelDate);
        group.add(this.labelWeek);
        group.add(this.refreshButton);
        group.add(new SeparatorToolItem());
        group.add(this.printButton);

        this.addToToolBar(group);

        this.odsButtonRetake.setToolTip(COMMON_CONSTANTS.ExportToOds() + " Only Retake with historic");
        this.odsButtonAll.setToolTip(COMMON_CONSTANTS.ExportToOds() + " All Without historic");
        this.odsTaskButton.setToolTip(COMMON_CONSTANTS.ExportToOds());

        group = new ButtonGroup(5);
        group.setHeading("Export Raw Data to ODS");

        AppEvent eventDetail = new AppEvent(StatEvents.ODS_EXPORT_CLICKED, "Detail");
        odsTaskButton.addSelectionListener(new ButtonClickListener(eventDetail));
        group.add(odsTaskButton);

        group.add(new SeparatorToolItem());

        Menu menu = new Menu();
        MenuItem constituent = new MenuItem("Constituents");
        AppEvent event = new AppEvent(StatEvents.ODS_EXPORT_CLICKED, "Detail");
        constituent.addSelectionListener(new MenuButtonClickListener(event));
        menu.add(constituent);
        MenuItem shot = new MenuItem("Shots");
        event = new AppEvent(StatEvents.ODS_EXPORT_CLICKED, "SequenceAll");
        shot.addSelectionListener(new MenuButtonClickListener(event));
        menu.add(shot);
        this.odsButtonAll.setMenu(menu);
        group.add(this.odsButtonAll);

        group.add(new SeparatorToolItem());

        menu = new Menu();
        constituent = new MenuItem("Constituents");
        event = new AppEvent(StatEvents.ODS_EXPORT_CLICKED, "Category");
        constituent.addSelectionListener(new MenuButtonClickListener(event));
        menu.add(constituent);

        shot = new MenuItem("Shots");
        event = new AppEvent(StatEvents.ODS_EXPORT_CLICKED, "Sequence");
        menu.add(shot);
        this.odsButtonRetake.setMenu(menu);
        group.add(odsButtonRetake);

        this.addToToolBar(group);

        this.nextButton.setEnabled(false);
    }

    private static native Document open(String url, String name, String features) /*-{
        var wnd = $wnd.open(url, name, features);
        return wnd;
    }-*/;

    private static native void setDocInnerHtml(JavaScriptObject wnd, String innerHtml) /*-{
        //doc.body.innerHTML = innerHtml;
        wnd.document.write(innerHtml);
    }-*/;

    private static native void printDoc(JavaScriptObject wnd) /*-{
        var javascript = " <script> "
        javascript += "function printFunction(){";
        javascript += "    window.print();";
        javascript += "}";
        javascript += "t = setTimeout('printFunction()', 600);";
        javascript += "</script>";
        wnd.document.write(javascript);
    }-*/;

    /**
     * @return CSS needed for displaying printable version of the statistics.
     */
    private String getCss()
    {
        String css = "";

        String href = Window.Location.getHref();
        String[] urlPart = href.split("/");
        StringBuilder url = new StringBuilder();
        for (int i = 0; i < urlPart.length - 1; i++)
        {
            url.append(urlPart[i] + "/");
        }
        NodeList<Element> cssLinks = Document.get().getElementsByTagName("link");
        int size = cssLinks.getLength();
        for (int i = 0; i < size; i++)
        {
            css += "<link rel='stylesheet' type='text/css' href='" + url.toString()
                    + cssLinks.getItem(i).getAttribute("href") + "' />";
        }
        return css;
    }

    /**
     * @return Column grid description.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        ColumnConfig nameCol = new ColumnConfig(StatRowModelData.NAME_FIELD, "Name", TC_COLUMN_WIDTH);
        nameCol.setRenderer(new BigTextRenderer<StatRowModelData>());

        ColumnConfig typeCol = new ColumnConfig(StatRowModelData.TYPE_FIELD, "Object Type", TC_COLUMN_WIDTH_2);
        typeCol.setRenderer(new PaddingTextRenderer<ModelData>());

        ColumnConfig totalCol = new ColumnConfig(StatRowModelData.TOTAL, "Nb Tasks", 60);
        totalCol.setRenderer(new PaddingTextRenderer<ModelData>());

        ColumnConfig statusCol = new ColumnConfig("status", "Status", COLUMN_WIDTH);
        statusCol.setRenderer(new StatsRenderer());
        statusCol.setSortable(false);
        statusCol.setResizable(false);

        ColumnConfig estimateCol = new ColumnConfig(StatRowModelData.ESTIMATE, "Estimate", 100);
        estimateCol.setRenderer(new DurationRenderer());

        ColumnConfig spendCol = new ColumnConfig(StatRowModelData.ELAPSED, "Elapsed", 100);
        spendCol.setRenderer(new DurationRenderer());

        ColumnConfig diffTimeCol = new ColumnConfig(StatRowModelData.DIFF, "Diff", 100);
        diffTimeCol.setRenderer(new DiffDurationRenderer());
        ColumnConfig pourcentTimeCol = new ColumnConfig(StatRowModelData.DIFF, "%", 60);
        pourcentTimeCol.setRenderer(new PourcentDiffDurationRenderer());

        columns.add(nameCol);
        columns.add(typeCol);
        columns.add(totalCol);
        columns.add(statusCol);
        columns.add(estimateCol);
        columns.add(spendCol);
        columns.add(diffTimeCol);
        columns.add(pourcentTimeCol);

        columnModel = new ColumnModel(columns);
        columnModel.addHeaderGroup(0, 0, new HeaderGroupConfig("Tasks", 1, 4));
        columnModel.addHeaderGroup(0, 4, new HeaderGroupConfig("Time", 1, 4));

        return columnModel;
    }

}
