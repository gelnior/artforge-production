package fr.hd3d.production.ui.client.stat;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;


public class StatsRenderer implements GridCellRenderer<StatRowModelData>
{

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    public Object render(StatRowModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<StatRowModelData> store, Grid<StatRowModelData> grid)
    {

        ContentPanel cp = new ContentPanel();
        cp.setHeaderVisible(false);
        cp.setBodyBorder(true);

        HBoxLayout layout = new HBoxLayout();
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);

        LayoutContainer container = new LayoutContainer();
        container.setLayout(layout);

        float res = 920 / (float) model.getTotal();

        float pourcent = 0;
        float f = 0;
        LayoutContainer cont = null;
        Integer total = model.getTotal();

        this.addTaskBar(container, ETaskStatus.OK, model.getOk(), total, res);
        this.addTaskBar(container, ETaskStatus.WAIT_APP, model.getWaitApp(), total, res);
        this.addTaskBar(container, ETaskStatus.WORK_IN_PROGRESS, model.getWorkInProgress(), total, res);
        this.addTaskBar(container, ETaskStatus.RETAKE, model.getRetake(), total, res);
        this.addTaskBar(container, ETaskStatus.TO_DO, model.getToDo(), total, res);
        this.addTaskBar(container, ETaskStatus.STAND_BY, model.getStandBy(), total, res);
        this.addTaskBar(container, ETaskStatus.NEW, model.getNew(), total, res);
        this.addTaskBar(container, ETaskStatus.CLOSE, model.getClose(), total, res);
        this.addTaskBar(container, ETaskStatus.CANCELLED, model.getCancelled(), total, res);

        if (model.getTotal() == 0)
        {
            f = ((float) ((int) (100 * 100))) / 100;
            cont = new LayoutContainer();
            cont.setWidth((int) Math.round((float) model.getCancelled() * res));
            cont.addText("<div class='default-stats-row'>" + "</div>");
            cont.setToolTip("<div class='stats-row-tooltip'>" + "0 Task" + "</div>");
            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }

        cp.add(container);
        return cp;
    }

    private void addTaskBar(LayoutContainer container, ETaskStatus status, Integer nbTasks, Integer total, float res)
    {
        if (nbTasks > 0)
        {
            float pourcent = (float) nbTasks / (float) total * 100;
            float f = ((float) ((int) (pourcent * 100))) / 100;
            LayoutContainer cont = new LayoutContainer();
            cont.setWidth((int) Math.round((float) (nbTasks * res)));
            String html = "<div class= 'stats-cell' style='background-color:"
                    + TaskStatusMap.getColorForStatus(status.toString()) + ";'>";
            if (status == ETaskStatus.OK)
                html += "<small>" + f + "%</small></div>";
            else
                html += "</div>";

            cont.addText(html);
            cont.setToolTip("<div class='stats-tooltip' style='background-color:"
                    + TaskStatusMap.getColorForStatus(status.toString()) + ";'>" + nbTasks + "  (" + f + "%) " + " "
                    + NoteStatusComboBox.getStatusAbbreviationMap().get(status.toString()) + "</div>");

            container.add(cont, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        }
    }

}
