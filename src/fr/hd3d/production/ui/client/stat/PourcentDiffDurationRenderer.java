package fr.hd3d.production.ui.client.stat;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class PourcentDiffDurationRenderer implements IExplorerCellRenderer<Hd3dModelData>
{
    static boolean negatif = false;

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        float res = 0;
        Long duration = model.get("Estimate");

        if (duration != 0)
        {
            Long durationActivities = model.get("Elapsed");

            if (durationActivities != 0)
            {
                res = (float) 100 * (float) durationActivities / (float) duration;
                res = ((float) ((int) (res * 100))) / 100;
            }
        }

        String borderColor = "green";
        if (res > 100)
        {
            borderColor = "red";
        }

        if (res == 0)
        {
            return "<div style='white-space: normal; padding: 4px;" + " border: 2px solid " + borderColor
                    + "';>0</div>";
        }

        return "<div style='white-space: normal; padding: 4px;" + " border: 2px solid " + borderColor + "';>" + res
                + "</div>";
    }

};
