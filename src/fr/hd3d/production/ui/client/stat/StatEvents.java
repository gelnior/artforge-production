package fr.hd3d.production.ui.client.stat;

import com.extjs.gxt.ui.client.event.EventType;


public class StatEvents
{
    public static final EventType LOAD_STEP = new EventType();
    public static final EventType LOAD_STAT = new EventType();
    public static final EventType REFRESH_STAT = new EventType();
    public static final EventType DATA_LOADED = new EventType();
    public static final EventType REFRESH_STAT_CHART = new EventType();
    public static final EventType PROJECT_CHANGED = new EventType();
    public static final EventType TASKS_LOADED = new EventType();
    public static final EventType STATS_PRINT = new EventType();
    public static final EventType ODS_EXPORT_CLICKED = new EventType();
}
