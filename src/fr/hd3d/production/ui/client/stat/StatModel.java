package fr.hd3d.production.ui.client.stat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.store.ListStore;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.AllLoadListener;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Model that handles task type statistics data.
 * 
 * @author HD3D
 */
public class StatModel
{
    /** Store containing activities to display in the TimeRecap grid. */
    private final ListStore<StatRowModelData> statsStore = new ListStore<StatRowModelData>();;

    final ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(new TaskReader());

    private Long projectId;

    private int maxValue;

    public Long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public int getMaxValue()
    {
        return this.maxValue;
    }

    private void setMaxValue(int val)
    {
        if (val > this.maxValue)
        {
            this.maxValue = val;
        }
    }

    public ListStore<StatRowModelData> getListStore()
    {
        return this.statsStore;
    }

    /**
     * Default constructor, configure stores.
     */
    public StatModel()
    {
        this.maxValue = 0;
        this.taskStore.addLoadListener(new AllLoadListener<TaskModelData>(taskStore, StatEvents.TASKS_LOADED));
    }

    public void statLoadTasks()
    {
        this.statsStore.removeAll();
        this.maxValue = 0;

        this.taskStore.setPath(ServicesPath.getTasksPath(null));
        this.taskStore.addParameter(new OrderBy(TaskModelData.NAME_FIELD));
        this.taskStore.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));

        this.taskStore.reload();
    }

    public void statLoadStats()
    {
        List<Long> taskTypeConstituentIds = new ArrayList<Long>();
        List<String> taskTypeConstituentNames = new ArrayList<String>();
        List<Long> taskTypeShotIds = new ArrayList<Long>();
        List<String> taskTypeShotNames = new ArrayList<String>();

        List<Long> taskTypeConstituentEstimate = new ArrayList<Long>();
        List<Long> taskTypeConstituentElapsed = new ArrayList<Long>();
        List<Long> taskTypeShotEstimate = new ArrayList<Long>();
        List<Long> taskTypeShotElapsed = new ArrayList<Long>();

        this.taskStore.setStoreSorter(new TaskTypeTaskSorter());
        this.taskStore.sort(TaskModelData.START_DATE_FIELD, SortDir.DESC);

        if (this.taskStore.getCount() == 0)
        {
            EventDispatcher.forwardEvent(StatEvents.DATA_LOADED);
        }

        for (TaskModelData task : this.taskStore.getModels())
        {
            if (task.getBoundEntityName() == null)
                continue;

            if (task.getBoundEntityName().equals(ConstituentModelData.SIMPLE_CLASS_NAME))
            {
                if (!taskTypeConstituentIds.contains(task.getTaskTypeId()))
                {
                    taskTypeConstituentIds.add(task.getTaskTypeId());
                    taskTypeConstituentNames.add(task.getTaskTypeName());

                    taskTypeConstituentEstimate.add(task.getDuration());
                    taskTypeConstituentElapsed.add(task.getTotalActivityDuration());
                }
                else
                {
                    int index = taskTypeConstituentIds.indexOf(task.getTaskTypeId());

                    taskTypeConstituentEstimate.set(index, taskTypeConstituentEstimate.get(index) + task.getDuration());
                    taskTypeConstituentElapsed.set(index,
                            taskTypeConstituentElapsed.get(index) + task.getTotalActivityDuration());
                }
            }
            else
            {
                if (!taskTypeShotIds.contains(task.getTaskTypeId()))
                {
                    taskTypeShotIds.add(task.getTaskTypeId());
                    taskTypeShotNames.add(task.getTaskTypeName());

                    taskTypeShotEstimate.add(task.getDuration());
                    taskTypeShotElapsed.add(task.getTotalActivityDuration());
                }
                else
                {
                    int index = taskTypeShotIds.indexOf(task.getTaskTypeId());

                    taskTypeShotEstimate.set(index, taskTypeShotEstimate.get(index) + task.getDuration());
                    taskTypeShotElapsed.set(index, taskTypeShotElapsed.get(index) + task.getTotalActivityDuration());
                }
            }

        }

        loadData(ConstituentModelData.SIMPLE_CLASS_NAME, taskTypeConstituentIds, taskTypeConstituentNames,
                taskTypeConstituentEstimate, taskTypeConstituentElapsed);

        loadData(ShotModelData.SIMPLE_CLASS_NAME, taskTypeShotIds, taskTypeShotNames, taskTypeShotEstimate,
                taskTypeShotElapsed);
    }

    private void loadData(final String boundEntityName, List<Long> taskTypeIds, final List<String> taskTypeNames,
            final List<Long> taskTypeEstimates, final List<Long> taskTypeElapseds)
    {
        if (taskTypeIds.size() == 0)
            return;

        String path = ServicesURI.TASK_STATUS_NB_BY_TASKTYPES;
        path = path + "?projectid=" + MainModel.getCurrentProject().getId() + "&entityname=" + boundEntityName
                + "&tasktypeids=";

        for (Long taskTypeId : taskTypeIds)
        {
            path = path + taskTypeId.toString() + ',';
        }

        RestRequestHandlerSingleton.getInstance().getRequest(path, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                try
                {
                    String json = null;

                    try
                    {
                        json = response.getEntity().getText();

                        JSONValue object = JSONParser.parse(json);
                        JSONArray resultMap = object.isObject().get(Hd3dModelData.ROOT).isArray();
                        JSONObject listTaskTypes = resultMap.get(0).isObject();
                        int k = 0;

                        for (String taskTypeName : taskTypeNames)
                        {
                            if (listTaskTypes.get(taskTypeName) == null)
                                continue;

                            TaskTypeModelData taskTypeModel = new TaskTypeModelData();
                            taskTypeModel.setName(taskTypeName);
                            StatRowModelData model = new StatRowModelData(taskTypeModel);

                            model.setType(boundEntityName);
                            model.setEstimate(taskTypeEstimates.get(k));
                            model.setElapsed(taskTypeElapseds.get(k));
                            model.setDiff(taskTypeEstimates.get(k) - taskTypeElapseds.get(k));

                            JSONArray val = listTaskTypes.get(taskTypeName).isArray();

                            for (int i = 0; i < val.size(); i++)
                            {
                                JSONObject obj = val.get(i).isObject();

                                this.setStatusValue(ETaskStatus.NEW.toString(), obj, model);
                                this.setStatusValue(ETaskStatus.TO_DO.toString(), obj, model);
                                this.setStatusValue(ETaskStatus.STAND_BY.toString(), obj, model);
                                this.setStatusValue(ETaskStatus.RETAKE.toString(), obj, model);
                                this.setStatusValue(ETaskStatus.WORK_IN_PROGRESS.toString(), obj, model);
                                this.setStatusValue(ETaskStatus.WAIT_APP.toString(), obj, model);
                                this.setStatusValue(ETaskStatus.OK.toString(), obj, model);
                            }
                            model.setTheTotal();

                            statsStore.add(model);
                            k++;
                        }
                        EventDispatcher.forwardEvent(StatEvents.DATA_LOADED);
                    }
                    catch (NumberFormatException nfe)
                    {
                        EventDispatcher.forwardEvent(StatEvents.DATA_LOADED);
                        Logger.error("ServerStatCallback pb : " + json, nfe);
                    }
                }
                catch (IOException e)
                {
                    EventDispatcher.forwardEvent(StatEvents.DATA_LOADED);
                    e.printStackTrace();
                }
            }

            private void setStatusValue(String status, JSONObject obj, StatRowModelData model)
            {
                if (obj.get(status) != null)
                {
                    String value = obj.get(status).isString().toString();
                    value = value.replace("\"", "");
                    int nb = Integer.parseInt(value);
                    model.set(status, nb);
                    setMaxValue(nb);
                }
            }

        });
    }
}
