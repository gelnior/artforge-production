package fr.hd3d.production.ui.client.stat;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


/**
 * Controller that handles statistics events.
 * 
 * @author HD3D
 */
public class StatController extends MaskableController
{
    /** Model that handles statistics data. */
    private final StatModel model;
    /** View that handles statistic widgets. */
    private final StatView view;

    public StatController(StatModel model, StatView view)
    {
        this.model = model;
        this.view = view;

        this.view.createOdsExportPanel();
        ExportStatsOdsController exportStatsOdsController = new ExportStatsOdsController(this.model, this.view);
        EventDispatcher.get().addController(exportStatsOdsController);

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == StatEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == StatEvents.REFRESH_STAT)
        {
            this.onRefresh();
        }
        else if (type == StatEvents.TASKS_LOADED)
        {
            this.onTasksLoaded();
        }
        else if (type == StatEvents.DATA_LOADED)
        {
            this.onLoading();
        }
        else if (type == StatEvents.STATS_PRINT)
        {
            this.view.print();
        }
        else if (type == StatEvents.REFRESH_STAT_CHART)
        {
            // this.refreshChart();
        }
    }

    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();

        if (project != null && project.getId() != this.model.getProjectId())
        {
            this.model.setProjectId(project.getId());
            EventDispatcher.forwardEvent(StatEvents.REFRESH_STAT);
        }
    }

    private void onRefresh()
    {
        this.view.displayLoadingMask();
        this.model.statLoadTasks();
    }

    private void onLoading()
    {
        this.view.unmask();
    }

    private void onTasksLoaded()
    {
        this.model.statLoadStats();
    }

    private void registerEvents()
    {
        registerEventTypes(CommonEvents.PROJECT_CHANGED);
        registerEventTypes(StatEvents.PROJECT_CHANGED);
        registerEventTypes(StatEvents.REFRESH_STAT);
        registerEventTypes(StatEvents.DATA_LOADED);
        registerEventTypes(StatEvents.REFRESH_STAT_CHART);
        registerEventTypes(StatEvents.TASKS_LOADED);
        registerEventTypes(StatEvents.STATS_PRINT);
        registerEventTypes(StatEvents.ODS_EXPORT_CLICKED);
    }
}
