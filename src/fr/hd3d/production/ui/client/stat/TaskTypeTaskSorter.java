package fr.hd3d.production.ui.client.stat;

import java.util.Date;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreSorter;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * Sorter that put task to the bottom of sorting if their date are not rightly set (start or end date is missing or
 * start date is after end date). If both task has wrong dates.
 * 
 * @author HD3D
 */
public class TaskTypeTaskSorter extends StoreSorter<TaskModelData>
{
    @Override
    public int compare(Store<TaskModelData> store, TaskModelData m1, TaskModelData m2, String property)
    {
        Date m1startDate = m1.getStartDate();
        Date m2startDate = m2.getStartDate();

        int comparaison = 0;
        if (m1startDate == null)
        {
            if (m2startDate == null)
            {
                comparaison = 1;
            }
            else
            {
                comparaison = -1;
            }
        }
        else
        {
            if (m2startDate == null)
            {
                comparaison = 1;
            }
            else
            {
                comparaison = m2startDate.compareTo(m1startDate);
            }
        }
        return comparaison;
    }
}
